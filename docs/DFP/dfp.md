# DFP

## Overview

* /themes/haymarket/includes/classes/DFPAPI.php
  * defines adslots
  * loads js assets
  * customize adslots by some page types
  * setup in article ads
  * setup proclivity
  * global methods for browser/device checking
* /themes/haymarket/assets/js/frontend/components/dfp-ads.js
  * exports the global hmAds object

## Testing

* add ad-debug=1 to url -> All dfp debugging is logged to the console  
* add dmd-test=1 to url -> Forces proclivity ads to run
* paste ```AIM.debug(); AIM.tag(); AIM.fetch()``` into console then refresh the page -> AIM returns debugging info