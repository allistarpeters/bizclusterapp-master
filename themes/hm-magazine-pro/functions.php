<?php
/**
 * Haymarket Magazine.
 *
 * This file adds the functions to the Magazine Pro Theme.
 *
 * @package Magazine
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/magazine/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'magazine_localization_setup' );
function magazine_localization_setup(){
	load_child_theme_textdomain( 'magazine-pro', get_stylesheet_directory() . '/languages' );
}

// Add the theme helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Add the Customizer options.
include_once( get_stylesheet_directory() . '/lib/customize.php' );

// Add the Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

// Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Add the WooCommerce customizer CSS.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Include notice to install Genesis Connect for WooCommerce.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', __( 'Magazine Pro', 'magazine-pro' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/magazine/' );
define( 'CHILD_THEME_VERSION', '3.2.4' );

// Enqueue required fonts, scripts, and styles.
add_action( 'wp_enqueue_scripts', 'magazine_enqueue_scripts' );
function magazine_enqueue_scripts() {

	wp_enqueue_script( 'magazine-entry-date', get_stylesheet_directory_uri() . '/js/entry-date.js', array( 'jquery' ), '1.0.0' );

	wp_enqueue_style( 'dashicons' );

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,400|Raleway:400,500,900', array(), CHILD_THEME_VERSION );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'magazine-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menus' . $suffix . '.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'magazine-responsive-menu',
		'genesis_responsive_menu',
		magazine_responsive_menu_settings()
	);

}

// Define our responsive menu settings.
function magazine_responsive_menu_settings() {

	$settings = array(
		'mainMenu'    => __( 'Menu', 'magazine-pro' ),
		'subMenu'     => __( 'Submenu', 'magazine-pro' ),
		'menuClasses' => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
				'.nav-secondary',
			),
		),
	);

	return $settings;

}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add image sizes.
add_image_size( 'home-middle', 630, 350, true );
add_image_size( 'home-top', 750, 420, true );
add_image_size( 'sidebar-thumbnail', 100, 100, true );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'default-text-color' => '000000',
	'flex-height'        => true,
	'header-selector'    => '.site-title a',
	'header-text'        => false,
	'height'             => 180,
	'width'              => 760,
) );

// Rename menus.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Before Header Menu', 'magazine-pro' ), 'secondary' => __( 'After Header Menu', 'magazine-pro' ) ) );

// Remove skip link for primary navigation.
add_filter( 'genesis_skip_links_output', 'magazine_skip_links_output' );
function magazine_skip_links_output( $links ) {

	if ( isset( $links['genesis-nav-primary'] ) ) {
		unset( $links['genesis-nav-primary'] );
	}

	$new_links = $links;
	array_splice( $new_links, 1 );

	if ( has_nav_menu( 'secondary' ) ) {
		$new_links['genesis-nav-secondary'] = __( 'Skip to secondary menu', 'magazine-pro' );
	}

	return array_merge( $new_links, $links );

}

// Add ID to secondary navigation.
add_filter( 'genesis_attr_nav-secondary', 'magazine_add_nav_secondary_id' );
function magazine_add_nav_secondary_id( $attributes ) {

	$attributes['id'] = 'genesis-nav-secondary';

	return $attributes;

}

// Reposition the primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before_header', 'genesis_do_nav' );

// Remove output of primary navigation right extras.
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

// Remove navigation meta box.
add_action( 'genesis_theme_settings_metaboxes', 'magazine_remove_genesis_metaboxes' );
function magazine_remove_genesis_metaboxes( $_genesis_theme_settings_pagehook ) {
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_theme_settings_pagehook, 'main' );
}

// Add primary-nav class if primary navigation is used.
add_filter( 'body_class', 'magazine_no_nav_class' );
function magazine_no_nav_class( $classes ) {

	$menu_locations = get_theme_mod( 'nav_menu_locations' );

	if ( ! empty( $menu_locations['primary'] ) ) {
		$classes[] = 'primary-nav';
	}

	return $classes;

}

// Customize search form input box text.
add_filter( 'genesis_search_text', 'magazine_search_text' );
function magazine_search_text( $text ) {
	return esc_attr( __( 'Search the site ...', 'magazine-pro' ) );
}

// Remove entry meta in entry footer.
add_action( 'genesis_before_entry', 'magazine_remove_entry_meta' );
function magazine_remove_entry_meta() {

	// Remove if not single post.
	if ( ! is_single() ) {
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
	}

}

// Add support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Relocate after entry widget.
remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );
add_action( 'genesis_entry_footer', 'genesis_after_entry_widget_area' );

// Register widget areas.
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'magazine-pro' ),
	'description' => __( 'This is the top section of the homepage.', 'magazine-pro' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home - Middle', 'magazine-pro' ),
	'description' => __( 'This is the middle section of the homepage.', 'magazine-pro' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'magazine-pro' ),
	'description' => __( 'This is the bottom section of the homepage.', 'magazine-pro' ),
) );

// Override Genesis site title 
remove_action( 'genesis_site_title', 'genesis_seo_site_title' );

add_action( 'genesis_site_title', 'cofense_seo_site_title' );

/**
 * Override the site-title section of Genesis header
 *
 */
function cofense_seo_site_title() {

	// Set what goes inside the wrapping tags.
	$inside = sprintf( '<a href="%s">Sharpening<br>Your Defenses<span class="site-subtitle">PRESENTED BY <img src="'. get_stylesheet_directory_uri() . '/images/Cofense.png" /></span></a>', trailingslashit( home_url() ), get_bloginfo( 'name' ) );

	// Determine which wrapping tags to use.
	$wrap = genesis_is_root_page() && 'title' === genesis_get_seo_option( 'home_h1_on' ) ? 'h1' : 'p';

	// A little fallback, in case an SEO plugin is active.
	$wrap = genesis_is_root_page() && ! genesis_get_seo_option( 'home_h1_on' ) ? 'h1' : $wrap;

	// Wrap homepage site title in p tags if static front page.
	$wrap = is_front_page() && ! is_home() ? 'p' : $wrap;

	// And finally, $wrap in h1 if HTML5 & semantic headings enabled.
	$wrap = genesis_html5() && genesis_get_seo_option( 'semantic_headings' ) ? 'h1' : $wrap;

	/**
	 * Site title wrapping element
	 *
	 * The wrapping element for the site title.
	 *
	 * @since 2.2.3
	 *
	 * @param string $wrap The wrapping element (h1, h2, p, etc.).
	 */
	$wrap = apply_filters( 'genesis_site_title_wrap', $wrap );

	// Build the title.
	$title = genesis_markup( array(
		'open'    => sprintf( "<{$wrap} %s>", genesis_attr( 'site-title' ) ),
		'close'   => "</{$wrap}>",
		'content' => $inside,
		'context' => 'site-title',
		'echo'    => false,
		'params'  => array(
			'wrap' => $wrap,
		),
	) );

	echo apply_filters( 'genesis_seo_title', $title, $inside, $wrap ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

}

add_filter( 'wp_nav_menu_items', 'custom_nav_search', 10, 2 );
/**
* Add search box to nav menu.
*/
function custom_nav_search( $items, $args ) {
    if ( 'secondary' === $args->theme_location ) { // affect only Primary Navigation Menu.
        $items .= '<li class="menu-item search right">' . get_search_form( false ) . '</li>';
    }

    return $items;

}
 /* Add Next Page Button in First Row */
 add_filter( 'mce_buttons', 'my_add_next_page_button', 1, 2 ); // 1st row
 
 /**
  * Add Next Page/Page Break Button
  * in WordPress Visual Editor
  */
 function my_add_next_page_button( $buttons, $id ){
  
	 /* only add this for content editor */
	 if ( 'content' != $id )
		 return $buttons;
  
	 /* add next page after more tag button */
	 array_splice( $buttons, 13, 0, 'wp_page' );
  
	 return $buttons;
 }
add_action('init', 'remove_parent_theme_author_shortcodes');

function remove_parent_theme_author_shortcodes() {
	remove_shortcode( 'post_author' );
	remove_shortcode( 'post_author_link' );

	add_shortcode( 'post_author', 'coauthor_override_genesis_post_author_shortcode' );
	add_shortcode( 'post_author_posts_link', 'coauthor_override_genesis_post_author_posts_link_shortcode' );

}

function coauthor_override_genesis_post_author_shortcode( $atts ) {

	if ( ! post_type_supports( get_post_type(), 'author' ) ) {
		return '';
	}
    if ( function_exists( 'coauthors' )) {
        $author = coauthors( $between = ',', $betweenLast = null, $before = null, $after = null, $echo = false ); 
    } else {
        $author = get_the_author();
	}

	if ( ! $author ) {
		return '';
	}

	$defaults = array(
		'after'  => '',
		'before' => '',
	);

	$atts = shortcode_atts( $defaults, $atts, 'post_author' );

	if ( genesis_html5() ) {
		$output  = sprintf( '<span %s>', genesis_attr( 'entry-author' ) );
		$output .= $atts['before'];
		$output .= sprintf( '<span %s>', genesis_attr( 'entry-author-name' ) ) . esc_html( $author ) . '</span>';
		$output .= $atts['after'];
		$output .= '</span>';
	} else {
		$output = sprintf( '<span class="author vcard">%2$s<span class="fn">%1$s</span>%3$s</span>', esc_html( $author ), $atts['before'], $atts['after'] );
	}

	return apply_filters( 'genesis_post_author_shortcode', $output, $atts );

}

function coauthor_override_genesis_post_author_posts_link_shortcode( $atts ) {

	if ( ! post_type_supports( get_post_type(), 'author' ) ) {
		return '';
	}

	if ( ! post_type_supports( get_post_type(), 'author' ) ) {
		return '';
	}
    if ( function_exists( 'coauthors' )) {
        $author = coauthors( $between = ',', $betweenLast = null, $before = null, $after = null, $echo = false ); 
    } else {
        $author = get_the_author();
	}
	
	if ( ! $author ) {
		return '';
	}

	$defaults = array(
		'after'  => '',
		'before' => '',
	);

	$atts = shortcode_atts( $defaults, $atts, 'post_author_posts_link' );

    if ( function_exists( 'coauthors_posts_links' )) {
        $coauthors_exist = 1;
        preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', coauthors_posts_links(null,null,null,null,false), $result);
		$url = $result['href'][0];
    } else {
		$url = get_author_posts_url( get_the_author_meta( 'ID' ) );
    }

	if ( genesis_html5() ) {
		$output  = sprintf( '<span %s>', genesis_attr( 'entry-author' ) );
		$output .= $atts['before'];
		$output .= sprintf( '<a href="%s" %s>', $url, genesis_attr( 'entry-author-link' ) );
		$output .= sprintf( '<span %s>', genesis_attr( 'entry-author-name' ) );
		$output .= esc_html( $author );
		$output .= '</span></a>' . $atts['after'] . '</span>';
	} else {
		$link   = sprintf( '<a href="%s" rel="author">%s</a>', esc_url( $url ), esc_html( $author ) );
		$output = sprintf( '<span class="author vcard">%2$s<span class="fn">%1$s</span>%3$s</span>', $link, $atts['before'], $atts['after'] );
	}

	return apply_filters( 'genesis_post_author_posts_link_shortcode', $output, $atts );

}
/**
 * Customizes Display Posts plugin for Resources page
 * @see https://displayposts.com/docs/the-output-filter/
 *
 */
function resources_dps_output_customization( $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class, $author, $category_display_text ) {
	if ( in_category('resources')) {
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $title . '<hr>' . $excerpt . $image . '</' . $inner_wrapper . '>';
	}
	return $output;
  }
  add_filter( 'display_posts_shortcode_output', 'resources_dps_output_customization', 10, 11 );