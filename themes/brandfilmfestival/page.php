<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Haymarket
 * @subpackage Serena_Williams
 * @since Serena Williams 1.0
 */
 get_header(); ?>

		<?php
	    while ( have_posts() ) : the_post(); ?>
	            <?php the_content(); ?>

	    <?php
	    endwhile; 
	    wp_reset_query(); 
    	?>

<?php get_footer(); ?>