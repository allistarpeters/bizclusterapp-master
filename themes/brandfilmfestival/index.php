<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Haymarket
 * @subpackage Brand Film Festival
 * @since Brand Film Festival 1.0
 */
get_header(); ?>

<?php
    while ( have_posts() ) : the_post(); ?>
    <?php the_content(); 
?>
<?php
    endwhile; 
    wp_reset_query(); 
?>

<?php get_footer(); ?>