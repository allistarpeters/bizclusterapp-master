'use strict';

/*-----------------------------------
Variables
-----------------------------------*/

var scrollBindController, mobileNavUIController, mobileNavController, categoryUIController, categoryController, videoUIController, videoController;


/*-----------------------------------
Scroll, Bind, Anchor functionality
-----------------------------------*/

scrollBindController = (function($){
    var Bind, createFixedClass;

    createFixedClass = function(string){
        var newString, fixedPrefix;

        newString = string.slice(1);
        fixedPrefix = 'fixed-'

        return fixedPrefix + newString;
    };

    Bind = function(anchor, element){
        var fixedName;
        fixedName = createFixedClass(element);

        $(window).scroll(function(){
            var scrollTop, anchorHeight, anchorOffset, elementHeight, anchorBottomNearWindowTop;

            scrollTop = $(window).scrollTop();
            anchorHeight = $(anchor).height();
            anchorOffset = $(anchor).offset().top;
            elementHeight = $(element).height();
            anchorBottomNearWindowTop = ((anchorHeight + anchorOffset) - elementHeight) - scrollTop;

            if(anchorBottomNearWindowTop <= 0) $('body').addClass(fixedName);
            else $('body').removeClass(fixedName);
        });
    };

    return{
        bindElement : function(anchor, element){
            //console.log('Initiate Scroll Bind on Element');
            var bind = new Bind(anchor, element);
        }
    }
})(jQuery);


/*-----------------------------------
Mobile Navigation
-----------------------------------*/

mobileNavUIController = (function(){
    var DOMStrings;

    DOMStrings = {
        nav_buton : '#nav-button',
        nav_link : '.main-navigation a',
        body : 'body'
    }

    return {
        getDOMStrings: function(){
            return DOMStrings;
        }
    }
})();

mobileNavController = (function($, UICtrl){
    var setUpEventListeners;

    setUpEventListeners = function(){
        var DOM = UICtrl.getDOMStrings();

        $(DOM.nav_buton).on('click touchstart', function(event){
            event.preventDefault();
            $(DOM.body).toggleClass('expand-navigation');
        })

        $(DOM.nav_link).on('click touchstart', 'i', function(event){
            event.preventDefault();
            var iconListElementParent = $(this).parent().parent();
            iconListElementParent.toggleClass('expand');
        })
    };

    return {
        init: function(){
            //console.log('Initiate Mobile Nav Button.')
            setUpEventListeners();
        }
    }
})(jQuery, mobileNavUIController);


/*-----------------------------------
Category Accordians
-----------------------------------*/

categoryUIController = (function($){
  var DOMStrings;

  DOMStrings = {
    category_module : '.category-module'
  }

  return {
    getDOMStrings: function(){
      return DOMStrings;
    }
  }
})(jQuery);

categoryController = (function($, UICtrl){
  var setUpEventListeners;

  var toggleElement = function(element){
    element.stop().slideToggle();
  };

  setUpEventListeners = function(){
    var DOM = UICtrl.getDOMStrings();

    $(DOM.category_module).on('click', 'a', function(event){
      event.preventDefault();
      $(this).parent().toggleClass('current');
      toggleElement($(this).parent().parent().find('div'));
      //console.log('category link clicked');
    });
  };

  return {
    init: function(){
      //console.log('Initiate Category Controllers');
      setUpEventListeners();
    }
  }
})(jQuery, categoryUIController);


/*-----------------------------------
Video YouTube Overlay
-----------------------------------*/

videoUIController = (function($){
    var DOMStrings, HTMLStrings;

    DOMStrings = {
        body : 'body',
        video_module : '.video-module',
        overlay : '.overlay',
        overlay_close : '.overlay .close'
    }

    HTMLStrings = {
        video_overlay : '<div class="overlay"><div class="wrapper"><div class="cell"><div class="video-wrap"><a href="#" class="close">Close</a><div class="video"><iframe src="https://www.youtube.com/embed/%video_id%?rel=0&amp;controls=0&amp;showinfo=0?ecver=2" frameborder="0"  allowfullscreen></iframe></div></div></div></div></div>',
        image_overlay : '<div class="overlay"><div class="wrapper"><div class="cell"><div class="video-wrap"><a href="#" class="close">Close</a><div class="video backup" style="background-image:url(%image_url%)"></div></div></div></div></div>'
    }

    return {
        seedVideo: function(id, videoIdPresent){
            var html, newHtml;
            html = (videoIdPresent) ? HTMLStrings.video_overlay : HTMLStrings.image_overlay;
            newHtml = (videoIdPresent) ? html.replace('%video_id%', id) : html.replace('%image_url%', id);
            $(DOMStrings.body).append(newHtml);
        },
        removeOverlay: function(){
            $(DOMStrings.overlay).remove();
        },
        getDOMStrings: function(){
            return DOMStrings;
        },
        getHTMLStrings: function(){
            return HTMLStrings;
        }
    }

})(jQuery);

videoController = (function($, UICtrl){
    var setUpEventListeners;

    setUpEventListeners = function(){
        var DOM = UICtrl.getDOMStrings();
        var overlayElements = DOM.overlay + ',' + DOM.overlay_close;

        $(DOM.video_module).on('click', 'a', function(event){
            var videoID, parentImgUrl, trimmedUrl;
            event.preventDefault();
            videoID = $(this).data('video');
            parentImgUrl = $(this).parent().css('background-image');
            trimmedUrl = parentImgUrl.substr(5, parentImgUrl.length - 7);

            if(videoID) UICtrl.seedVideo(videoID, true);
            else UICtrl.seedVideo(trimmedUrl, false);
        });

        $(DOM.body).on('click', overlayElements, function(event){
            event.preventDefault();
            UICtrl.removeOverlay();
        });
    }

    return {
        init: function(){
            //console.log('Initiate Video Controller');
            setUpEventListeners();
        }
    }
})(jQuery, videoUIController);


/*-----------------------------------
Slick Carousel http://kenwheeler.github.io/slick/
-----------------------------------*/
(function($){
    $('.video-module .bottom').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive : [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 570,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
})(jQuery);


/*-----------------------------------
Scrolling and Animation
-----------------------------------*/
var scrollTargetingController;

scrollTargetingController = (function($){
    var setUpScrollTargeting;

    setUpScrollTargeting = function(element, distance, className){
        $(window).scroll(function(){
            var scrollTop = $(window).scrollTop();
            $(element).each(function(){
                var elementScrollTop = $(this).offset().top;
                if((elementScrollTop - scrollTop) <= distance) $(this).addClass(className);
            });
        });
    };

    return {
      watch: function(element, distance, className){
          setUpScrollTargeting(element, distance, className);
      }
    }
})(jQuery);

// Disabled animation on home page.
/*scrollTargetingController.watch('.main-section .about p', 500, 'bffBounceInUp');
scrollTargetingController.watch('.main-section .video-wysiwyg', 400, 'bffFlipInY');
scrollTargetingController.watch('.main-section .button', 500, 'bffBounceInLeft');
scrollTargetingController.watch('.main-section .news-brief > div', 500, 'bffFadeInUp');
scrollTargetingController.watch('.main-section .speakers-left-column', 500, 'bffFadeInDown');
scrollTargetingController.watch('.main-section .speakers-right-column', 500, 'bffFadeInUp');*/


/*-----------------------------------
Find hash in browser location and navigate to it
-----------------------------------*/
var findPageAnchorAndNavigate;

findPageAnchorAndNavigate = (function($){
  var hash = window.location.hash;
  var offset = (hash &&  $('a[name=' + hash + ']').length > 0) ? $('a[name=' + hash + ']').offset().top : null;

  return {
    init : function(){
      if (hash) $('html, body').animate({scrollTop: offset}, 1000);
    }
  }
})(jQuery);

/*-----------------------------------
Initializers
-----------------------------------*/

videoController.init();
categoryController.init();
scrollBindController.bindElement('#main-header', '#main-navigation');
mobileNavController.init();
findPageAnchorAndNavigate.init();
