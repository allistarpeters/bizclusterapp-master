<?php
/**
 * Brand Film Festival functions and definitions
 *
 *
 * @package Haymarket
 * @subpackage Brand Film Festival
 * @since Brand Film Festival 1.0.1
 */

CONST VERSION = '1.0.1';

/** Enqueue the theme styles
 * @see https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 * @see https://codex.wordpress.org/Function_Reference/wp_register_style
 * @see https://codex.wordpress.org/Function_Reference/get_stylesheet_uri
 * @see https://codex.wordpress.org/Function_Reference/get_stylesheet_directory_uri
 */
function add_theme_styles() {
	$media = 'all';
	$sheet_dir =  get_stylesheet_directory_uri();
	$sheets = array(
		'style'          => get_stylesheet_uri(),
		'font-awesome'   => $sheet_dir . '/stylesheets/font-awesome.css',
		'bff-fonts'      => $sheet_dir . '/stylesheets/fonts.css',
		'bff-normal'     => $sheet_dir . '/stylesheets/normalize.css',
		'bff-animate'    => $sheet_dir . '/stylesheets/animate.css',
		'bff-animation'  => $sheet_dir . '/stylesheets/animation.css',
		'bff-theme'      => $sheet_dir . '/stylesheets/theme.css',
		'bff-responsive' => $sheet_dir . '/stylesheets/responsive.css',
		'bff-hacks'      => $sheet_dir . '/stylesheets/hacks.css',
		'bff-custom'     => $sheet_dir . '/stylesheets/custom.css',
	);


	wp_enqueue_style( 'style', get_stylesheet_uri() );

	foreach ( $sheets as $handle => $asset_path ) {
		wp_enqueue_style(
			$handle,
			$asset_path,
			array(), // no dependencies specified
			VERSION,
			$media
		);
	}

}

function add_theme_scripts(){
	/**
	 * Separation of concerns
	 */
	add_theme_styles();
    wp_enqueue_script( 'slick-carousel', get_template_directory_uri() . '/javascript/slick.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'countdown-js', get_template_directory_uri() . '/javascript/jquery.countdown.js', array( 'jquery' ) );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/javascript/scripts.js', array('slick-carousel','countdown-js', 'jquery'), '1.1', true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

add_filter( 'pods_shortcode', function( $tags )  {
  $tags[ 'shortcodes' ] = true;

  return $tags;

});

/**
 * short term addition to bring the site back online.
 */
function bfl_custom_mime_types( $mimes ) {
	
    // New allowed mime types.
    $mimes['mp4']  = 'video/mp4';
    $mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
    $mimes['doc']  = 'application/msword'; 

    // Optional. Remove a mime type.
    unset( $mimes['exe'] );

	return $mimes;
}
add_filter( 'upload_mimes', 'bfl_custom_mime_types' );

