<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="advocacy-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
		<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<!-- About page content-->

<section class="sheet-section">

	<div class="pages-hexset-wrap-bottom advocacy-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/advocacy_hex.png" alt="" class="advocacy-bott-hex">
	</div>

	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
	</div>
	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
	</div>

	<div class="sheet-container advocacy-page">
		<div class="sheet">

			<?php get_template_part('inc/contact-form') ?>

			<div class="advocacy sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php if ( have_rows('sub_pages_links') ): ?>

		<!-- New COVID start -->

			<div class="covid-banner-1">
				<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/corner_1.svg" alt="" class="banner-corner-1">
				<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/corner_2.svg" alt="" class="banner-corner-2">
				<p class="banner-title">AWIR Letter to Insurers, Vendors during COVID-19 Pandemic and Manufacturers Advocating for Provider Relief</p>
				<div class="banner-col">
					<div class="banner-col-1">
					<a href="<?= get_permalink( get_page_by_path( 'covid19' ) )?>">
						<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/banner_icon.svg" alt="" class="banner-icon">
					</a>
					</div>
					<div class="banner-col-2">
						<p class="banner-text">Use this letter to advocate for yourself and the medical community to allow everyone to focus on patient care and remove administrative hurdles.</p>
						<p class="banner-text"><b>The letter can be viewed, edited, and downloaded by clicking on <a href="https://awirgroup.org/wp-content/uploads/2020/03/COVID19-Letter-of-Request-to-Insurers.Vendors.Drug-Manufacturers.docx" target="_blank"> the link</a>.</b></p>
					</div>
				</div>
			</div>

			<a href="<?= get_permalink( get_page_by_path( 'covid19' ) )?>" class="covid-banner-2">
			<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/banner_bg.png" alt="" class="covid-desktop">
			<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/banner_bg-mob.png" alt="" class="covid-mob">
			</a>

		<!-- New COVID end-->

			<div class="advocacy-resources">

				<?php for ( $i = 0; have_rows('sub_pages_links'); $i++ ) : the_row()?>

				<?php $page = get_sub_field('page') ?>
				<?php 	if ( $i % 2 == 0 ): ?>
				<div class="advocacy-resources__item-wrap">
				<?php 	endif ?>
					<div class="advocacy-resources__item">
						<a href="<?= get_permalink( $page ) ?>" class="btn btn__main btn__download"><?= esc_html($page->post_title) ?>
							<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/btn-blur.svg" alt="" class="downloads__btn-blur">
						</a>
					</div>
				<?php 	if ( $i % 2 ): ?>
				</div>

				<?php 	endif ?>

				<?php endfor ?>

				<?php if ( $i % 2 ): ?>
				</div>
				<?php endif ?>

			</div>

			<!--  -->
			<a href="https://awirgroup.org/advocacy_training/" class="single-banner advocacy-banner" target="_blank" data-wpel-link="internal">
				<img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_training.png" alt="" class="single-banner--desktop">
				<img src="<?= get_template_directory_uri() ?>/assets/img/education/mobile_banner_training.png" alt="" class="single-banner--mobile">
			</a>

			<?php endif ?>

		</div>
	</div>
</section>

<?php get_footer();
