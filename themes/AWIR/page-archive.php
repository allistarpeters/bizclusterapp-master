<?php

//awir_mailchimp::getInstance()->fetch_campaigns();

get_header();

the_post();
$year = '';

?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">

		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_2.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>

<div class="pages-headline exhibitors">
	<p class="main-text pages-headline__category">Newsletter /</p>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

	<section class="sheet-section calendar-section">

		<div class="pages-hexset-wrap-bottom exhibitors-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
		</div>

		<div class="sheet-container">
			<div class="sheet">

				<?php $started = false; ?>
				<?php foreach ( awir_mailchimp::get_campaigns() as $campaign ): ?>

				<?php 	if ( ($y = date('Y', $campaign['time'])) && $y !== $year ): $year = $y ?>
				<?php 		if ( $started ): ?>
				</div>
				<?php 		endif; $started = true; ?>
				<div class="sheet-text-wrap new-archive-text-wrap">
					<h1><?= $year ?></h1>
				<?php 	endif ?>
					<a href="<?=esc_url($campaign['url'])?>"><h4><?= esc_html($campaign['title'])?></h4></a>

				<?php endforeach ?>
				<?php if ( $started ): ?>
				</div>
				<?php endif ?>

			</div><!--End Sheet -->
		</div>
	</section>

<?php get_footer();
