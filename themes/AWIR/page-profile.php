<?php

$profile = new awir_profile();

if ( !$profile->ID )
	awir::user_page_redirect('become_member');

if ( $profile->process_form() )
	awir::user_page_redirect('user', ['edited'=>1]);

get_header();

?>

	<section class="sheet-section user-sheet">

		<div class="sheet-container user-page">
			<div class="sheet">
				<h2 class="title-text sheet__title">Edit profile</h2>

				<form action="<?=get_permalink()?>" class="main-form main-form--left" method="post" enctype="multipart/form-data">

					<label class="inline-upload-avatar">
						<div class="member add-photo">
							<a class="member__photo photo-back-hex edit-photo">
								<img src="<?= awir_theme::avatar() ?>" alt="AWIR" class="member__photo-img">
							</a>
							<div class="member__data">
								<a class="main-text member__edit">Add photo</a>
							</div>
						</div>
						<input type="file" name="<?=$profile->form_prefix?>_avatar" style="display:none"/>
						<input type="hidden" name="<?=$profile->form_prefix?>[avatar]" value="<?= $profile->avatar ?>" />
					</label>

					<!-- reg form row 1 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<?php $profile->the_field('email', ['placeholder'=>'New Email']) ?>
						</div>
						<div class="main-form__input-wrap">
							<?php $profile->the_field('email_verify', ['placeholder'=>'Verify New Email','required'=>false]) ?>
						</div>
					</div>

					<!-- reg form row password -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<?php $profile->the_field('password', ['placeholder'=>'Old Password','required'=>false]) ?>
						</div>
						<div class="main-form__input-wrap">
							<?php $profile->the_field('new_password', ['placeholder'=>'New Password','required'=>false]) ?>
						</div>
						<div class="main-form__input-wrap">
							<?php $profile->the_field('password_verify', ['placeholder'=>'Verify New Password','required'=>false]) ?>
						</div>
					</div>

					<!-- reg form row 2 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<?php $profile->the_field('first_name') ?>
						</div>
						<div class="main-form__input-wrap">
							<?php $profile->the_field('last_name') ?>
						</div>
					</div>

					<!-- reg form row 3 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<p class="main-form__input-title">Country</p>
							<div class="select-box">
								<?php $profile->the_field('country',['placeholder'=>'Select a country']) ?>
							</div>
						</div>
						<div class="main-form__multiple-input">
							<div class="main-form__input-wrap">
								<p class="main-form__input-title">State</p>
								<div class="select-box">
									<?php $profile->the_field('state',['placeholder'=>'Select a state']) ?>
								</div>
							</div>
							<div class="main-form__input-wrap">
								<p class="main-form__input-title">City</p>
								<?php $profile->the_field('city') ?>
							</div>
						</div>
					</div>

					<!-- reg form row 4 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<?php $profile->the_field('address') ?>
						</div>
						<div class="main-form__input-wrap">
							<?php $profile->the_field('address_line_2') ?>
						</div>
					</div>

					<!-- reg form row 5 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<?php $profile->the_field('phone') ?>
							</div>
							<div class="main-form__multiple-input birth-date">
								<div class="main-form__input-wrap">
								<p class="main-form__input-title">Date of Birth</p>
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[day]">
											<?php $day = (int)$profile->birth_date('d') ?>
											<?php for ( $i = 1; $i <= 31; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $day ) echo 'selected ' ?>><?php printf('%02d', $i) ?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
								<div class="main-form__input-wrap">
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[month]">
											<?php $month = (int)$profile->birth_date('m') ?>
											<?php for ( $i = 1; $i <= 12; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $month ) echo 'selected ' ?>><?= date('F', mktime(0,0,0,$i,15)) ?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
								<div class="main-form__input-wrap">
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[year]">
											<?php $profile_year = (int)$profile->birth_date('Y') ?>
											<?php $current_year = date('Y') ?>
											<?php for ( $i = $current_year-100; $i <= $current_year-16; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $profile_year ) echo 'selected ' ?>><?=$i?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
						</div>
					</div>

					<!-- reg form row 6 -->
					<div class="main-form__row">


						<div class="main-form__radio-wrap">
							<div class="main-form__title-wrap">
								<p class="main-form__input-title">Gender</p>
								<?php $profile->the_field('gender') ?>
							</div>
						</div>

						<div class="main-form__radio-wrap">
							<div class="main-form__title-wrap">
								<p class="main-form__input-title">Are you a member of ACR</p>
								<?php $profile->the_field('acr_member') ?>
							</div>
						</div>
					</div>

					<!-- reg form row 7 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<div class="select-box select-box-small">
								<?php $profile->the_field('race_ethnicity', ['placeholder'=>'Race/Ethnicity']) ?>
							</div>
						</div>
						<div class="main-form__input-wrap">
							<div class="select-box select-box-small">
								<?php $profile->the_field('med_degree', ['placeholder'=>'What is your Medical Degree?']) ?>
							</div>
						</div>
					</div>

					<div class="registration-divider"></div>

					<div class="your-interests">
						<h3><strong>What are you Primary Areas of Interest</strong> (check all that apply)</h3>
						<div class="your-interests__area">
							<?php $profile->the_field('interests', [
								'wrap' => ['<div class="your-interests__item">', '</div>'],
							]) ?>
						</div>

						<h3><strong>What are your Primary Professional Activities. These may be different from your «Primary Areas of Interest»</strong> (check all that apply)</h3>
						<div class="your-interests__area">
							<?php $profile->the_field('activities', [
								'wrap' => ['<div class="your-interests__item">', '</div>'],
							]) ?>
						</div>
					</div>

					<div class="consent">
						<p class="consent__title">VIDEO AND MEDIA CONSENT AND RELEASE</p>
						<div class="main-form__radio-wrap main-form__consent">
							<div class="main-form__title-wrap">
								<?php $profile->the_field('media_consent') ?>
							</div>
						</div>
						<a href="" class="consent__details-btn">Details</a>
					</div>

					<div class="discount-divider"></div>

					<?php $profile->nonce_field() ?>
					<button type="submit" class="btn btn-centered btn__main edit-profile"><span class="btn__text">Save profile</span></button>

				</form>

			</div>
		</div>
  </section>

  <script>
	var types = <?= json_encode( $types, JSON_FORCE_OBJECT ) ?>;
	jQuery( function($) {
		$('.consent__details-btn').click(function(e){
			e.preventDefault();
			$('#popup-consent').addClass('active');
		});
	} );
	</script>

	<div id="popup-consent" class="popup-main popup-main-middle popup-large ">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<div class="sheet-text-wrap">
						<?php wp_reset_query() ?>
						<?php the_content() ?>
					</div>
				</div>
		</div>
	</div>

<?php get_footer();
