<?php

get_header();

the_post();
?>

<!-- Pages Hero Section -->
<div class="local-chapters">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->


<div class="pages-headline">
  <a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

<section class="sheet-section advocacy-page">

	<div class="sheet-container">
		<div class="sheet">

			<div class="awr">
				<?php the_content() ?>
			</div>

		</div>
	</div>
</section>

<?php get_footer();
