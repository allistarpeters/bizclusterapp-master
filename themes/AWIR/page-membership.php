<?php

get_header();

the_post();
?>

<!-- Pages Hero Section -->
<div class="membership-header">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<div class="pages-headline pages-headline__alone">
	<!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

<section class="sheet-section">

		<div class="membership-hexset-top">
			<img src="<?= get_template_directory_uri() ?>/assets/img/membership/membership_2.png" alt="" class="hexset-middle">
		</div>
		<div class="membership-hexset-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/membership/membership-hex-l.png" alt="" class="hexset-middle">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

	<div class="sheet-container membership-page">
		<div class="sheet">

			<div class="sheet-text-wrap exhibitors-text-wrap">
				<h1><?php the_field('membership_title') ?></h1>
			</div>

			<?php while ( have_rows('membership_banner') ): the_row() ?>

			<div class="membership-banner">
				<div class="banner__overlay"></div>
				<div class="membership-banner__text-wrap">
					<h2 class="membership-banner__title"><?php the_sub_field('title') ?></h2>
					<p class="membership-banner__subtitle"><?php the_sub_field('sub_title') ?></p>
					<p class="membership-banner__info"><?php the_sub_field('text') ?></p>
				</div>
				<div class="membership-banner__button-wrap">
					<a href="<?= awir::user_page_url('memberships')?>" class="btn btn__become-member">Become a Member</a>
				</div>
			</div>

			<?php endwhile ?>

			<div class="membership-benefits">
				<div class="sheet-text-wrap new-archive-text-wrap">
					<h2><?php the_field('accordion_title') ?></h2>

					<?php while ( have_rows('accordion_items') ): the_row() ?>

					<a class="benefit" href="javascript:void(0);">
						<h3><?php the_sub_field('title') ?></h3>
						<div class="benefits-description benefits-description benefits-description__1">
							<?php the_sub_field('text') ?>
						</div>
					</a>

					<?php endwhile ?>

				</div>

			</div>

			<div class="membership-text-wrap sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php reset_rows() ?>
			<?php while ( have_rows('membership_banner') ): the_row() ?>
			<div class="membership-banner membership-banner__small">
				<div class="banner__overlay"></div>
				<div class="membership-banner__text-wrap">
					<h2 class="membership-banner__title"><?php the_sub_field('title') ?></h2>
					<p class="membership-banner__subtitle"><?php the_sub_field('sub_title') ?></p>
				</div>
				<div class="membership-banner__button-wrap">
					<a href="<?= awir::user_page_url('memberships')?>" class="btn btn__become-member">Become a Member</a>
				</div>
			</div>
			<?php endwhile ?>

			<?php get_template_part('inc/contact-form') ?>

		</div>
	</div>
</section>


<?php get_footer();
