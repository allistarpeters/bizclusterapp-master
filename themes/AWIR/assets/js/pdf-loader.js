jQuery( function($){
    $(document).ready(function(){
        var list = document.querySelectorAll('.blogs-content .sheet-text-wrap');
        var config = { attributes: false, childList: true, subtree: true };

        list.forEach(function(el){
            var loader = el.querySelector('.custom-pdf-preloader');
            var targetNode = el.querySelector('.pdfemb-viewer');

            // Callback function to execute when mutations are observed
            var callback = function(mutationsList, observer) {
                var errorNode = targetNode.querySelector('.pdfemb-errormsg');
                var contentNode = targetNode.querySelector('.pdfemb-pagescontainer');

                if(errorNode || contentNode){
                    $(loader).fadeOut(); // remove loader
                    observer.disconnect(); // stop observing
                }
            };

            // Create an observer instance linked to the callback function
            var observer = new MutationObserver(callback);

            // Start observing the target node for configured mutations
            observer.observe(targetNode, config);
        });
    });
} );