var gulp = require('gulp'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  webpack = require('webpack'),
  plumber = require('gulp-plumber'),
  watch = require('gulp-watch'),
  webpackStream = require('webpack-stream'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cache = require('gulp-cache'),
  del = require('del'),
  imagemin = require('gulp-imagemin');


gulp.task('scripts', ['usual-scripts', 'pdf-loader'], function () {
  return gulp.src('./src/js/*.js')
    .pipe(webpackStream({
     entry: ["babel-polyfill", "./src/js/app.js"],
      output: {
        filename: 'app.js',
      },
      // plugins:[
      //   new webpack.DefinePlugin({
      //     'process.env.NODE_ENV': JSON.stringify('production')
      //   })
      // ],
      resolve: {
        alias: {
          'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
        }
      },
      module: {
        rules: [{
          test: /\.(js)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: [["env", {
             "targets": {
              "ie": "11"
             }
            }]]
          }
        }]
      },
      externals: {
        jquery: 'jQuery'
      }
    }))

    .pipe(gulp.dest('./js/'))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./js/'))
});

gulp.task('usual-scripts', function () {
  return gulp.src('./src/js/scripts.js')
    .pipe(gulp.dest('./js/'))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./js/'))
});

gulp.task('pdf-loader', function () {
  return gulp.src('./src/js/pdf-loader.js')
    .pipe(gulp.dest('./js/'))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./js/'))
});

// Compiles SCSS to CSS
gulp.task('styles', function () {
  return gulp.src('src/scss/styles.scss')
    .pipe(sass({
      style: 'expanded'
    }))
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('./css/'))
});

gulp.task('imagemin', function() {
  return gulp.src('src/img/**/*')
    // .pipe(imagemin([
    //     imagemin.mozjpeg({quality: 75, progressive: true}),
    //     imagemin.optipng({optimizationLevel: 5})
    // ]))
  // .pipe(cache(imagemin({
  //   plugins: [
  //     imagemin.optipng(),
  //     imagemin.svgo()
  //  ]
  // }))) // Cache Images
  .pipe(gulp.dest('./img/'));
});

gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
  .pipe(gulp.dest('./fonts/'));
});

gulp.task('watch', function () {
  gulp.watch('src/js/**/*.js', ['scripts']);
  gulp.watch('src/scss/**/*.scss', ['styles']);
  gulp.watch('src/img/**/*', ['imagemin']);
});

gulp.task('set-dev-env', function () {
  return process.env.NODE_ENV = 'development';
});

gulp.task('set-prod-env', function () {
  return process.env.NODE_ENV = 'production';
});


gulp.task('default', ['set-dev-env', 'scripts', 'styles', 'imagemin', 'fonts', 'watch']);
gulp.task('build', ['set-prod-env', 'scripts', 'styles', 'imagemin', 'fonts']);
