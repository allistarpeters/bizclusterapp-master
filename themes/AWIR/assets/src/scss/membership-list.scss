.pay-page {
	.advocacy.sheet-text-wrap {
		max-width: 632px;
		margin: 0 auto;
	}
}

.ms-wrap {
	.ms-form-table {
		width: 100%;
	}
	.ms-title-row {
		font-family: 'Roboto', sans-serif;
		font-size: 24px;
		font-weight: normal;
		line-height: 1.33;
		text-align: left;
		color: #401557;
		font-weight: bold;
		margin-top: 30px;
		display: block;

		@include small-down {
			margin-top: 30px;
		}
	}
	.wpmui-field-label {
		font-family: 'Roboto', sans-serif;
		font-size: 12px;
		font-weight: normal;
		line-height: 1.2;
		text-align: left;
		color: #767676;
		margin-bottom: 5px;
		white-space: nowrap;
	}
	.wpmui-input-wrapper,
	.wpmui-select-wrapper {
		border-bottom: 1px #b3b3b3 solid;
		position: relative;
		display: flex;
		flex-direction: column;
		justify-content: flex-end;
		margin: 15px 0;
		margin-right: 30px;
		&:before,
		&:after {
			content: '';
			position: absolute;
			left: 0;
			bottom: 0;
			width: 1px;
			height: 2px;
			background-color: #ae9dc9;
		}
		&:after {
			right: 0;
			left: auto;
		}
	}
	.wpmui-field-input {
		width: 100%;
		font-family: 'Roboto', sans-serif;
		font-size: 16px;
		font-weight: 500;
		line-height: 1.86;
		text-align: left;
		color: #250f47;
		border: none;
		min-height: 28px;
		&::placeholder {
			font-weight: normal;
		}
	}

	.ms-card-info {
		table {
			width: 100%;
			max-width: 400px !important;
		}
		.ms-col-card_code {
			max-width: 50px;
			.wpmui-input-wrapper {
				margin-right: 0;
				input {
					text-align: center;
					&::placeholder {
						text-align: left;
					}
				}
			}
		}
		.wpmui-field-input {
			display: inline-block;
			width: auto;
		}
	}

	.ms-col-expire {
		max-width: 100%;
		padding-bottom: 50px;
		border-bottom: 1px #e8e8e8 solid;
		@include small-down {
			padding-bottom: 30px;
		}
	}
  .wpmui-wrapper {
    &.wpmui-select-wrapper{
      min-width: 130px;
    }
  }
  #card_code-error{
    display: inline !important;
    white-space: normal !important;
    bottom: -30px !important;
    font-size: 11px !important;
    line-height: 1 !important;
  }
	.ms-select-wrapper {
		margin-right: 30px;
		display: inline-block;
		&:last-child {
			margin-right: 0;
		}
		&:after {
			content: '';
			position: absolute;
			right: 8px;
			bottom: 15px;
			width: 11px;
			height: 6px;
			background-image: url('../img/icons/pink-arrow-bottom.svg');
			background-position: center;
			background-size: contain;
			background-repeat: no-repeat;
			background-color: rgba(#fff, 0);
		}
	}

	.ms-select {
		font-size: 16px;
		line-height: 1.5;
		color: #250f47;
		position: relative;
		font-weight: 500;
		padding-right: 25px;
		font-family: 'Roboto', sans-serif;
		text-align: left;
		color: #250f47;
		border: none;
		margin-bottom: 5px;
		min-height: 28px;
	}

	.select2-container {
		display: none;
	}

	.wpmui-submit.button-primary {
		background-color: #884fc6;
		color: #ffffff;
		padding: 19px 10px 18px 10px;
		width: 100%;
		max-width: 180px;
		margin: 0 auto;
		font-size: 16px;
		transition: all 0.3s ease;
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
		line-height: 1em;
		border-radius: 4px;
		border: none;
		margin-top: 50px;

		@include small-down {
			margin-top: 30px;
		}
	}



	.ms-col-country {
		padding-bottom: 50px;
		border-bottom: 1px #e8e8e8 solid;
		.ms-select-wrapper {
			max-width: 53%;
			box-sizing: border-box;
			@include xxsmall-down {
				max-width: 100%;
			}
		}
		@include small-down {
			padding-bottom: 30px;
		}
	}

	.ms-col-last_name .wpmui-input-wrapper {
		margin-right: 0;
	}
	.ms-col-first_name .wpmui-input-wrapper {
		min-width: 220px;
		@include xsmall-down {
			margin-right: 0;
		}
	}
	.ms-col-last_name,
	.ms-col-first_name {
		@include xsmall-down {
			display: block;
			margin-right: 0;
		}
	}
}

.ms-wrap{
	#card_num-error,
	#card_code-error,
	#exp_month-error,
	#exp_year-error,
	#first_name-error,
	#last_name-error {
		display: none;
		color: #da285b;
		text-align-last: auto;
		font-family: 'Roboto', sans-serif;
		font-size: 14px;
		font-weight: normal;
		line-height: 1.2;
		white-space: nowrap;
		overflow: hidden;
		position: absolute;
    bottom: -23px;
    left: 0;
    max-width: 230px;
	}
	#card_num-error.active,
	#card_code-error.active,
	#exp_month-error.active,
	#exp_year-error.active,
	#first_name-error.active,
	#last_name-error.active {
		display: inline-block;
	}
}
.card_failed, .ms-validation-error p{
	border: none;
  margin: 0;
  margin-left: auto;
  padding: 0;
  font-size: 16px;
	font-weight: 400;
  color: #f00 !important;
  width: 100%;
	padding-bottom: 0 !important;
}
