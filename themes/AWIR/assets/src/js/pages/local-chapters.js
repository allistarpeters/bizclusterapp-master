import Sticky from 'sticky-js';

const LocalChapters = function() {
  if (window.currentPage !== 'local-chapters') return;
  let $places = $('.place');
  let activeStatesArray = getActiveStates();
  var sticky = new Sticky('.js-sticky', {
    'marginTop': 120,
  });
  function getActiveStates() {
    let statesArray = [];
    let localPlaces = Array.from(document.querySelectorAll('.place'));
    localPlaces.forEach(element => {
      let state = element.dataset.state.toLowerCase();
      if (statesArray.indexOf(state) === -1) {
        statesArray.push(state);
      }
    });
    return statesArray;
  };

  function blockHover(el, state) {
    let $selector = state ? $(`[data-state="${state.toUpperCase()}"]`) : $(el.currentTarget);
    if (el) {
      showMapTooltip(null, el.currentTarget.dataset.state.toLowerCase());
    }
    $places.removeClass('place-active');
    $selector.addClass('place-active');
  };

  function hoverElement(selector) {
    let $chapter = $places.on('mouseenter', blockHover)
  };

  function showMapTooltip(el, state) {
    let selector = state ? state : el.currentTarget.id;
    let $tooltipsAll = $('.select-map__map [id^=tooltip_] > g:first-child');
    let $tooltipEl = $(`.select-map__map #tooltip_${selector} > g:first-child`);
    $tooltipsAll.hide();
    $tooltipEl.show();
    blockHover(null, selector)
  }

  function initMapState() {
    let $tooltipEl = $('.select-map__map [id^=tooltip_]');
    $tooltipEl.hide();
    $(activeStatesArray.map(item => `.select-map__map #${item}`).join(', '))
      .css({ fill: "#884fc6" })
      .addClass('active-map-state')
      .on('mouseenter', showMapTooltip)

    $tooltipEl.toArray().forEach(element => {
      let tooltipState = element.id.substr(8, 2);
      //$(element.children[0]).hide();
      $(element).children().not('#placeholder').hide();
      if (activeStatesArray.indexOf(tooltipState) !== -1) {
        $(element).show();
        $(element).find('#placeholder use:last-child').css( { fill: '#000000' } )
      }
    });
  };

  const init = () => {
    hoverElement();
    initMapState();
  };

  return {
    init
  };

}();

export default LocalChapters;
