import Vue from 'vue';
import vueEventCalendar from 'vue-event-calendar';
import Sticky from 'sticky-js';

Vue.use(vueEventCalendar, {locale: 'en'})

const CalendarEvents = function() {
  function initApp() {
    var sticky = new Sticky('.js-sticky', {
      'marginTop': 120,
    });

    var app = new Vue({
      el: '#calendar',
      data: {
        filterNational: true,
        filterGlobal: true,
        filterLocal: true,
        filterSummits: true,
        filterEndorsed: true,
        demoEvents: [{
          date: '2018/06/23', // Required
          title: 'Foo' // Required
        }, {
          date: '2018/06/20',
          title: 'Bar',
        }],
        message: 'Hello Vue!'
      },
      mounted() {
        this.demoEvents = window.awir_events;
      },
      computed: {
       activeEvents() {
        return this.demoEvents
                    .filter(({category}) => ~this.activeCategoryList.indexOf(category));
       },
       activeCategoryList() {
        let catList = []; 
        this.filterNational && (catList.push('national'));
        this.filterGlobal && (catList.push('global-colleagues'));
        this.filterLocal && (catList.push('local'));
        this.filterSummits && (catList.push('summits'));
        this.filterEndorsed && (catList.push('endorsed'));
        return catList;
       },
       eventCategoryList() {
          let eventsArray = [];
          this.demoEvents.forEach(element => {
            if (eventsArray.indexOf(element.category) === -1) {
              eventsArray.push(element.category)
            }
          });
          return eventsArray;
        }
      },
      methods: {
        handleDayChanged (data) {
          if (data.events.length) {
            $(`[data-date='${data.date}']`).addClass('meeting-open')
            $('html, body').animate({
              scrollTop: $(`[data-date='${data.date}']`).offset().top - $('.top-menu').height() - 160
            }, 1500);
          }
        },
      }
    })
  }


  const init = () => {
    initApp();
  };

  return {
    init
  };

}();

export default CalendarEvents;
