import Sticky from 'sticky-js';

const GlobalChapters = function () {
    if (window.currentPage !== 'global-chapters') return;
    let $areas = {};
    let $_mapa;
    let $tt = $('.pri-tooltip');
    // let panZoom, currentHighlight;
    // let $places = $('.place');
    //
    // let $tooltip = $('#tooltip');
    // let $tooltip_plate = $('#tt-rect');
    // let $tooltip_text = $('#tt-text tspan');
    //
    // let $rollover = $('#Rollover');
    // let $rohint = $('#ro-hint tspan');
    //
    // let activeCountriesArray = getActiveCountries();
    // let activeCountriesPartArray = getActiveCountriesPart();
    new Sticky('.js-sticky', {
        'marginTop': 120,
    });

    // function fadeHint(){
    //     $tooltip.fadeOut(700);
    // }
    //
    // function moveMapTo(coordinates, zoom, tt_text){
    //     $tooltip.hide();
    //     $tooltip_text.text(tt_text);
    //     let _new_width = tt_text.length * 9;
    //     // here comes the magic.
    //     // do no touch!
    //     $tooltip_plate.attr('width', _new_width).attr('x',  - ((_new_width -94) / 2));
    //     $tooltip_text.attr('x', (_new_width / 2) + (- ((_new_width -94) / 2)));
    //     panZoom.zoom(1);
    //     setTimeout(function(){
    //         panZoom.center();
    //         setTimeout(function(){
    //             panZoom.pan(coordinates);
    //             setTimeout( function(){
    //                 panZoom.zoom(zoom);
    //                 $tooltip.fadeIn(250);
    //             }, 200);
    //         }, 200);
    //     },200);
    // }
    //
    // function getActiveCountries() {
    //     let countriesArray = [];
    //     let globalPlaces = Array.from(document.querySelectorAll('.place.type-chapter'));
    //     globalPlaces.forEach(element => {
    //         let country = element.dataset.country.toLowerCase();
    //         if (countriesArray.indexOf(country) === -1) {
    //             countriesArray.push(country);
    //         }
    //     });
    //     return countriesArray;
    // }
    //
    // function getActiveCountriesPart() {
    //     let countriesPartArray = [];
    //     let globalPartPlaces = Array.from(document.querySelectorAll('.place.type-partner'));
    //     globalPartPlaces.forEach(element => {
    //         let country = element.dataset.country.toLowerCase();
    //         if (countriesPartArray.indexOf(country) === -1) {
    //             countriesPartArray.push(country);
    //         }
    //     });
    //     return countriesPartArray;
    // }
    //
    // function blockHover(el, country) {
    //     let $selector = country ? $(`[data-country="${country.toUpperCase()}"]`) : $(el.currentTarget);
    //     if (el) {
    //         showMapTooltip(null, el.currentTarget.dataset.country.toLowerCase());
    //     }
    //     $places.removeClass('place-active');
    //     $selector.addClass('place-active');
    // }
    //
    // function hoverElement(selector) {
    //     let $chapter = $places.on('mouseenter', blockHover)
    // }
    //
    // function showRollOver(country_code){
    //     let $block = $(`[data-country="${country_code.toUpperCase()}"]`);
    //     let $title = $block.attr('data-label');
    //     $rohint.text($title);
    //     $rollover.stop().fadeIn(150);
    // }
    //
    // function clearRollover(){
    //     $rollover.stop().fadeOut(750);
    // }
    //
    // function showMapTooltip(el, country) {
    //     let selector = country ? country : el.currentTarget.id;
    //     let $tooltipsAll = $('svg [id^=tooltip_] > g:first-child');
    //     let $tooltipEl = $(`svg #tooltip_${selector} > g:first-child`);
    //     $tooltipsAll.hide();
    //     $tooltipEl.show();
    //     blockHover(null, selector);
    //     showRollOver(selector);
    // }
    //
    // function showOnMap(el){
    //     let $country_code = $(el.currentTarget).attr('id').toUpperCase();
    //     let $country_block = $(`[data-country=${$country_code}]`);
    //     let $coordinates = {x: $country_block.attr('data-offset-x'), y: $country_block.attr('data-offset-y')};
    //     let $zoom = $country_block.attr('data-zoom');
    //     let $label = $country_block.attr('data-label');
    //     moveMapTo($coordinates, $zoom, $label);
    // }
    //
    // function initMapCountry() {
    //     let $tooltipEl = $('svg [id^=tooltip_]');
    //     $tooltipEl.hide();
    //     $(activeCountriesArray.map(item => `svg #${item}`).join(', '))
    //         .css({fill: "#884fc6"})
    //         .addClass('active-map-state')
    //         .on('mouseenter', showMapTooltip)
    //         .on('mouseout', clearRollover)
    //         .on('click', showOnMap);
    //
    //     $(activeCountriesPartArray.map(item => `svg #${item}`).join(', '))
    //         .css({fill: "#7f01c6"})
    //         .addClass('active-map-state')
    //         .on('mouseenter', showMapTooltip)
    //         .on('mouseout', clearRollover)
    //         .on('click', showOnMap);
    //     $tooltipEl.toArray().forEach(element => {
    //         let tooltipCountry = element.id.substr(8, 2);
    //         $(element).children().not('#placeholder').hide();
    //
    //         if (activeCountriesArray.indexOf(tooltipCountry) !== -1) {
    //             $(element).show();
    //             $(element).find('#placeholder use:last-child').css({fill: '#000000'})
    //         }
    //         if (activeCountriesPartArray.indexOf(tooltipCountry) !== -1) {
    //             $(element).show();
    //             $(element).find('#placeholder use:last-child').css({fill: '#000000'})
    //         }
    //     });
    // };
    //
    // const init = () => {
    //     hoverElement();
    //     initMapCountry();
    //
    //     let zoom_control = $('.current-zoom');
    //
    //     panZoom = svgPanZoom('.select-map__map.global', {
    //         viewportSelector: '#map',
    //         panEnabled: true, // разрещить таскать
    //         zoomEnabled: true,
    //         controlIconsEnabled: false,
    //         minZoom: 1,
    //         maxZoom: 5,
    //         fit: false,
    //         contain: false,
    //         center: false,
    //         eventsListenerElement: document.querySelector('.select-map__map.global #map'),
    //         onPan: function(){
    //             fadeHint();
    //         },
    //         onZoom: function(e){
    //             var cur_zoom = (Math.round(e*100)) + " %";
    //             zoom_control.html(cur_zoom);
    //         },
    //         customEventsHandler: {
    //             init: function(){
    //                 zoom_control.html('100 %');
    //             }
    //         },
    //
    //     });
    //
    //     $('.place').on('mouseenter', function(){
    //         let _initiator = this;
    //         if(_initiator == currentHighlight) return;
    //
    //         let $offset_x = JSON.parse($(this).attr('data-offset-x'));
    //         let $offset_y = JSON.parse($(this).attr('data-offset-y'));
    //
    //         let $tt_text = $(this).attr('data-label');
    //
    //         moveMapTo({x: $offset_x, y: $offset_y}, $(this).attr('data-zoom'), $tt_text);
    //         currentHighlight = _initiator;
    //     });
    //
    //     $('#zoom-in').on('click', function (){
    //         panZoom.zoomBy(1.25);
    //     });
    //     $('#zoom-out').on('click', function (){
    //         panZoom.zoomBy(0.75);
    //     });
    //     $('#get-location').on('click', function (){
    //         panZoom.getPan();
    //     });
    // };

    function initAreas(){
        $('.place').each(function (i, elem) {
            let _code = $(elem).data('country');
            if(_code === 'CS'){
                let _elem = {
                    tooltip: {content: 'Caribbean'},
                    attrs: {
                        fill: $(elem).hasClass('type-partner') ? '#884fc6' : '#b60dc6'
                    },
                    attrsHover: {
                        fill: "#5f378b"
                    }
                };
                $areas['TT'] = _elem;
                $areas['CU'] = _elem;
                $areas['DO'] = _elem;
                $areas['HT'] = _elem;
                $areas['BS'] = _elem;
                $areas['JM'] = _elem;
            }else{
                let _elem = {
                    tooltip: {content: $(elem).data('label')},
                    attrs: {
                        fill: $(elem).hasClass('type-partner') ? '#884fc6' : '#b60dc6'
                    },
                    attrsHover: {
                        fill: "#5f378b"
                    }
                };
                $areas[_code] = _elem;
            }
        }).on('mouseenter', function(){
            let _code = $(this).data('country');
            let _label;
            if(_code === 'CS'){
                _label = 'Caribbean';
                _code = 'DO';
            }else{
                _label = $(this).data('label');
            }
            let _lat = $(this).data('lat');
            let _lng = $(this).data('lng');
            $('.place').removeClass('place-active');
            $(this).addClass('place-active');
            // $_mapa.trigger('zoom', {level: 10, latitude: _lat, longitude: _lng});
            $_mapa.trigger('zoom', {area:_code, areaMargin: 50});
            $('.pri-tooltip span.text').text(_label);
            $tt.fadeIn(400);
        }).on('mouseleave', function(){
            $tt.fadeOut(400);
        });
    }

    function activatePlace(id){
        $('.place').removeClass('place-active');

        let car_isls = ['JM', 'CU', 'DO', 'HT', 'BS', 'TT'];
        if(car_isls.includes(id)){
            $('.place[data-country=CS]').addClass('place-active');
        }else{
            $('.place[data-country='+id+']').addClass('place-active');
        }
    }

    function initMap(){
        $_mapa = $(".mapcontainer").mapael({
            map: {
                // Set the name of the map to display
                name: "world_countries_mercator",
                // width: 496,
                zoom: {
                    animDuration:350,
                    enabled: true,
                    minLevel: 1,
                    step: 1,
                    maxLevel: 20,
                    buttons: {
                        reset: false,
                        in: false,
                        out: false
                    },
                    init: {
                        level: 1
                    }
                },
                defaultArea: {
                    attrs: {
                        fill: "#bcbcbc",
                        stroke: "white"
                    }
                    , attrsHover: {
                        fill: "#bcbcbc",
                        stroke: "white"
                    }
                    , text: {
                        attrs: {
                            fill: "#505444"
                        }
                        , attrsHover: {
                            fill: "#000"
                        }
                    },
                    eventHandlers: {
                        click: function (e, id) {
                            console.log(id);
                            $_mapa.trigger('zoom', {
                                area: id,
                                areaMargin: 50
                            });
                            activatePlace(id);
                        },
                        mouseover: function(e, id) {
                            activatePlace(id);
                        }
                    }
                }
            },
            areas: $areas
        });
        $('.current-zoom').html(100 + " %");

        $_mapa.on('zoom', function(){
            $('.current-zoom').html((100 + (($_mapa.data('mapael').zoomData.zoomLevel + 1) * 10)) + " %");
            // $('.current-zoom').html($_mapa.data('mapael').zoomData.zoomLevel);

        });

        $('#zoom-out').click(function(){
            $_mapa.trigger('zoom', {level: $_mapa.data('mapael').zoomData.zoomLevel -1});
        });
        $('#zoom-in').click(function(){
            $_mapa.trigger('zoom', {level: $_mapa.data('mapael').zoomData.zoomLevel +1});
        });

    }

    const init = () => {
        initAreas();
        initMap();
    };

    return {
        init
    };


}();

export default GlobalChapters;
