import Vue from 'vue';
import VueLightbox from 'vue-lightbox'

Vue.component("Lightbox",VueLightbox);

const PolicyLightbox = function() {
  function initApp() {
		new Vue({
      el: '#policy'
    });
  }

  const init = () => {
    initApp();
  };

  return {
    init
  };

}();

export default PolicyLightbox;