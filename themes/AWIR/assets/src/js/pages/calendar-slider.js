import Sticky from 'sticky-js';

const CalendarSlider = function() {
  function sliderHello() {

		var stickySlider = new Sticky('.js-sticky-slider', {
      'marginTop': 120,
    });

		var sliderTop						= $('.meetings-slider').offset().top,
				eventHeight					= $('.meetings-slider__wrap').outerHeight(),
				eventContentHeight	= $('.meetings-slider__content-wrap').outerHeight(),
				titleHeight					= $('.meetings-slider__title').outerHeight(),
				eventSlider					= $('.meetings-slider'),
				eventSlide					= $('.meetings-slider__wrap'),
				eventDots						= $('.slider-nav__dot'),
				eventPrev						= $('.slider-nav__prev'),
				eventNext						= $('.slider-nav__next'),
				listWidth						=	$('.meetings-list').width(),
				sliderOpenBtn				=	$('.slider-open-info'),
				sheet								=	$('.event-sheet'),
				docTop							= $(document).scrollTop(),
				sheetTop 						= sheet.height();

		// fixed element width
		$('.float-event').css('width', listWidth );
		$(window).resize(function() {
			listWidth	=	$('.meetings-list').width();
			$('.float-event').css('width', listWidth );
		});

		var smallSheet = false;
		var plankTop = Math.max.apply(null, $(".slider-open-info").map(function ()
			{	return $(this).offset().top;
			}).get());

		// scroll
		var	plankVisible = false;
		var	infoOpen = false;
		var infoEnd = Math.max.apply(null, $(".slider-open-info").map(function (){
			return $(this).offset().top;
		}).get());

			$(window).scroll(function() {
			  if ($(this).scrollTop()> $('.s-footer').offset().top - 200)
			  {
				$('.float-event').stop();
				$('.float-event').fadeOut();
			  }
			  else if ($(this).scrollTop() > $('#meeting-slider').offset().top + $('#meeting-slider').height() && infoOpen == false)
			  {
				$('.float-event').stop();
				$('.float-event').fadeIn();
			  }
			});

		$(window).scroll(function() {
				docTop = $(document).scrollTop();
				plankTop = Math.max.apply(null, $(".slider-open-info").map(function ()
						{	return $(this).offset().top;
						}).get());
        if (docTop + 178 >= plankTop && plankVisible == false && infoOpen == false) {
					$('.float-event').show();
					eventSlider.fadeTo(0.1, 0);
					plankVisible = true;
				}
        else if (docTop + 178 < plankTop && plankVisible == true && infoOpen == false){
					$('.float-event').hide();
					eventSlider.fadeTo(0.1, 1);
					plankVisible = false;
        } else if (docTop + 178 < plankTop && plankVisible == true && infoOpen == false){
			$('.float-event').hide();
			eventSlider.fadeTo(0.1, 1);
			plankVisible = false;
		}
	  });

		$('.slider-open-info').on('click', function(){
			$(this).css('opacity', '0');
			$('.full-info').addClass('full-info-active');
			infoOpen = true;
			infoEnd = Math.max.apply(null, $(".slider-open-info").map(function (){
				return $(this).offset().top;
			}).get());
			$('.slider-nav').hide();

		});
		$('.slider-close-info').on('click', function(){
			$('.full-info').removeClass('full-info-active');
			infoOpen = false;
			infoEnd = Math.max.apply(null, $(".slider-open-info").map(function (){
				return $(this).offset().top;
			}).get());
			$('.slider-nav').show();
			var sheetTop = $(".calendar-section").offset().top;
			$('html, body').animate({
        scrollTop: sheetTop - 90
    	}, 600)
		});

		// on plank click
		$('.float-event__link').on('click', function(){
			var sheetTop = $(".calendar-section").offset().top;
			$('html, body').animate({
        scrollTop: sheetTop - 90
    	}, 1000, function(){
				$('.full-info').addClass('full-info-active');
				$('.slider-open-info').css('opacity', '0');
				infoOpen = true;
				plankVisible = false;
			});
		});

		//Update plank title
		var plankTitle = $('.float-event__title'),
				plankImage = $('.float-event__wrap');
		function updatePlank() {
				var eventImage = $('.meetings-slider__slide.active').css('background-image');
				var eventTitle = $('.meetings-slider__slide.active .meetings-slider__title').text();
				plankImage.css('background-image', eventImage );
				plankTitle.text(eventTitle);
		}

		//float-event__link
		var slideIndex = 1;
		showDivs(slideIndex);
		eventPrev.on('click', function(){
			plusDivs(-1);
		});
		eventNext.on('click', function(){
			plusDivs(1);

		});
		 eventDots.on('click', function(){
			var dotData = $(this).data('id');
			currentDiv(dotData);
		});

		function plusDivs(n) {
			showDivs(slideIndex += n);
		}

		function currentDiv(n) {
			showDivs(slideIndex = n);
		}

		function showDivs(n) {
			var i;
			var x = document.getElementsByClassName("meetings-slider__slide");
			var dots = document.getElementsByClassName("slider-nav__dot");
			var info = document.getElementsByClassName("full-info");
			if (n > x.length) {slideIndex = 1}
			if (n < 1) {slideIndex = x.length}
			for (i = 0; i < x.length; i++) {
				 x[i].style.display = "none";
				 x[i].className = x[i].className.replace(" active", "");
			}
			for (i = 0; i < dots.length; i++) {
				 dots[i].className = dots[i].className.replace(" active", "");
			}
			for (i = 0; i < info.length; i++) {
				 info[i].className = info[i].className.replace(" current", "");
			}
			x[slideIndex-1].style.display = "block";
			x[slideIndex-1].className += " active";
			dots[slideIndex-1].className += " active";
			info[slideIndex-1].className += " current";

			plankTop = Math.max.apply(null, $(".slider-open-info").map(function ()
			{	return $(this).offset().top;
			}).get());
			sheetTop = sheet.height();
			updatePlank();
		}
  }

  const init = () => {
	  if ( !$('.meetings-slider').length )
		  return;

    sliderHello();
  };

  return {
    init
  };

}();

export default CalendarSlider;
