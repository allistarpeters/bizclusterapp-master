import Vue from 'vue';
import VueResource from 'vue-resource';
import VueMoment from 'vue-moment';
import _ from 'lodash';

Vue.use(VueResource);
Vue.use(VueMoment);

const DirectMessages = function() {
  function initApp() {
    var app = new Vue({
      el: '#app-direct-messages',
      data: {
        isLoading: false,
        messages: [],
        chat_message: '',
        chat_user_id: '',
        filterString: '',
        new_messages_notification: [],
        chat_contacts: window.chat_contacts,
        chat_dialog_nonce: window.chat_dialog_nonce,
        chat_message_nonce: window.chat_message_nonce,
        chat_current_user_id: window.chat_current_user_id,
        chat_current_user_avatar: window.chat_current_user_avatar,
        chat_ajaxurl: window.chat_ajaxurl,
        chat_home: window.chat_home,
      },
      mounted() {
      },
      computed: {
        userProfileUrl() {
          return `${this.chat_home}/user/?user=${this.chat_user_data.user_login}`;
        },
        filteredUserList() {
          return _.filter(this.chat_contacts, _.method('display_name.match', new RegExp(this.filterString, 'i')))
        },
        sortedMessages:  function () {
          return _.orderBy(this.messages, 'timestamp').reverse();
        },
        chat_user_data() {
          return _.find(this.chat_contacts, { 'user_id': this.chat_user_id })
        },
        chat_display_username() {
          return this.chat_user_data.display_name;
        }
      },
      methods: {
        keymonitor(e) {
          if ((e.metaKey || e.ctrlKey) && e.keyCode == 13) {
            this.onSubmit();
          }
        },
        onSubmit: _.throttle(function() {
          let contactIndex = _.findIndex(this.chat_contacts, (o) => {
            return o.user_id == this.chat_user_id;
          });
          if (contactIndex !== 0) {
            let pushItem = this.chat_contacts[contactIndex]
            this.chat_contacts.splice(contactIndex, 1);
            this.chat_contacts.unshift(pushItem);
          }
          if (this.chat_message) {
            let chatTempDisplay = {
              message: this.chat_message,
              timestamp: Math.floor(Date.now() / 1000),
              author_id: this.chat_current_user_id
            }
            this.messages.push(chatTempDisplay);
            this.$http.post(this.chat_ajaxurl, {
              action: 'awir_add_message',
              to: this.chat_user_id,
              message: this.chat_message,
              _ajax_nonce: this.chat_message_nonce
            }, {
                emulateJSON: true
            }).then(response => {
              // this.messages.push(response.body.data);
            }, response => {
              console.log(response);
            });
            this.chat_message = '';
          }
        }, 1000, {trailing: false}),
        startConversatinon(id) {
          this.chat_user_id = id;
          this.isLoading = true;
          this.$http.post(this.chat_ajaxurl, {
            action: 'awir_get_dialog',
            user_id: id,
            offset: 0,
            count: 100,
            _ajax_nonce: this.chat_dialog_nonce
          }, {
            emulateJSON: true
         }).then(response => {
            this.isLoading = false;
            if (response.body.data.length) {
              this.messages = response.body.data;
              this.getChatMessages();
            } else {
              this.messages = [];
              this.getChatMessages();
            }
          }, response => {
            console.log(response);
          });
        },
        getUnreadMessages() {
          setTimeout(() => {
            this.$http.post(this.chat_ajaxurl, {
              action: 'awir_new_messages',
            }, {
              emulateJSON: true
            }).then(response => {
              this.new_messages_notification = response.body.data;
              this.getUnreadMessages();
            }, response => {
              console.log(response);
            });
          }, 2000)
        },
        getChatMessages() {
          if (this.chat_user_id) {
            setTimeout(() => {
              this.$http.post(this.chat_ajaxurl, {
                action: 'awir_unread_messages',
                user_id: this.chat_user_id,
                _ajax_nonce: this.chat_dialog_nonce
              }, {
                emulateJSON: true
              }).then(response => {
                if (response.body.data.length) {
                  response.body.data.forEach(element => {
                    this.messages.push(element);
                  });
                }
                this.getChatMessages();
              }, response => {
                console.log(response);
              });
            }, 1000)
          }
        },
      },
      mounted() {
        this.getUnreadMessages();
        $(this.$el).children().animate({opacity: 1});
      },
    })
  }


  const init = () => {
    initApp();
  };

  return {
    init
  };

}();

export default DirectMessages;