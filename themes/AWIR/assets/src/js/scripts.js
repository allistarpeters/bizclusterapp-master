// Rollover submenus on desktop
function addActive(idToActivate) {
	document.getElementById(idToActivate).classList.add('active');
}

function removeActive(idToDeactivate) {
	document.getElementById(idToDeactivate).classList.remove('active');
}

var mobileMenuDisplayed = false;
var sideMenuDisplayed = false;

function openMobileMenu(buttonId, menuId) {
	if (mobileMenuDisplayed === false) {
		document.getElementById(buttonId).classList.add('open');
		document.getElementById(menuId).classList.add('open');
		document.getElementsByTagName('BODY')[0].classList.add('mobilemenu-open');
		document.getElementsByTagName('HTML')[0].classList.add('mobilemenu-open');
		mobileMenuDisplayed = true;
	} else {
		document.getElementById(buttonId).classList.remove('open');
		document.getElementById(menuId).classList.remove('open');
		document.getElementsByTagName('BODY')[0].classList.remove('mobilemenu-open');
		document.getElementsByTagName('HTML')[0].classList.remove('mobilemenu-open');
		mobileMenuDisplayed = false;
	}
}

function openSideMenu(buttonId, menuId) {
	if (sideMenuDisplayed === false) {
		document.getElementById(buttonId).classList.add('open');
		document.getElementById(menuId).classList.add('open');
		sideMenuDisplayed = true;
	} else {
		document.getElementById(buttonId).classList.remove('open');
		document.getElementById(menuId).classList.remove('open');
		sideMenuDisplayed = false;
	}
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

$('#more-btn-1').on('click', function(){
    $('#more-content-1').toggleClass('active');
});
$('.meeting-main').on('click', function(){
    $(this).toggleClass('meeting-open');
});
// open info for event archive
$('.meetings-slider__link').on('click', function(){
    $('.full-info').addClass('full-info-active');
});

// benefit open/close
$('.benefit').on('click', function(){
    $(this).toggleClass('benefit-active');
});
//mobile chat simulation
$('#to-messages').on('click', function(){
		$('.messages-area').removeClass('area-hiden');
    $('.messages-area').addClass('area-visible');
		$('.members-area').removeClass('area-visible');
    $('.members-area').addClass('area-hiden');
});
$('#to-contacts').on('click', function(){
		$('.members-area').removeClass('area-hiden');
    $('.members-area').addClass('area-visible');
    $('.messages-area').removeClass('area-visible');
    $('.messages-area').addClass('area-hiden');
});

/* Popups and popup-menu */
$('#sing-in-btn').on('click', function(){
		$('.popup-main').removeClass('active');
    $('#popup-singin').addClass('active');
    console.log('123');
});
$('#sing-in-pdf').on('click', function(){
  $('.popup-main').removeClass('active');
  $('#popup-singin').addClass('active');
  console.log('123');
});
$('#forgot').on('click', function(){
		$('.popup-main').removeClass('active');
    $('#popup-forgot').addClass('active');
});
// if(window.location.href.toString().indexOf("reset") > -1) {
if(findGetParameter('reset') !== null) {
  $('.popup-main').removeClass('active');
  $('#popup-forgot').addClass('active');
}
$('#to-singin1, #to-singin2, #to-singin3, #to-singin4, #to-singin5').on('click', function(){
		$('.popup-main').removeClass('active');
    $('#popup-singin').addClass('active');
});
$('#reg-test').on('click', function(){
    $('#reg-popup-test').addClass('meeting-popup-status success');
});
$('#reg-test-error').on('click', function(){
    $('#reg-popup-test').addClass('meeting-popup-status error');
});
$('#reg-thankyou').on('click', function(){
    $('.register-popup').removeClass('register-popup-active');
		$('#reg-popup-test').removeClass('meeting-popup-status success');
		$('#reg-popup-test').removeClass('meeting-popup-status error');
    $('.popup-main').removeClass('active');
});
$('.close-btn, .close-popup').on('click', function(){
    $('.register-popup').removeClass('register-popup-active');
    $('.popup-main').removeClass('active');
});
$('.meeting__register').on('click', function(){
	if ( $(this).hasClass( 'btn__register-custom_link' ) )
		return true;
    $('.register-popup').toggleClass('register-popup-active');
});
$('#footer-subscribe').on('click', function(){
    $('.popup-footer').toggleClass('active');
});
$('.user-nav__avatar-wrap').on('click', function(){
		$('.user-popup-menu').toggleClass('active');
});
$('#profile-menu').on('click', function(){
		$('#user-menu').toggleClass('active');
});

//Mobile nav
(function($) {
    var $window = $(window),
        $nav = $('.header-nav');
    function resize() {
        if ($window.outerWidth() < 1024) {
            return $nav.addClass('mobile-nav');
        }
        $nav.removeClass('mobile-nav');
    }
    $window
        .resize(resize)
        .trigger('resize');
})(jQuery);

//fotorama
(function (){
		var $fotoramaDiv = jQuery('.fotorama').fotorama();
		var fotorama = $fotoramaDiv.data('fotorama');
		$('#gallery-prev').click(function(){
				fotorama.show('<');
		});
		$('#gallery-next').click(function(){
				fotorama.show('>');
		});
	}
)(jQuery);

// Close on Esc
$(document).keydown(function(e) {
    // ESCAPE key pressed
    if (e.keyCode == 27) {
        closePopups();
				console.log('esc');
    }
});
function closePopups(){
	var beClosed = $('.user-popup-menu, .popup-main');
	beClosed.each(function(){
		$(this).removeClass('active');
	});
}

// Open archive info
(function (){
	$('.meetings-slider-archive').on('click', function(){
		$('.meetings-slider-archive').removeClass('active');
		$(this).addClass('active');
	});
	$('.meetings-slider__link').on('click', function(){
		$('.meetings-slider__link').fadeTo( "fast", 1 );
		$(this).fadeTo( "fast", 0 );
	});
	$('.collapse').on('click', function(){
			$('.full-info').removeClass('full-info-active');
			$('.meetings-slider__link').fadeTo( "fast", 1 );
	});
}
)(jQuery);

// Close on side-click
$(document).click(function (e) {
	if (!$(e.target).hasClass('main-nav__expander') && !$(e.target).hasClass('main-nav__for-touch')) {
		$('.main-nav__second-level').removeClass('active')
		console.log('ready!');
	}
	if (!$(e.target).hasClass('user-nav__avatar-wrap')) {
		$('.user-popup-menu').removeClass('active')
	}
	if (!$(e.target).hasClass('profile-menu__button')) {
		$('.user-menu').removeClass('active');
	}
	if ($(e.target).hasClass('popup-main') && !$(e.target).hasClass('popup-main__window')) {
		$('.popup-main').removeClass('active');
	}
});

window.addEventListener('load', function() {
  var preloaderPane = document.getElementById("preloader-pane");
  setTimeout(function () {
    preloaderPane.classList.add("preloadHide");
    setTimeout(function () {
      preloaderPane.parentElement.removeChild(preloaderPane);
      preloaderPane = null;
    }, 800);
  }, 100);
}); 


(function (){
	var isTouch = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch
	if(isTouch){
		$('.sub-navigation').addClass('isTouch');
	} else {
		$('.sub-navigation').addClass('noTouch');
	}
	var gap = $(window).width() < 768 ? 60 : 20;
	$('a[href^="#"]').click(function(event){
		event.preventDefault();
		try {
			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top - document.querySelector(".s-header").clientHeight - gap
			}, 500);
		} catch (error) {
			
		} 
	});

	var covid_sections = $(".covid-section"); 
	var itemId = '';
	var tolerance = window.innerHeight * 0.5;
	updateActiveSubnav();
	$(window).scroll(function () {
		updateActiveSubnav();
	});
	$(window).resize(function () {
		updateActiveSubnav();
	});
	function updateActiveSubnav(){
		for(var i=0; i < covid_sections.length; i++){
			if ($(covid_sections[i]).is(':in-viewport(' + tolerance + ')')) {
				itemId =  $.attr(covid_sections[i], 'id'); 
				$('#sub-navigation-' + i).addClass('active');
			} else {
				itemId = $.attr(covid_sections[i], 'id')
				$('#sub-navigation-' + i).removeClass('active');
			}
		}
	}
}
)(jQuery);