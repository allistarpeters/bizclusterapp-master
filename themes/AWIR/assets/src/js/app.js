import localChapters from './pages/local-chapters';
import GlobalChapters from './pages/global-chapters';
import CalendarSlider from './pages/calendar-slider';
import CalendarEvents from './pages/calendar-events';
import DirectMessages from './pages/direct-messages';
import PolicyLightbox from './pages/policy-lightboxes';

if (window.currentPage === 'local-chapters') {
	localChapters.init();
}

if (window.currentPage === 'global-chapters') {
    GlobalChapters.init();
}
if (window.currentPage === 'events') {
	CalendarSlider.init();
  CalendarEvents.init();
}
if (window.currentPage === 'messages') {
	DirectMessages.init();
}
if (window.currentPage === 'policy') {
  PolicyLightbox.init()
}

$('#to-messages, .resent-contact').on('click', function(){
  if ( $(window).width() < 577 ) {
    $('.messages-area_wrap').addClass('area-visible');
    $('.messages-area_wrap').removeClass('area-hiden');
    $('.members-area').animate({
      left: "-=100%",
    }, 200, function() {
      $('.members-area').hide();
    });
  }
});

$('#to-contacts').on('click', function(){
  $('.messages-area_wrap').removeClass('area-visible');
  $('.messages-area_wrap').addClass('area-hiden');
  $('.members-area').show();
  $('.members-area').animate({
    left: "+=100%",
  }, 200, function() {

  });
});

$('.close-btn').on('click', function(){
  $('.subscribe__form')[0].reset();
  $('.mc4wp-form')[0].reset();
});
$(document).click(function (e) {
	if ($(e.target).hasClass('popup-main') && !$(e.target).hasClass('popup-main__window')) {
		$('.mc4wp-form')[0].reset();
	}
});


$('video').attr('controlsList', 'nodownload');

if (!$(e.target).hasClass('main-nav__expander') && !$(e.target).hasClass('main-nav__for-touch')) {
  $('.main-nav__second-level').removeClass('active');
  console.log('ready!');
}
