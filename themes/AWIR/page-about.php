<?php

get_header();

the_post();

?>

<!-- Pages Hero Section -->
<div class="about-awir">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<!-- About page content-->

<div class="pages-headline">
  <a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>


<section class="sheet-section">

	<div class="pages-hexset-wrap-middle">
		<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/about-hex/about-hex5.png" alt="" class="pages-set3">
	</div>

	<div class="pages-hexset-wrap-bottom">
		<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/about-hex/about-hex6.svg" alt="" class="pages-set4">
	</div>

	<div class="sheet-container">

		<div class="sheet">
			<?php if ( get_field('top_video_title') ): ?>
			<h2 class="title-text sheet__title-main"><?php the_field('top_video_title') ?></h2>
			<?php endif ?>

			<div class="pages-video">

				<?php if ( get_field('top_video') ): ?>
				<!-- Responsive iFrame -->
				<div class="flexible-container who-look__placeholder">
					<?php the_field('top_video') ?>
				</div>
				<?php elseif ( ($img_id = get_field('fallback_image')) && ($image = awir_theme::image_url($img_id, 'video_fallback')) ): ?>
				<img src="<?= $image ?>" alt="" class="pages-video__placeholder">
				<?php endif ?>
			</div>

			<?php if ( get_field('top_section_title') || get_field('top_section_text') ): ?>
			<div class="accented-info">

				<img src="<?= get_template_directory_uri() ?>/assets/img/home/who-bg-back.svg" alt="" class="accented-info__bg-bott">
				<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/about-hex/about-hex3.svg" alt="" class="about-hexset1">
				<img src="<?= get_template_directory_uri() ?>/assets/img/home/who-bg-mid.svg" alt="" class="accented-info__bg-mid">
				<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/about-awir-top1.png" alt="" class="accented-info__bg-top">

				<img src="" alt="" class="accented-info__bd2">
				<img src="" alt="" class="accented-info__bd3">

				<div class="accented-info__content">
					<h2 class="title-text title-text-white title-underline title-underline-left accented-info__title"><?php the_field('top_section_title') ?></h2>
					<p class="main-text main-text-white accented-info__text"><?php the_field('top_section_text') ?></p>
				</div>
			</div>
			<?php endif ?>

			<?php get_template_part('inc/board-members') ?>

			<h2 class="title-text sheet__title no-margin">Gallery</h2>
			<?= do_shortcode("[photonic type='instagram' user_id='1698706952' media='all']") ?>

			<?php /*

				<div class="about-gallery">
					<div class="fotorama" data-width="800" data-ratio="800/360" data-nav="thumbs" data-thumbmargin="15" data-thumbwidth="90" data-thumbheight="80" data-fit="contain" data-navwidth="90%" data-arrows="false" data-click="true" data-swipe="true">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-1.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-2.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-3.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-4.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-5.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-6.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-2.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-5.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-6.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-3.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-4.jpg">
						<img src="<?= get_template_directory_uri() ?>/assets/img/about-awir/gallery/gallery-slide-1.jpg">

					</div>
					<div id="gallery-prev" class="about-gallery__nav about-gallery__leftArrow"></div>
					<div id="gallery-next" class="about-gallery__nav about-gallery__rightArrow"></div>
				</div>
			*/ ?>
		</div>
	</div>
</section>

	<!-- About page content-->

<?php get_footer();
