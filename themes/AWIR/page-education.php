<?php

get_header();

the_post();
?>

  <!-- Pages Hero Section -->
  <div class="education-header">
    <?php get_template_part( 'inc/pages-header' ) ?>
  </div>
  <!-- End Pages Hero Section -->

  <div class="pages-headline pages-headline__alone">
    <!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
    <h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
  </div>

  <section class="sheet-section education-section">

    <div class="pages-hexset-wrap-bottom callendar-hexset">
      <img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
      <img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set8">
    </div>

    <div class="sheet-container advocacy-page">
      <div class="sheet sheet-nopadding">
        <div class="sheet-inner sheet-inner-top clearfix">
          <div class="sheet-text-wrap no-margin">
            <?php the_content() ?>
          </div>
	<!-- New COVID start -->
          <a class="covid-banner-3" href="<?= get_permalink( get_page_by_path( 'covid19' ) )?>">
          <img src="<?= get_template_directory_uri() ?>/assets/img/education/covid_bg-2.png" alt=""></a>
	<!-- New COVID end -->

          <h2>Bringing you the best of the AWIR 2019 Immunology Bootcamp</h2>

          <!--    NEW BANNERS      -->
          <div class="three-banners ">
            <div>
              <h4>Best of the Meeting Webcast<br>CME=2.75</h4>
              <a target="_blank" class="single-banner"
                    href="https://cme.healio.com/rheumatology/education-lab/2019/10_october/association-of-women-in-rheumatology-national-conference/cme-information">
                <img src="<?= get_template_directory_uri() ?>/assets/img/education/best_of_webcast.png" alt="" class="">
              </a>
            </div>

            <div>
              <h4>Highlights of the Meeting eMonograph<br>CME=1.50</h4>
              <a target="_blank" class="single-banner"
                     href="https://cme.healio.com/rheumatology/20191121/highlights-of-the-3rd-annual-association-of-women-in-rheumatology-national-conference-immunology-bootcamp">
                <img src="<?= get_template_directory_uri() ?>/assets/img/education/highlights_from_emonograph.png"
                     alt="" class="">
              </a>
            </div>
            <div>
              <h4>Precision Decision: Realistic Cases<br>CME=0.25</h4>
              <a target="_blank" class="single-banner"
                    href="https://cme.healio.com/rheumcme/immunology-and-biologics/2019/10_october/awir-national-conference-immunology-bootcamp-precision-decision-realistic-cases">
                <img
                  src="<?= get_template_directory_uri() ?>/assets/img/education/precision_decision_realistic_case.png"
                  alt="" class="">
              </a>
            </div>
          </div>




          <a href="https://cme.healio.com/rheumcme/immunology-bootcamp"
             class="single-banner training-banner " target="_blank" data-wpel-link="internal">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_0.png" alt=""
                 class="single-banner">
          </a>


          <div class="two-resourses">
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-1-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_1.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-2-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_2.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-3-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_3.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-4-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_4.png" alt=""
                   class="two-resourses__img">
            </a>
          </div>
          <div class="two-resourses">
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-5-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_5.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-6-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_6.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-7-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_7.png" alt=""
                   class="two-resourses__img">
            </a>
            <a
              href="https://pro-c.me/courses/index.html?collection=180200447&presentation=180200447-8-p1#main"
              class="two-resourses__item" target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_8.png" alt=""
                   class="two-resourses__img">
            </a>
          </div>

          <!--    / NEW BANNERS      -->

          <a href="https://www.rheumahighlights.com/" target="_blank" class="single-banner rheumahighlight-banner">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/rheumahighlights_banner_new.png" alt=""
                 class="single-banner single-banner--mobile">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/rheumahighlights_banner_desktop.png" alt=""
                 class="single-banner single-banner--desktop">
          </a>



          <div class="rheum-resourse">
            <a href="http://www.rheumcme.com" class="single-banner " target="_blank" data-wpel-link="internal">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/bootcamp/ib-part-4.1.png" alt=""
                   class="single-banner--desktop">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/bootcamp/ib-part-4.png" alt=""
                   class="single-banner--mobile">
            </a>
          </div>
          <a href="https://awirgroup.org/advocacy_training/" class="single-banner training-banner" target="_blank"
             data-wpel-link="internal">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/banner_training.png" alt=""
                 class="single-banner--desktop">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/mobile_banner_training.png" alt=""
                 class="single-banner--mobile">
          </a>

        </div>


        <div class="sheet-inner sheet-inner-middle clearfix">
          <?php if ( get_field( 'educational_title' ) ): ?>
            <div class="sheet-text-wrap no-margin margin-t-large">
              <h1><?php the_field( 'educational_title' ) ?></h1>
            </div>
          <?php endif ?>

          <!-- must be added to the admin panel -->
          <div class="home-lable-wrap">
            <div class="home-lable education-lable">
              <h3 style="text-align: center;">THE ROAD TO RHEUMATOLOGY:</h3>
              <p>A Quick-Start Guide for Non-Physician Providers</p>
              <h1 style="text-align: center;">
                <?php if ( is_user_logged_in() ): ?>
                  <a href="/board/quickstart-pa-np-handbook/" data-wpel-link="internal">READ</a>
                <?php else: ?>
                  <a href="javascript:void(0);" id="sing-in-pdf" data-wpel-link="internal">Log In</a>
                <?php endif ?>
              </h1>
            </div>
          </div>
          <!-- \\ -->

          <?php $classes = [ 'rheumatoid', 'psoriatic', 'ankylosing', 'crohns' ] ?>
          <?php for ( $i = 0; have_rows( 'educational_tools' ); $i ++ ): the_row() ?>

            <?php if ( $i % 2 == 0 ): ?>
              <div class="educational-tools-wrap">
            <?php endif ?>
            <a class="educational-tools" href="<?php the_sub_field( 'url' ) ?>" target="_blank">
              <div class="e-tool e-tool-<?= $classes[ $i % 4 ] ?>">
                <p class="e-tool__title"><?php the_sub_field( 'title' ) ?></p>
                <p class="e-tool__text"><?php the_sub_field( 'description' ) ?></p>
              </div>
            </a>

            <?php if ( $i % 2 ) : ?>
              </div>
            <?php endif ?>

          <?php endfor ?>

          <!-- must be added to the admin panel -->
          <a class="arthritis-tool" href="https://www.ra-redefined.com/" target="_blank" data-wpel-link="external">
            <img src="<?= get_template_directory_uri() ?>/assets/img/education/arthritis-banner.jpeg" alt="">
          </a>
          <!-- \\ -->

          <?php if ( $i % 2 ) : ?>
        </div>
        <?php endif ?>

        <?php if ( get_field( 'banners_title' ) ) : ?>
          <div class="sheet-text-wrap additional-tool-title">
            <h2><?php the_field( 'banners_title' ) ?></h2>
          </div>
        <?php endif ?>

        <?php if ( have_rows( 'three_in_row_banners' ) ): ?>
          <div class="three-resourses">
            <?php while ( have_rows( 'three_in_row_banners' ) ): the_row() ?>
              <?php $url = get_sub_field( 'file' ) ?: get_sub_field( 'url' ) ?>
              <a href="<?= $url ?>" class="three-resourses__item" target="_blank">
                <img src="<?php the_sub_field( 'image' ) ?>" alt="" class="three-resourses__img">
              </a>
            <?php endwhile ?>
          </div>
        <?php endif ?>

        <!-- must be added to the admin panel -->
        <a class="arthritis-tool" href="http://lupuspregnancy.org/" target="_blank" data-wpel-link="external">
          <img src="<?= get_template_directory_uri() ?>/assets/img/education/conversation-banner.jpeg" alt="">
        </a>
        <!-- \\ -->

        <?php if ( get_field( 'more_link_text' ) ): ?>
          <a href="<?php the_field( 'more_link_url' ) ?>" class="educational-more-btn"
             target="_blank"><?php the_field( 'more_link_text' ) ?>
          </a>
        <?php endif ?>

        <?php if ( have_rows( 'two_in_row_banners' ) ): ?>
          <div class="educational-tools-wrap">
            <?php while ( have_rows( 'two_in_row_banners' ) ): the_row() ?>

              <div class="educational-tools">
                <?php if ( get_sub_field( 'title' ) ): ?>
                  <h3 class="educational-tools__small-title"><?php the_sub_field( 'title' ) ?></h3>
                <?php endif ?>
                <a class="additional-tool additional-tool--left" href="<?php the_sub_field( 'url' ) ?>"
                   style="background-image:url('<?php the_sub_field( 'image' ) ?>');" target="_blank">
                </a>
              </div>

            <?php endwhile ?>

          </div>
        <?php endif ?>
      </div>

      <div class="sheet-inner sheet-inner-bottom clearfix">
        <!-- must be added to the admin panel -->
        <div class="sheet-text-wrap no-margin margin-t-large">
          <h1>Educational Tools for Patients</h1>
        </div>
        <div class="educational-tools-wrap wide-wrap">
          <div class="educational-tools">
            <a class="" href="https://www.aimotherhood.com/" target="_blank" data-wpel-link="external">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/motherhood.jpeg" alt="">
            </a>
          </div>
          <div class="educational-tools">
            <a class="" href="https://www.usinlupus.com/" target="_blank" data-wpel-link="external">
              <img src="<?= get_template_directory_uri() ?>/assets/img/education/usinlupus.jpeg" alt="">
            </a>
          </div>
        </div>
        <!-- \\ -->
      </div>
    </div>
    </div>
  </section>


<?php get_footer();
