<div class="<?= is_front_page() ? 'home-footer' : 'pages-footer' ?>">
	<?php get_template_part('inc/footer') ?>
</div>

<script>
	window.ajaxurl = <?= json_encode(admin_url('admin-ajax.php')) ?>
</script>
<?php wp_footer() ?>

</body>

</html>