<?php

get_header();

the_post();
?>

<!-- Pages Hero Section -->
<div class="newsletter-header">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<div class="pages-headline">
	<a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

<section class="sheet-section">

	<div class="pages-hexset-wrap-bottom callendar-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set8">
	</div>

	<div class="sheet-container newsletter-page">
		<div class="sheet">

			<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_field('mailchimp_block_title') ?></h2>

			<div class="contacts">
					<div class="contacts-form-side">

						<div class="subscribe__form message-form">
							<?= do_shortcode(sprintf('[mc4wp_form id="%d"]', get_field('maichimp_form'))) ?>
						</div>

					</div><!--// contacts-form-side-->

					<div class="contacts-data">
							<div class="previous__buttons">
								<p class="previous__title"><?php the_field('previous_newsletters_title') ?></p>
								<?php
									$campaigns = awir_mailchimp::get_campaigns();

									$i = 0;
									foreach ( $campaigns as $campaign )
									{
										$i++;
								?>
								<a href="<?=esc_url($campaign['url'])?>" class="btn btn__main btn__main-previous" target="_blank"><?= esc_html( $campaign['title'] ) ?></a>
								<?php
										if ( $i >= 4 )
											break;
									} ?>

								<a href="<?= awir::user_page_url('newsletter/archive') ?>" class="previous__cta"><?php the_field('archive_link_title') ?></a>
							</div>
					</div><!--// contacts-data -->
				</div>
		</div>
	</div>
</section>


<?php get_footer();
