<?php

get_header();

?>

	<section class="sheet-section user-sheet user-sheet-blog">

		<div class="membership-hexset-top">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5-copy.png" alt="" class="hexset-middle">
		</div>
		<div class="membership-hexset-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/meeting/meeting-hex.png" alt="" class="hexset-middle">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container message-board">
			<div class="sheet">

				<div id="side-menu" class="blogs-sidebar">
					<div class="btn-menu btn-menu-sidebar is-hidden-medium" id="toggle-side-btn" onclick="openSideMenu('toggle-side-btn','side-menu');">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
					<ul class="blogs-sidebar__list">
						<li class="blogs-sidebar__link<?php if ( is_home() ) echo ' active'?>"><a href="<?= get_post_type_archive_link('post') ?>">All</a></li>
						<?php foreach ( get_terms(['taxonomy' => 'category','hide_empty' => false]) as $term ): ?>
						<li class="blogs-sidebar__link<?php if ( is_category( $term->slug ) ) echo ' active'?>"><a href="<?=get_category_link($term)?>"><?= esc_html( $term->name ) ?></a></li>
						<?php endforeach ?>
					</ul>

				</div>

				<div class="blogs-wrap">
					<div class="blogs-content">

            <?php while ( have_posts() ): the_post() ?>

            <?php if ( !awir::is_member() ): ?>
              <div class="blogs-content__item <?php if ( get_field('pdf_protected') ):?> item-protected<?php endif ?>" data-id="<?php the_ID() ?>">
            <?php 	else: ?>
              <div class="blogs-content__item" data-id="<?php the_ID() ?>">
            <?php endif ?>

							<?php get_template_part('inc/list-item') ?>

							<?php if ( ($terms = get_the_terms( get_the_ID(), 'category' )) && !is_wp_error( $terms ) ): ?>
							<div class="user-info user-info-interest">
								<?php foreach ( $terms as $term ) : ?>
								<div class="user-info__item user-info__item-interest">
									<a href="<?= get_category_link( $term ) ?>" class="user-info__tag"><?= esc_html( $term->name ) ?></a>
								</div>
								<?php endforeach ?>
							</div>
							<?php endif ?>

						</div>
						<?php endwhile ?>

						<?php get_template_part('inc/pagination') ?>

					</div>
				</div>

			</div>
		</div>
		</section>

		<script>window.awir_views_nonce = <?=json_encode(wp_create_nonce( 'pvc-check-post' ))?>;</script>

<?php get_footer();
