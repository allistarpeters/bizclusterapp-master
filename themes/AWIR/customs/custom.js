'use strict'

jQuery(function($) {

	var popup = $('.register-popup');

	$('.meeting__register').on('click', function(e)
	{
		if ( $(this).hasClass( 'btn__register-custom_link' ) )
			return true;

		e.preventDefault();
		var data = $(this).data('event_info');

		popup
			.find('.meeting__number').text( data.day ).end()
			.find('.meeting__month').text( data.month ).end()
			.find('.meeting__title').text( data.title ).end()
			.find('.meeting__time').text( data.time ).end()
			.find('.meeting__place').text( data.place ).end()
			.find('#rtec_event_id').val( data.id ).end()
			.find('.meeting-popup').removeClass( 'meeting-popup-status success error' );
		//.addClass('register-popup-active');

	});

	$('.subscribe__form', popup).submit( function(e)
	{
		var data = $(this).serialize();
		$.post( ajaxurl, data )
			.done( function( data )
			{
				if ( data == 'success' )
					$('.meeting-popup', popup).addClass( 'meeting-popup-status success' );
				else
					fail();
			} )
			.fail( fail )

		function fail(){
			popup.find('.meeting-popup').addClass('meeting-popup-status error');
		}

		e.preventDefault();
	} );

	$('#reg-finish', popup).click( function(){
		popup.removeClass('active register-popup-active');
	} );

	$('#reg-reset', popup).click( function(){
		popup.find('.meeting-popup').removeClass( 'meeting-popup-status success error' );
	} );

	$('#footer-subscribe').on('click', function(){
		$('.popup-main').find('input[name=EMAIL]').val( $('.footer-subscribe input[type=email]').val() );
	});

	$('.member__more').on('click', function(){
		$(this).siblings('.member__more-content').toggleClass('active');
	});

	$('.ajax-avatar-upload input[type=file]').change( function() {

	} );

	$('.membership-plans .membership-plans__btn').click( function(e){
		$(this).siblings('form').submit();
		e.preventDefault();
  });

  var toPdf = false;
  $('#sing-in-pdf').on('click', function(){
    toPdf = true;
	});

	$('#popup-singin form').submit( function(e){
    e.preventDefault();

		var data = $(this).serialize();
    data += '&action=awir_login';

    var url = window.awir_login_url || '/board/';
    if( toPdf == true ){
      var url = '/board/quickstart-pa-np-handbook/';
    }

		awir_ajax( data, function(){
			window.location.assign( url );
		}, function( error ) {
			$('#popup-singin')
				.addClass('. popup-main-error')
				.find('.popup-main__text').remove().end()
				.find('.popup-main__title').text('Login Error')
					.after( '<p class="popup-main__text">Please try again</p>' )
					.after( $('<p class="popup-main__text popup-main__text-error">').html(error) );

		} );
	} );

	$('#popup-forgot form').submit( function(e){
		e.preventDefault();

		var data = $(this).serialize() + '&action=awir_forgot_password';

		awir_ajax( data, function(){
			$('#popup-forgot').removeClass('active');
			$('#popup-success').addClass('active');
		}, function( error ) {
			$('#popup-forgot')
				.addClass('. popup-main-error')
				.find('.popup-main__text').remove().end()
				.find('.popup-main__title')
					.text('Login Error')
					.after( '<p class="popup-main__text">Please try again</p>' )
					.after( $('<p class="popup-main__text popup-main__text-error">').html(error) );
		} );

	});

	var reply_form = $('#add-comment-reply');
	if ( reply_form.length > 0 )
	{
		$('.comment__reply').click( function(e) {
			e.preventDefault();

			var comment = $(this).closest('.comment');
			if ( comment.find('#add-comment-reply').length )
				return;

			reply_form
				.hide()
				.find('[name=parent]').val( comment.data('id') ).end()
				.appendTo( comment )
				.slideDown();
		} );
	}

	$('.inline-upload-avatar input[type=file]').change( function(e){
		if ( !this.files )
			exit;

		var input = this;

		var box = $(this).closest('.inline-upload-avatar');

		box.find('.main-text').text('Uploading...');

		var file = input.files[0];
		var formData = new FormData();

		formData.append('action', 'awir_upload_avatar');
		formData.append('image', file);

		awir_ajax( formData, function( data ){
			box
				.find('input[type=hidden]').val( data.id ).end()
				.find('img').attr( 'src', data.url ).end()
				.find('.main-text').text('Add photo');
		}, function( error ){
			alert( error );
		}, {
			processData: false,
			contentType: false
		} )
		.always( function(){
			box.find('.main-text').text('Add photo');

			input.value = ''

			if ( !/safari/i.test(navigator.userAgent) )
			{
				input.type = ''
				input.type = 'file'
			}
		} );

	} );

	if ( window.awir_views_nonce )
	$('.blogs-content__item[data-id]').on('click', '.pdfemb-next', function(e){
		$.post( ajaxurl, {
			action: 'pvc-check-post',
			pvc_nonce: window.awir_views_nonce,
			id: $( e.delegateTarget ).data('id')
		});
	});

	function awir_ajax( data, success, fail, custom_options )
	{
		var options = {
			url: ajaxurl,
			method: 'POST',
			data: data
		};
		if ( typeof custom_options == typeof {} )
			$.extend( options, custom_options )

		return $.ajax( options )
		.done( function( resp ){
			if ( resp.success )
				success( resp.data );

			else
			{
				var error = resp.message;
				if ( typeof error != typeof '' || !error.length )
					error = 'Connection error. Try again later';

				fail( error );
			}
		})
		.fail( function() {
			fail( 'Connection error. Try again later' );
		} );
	}
} );
