<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="advocacy-toolkit-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
		<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

<section class="sheet-section advocacy-page advocacy-toolkit">

	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
	</div>
	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
	</div>

	<div class="sheet-container">
		<div class="sheet toolkit-sheet">

			<div class="advocacy sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php get_template_part('inc/downloads') ?>

		</div>
	</div>
</section>

<?php get_footer();
