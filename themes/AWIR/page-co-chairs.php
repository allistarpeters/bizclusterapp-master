<?php

get_header();

the_post();
?>

<!-- Pages Hero Section -->
<div class="advocacy-policy-header">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<div class="pages-headline">
	<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

		<section class="sheet-section">

		<div class="pages-hexset-wrap-bottom clinicals-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/clinical-trials/clinical_trial_4.png" alt="" class="clinicals-bott-hex">
		</div>


		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

	<div class="sheet-container co-chairs-page">
		<div class="sheet">

			<?php get_template_part('inc/contact-form') ?>

			<div class="membership-text-wrap sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php if ( get_field('team_title') ): ?>
			<div class="sheet-text-wrap">
				<h1><?php the_field('team_title') ?></h1>
			</div>
			<?php endif ?>

			<?php if ( have_rows('team_members') ): ?>
			<div class="hpad-team">
				<?php while ( have_rows('team_members') ): the_row() ?>
				<div class="hpad-team__item">
					<div class="hpad-team__wrap">
						<p class="hpad-team__name"><?php the_sub_field('name') ?></p>
						<p class="hpad-team__title"><?php the_sub_field('title') ?></p>
						<a href="mailto:<?php the_sub_field('email') ?>" class="hpad-team__link"><?php the_sub_field('email') ?></a>
					</div>
				</div>
				<?php endwhile ?>
			</div>
			<?php endif ?>

		</div>
	</div>
</section>




<?php get_footer();
