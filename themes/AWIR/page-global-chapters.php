<?php

wp_register_script('raphael', '//cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js', ['jquery']);
wp_enqueue_script('jquery-mapael', get_stylesheet_directory_uri() . '/assets/js/jquery.mapael.js', ['jquery', 'raphael']);
wp_enqueue_script('jquery-mousewheel', '//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js', ['jquery']);
wp_enqueue_script('world-map', get_stylesheet_directory_uri() . '/assets/js/world_countries_mercator.js', ['jquery', 'raphael', 'jquery-mapael']);

get_header();

the_post();

?>

<!-- Pages Hero Section -->
<div class="global-chapters">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<!-- About page content-->

<div class="pages-headline">
  <a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>


	<section class="sheet-section chapters">

		<div class="pages-hexset-wrap-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-middle.png" alt="" class="pages-set3 pages-set3-chapters">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container">
			<div class="sheet">

				<?php get_template_part('inc/contact-form' ) ?>

				<div class="about-text-wrap sheet-text-wrap no-limit">
					<?php the_content() ?>
				</div>

				<?php get_template_part('inc/global-leaders') ?>
			</div>
		</div>
	</section>


	<!-- About page content-->

<?php get_footer();
