<div class="event-content" style="border:solid black 1px;">
	<h4>Spoiler content:</h4>
	<div class="post-content">
		<?php the_content() ?>
		<p>
			<strong>Location:</strong>
			<?= tribe_get_full_address() ?>
		</p>
	</div>
	<?php if ( have_rows('sponsors') ): ?>
	<div class="event-sponsors">

		<?php if ( get_field('sponsors_title') ) : ?>
		<h4><?php the_field('title') ?></h4>
		<?php endif ?>

		<?php while ( have_rows('sponsors') ): the_row() ?>
		<div class="sponsor">
			<?php $type = get_sub_field('type') ?>
			<h5><?= $type == 'Other' ? get_sub_field('other_type') : $type ?></h5>
			<?php the_sub_field( 'content' ) ?>
		</div>
		<?php endwhile ?>

	</div>
	<?php endif ?>
	<?= do_shortcode( sprintf('[rtec-registration-form event=%d]', get_the_ID() ) ) ?>
</div>