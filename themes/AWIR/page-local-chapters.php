<?php

get_header();

the_post();

?>

<!-- Pages Hero Section -->
<div class="local-chapters">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<!-- About page content-->

<div class="pages-headline">
  <a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>


	<section class="sheet-section chapters">

		<div class="pages-hexset-wrap-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-middle.png" alt="" class="pages-set3 pages-set3-chapters">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container">
			<div class="sheet">

				<?php get_template_part('inc/contact-form' ) ?>

				<div class="about-text-wrap sheet-text-wrap">
					<?php the_content() ?>
				</div>

				<?php get_template_part('inc/leaders') ?>

			</div>
		</div>
	</section>


	<!-- About page content-->

<?php get_footer();
