<?php


$login = stripslashes( $_REQUEST['login'] );
$key = stripslashes( $_REQUEST['key'] );

$user = get_user_by('login', $login );
if ( $user && !is_wp_error( $user ) && get_user_meta( $user->ID, 'initial_reset_password', true ) )
	add_filter( 'password_reset_expiration', YEAR_IN_SECONDS );

$user = check_password_reset_key( $key, $login );

if ( !$user || is_wp_error( $user ) )
{
	//$error = $user->get_error_message();
	$error = 'Sorry, this link is not active!';
	$user = false;
}
else
{
	if ( isset($_POST['pass1']) && isset($_POST['pass2']) )
	{
		if ( $_POST['pass1'] !== $_POST['pass2'] ){
			$error = 'The passwords do not match.';
		}elseif ( !($pass = stripslashes($_POST['pass1'])) ){
			$error = 'The password is empty';
		}elseif ( strlen( $_POST['pass1'] ) < 8 ){
			$error = 'Please use at least 8 symbols.';
		}else {
			reset_password( $user, $pass );
			wp_set_current_user( $user->ID );
			wp_set_auth_cookie( $user->ID );
			do_action( 'wp_login', $user->user_login );

			global $wpdb;
			$wpdb->update( $wpdb->users, array( 'user_activation_key' => '' ), array( 'ID' => $user->ID ) );
			delete_user_meta($user->ID, 'initial_reset_password');

			awir::user_page_redirect( 'user', [ 'restored' => 1 ] );
		}
	}
}

get_header();

the_post();
?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>

	<!-- End Pages Hero Section -->

	<div class="pages-headline exhibitors">
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<!-- page content-->

	<section class="sheet-section exhibitors-section">

		<div class="pages-hexset-wrap-bottom exhibitors-hexset">
			<img src="<?= get_template_directory_uri() ?>assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
		</div>
		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
		</div>

		<div class="sheet-container reset-password">
			<div class="sheet">

				<div class="new-password<?php if ( !$user ) echo ' new-password-error' ?>">
					<?php if ( $error ): ?>
					<h3 class="new-password__title new-password__title--error"><?= $error ?></h3>
					<?php endif ?>
					<?php if ( $user ): ?>
					<h3 class="new-password__title">Please create new password to finish signing in.</h3>
					<form action="<?php the_permalink() ?>" class="subscribe__form message-form" method="post">
						<input type="hidden" name="key" value="<?=esc_attr($key)?>" />
						<input type="hidden" name="login" value="<?=esc_attr($login)?>" />
						<div class="subscribe__input-wrap">
							<input name="pass1" type="password" placeholder="New password" required>
						</div>
						<div class="subscribe__input-wrap">
							<input name="pass2" type="password" placeholder="Retype password" required>
						</div>
						<button type="submit" class="btn btn__main"><span class="btn__text">Change Password</span></button>
					</form>
					<?php endif ?>
					<a href="javascript:void(0);" id="to-singin5" class="new-password__back">Back to sign in</a>
				</div>
			</div><!--End Sheet -->
		</div>
	</section>

<?php get_footer();
