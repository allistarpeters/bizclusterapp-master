<?php

get_header();

the_post();

?>

  <!-- Pages Hero Section -->
  <!-- <div class="education-header">
    <?php get_template_part( 'inc/pages-header' ) ?>
  </div> -->
  <!-- End Pages Hero Section -->

    <!-- sub-navigation -->
    <?php $sub_naws = array(
    'Telehealth Now Available by CMS for <br>Medicare Patients',
    '<nobr>COVID-19</nobr> Global Rheumatology Alliance — New <br>International Case Reporting Registry',
    'CMS 1135 Waivers to Ease Administrative <br>Burden on Providers for Medicaid Patients',
    'Global Healthy Living Foundation and <br>Creaky Joints Launch <nobr>COVID-19</nobr> Patient <br>and Family Support Program',
    'Trials Underway to Evaluate Treatment <br>and Prevention of <nobr>COVID-19</nobr>',
    'Hydroxychloroquine and Chloroquine Shortages <br>and Usage for <nobr>COVID-19</nobr> Treatment',
    'AWIR Letter to Insurers, Vendors and <br>Manufacturers Advocating for Provider Relief',
    'Other Resources'
  ) ?>
    <div class="sub-navigation">
      <div class="sub-navigation__wrapper">
        <?php for ( $i = 0; $i < 8; $i ++ ): the_row() ?>
          <a href="#covid-section-<?php echo $i ?>" id="sub-navigation-<?php echo $i ?>" class="sub-navigation__item">
            <div class="sub-navigation__item-inner"></div>
            <div class="sub-navigation__tooltip">
              <?php  echo $sub_naws[$i] ?>
            </div>
          </a>
        <?php endfor ?>
      </div>
    </div>
    <!-- END sub-navigation -->

  <div class="pages-headline pages-headline__alone covid">
    <!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
    <h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
    <p class="page-subtitle">Please check back regularly for updates for<br> new developments, events, and resources</p>

  </div>
<div class="vectot">
  <img src="<?= get_template_directory_uri() ?>/assets/img/covid/vector-1.svg" alt="" class="vector-1">
  <img src="<?= get_template_directory_uri() ?>/assets/img/covid/vector-2.svg" alt="" class="vector-2">
  <img src="<?= get_template_directory_uri() ?>/assets/img/covid/vector-3.svg" alt="" class="vector-3">
  <img src="<?= get_template_directory_uri() ?>/assets/img/covid/vector-4.svg" alt="" class="vector-4">
  <img src="<?= get_template_directory_uri() ?>/assets/img/covid/vector-5.svg" alt="" class="vector-5">
</div>


  <section class="sheet-section education-section covid">

    <div class="sheet-container">
      <div class="section_block covid-section" id="covid-section-0">
        <div class="section_block-col-1">
          <h3 class="section_title">Telehealth Now Available by CMS for Medicare Patients</h3>
          <ul class="list">
            <li class="list_item">Provide treatment to your patients outside of offices and/or healthcare facilities and be reimbursed</li>
            <li class="list_item">Use publicly available technology such as Facetime or Skype with the new HIPPA governance waiving the need for HIPAA secured technology</li>
            <li class="list_item">New CPT and HCPCS codes have been established for telehealth services</li>
            <li class="list_item">More information on the telehealth expansion for Medicare can be found <a href="https://www.cms.gov/newsroom/fact-sheets/medicare-telemedicine-health-care-provider-fact-sheet" target="_blank">here</a>.</li>
            <li class="list_item">Click on <a href="https://americanrheum.com/news/american-rheumatology-telehealth-overview-caring-for-patients-during-the-covid-19-pandemic/" target="_blank">the link</a> for the American Rheumatology Network telehealth summary and codes</li>
          </ul>

              <!--!!!!!!!!  Don't delete !!!!!!!! -->
          <!-- <div class="download">
            <p class="download_title">Additional files</p>
            <a href="https://dev.awirgroup.org/wp-content/uploads/2020/04/placeholder.pptx" target="_blank" class="download_col">
              <div class="download_col-1">
                <img src="<?= get_template_directory_uri() ?>/assets/img/covid/download-img.svg" alt="" class="download_img">
                <p class="download_name">Placeholder</p>
              </div>
              <div class="download_col-2">
                <p class="download_download">Download</p>
                <img src="<?= get_template_directory_uri() ?>/assets/img/covid/download-icon.svg" alt="" class="download_icon">
              </div>
            </a>
          </div> -->
              <!--!!!!!!!!  Don't delete !!!!!!!! -->


        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-0.svg" alt="" class="section_block-img">
        </div>
      </div>


      <div class="section_block section_block--reversed covid-section" id="covid-section-1">
        <div class="section_block-col-1">
          <h3 class="section_title"><nobr>COVID-19</nobr> Global Rheumatology Alliance — New International Case Reporting Registry</h3>
          <ul class="list">
            <li class="list_item">AWIR is an official supporter of the International Registry</li>
            <li class="list_item">Enter data on your patients infected with <nobr>COVID-19</nobr></li>
            <li class="list_item">Help answer urgent questions about treatment options and infection risks amongst your patients</li>
            <li class="list_item">De-identified registry data will be disseminated for analysis</li>
            <li class="list_item">Click on <a href="https://rheum-covid.org/" target="_blank">the link</a> to earn more about the registry and how to use it</li>
          </ul>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-1.svg" alt="" class="section_block-img">
        </div>
      </div>



      <div class="section_block covid-section" id="covid-section-2">
        <div class="section_block-col-1">
          <h3 class="section_title">CMS 1135 Waivers to Ease Administrative Burden on Providers for Medicaid Patients</h3>
          <ul class="list">
            <li class="list_item">Treat Medicaid patients with greater freedom and less paperwork</li>
            <li class="list_item">For example, states can decide to reimburse for telehealth services, remove the need for FFS prior authorizations, reimburse physicians for services provided out of state and more  <a class="icon-link icon-link-text" href="https://www.medicaid.gov/resources-for-states/disaster-response-toolkit/coronavirus-disease-2019-covid-19/index.html" target="_blank"> <img src="<?= get_template_directory_uri() ?>/assets/img/covid/link.svg" alt=""></a></li>
            <li class="list_item">Options vary from state to state. More information on the Section 1135 Waivers can be found  <a href="https://www.medicaid.gov/resources-for-states/disaster-response-toolkit/federal-disaster-resources/index.html" target="_blank">here</a></li>
          </ul>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-2.svg" alt="" class="section_block-img">
        </div>
      </div>



      <div class="section_block section_block--reversed covid-section" id="covid-section-3">
        <div class="section_block-col-1">
          <h3 class="section_title">Global Healthy Living Foundation and Creaky Joints Launch <nobr>COVID-19</nobr> Patient and Family Support Program</h3>
          <ul class="list">
            <li class="list_item">Made for patients and caregivers, this program allows patients to get answers on many questions about the exposure and prevention from <nobr>COVID-19</nobr> from healthcare professionals, receive emails on the latest news and stories on how other patients are coping</li>
            <li class="list_item">Click on <a href="https://www.ghlf.org/awir/" target="_blank">the link</a> to sign up for the program</li>
          </ul>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-3.svg" alt="" class="section_block-img">
        </div>
      </div>



      <div class="section_block covid-section" id="covid-section-4">
        <div class="section_block-col-1">
          <h3 class="section_title">Trials Underway to Evaluate Treatment and Prevention of <nobr>COVID-19</nobr></h3>
          <ul class="list">
            <li class="list_item">On March 16, 2020 the combined forces of the National Institute of Allergy and Infectious Disease and Moderna, already working on a vaccine for MERS, announced new Phase I studies for a vaccine targeting <nobr>COVID-19</nobr> with mRNA-1273</li>
            <li class="list_item">Almost simultaneously several additional treatment options went into clinical trials globally; including Phase 2/3 studies using tocilizumab and sarilumab, by Genentech and Regeneron/Sanofi, respectively, to treat cytokine release syndrome (CRS) and pneumonia in <nobr>COVID-19</nobr> patients</li>
            <li class="list_item">Genentech launched a website to provide U.S. Healthcare Providers (HCPs) with up-to-date information about Actemra  (tocilizumab) and Coronavirus Disease 2019 <nobr>(COVID-19)</nobr>, which can be found <a href="https://www.actemrainfo.com/" target="_blank">here</a></li>
          </ul>
          <p class="quote">"We have received a surge of questions from HCPs across the globe regarding the use of Actemra for <nobr>COVID-19</nobr>, therefore the intent of this website is to make such information available to enable physicians to make informed individual treatment decisions"</p>
          <p class="quote quote_autor">– Genentech</p>
          <ul class="list">
            <li class="list_item">The first successful clinical trial with positive interim results on a treatment for <nobr>COVID-19</nobr> were made public April 27th  with interim results demonstrating that patients treated with remdesivir had a 31% faster time to recovery than those on <nobr>placebo (<i>P</i>&#60;0.001)</nobr></li>
            <li class="list_item">The remdesivir trial, the Adaptive <nobr>COVID-19</nobr> Treatment Trial, was sponsored by the National Institute of Allergy and Infectious Diseases, a part of the National Institute of Health</li>
            <li class="list_item">Other antivirals, alone or in combination with other drugs, are also under investigation</li>
            <li class="list_item">A full listing on clinicaltrials.gov of all trials for <nobr>COVID-19</nobr> can be found <a href="https://clinicaltrials.gov/ct2/results?cond=COVID-19" target="_blank">here</a></li>
          </ul>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-4.svg" alt="" class="section_block-img">
        </div>
      </div>



      <div class="section_block section_block--reversed covid-section" id="covid-section-5">
        <div class="section_block-col-1">
          <h3 class="section_title">Hydroxychloroquine and Chloroquine Shortages and Usage for <nobr>COVID-19</nobr> Treatment</h3>
          <ul class="list">
            <li class="list_item">There have been reports of shortages and lack of availability of both drugs for lupus and RA patients. Consider dose adjustments as appropriate for your patients</li>
            <li class="list_item">Trials are underway to test the effectiveness of hydroxychloroquine in clinical trials</li>
            <li class="list_item">Click on <a href="https://www.fda.gov/emergency-preparedness-and-response/counterterrorism-and-emerging-threats/coronavirus-disease-2019-covid-19" target="_blank">the link</a> to learn more from the FDA website</li>
            <li class="list_item">Click on <a href="https://www.cdc.gov/coronavirus/2019-ncov/hcp/therapeutic-options.html" target="_blank">the link</a> for guidance from the CDC on therapeutic options</li>
          </ul>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-5.svg" alt="" class="section_block-img">
        </div>
      </div>


      <div class="section_block covid-section" id="covid-section-6">
        <div class="section_block-col-1">
          <h3 class="section_title">AWIR Letter to Insurers, Vendors,  and Manufacturers Advocating for Provider Relief</h3>
          <p class="text"> Use this letter to advocate for yourself and the medical community to allow everyone to focus on patient care and remove administrative hurdles. <b>The letter can be viewed, edited, and downloaded by clicking on </b><a href="https://awirgroup.org/wp-content/uploads/2020/03/COVID19-Letter-of-Request-to-Insurers.Vendors.Drug-Manufacturers.docx" target="_blank">the link</a>.</p>
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-6.svg" alt="" class="section_block-img">
        </div>
      </div>


      <div class="section_block section_block--reversed covid-section" id="covid-section-7">
        <div class="section_block-col-1">
          <h3 class="section_title">Other Resources</h3>
            <a class="icon-link" href="https://www.cdc.gov/coronavirus/2019-nCoV/hcp/index.html" target="_blank">The Centers for Disease Control and Prevention (CDC)</a>
            <a class="icon-link" href="https://www.fda.gov/emergency-preparedness-and-response/mcm-issues/coronavirus-disease-2019-covid-19" target="_blank">General Food and Drug Administration Guidance on <nobr>COVID-19</nobr></a>

            <a class="icon-link" href="https://www.ncsl.org/research/health/state-action-on-coronavirus-covid-19.aspx" target="_blank">State Action on <nobr>COVID-19</nobr> from the National Conference of State Legislators</a>
            <a class="icon-link" href="https://www.ama-assn.org/practice-management/digital/ama-quick-guide-telemedicine-practice" target="_blank">American Medical Association’s (AMA) Quick Telehealth Guide for Providers</a>
            <a class="icon-link" href="https://www.rheumatology.org/announcements" target="_blank">American College of Rheumatology <nobr>COVID-19</nobr> Information</a>
            <a class="icon-link" href="https://coronavirus.jhu.edu/map.html" target="_blank">Johns Hopkins Department of Medicine — Coronavirus Research Center World Map of Cases & Data</a>
           
        </div>
        <div class="section_block-col-2">
          <img src="<?= get_template_directory_uri() ?>/assets/img/covid/union-7.svg" alt="" class="section_block-img">
        </div>
      </div>
    </div>
  </section>


<?php get_footer();
