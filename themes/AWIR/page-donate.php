<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="advocacy-policy-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
		<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

		<section class="sheet-section">

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-resourses">
			<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/advocacy_hex.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

	<div class="sheet-container">
		<div class="sheet">

			<div class="membership-text-wrap sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php if ( get_field('donate_banner') ): ?>
			<div class="pages-lable donate-label">
				<?php the_field('donate_banner') ?>
			</div>
			<?php endif ?>

		</div>
	</div>
</section>



<?php get_footer();
