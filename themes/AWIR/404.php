<?php

get_template_part('inc/head');

?>

<div class="not-found-wrapper">

	<div class="not-found-bg-wrap">
		<div class="not-found-hex-wrap">
			<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-hexset-1.png" alt="" class="not-found-hex not-found-hex--left">
			<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-hexset-2.png" alt="" class="not-found-hex not-found-hex--right">
		</div>
		<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-bott.png" alt="" class="not-found-bg not-found-bg--bott">
		<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-mid.png" alt="" class="not-found-bg not-found-bg--mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-top.png" alt="" class="not-found-bg not-found-bg--top">

		<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-small-bott.png" alt="" class="not-found-bg not-found-bg--smal--bott">
		<img src="<?= get_template_directory_uri() ?>/assets/img/404/404-small-top.png" alt="" class="not-found-bg not-found-bg--small-top">
	</div>

	<div class="not-found">
		<h1 class="not-found__title">404</h1>
		<p class="not-found__message"><span>Ooops! </span>Page Not found</p>
		<a href="<?= home_url() ?>" class="btn btn-centered btn__main btn__main-on_dark registration-btn"><span class="btn__text">Back to Home Page</span></a>
	</div>

</div>
