<?php
// if its a member redirect
if ( awir::is_member() )
	awir::user_page_redirect();

// get and sanitize post data
$post_profileID = isset( $_POST['profile_id'] ) ? sanitize_key($_POST['profile_id']) : NULL;
$post_couponCode = isset( $_POST['coupon_code'] ) ? sanitize_text_field($_POST['coupon_code']) : NULL;

// get or create profile
$profile = new awir_profile( isset( $post_profileID ) ? $post_profileID : NULL );

// if dont have profile - back to registration
if ( !$profile->ID )
	awir::user_page_redirect('register');

$payment_form = $payment_error = false;

$fee_form = false;
$couponConfirmed = false;

//var_dump( $profile ); exit;

if ( $profile->member_type != awir_memberships::TYPE_FREE )
{
	$payment_form = true;

	// if its base type - Member - show fee form
	if ( $profile->member_type === awir_memberships::TYPE_DEFAULT ){
		$fee_form = true;
	}

	
	$membership = MS_Factory::load( 'MS_Model_Membership', $profile->membership );
	if ( !$membership )
		awir::user_page_redirect('register');

	$member = MS_Model_Member::get_current_member();
	$subscription = MS_Model_Relationship::create_ms_relationship( $membership->id, $member->id, '' );

	if ( !$subscription )
		awir::user_page_redirect('register');

	$invoice = $subscription->get_current_invoice();


	if ( !empty( $_POST['ms_relationship_id'] ) && wp_verify_nonce( @$_POST['_wpnonce'], 'authorize_'.$_POST['ms_relationship_id'] ) )
	{
		do_action(
			'ms_controller_frontend_signup_process_purchase',
			null
		);

		$subscription = MS_Factory::load('MS_Model_Relationship', (int)$_POST['ms_relationship_id']);
		if ( $subscription )
			$membership = MS_Factory::load( 'MS_Model_Membership', $subscription->membership_id );

		if ( !$subscription || !$membership )
			awir::user_page_redirect( 'checkout' );

		$payment_error = true;
	}

	else
	{
		

		if ( $profile->member_type !== awir_memberships::TYPE_DEFAULT ){
			while ( !in_array( $invoice->status, [MS_Model_Invoice::STATUS_NEW, MS_Model_Invoice::STATUS_PENDING] ) )
			{
				$invoice->delete();
				$invoice = $subscription->get_next_invoice();
			}

			$invoice = apply_filters(
				'ms_signup_payment_details',
				$invoice,
				$subscription,
				$membership
			);
			$invoice->save();
		}
		
		/** Check coupon if we got some */
		if ( isset( $_POST['apply_coupon_code'] ) && isset($post_couponCode) ){
			$couponModel = MS_Addon_Coupon_Model::load_by_code($post_couponCode);

			if($couponModel !== NULL && $couponModel->is_valid($membership->id)){
				$couponAddon = new MS_Addon_Coupon;
				$invoice = $couponAddon->apply_discount($invoice, $subscription);
			
				$invoice = apply_filters(
					'ms_signup_payment_details',
					$invoice,
					$subscription,
					$membership
				);
				$invoice->save();
				$couponConfirmed = true;

				// if we got 100% discount
				if( $invoice->discount > 0 && $invoice->discount == $invoice->amount ){
					// make pay
					$payment_form = false;
					$invoice->pay_it(null, null);
					$invoice->save();

					// register membership
					$member->add_membership( $membership->id );
					$member->is_member = true;
					$member->save();
				}
			}
		}
	}

	$_POST['gateway'] = 'authorize';
	$_POST['ms_relationship_id'] = $subscription->id;
	$_POST['step'] = 'gateway_form';

	add_filter( 'ms_gateway_authorize_view_form_to_html', function( $dumm, $form_view ){
		$func = function() {
			$this->data['cim_profiles'] = [];
		};
		$func = Closure::bind( $func, $form_view, $form_view );
		$func();
		return $dumm;
	}, 10 ,2 );

	do_action(
		'ms_controller_frontend_signup_gateway_form',
		null
	);
}
else
{
	add_filter( 'document_title_parts', function( $parts ){
		$parts['title'] = 'Awaiting confirmation';
		return $parts;
	}, 100 );

	awir_memberships::user_pending();
}
get_header();

the_post();
?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>

	<!-- End Pages Hero Section -->

	<div class="pages-headline exhibitors">
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<section class="sheet-section">
		<div class="sheet-container pay-page">
			<div class="sheet">

				<?php 
				// debug_print('Amount: ' . $invoice->amount);
				// debug_print('Fee: ' . $invoice->discount);
				// if($invoice->is_paid() === true){
				// 	echo 'PAID';
				// }else{
				// 	echo 'NOT PAID';
				// }
				?>

				<?php if($fee_form): ?>
				<!-- membership fees  -->
				<div class="membership-fees">
					<div class="fees-code-wrap">
						<p class="membership-fees__title">Membership Fees</p>
						<div class="fees-code">
							<div class="fees-code__item">
								<div class="membership-fees__info">
									<p class="membership-fees__price"><b>Price:</b> $<?= number_format($invoice->amount, 2, '.', ' ') ?></p>
								</div>
							</div>
							<div class="fees-code__item">
								<h2 class="fees-code__title">DISCOUNT CODE (optional)</h2>
								<form action="" class="fees-code__input" method="post">
									<input type="text" name="coupon_code" <?= $couponConfirmed === true || $invoice->discount > 0 ? 'disabled' : '' ?> <?= $couponConfirmed === true && isset($post_couponCode) ? 'value="'.$post_couponCode.'"' : '' ?>>
									<input type="hidden" name="profile_id" value="<?= $profile->ID ?>">
									<button class="fees-code__btn" name="apply_coupon_code" <?= $couponConfirmed === true || $invoice->discount > 0 ? 'disabled' : '' ?>>Apply</button>
								</form>
							</div>
						</div>
					</div>
					<p class="membership-fees__total">Charges Total: $<?= number_format($invoice->__get('total'), 2, '.', ' ') ?></p>
				</div>
				<!-- End membership fees -->
				<?php endif ?>
				<?php if ( $payment_form ): ?>

					<?php if($profile->member_type === awir_memberships::TYPE_SPECIAL): ?>
					<div class="membership-plans">
						<div class="membership-plans__item">
							<div class="membership-plans__info">
								<h3 class="membership-plans__title">Allied Speciality Member</h3>
								<p class="membership-plans__price">USD <?= number_format($invoice->amount, 2, '.', ' ') ?></p>
							</div>
						</div>
					</div>
					<?php endif?>

				<style>.ms-validation-error{display:none}</style>
				<script>jQuery( function($){
					var last_cc_num = '';
					$('#card_num').on('keyup input change', function(e)
					{
						var value = $(this).val().replace(/\s+$/, '');

						var clean_value = value.replace( /\D/g, "" );
						if ( clean_value )
							clean_value = clean_value.match( /.{1,4}/g ).join( " " );

						if ( clean_value != value )
							$(this).val( clean_value );
					} );
				});
				</script>

				<div class="advocacy sheet-text-wrap">
					<?php if ( $payment_error ): ?>
					<p class="card_failed">Sorry, your signup request has failed. Try again.</p>
					<?php endif ?>
					<?= apply_filters('the_content', '[ms-membership-signup]') ?>
				</div>


				<?php else: ?>

				<?php 
				if($profile->member_type === awir_memberships::TYPE_DEFAULT && $member->has_membership()){
					?>
					<a href="<?= awir::user_page_url( 'user', ['registered'=>'1'] ); ?>" class="btn btn-centered btn__main registration-btn">Continue</a>
					<?php
				}else{
					?>
					<div class="pages-lable register-lable">
						<?php the_content() ?>
					</div>
					<?php
				}
				?>

				<?php endif ?>

				<p style="text-align:center">
					<a href="<?= awir::user_page_url('register') ?>" id="to-singin5" class="new-password__back" data-wpel-link="internal">Change registration details</a>
				</p>

			</div><!--End Sheet -->
		</div>
	</section>

<?php get_footer();
