<?php

get_header();

get_template_part('inc/section', 'hero');

get_template_part('inc/section', 'mission');

get_template_part('inc/section', 'events');

get_template_part('inc/section', 'value');

get_template_part('inc/section', 'who');

get_template_part('inc/section', 'partners');

get_template_part('inc/section', 'subscribe');

if ( !is_user_logged_in() && isset( $_GET['login_popup'] ) ):
	$url = esc_url( stripslashes( $_GET['login_popup'] ) );
	$info = parse_url( $info );
	if ( !$info || ( $info['host'] && $info['host'] != $_SERVER['HTTP_HOST'] ) )
		$url = '';
	?>
	<script>
		jQuery( function($){
			$('#popup-singin').addClass('active');
			<?php if ( $url ): ?>
			window.awir_login_url = <?= json_encode( $url ) ?>;
			<?php endif ?>
		} );
	</script>
	<?php
endif;

get_footer();
