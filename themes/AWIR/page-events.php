<?php

get_header();

the_post();

?>

<!-- Pages Hero Section -->
<div class="calendar-archive">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

	<div class="pages-headline">
    <a href="<?= home_url('events/') ?>" class="main-text pages-headline__category">Meetings /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<section class="sheet-section calendar-section">

		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set8">
		</div>

		<div class="sheet-container">
			<div class="sheet event-sheet" data-sticky-container>

			<img src="<?= get_template_directory_uri() ?>/assets/img/calendar/calendar-sheet-bg.svg" alt="" class="calendar-sheet-bg">

				<div class="meetings-list">

					<?php get_template_part('inc/events-slider') ?>

					<?php get_template_part('inc/events-calendar' ) ?>

					<?php get_template_part('inc/events-list') ?>

				</div>

			</div>
		</div>
	</section>

	<?php get_template_part('inc/events-register') ?>

<?php get_footer();
