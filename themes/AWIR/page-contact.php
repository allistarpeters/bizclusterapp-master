<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="exhibitors-header">

		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_2.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline exhibitors pages-headline__alone">
		<!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>


<section class="sheet-section calendar-section">

	<div class="pages-hexset-wrap-bottom exhibitors-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
	</div>

	<div class="sheet-container advocacy-page">
		<div class="sheet">

			<?php get_template_part('inc/contact-form') ?>

			<div class="contact-links-wrap">
				<div class="contact-social">

					<div class="sheet-text-wrap contact-text-wrap">
						<h4><?php the_field('social_links_title') ?></h4>
					</div>

					<ul class="social-list">
						<?php while ( have_rows('social_links') ): the_row() ?>
						<li><a href="<?php the_sub_field('url') ?>" target="_blank"><img src="<?php the_sub_field('icon') ?>" alt="" class="social-list__link social-list__link-twitter"><?php the_sub_field('name') ?></a></li>
						<?php endwhile ?>
					</ul>

				</div>

				<?php while ( have_rows('right_links') ): the_row() ?>
				<div class="contact-leaders-wrap">

					<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_6.png" alt="" class="leaders-bg leaders-bg-bott">
					<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_7.png" alt="" class="leaders-bg leaders-bg-mid">
					<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_5.png" alt="" class="leaders-bg leaders-bg-top">


					<div class="contact-leaders">
						<div class="sheet-text-wrap contact-text-wrap">
							<h4><?php the_sub_field('title') ?></h4>
							<p><?php the_sub_field('text') ?></p>
						</div>
						<a href="<?= home_url('about/local-chapters/') ?>" class="contact-leaders__btn"><?php the_sub_field('link_text') ?> <img src="<?= get_template_directory_uri() ?>/assets/img/icons/red-right-arrow.svg" alt=""></a>
					</div>
				</div>
				<?php endwhile ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer();
