<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="local-chapters">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
    <a href="<?= home_url('about/') ?>" class="main-text pages-headline__category">About /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<!-- About page content-->

	<section class="sheet-section chapters">

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container">
			<div class="sheet">

        <?php if ( get_field('section_title') ): ?>
          <h2 class="title-text title-underline title-underline-left sheet__title-main"><?php the_field('section_title') ?></h2>
        <?php endif ?>

        <div class="partners-block">
        <?php

          $chapters = get_field('rep_chapters');
          if($chapters){
            foreach ($chapters as $key => $chapter){
              $img_src = wp_get_attachment_image_src( $chapter['logo'], 'full');
              $url = $chapter['url'];
              if (preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/',$url)){
                $host = $url;
                $url = 'mailto:' . $url;
              }
              else if ( preg_match('~^[a-z]+://~i', $url ) )
              {
                if ( $info = parse_url( $url ) && $info['host'] )
                  $host = $info['host'];
                else
                  $host = $url;
              }
              else
              {
                $host = $url;
                $url = 'http://'.$host;
              }

              ?>
              <div class="partners-block__item">
                <div class="partners-block-wrap">
                  <a target="_blank" href="<?= $url; ?>">
                    <img class="partners-block__img" src="<?= $img_src[0]; ?>" alt="">
                  </a>
                </div>
                <a href="<?= $url; ?>" class="main-text sheet__text member__more"><?= $host; ?></a>
              </div>
              <?php
              if($key % 2 == 1){
                echo '</div><div class="partners-block">';
              }
            }
          }

        ?>
        </div>

				<?php while ( have_rows('section_partners') ) : the_row() ?>

				<?php if ( get_sub_field('title') ): ?>
				<h2 class="title-text title-underline title-underline-left sheet__title-main"><?php the_sub_field('title') ?></h2>
				<?php endif ?>

				<div class="partners-block">

					<?php while ( have_rows('partners') ):
						the_row();
						if ( $url = get_sub_field('url') )
						{
						    if (preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/',$url)){
                                $host = $url;
                                $url = 'mailto:' . $url;
//                                d($url);
                            }
							else if ( preg_match('~^[a-z]+://~i', $url ) )
							{
								if ( $info = parse_url( $url ) && $info['host'] )
									$host = $info['host'];
								else
									$host = $url;
							}
							else
							{
								$host = $url;
								$url = 'http://'.$host;
							}
						}
					?>
					<div class="partners-block__item">
						<div class="partners-block-wrap">
							<a target="_blank"<?php if ( $url ): ?> href="<?= $url ?>"<?php endif ?>><img src="<?php the_sub_field('logo')?>" alt="" class="partners-block__img"></a>
							<?php if ( get_sub_field('name') ): ?>
							<h2 class="title-text sheet__medium-title"><?php the_sub_field('name') ?></h2>
							<?php endif ?>
							<?php if ( get_sub_field('info') ): ?>
							<p class="main-text sheet__text sheet__text-average"><?php the_sub_field('info') ?></p>
							<?php endif ?>
						</div>
						<?php if ( $url ): ?>
						<a target="_blank" href="<?= $url ?>" class="main-text sheet__text member__more"><?= $host ?></a>
						<?php endif ?>
					</div>
					<?php endwhile ?>

				</div>

				<?php endwhile ?>

			</div>
		</div>
	</section>


<?php get_footer();
