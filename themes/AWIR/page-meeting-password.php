<?php

$password_error = false;
$has_post = false;

if ( !empty( $_POST['meeting_password'] ) && is_string($_POST['meeting_password']) && ($password = trim($_POST['meeting_password'])) )
{
	if ( awir_memberships::is_meeting_password( $password ) )
	{
		awir_memberships::save_meeting_password( $password );
		awir::user_page_redirect('meeting_content');
	}
	$query = new WP_Query([
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'meta_key' => 'guests_password',
		'meta_value' => stripslashes( $password ),
	]);

	if ( $query->have_posts() )
	{
		$query->the_post();
		setup_postdata( $post );
		$has_post = true;

		add_filter( 'document_title_parts', function( $parts ){
			$parts['title'] = get_the_title();
			return $parts;
		}, 100 );
	}
	else
		$password_error = true;
}

get_header();

?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>

	<!-- End Pages Hero Section -->

	<div class="pages-headline exhibitors">
    <a href="<?= home_url('events/') ?>" class="main-text pages-headline__category">Meetings /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<!-- page content-->

	<section class="sheet-section exhibitors-section">

		<div class="pages-hexset-wrap-bottom exhibitors-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
		</div>
		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
		</div>

		<?php if ( $has_post ): ?>
		<div class="sheet-container message-board">
			<div class="sheet">

				<div id="side-menu" class="blogs-sidebar internal-sidebar">
					<a href="<?= get_post_type_archive_link('post') ?>" class="blogs-sidebar__back"><span>Back</span></a>
					<div class="blogs-sidebar__social-list">
						<?php while ( have_rows('social_icons', 'options') ): the_row() ?>
						<?php if ( !get_sub_field('icon_alt') ) continue ?>
						<a href="<?php the_sub_field('url') ?>" class="blogs-sidebar__social"><img src="<?php the_sub_field('icon_alt') ?>" alt="" class="blogs-sidebar__icon"></a>
						<?php endwhile ?>
					</div>
				</div>

				<div class="blogs-wrap internal-content">
					<div class="internal-content-wrap">
						<div class="blogs-content">
							<div class="blogs-content__item">
								<p class="blogs-content__subtitle"><?= get_the_date( 'F j, Y' ) ?></p>
								<h2 class="title-text sheet__title blogs-content__title"><?php the_title() ?></h2>
								<?php if ( has_post_thumbnail() ): ?>
								<img src="<?php the_post_thumbnail_url('full') ?>" alt="" class="blogs-content__image">
								<?php endif ?>
								<div class="sheet-text-wrap blogs-text-wrap">
									<?php the_content() ?>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<?php else: ?>

		<div class="sheet-container reset-password">
			<div class="sheet">
				<div class="new-password meeting-password">
					<?php if ( $password_error ): ?>
						<h3 class="new-password__title new-password__title--error">Invalid password</h3>
					<?php endif ?>
					<h3 class="new-password__title">Please enter the password</h3>
					<form action="<?php the_permalink() ?>" method="post" class="subscribe__form message-form">
						<div class="subscribe__input-wrap">
							<input type="password" name="meeting_password" placeholder="Password">
						</div>
						<button type="submit" class="btn btn__main"><span class="btn__text">Go</span></button>
					</form>
					<a href="/" class="new-password__back">Back</a>
				</div>
			</div><!--End Sheet -->
		</div>

		<?php endif ?>

	</section>


	<!-- About page content-->

<?php get_footer();
