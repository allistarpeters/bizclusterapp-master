<?php

add_filter( 'header_user_id', function( $user_id = null )
{
	if ( !empty( $_GET['user'] ) && ($user = get_user_by( 'login', $_GET['user'] )) )
		$user_id = $user->ID;

	return $user_id;
} );

$user_id = apply_filters( 'header_user_id', get_current_user_id() );
$user = get_userdata( $user_id );

if ( !$user )
{
	wp_redirect( home_url() );
	exit;
}

$acf_id = 'user_'.$user_id;

$is_mine = $user_id == get_current_user_id();

get_header();

?>

	<section class="sheet-section user-sheet<?= $is_mine ? '' : ' user-profile-sheet'?>">

		<div class="sheet-container user-page">
			<div class="sheet">
				<h2 class="title-text sheet__title"><?= $is_mine ? 'My Profile' : 'User Profile' ?></h2>
					<div class="member user-member">
						<div class="member__photo photo-back-hex<?= $is_mine ? '' : '-bottom'?>">
							<img src="<?= awir_theme::avatar( $user_id ) ?>" alt="AWIR" class="member__photo-img">
						</div>
						<div class="member__data">
							<p class="main-text member__name"><?= esc_html( $user->display_name ) ?></p>
							<p class="main-text member__degree"><?php the_field( 'med_degree', $acf_id ) ?></p>
						</div>

						<?php if ( !$is_mine ): ?>
						<div class="user-message-wrap">
							<a href="<?= awir::user_page_url('messages')?>#to=<?=$user_id?>" class="btn btn__main user-message">Send Message</a>
						</div>
						<?php endif ?>

					</div>

					<div class="user-info">
						<?php if ( get_field('gender', $acf_id ) ): ?>
						<div class="user-info__item user-info__item-gender">
							<p class="user-info__title">Gender</p>
							<p class="user-info__text"><?php the_field( 'gender', $acf_id ) ?></p>
						</div>
						<?php endif ?>

						<?php if ( get_field('country', $acf_id ) ): ?>
						<div class="user-info__item user-info__item-cuntry">
							<p class="user-info__title">Country</p>
							<p class="user-info__text"><?php the_field( 'country', $acf_id ) ?></p>
						</div>
						<?php endif ?>

						<?php if ( get_field('state', $acf_id ) ): ?>
						<div class="user-info__item user-info__item-state">
							<p class="user-info__title">State</p>
							<p class="user-info__text"><?php the_field( 'state', $acf_id ) ?></p>
						</div>
						<?php endif ?>
					</div>

					<div class="user-info user-info-interest">
						<?php $the_first = true;
							foreach ( awir_message_board::user_tags( $user_id ) as $tag ): ?>
						<div class="user-info__item user-info__item-interest">
							<?php if ( $the_first ): ?>
							<p class="user-info__title">Interest</p>
							<?php endif ?>
							<a href="<?= awir_message_board::url( $tag ) ?>" class="user-info__tag"><?=esc_html($tag->name)?></a>
						</div>
						<?php $the_first = false;
							endforeach ?>
					</div>

					<?php
					$profile = new awir_profile( $user );

					if( $user->user_login !== 'root' && $profile->member_type != awir_memberships::TYPE_FREE):
						$membership = MS_Factory::load( 'MS_Model_Membership', $profile->membership );
						$member = MS_Model_Member::get_current_member();
						$subscription = MS_Model_Relationship::create_ms_relationship( $membership->id, $member->id, '' );

						$invoice = $subscription->get_current_invoice();
					?>
					<a href="<?= get_permalink( $invoice->id ) ?>" class="see-invoice__link">See Invoice</a>
					<?php endif; ?>

				<?php if ( $is_mine ): ?>
					<?php foreach ( awir_discounts::get_list() as $discount ): ?>
					<div class="discount-code-wrap">
						<div class="discount-code">
							<h2 class="discount-code__title"><?= esc_html( $discount['title'] ) ?></h2>
							<form action="" class="discount-code__input">
								<input type="text" disabled value="<?= esc_attr( $discount['code'] ) ?>">
								<a href="javascript:void(0);" class="discount-code__copy">copy</a>
							</form>
						</div>
						<div class="discount-note"><?= apply_filters('the_content', $discount['description']) ?></div>

					</div>
					<?php endforeach ?>

					<div class="discount-divider"></div>

					<a href="<?= awir::user_page_url('profile') ?>" class="btn btn-centered btn__main edit-profile"><span class="btn__text">Edit Profile</span></a>
				<?php endif ?>

			</div>
		</div>
	</section>

	<?php
		$success =
			!empty( $_GET['edited'] ) ? 'Profile changed successfully' : (
			!empty( $_GET['restored'] ) ? 'Your password has been changed successfully.': (
			!empty( $_GET['registered'] ) ? 'Your payment processed successfully!' : (
			''
		) ) );
	?>
	<?php if ( $success ): ?>
	<div id="popup-success" class="popup-main popup-main-middle popup-main-success active">
		<div class="popup-main__window">
			<div class="close-btn">
				<img src="assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
			</div>
			<div class="popup-main__inner">
				<h2 class="popup-main__title">Success!</h2>
				<p class="popup-main__text"><?= $success ?></p>
			</div>
		</div>
		<a href="javascript:void(0);" class="close-popup popup-main__bottom-btn">Back to My Profile</a>
	</div>
	<?php endif ?>

<?php get_footer();
