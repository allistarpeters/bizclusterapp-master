<?php

if ( !awir::is_member() )
	awir::user_page_redirect('memberships');

$user_messages = new awir_user_messages();

get_header();

$contacts = $user_messages->my_contacts();
foreach ( $contacts as $i => $contact )
	$contacts[ $i ]->avatar = awir_theme::avatar( $contact->user_id );


?>


<section class="sheet-section user-sheet direct-message-sheet">
<?php get_template_part('inc/member-header') ?>
	<div class="sheet-container">
    <div class="sheet" id="app-direct-messages">
  <div class="members-area">
    <div class="members-area__header">
      <h2 class="members-area__header-title">AWIR Members</h2>
      <div id="to-messages"></div>
    </div>
    <form action="" class="members-area__search">
      <input v-model="filterString" type="search" placeholder="Search contacts…">
    </form>
    <!-- <p class="members-area__category">Resent</p> -->

    <div class="cintacts-wrap">
      <div
          v-for="item in filteredUserList"
          class="resent-contact"
          :class="{
            'contact-online': new_messages_notification.indexOf(item.user_id) !== -1,
            'resent-contact--active': parseInt(chat_user_id) === parseInt(item.user_id)
          }"
          @click="startConversatinon(item.user_id)">
        <div class="chat-avatar chat-avatar-icon"><span>G</span></div>
        <img :src="item.avatar" alt="" class="chat-avatar">
        <div class="resent-contact__info">
          <div class="resent-contact__name">{{ item.display_name }}</div>
          <div class="resent-contact__degree">{{ item.med_degree }}</div>
        </div>
      </div>
    </div>

  </div>

<div class="messages-area_wrap">
<div id="to-contacts"></div>
    <div class="lds-ring"
         :class="{ 'lds-ring--loading': isLoading }"
    ><div></div><div></div><div></div><div></div>
    </div>
    <div
      class="messages-area"
      :class="{'messages-area--loading': isLoading || !chat_user_data}"
    >
      <div class="message-window__header" v-if='chat_user_data && !isLoading'>
        <a :href="userProfileUrl" class="message-window__user-name">{{ chat_display_username }}</a>
      </div>
      <div class="chat-fade"></div>
      <div class="message-window">
        <div class="message-window__container">

          <div
            v-for="item in sortedMessages"
            class="single-message"
            :class="[parseInt(chat_current_user_id) === parseInt(item.author_id) ? 'single-message--even' : 'single-message--odd']"
          >
            <div class="single-message__autor">
              <img v-if="parseInt(chat_current_user_id) === parseInt(item.author_id)" :src="chat_current_user_avatar" alt="avatar" class="chat-avatar">
              <img v-if="(parseInt(chat_current_user_id) !== parseInt(item.author_id)) && chat_user_data" :src="chat_user_data.avatar" alt="avatar" class="chat-avatar">

              <div class="single-message__message">
                <p class="single-message__text">{{ item.message }}</p>
                <p class="single-message__time">{{ (new Date(item.timestamp*1000)) | moment("from", "now", true) }} ago</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="type-message" v-if='chat_user_data'>
        <div class="type-message__autor">
          <img :src="chat_current_user_avatar" alt="" class="chat-avatar">
        </div>
        <form action="" v-on:submit.prevent="onSubmit" class="type-message__input-area">
          <textarea type="text" v-on:keydown="keymonitor($event)" v-model="chat_message" placeholder="Type something…"></textarea>
          <button type="submit"  class="btn btn__main add-comment__btn new-message__btn"><span class="btn__text">Send</span></button>
        </form>
      </div>
    </div>
</div>

  </div>


<!-- <div id="direct-messages" style="clear: both">
</div> -->


<script>
'use strict';
window.chat_home = '<?= home_url() ?>';
window.chat_contacts = <?= json_encode( $contacts ) ?>;
window.chat_dialog_nonce = '<?= wp_create_nonce('awir_get_dialog') ?>';
window.chat_message_nonce = '<?= wp_create_nonce('awir_add_message') ?>';
window.chat_current_user_id = <?= get_current_user_id() ?>;
window.chat_current_user_avatar = '<?= awir_theme::avatar(); ?>';
window.chat_ajaxurl = <?= json_encode( admin_url('admin-ajax.php') ) ?>

/*
jQuery( function($){
	var contacts = <?= json_encode( $contacts ) ?>;
	var dialog_nonce = '<?= wp_create_nonce('awir_get_dialog') ?>';
	var message_nonce = '<?= wp_create_nonce('awir_add_message') ?>';
	var current_user_id = <?= get_current_user_id() ?>;
	var ajaxurl = <?= json_encode( admin_url('admin-ajax.php') ) ?>

	var box = $('#direct-messages');

	var contactbox = $('<ul id="contcts">');
	var dialog = $('<ul id="dialog">');
	var editor = $('<textarea id="add-message">');
	var button = $('<button id="add-message-button">Send</button>');

	var load_interval;


	for ( var i in contacts )
	{
		var contact = contacts[i];
		var item = $('<li>')
			.text( contact.display_name )
			.append( '<br>' )
			.append(
				$('<i>').text( contact.med_degree )
			)
			.data( contact );

		if ( !(contact.is_readed*1) && contact.latest_author_id && contact.latest_author_id != current_user_id ) {
			item.append( ' (new)' );
		}

		contactbox.append( item );
	}

	function load_dialog( user_id )
	{
		if ( !user_id )
		{
			user_id = dialog.data('user_id');
		}
		else if ( dialog.data('user_id') != user_id )
		{
			dialog
				.html('')
				.data('user_id', user_id);
		}

		$.post( ajaxurl, {
			action: 'awir_get_dialog',
			user_id: user_id,
			offset: 0,
			count: 25,
			_ajax_nonce: dialog_nonce
		} )
		.done( function( resp ){
			if ( !resp || !resp.success )
				return;

			for ( var i = resp.data.length-1; i>=0; i-- )
			{
				var message = resp.data[i];

				if ( dialog.children('#msg-'+message.ID).length )
					continue;

				var item = $('<li>')
					.text( message.message )
					.attr( 'id', 'msg-'+message.ID )
					.append( user_id == message.author_id ? '(incoming)' : '(outgoing)' )
					.appendTo( dialog )
			}

			if ( !load_interval )
				load_interval = setInterval( load_dialog, 5000 );
		} )
	}

	contactbox.on('click', '> *', function(){
		load_dialog( $(this).data('user_id') );
		editor.show();
		button.show();
	});

	button.click( function() {
		var message = editor.val();
		if ( !message.length )
			return false;

		$.post( ajaxurl, {
			action: 'awir_add_message',
			to: dialog.data('user_id'),
			message: message,
			_ajax_nonce: message_nonce
		} )
		.done( function( resp ){
			if ( !resp.success )
			{
				alert('Error');
				return;
			}

			editor.val('');
			load_dialog();
		} );

		return false;
	} );

	editor.hide();
	button.hide();

	box
		.append( contactbox )
		.append( '<hr>' )
		.append( dialog )
		.append( editor )
		.append( button )

	$.post( ajaxurl, {
		action: 'awir_new_messages'
	} )
	.done( function(data) {
		console.log(data);
	} );

	$.post( ajaxurl, {
		action: 'awir_unread_messages',
		user_id: 1,
		_ajax_nonce: dialog_nonce
	} )
	.done( function(data) {
		console.log(data);
	} );
} ); */

</script>

		</div><!-- #primary -->
	</div><!-- .wrap -->
</section>

<?php get_footer();
