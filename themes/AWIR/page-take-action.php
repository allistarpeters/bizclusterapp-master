<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="advocacy-policy-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
		<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

		<section class="sheet-section">

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-resourses">
			<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/advocacy_hex.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

	<div class="sheet-container take-action-sheet">
		<div class="sheet">

			<div class="membership-text-wrap sheet-text-wrap">
				<?php the_content() ?>

				<?php while ( have_rows('take_action_items') ): the_row() ?>
				<div class="take-action-item">
					<?php the_sub_field('content') ?>
				</div>
				<?php endwhile ?>

			</div>

			<?php if ( get_field('opinion_banner') ): ?>
			<div class="pages-lable action-label">
				<?php the_field('opinion_banner') ?>
			</div>
			<?php endif ?>

			<?php if ( get_field('take_action_banner') ): ?>
			<div class="pages-lable action-label">
				<?php the_field('take_action_banner') ?>
			</div>
			<?php endif ?>

		</div>
	</div>
</section>



<?php get_footer();
