<?php

get_header();

?>

	<section class="sheet-section user-sheet">

		<div class="membership-hexset-top">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5-copy.png" alt="" class="hexset-middle">
		</div>
		<div class="membership-hexset-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5.png" alt="" class="hexset-middle">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container message-board">
			<div class="sheet">

				<div id="side-menu" class="blogs-sidebar">
					<?php $current = $GLOBALS['wp_query'] ? $GLOBALS['wp_query']->get('board_tag') : '' ?>
					<div class="btn-menu btn-menu-sidebar btn-filter is-hidden-medium" id="toggle-side-btn" onclick="openSideMenu('toggle-side-btn','side-menu');"><p>Filters</p></div>

					<ul class="blogs-sidebar__list">
						<li class="blogs-sidebar__link<?php if ( !$current) echo ' active' ?>"><a href="<?= awir_message_board::url() ?>">All</a></li>
					</ul>

					<?php 
					// get `QuickStart` term by slug
					$quickStartTag = get_term_by('slug', 'quickstart-guide', 'board_tag');
					?>
					<ul class="blogs-sidebar__list">
						<li class="blogs-sidebar__link<?php if ( $current == 'quickstart-guide' ) echo ' active'?>"><a href="<?= awir_message_board::url( $quickStartTag ) ?>"><?= esc_html($quickStartTag->name)?></a></li>
					</ul>
					
					<?php
						$areas = [
							'interests' => 'Primary Areas of Interest',
							'activities' => 'Primary Professional Activities',
						];
					?>
					<?php foreach ( $areas as $key => $title ):
							if ( !($tags = awir_message_board::user_tags( null, $key )) )
								continue;
					?>
					<ul class="blogs-sidebar__list">
						<p class="blogs-sidebar__title"><?= $title ?></p>
						<?php foreach ( $tags as $tag ): ?>
						<li class="blogs-sidebar__link<?php if ( $current == $tag->slug ) echo ' active'?>"><a href="<?= awir_message_board::url( $tag ) ?>"><?= esc_html($tag->name)?></a></li>
						<?php endforeach ?>
					</ul>
					<?php endforeach ?>
				</div>

				<div class="blogs-wrap">
					<div class="blogs-content">

						<?php while ( have_posts() ): the_post() ?>

            <?php if ( !is_user_logged_in() ): ?>
              <div class="blogs-content__item <?php if ( get_field('pdf_protected') ):?> item-protected<?php endif ?>" data-id="<?php the_ID() ?>">
            <?php 	else: ?>
              <div class="blogs-content__item" data-id="<?php the_ID() ?>">
            <?php endif ?>

							<?php get_template_part('inc/list-item') ?>

							<?php if ( ($terms = get_the_terms( get_the_ID(), 'board_tag' )) && !is_wp_error( $terms ) ): ?>
							<div class="user-info user-info-interest">
								<?php foreach ( $terms as $term ) : ?>
								<div class="user-info__item user-info__item-interest">
									<a href="<?= awir_message_board::url( $term ) ?>" class="user-info__tag"><?= esc_html( $term->name ) ?></a>
								</div>
								<?php endforeach ?>
							</div>
							<?php endif ?>
						</div>

						<?php endwhile ?>

						<?php get_template_part('inc/pagination') ?>

					</div>
				</div>

			</div>
		</div>
		</section>

		<script>window.awir_views_nonce = <?=json_encode(wp_create_nonce( 'pvc-check-post' ))?>;</script>


<?php get_footer();
