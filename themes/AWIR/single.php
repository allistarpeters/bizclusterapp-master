<?php


get_header();

the_post();

?>

	<section class="sheet-section user-sheet">

		<div class="membership-hexset-top">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5-copy.png" alt="" class="hexset-middle">
		</div>
		<div class="membership-hexset-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5.png" alt="" class="hexset-middle">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container message-board">
			<div class="sheet">

				<div id="side-menu" class="blogs-sidebar internal-sidebar">
					<a href="<?= get_post_type_archive_link('post') ?>" class="blogs-sidebar__back"><span>Back</span></a>
					<div class="blogs-sidebar__social-list">
						<?php while ( have_rows('social_icons', 'options') ): the_row() ?>
						<?php if ( !get_sub_field('icon_alt') ) continue ?>
						<a href="<?php the_sub_field('url') ?>" class="blogs-sidebar__social"><img src="<?php the_sub_field('icon_alt') ?>" alt="" class="blogs-sidebar__icon"></a>
						<?php endwhile ?>
					</div>
				</div>

				<div class="blogs-wrap internal-content">
					<div class="internal-content-wrap">
						<div class="blogs-content">
							<div class="blogs-content__item">
								<p class="blogs-content__subtitle"><?= get_the_date( 'F j, Y' ) ?></p>
								<h2 class="title-text sheet__title blogs-content__title"><?php the_title() ?></h2>
								<?php if ( has_post_thumbnail() ): ?>
								<img src="<?php the_post_thumbnail_url('full') ?>" alt="" class="blogs-content__image">
								<?php endif ?>
								<div class="sheet-text-wrap blogs-text-wrap <?php if ( get_field('pdf_protected') ):?> pdf-protected<?php endif ?>">
                  <?php if ( is_user_logged_in() ): ?>
                    <div class="sheet-text-wrap<?php if ( get_field('pdf_protected') ):?> user-logged pdf_protected<?php endif ?>">
                  <?php 	else: ?>
                    <div class="sheet-text-wrap<?php if ( get_field('pdf_protected') ):?> pdf_protected<?php endif ?>">
                  <?php endif ?>
                 <div class="protection-overlay"></div>
                    <div class="protection-window">
                      <p class="protection-window__title">Terms of Use for QuickStart PA/NP Handbook</p>
                      <ul class="protection-window__list">
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>1. READ THIS:</b> This Terms of Use Agreement (“Agreement” or “Terms of Use”) is made by and between the Association of Women in Rheumatology [hereinafter "Association"], a North Carolina organization, and you, the user (“you”, “your” or “User”).</p>
                          <p class="protection-window__text">This Agreement contains the terms and conditions that govern your access and use of the QuickStart PA/NP Handbook (the “Handbook”). BY ACCESSING, DOWNLOADING, BROWSING, OR OTHERWISE USING THE HANDBOOK, YOU AGREE THAT YOU HAVE READ, UNDERSTAND, AND AGREE TO BE BOUND BY THIS AGREEMENT.</p>
                          <p class="protection-window__text">IF YOU DO NOT AGREE TO BE BOUND BY THIS AGREEMENT, DO NOT ACCESS, DOWNLOAD, REVIEW OR OTHERWISE USE THE HANDBOOK.</p>
                        </li>
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>2. ACCESS TO THE HANDBOOK.</b> You are permitted to access and use this Handbook only for so long as you are a member of the Association. Your right to access and use the Handbook shall terminate immediately upon termination of your membership in the Association</p>
                        </li>
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>3. PERSONAL AND NON-COMMERCIAL USE LIMITATION.</b> The Handbook is meant for your
                personal and non-commercial use, unless otherwise specified in writing. You may not use the
                Handbook for any other purpose, including any commercial purpose, without the prior express written
                permission of an authorized representative of the Association in each instance, which permission will
                be at the Association’s sole and absolute discretion.</p>
                        </li>
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>4. PROPRIETARY INFORMATION.</b> The Handbook is the copyright protected intellectual property of
                the Association. No right or title to any portion of the Handbook shall be considered transferred or
                assigned to the you at any time. Subject to all applicable laws, you agree that you will not copy,
                distribute, republish, modify, create derivative works of, or otherwise use the Handbook in any
                unauthorized way, without the prior written consent of the Association in each instance, except that you
                may print out and/or save one copy of the Handbook for your personal use only</p>
                        </li>
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>5. DISCLAIMER & LIMITATIONS ON LIABILITY.</b> YOUR USE OF THE HANDBOOK IS AT YOUR OWN RISK. THE HANDBOOK IS PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED. THE ASSOCIATION DISCLAIMS ALL WARRANTIES, INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, OR NON-INFRINGEMENT. THE ASSOCIATION DOES NOT GUARANTY THAT THE INFORMATION IN THE HANDBOOK IS ACCURATE OR THAT FOLLOWING THE ADVICE IN THE HANDBOOK WILL RESULT IN ANY PARTICULAR OUTCOME. THE ASSOCIATION, ITS EMPLOYEES, AGENTS, OFFICERS, AND DIRECTORS, WILL NOT BE LIABLE FOR ANY INCIDENTAL, DIRECT, INDIRECT, PUNITIVE, ACTUAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, OR OTHER DAMAGES, INCLUDING LOSS OF REVENUE OR INCOME, PAIN AND SUFFERING, EMOTIONAL DISTRESS, OR SIMILAR DAMAGES ARISING FROM USE OF THE HANDBOOK, EVEN IF THE ASSOCIATION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR SUCH DAMAGES WERE REASONABLY FORSEEABLE. IN NO EVENT WILL THE COLLECTIVE LIABILITY OF THE ASSOCIATION OR ITS EMPLOYEES, AGENTS, OFFICERS, AND DIRECTORS, REGARDLESS OF THE FORM OF ACTION (WHETHER IN CONTRACT, TORT, OR OTHERWISE), EXCEED THE GREATER OF $100 OR THE AMOUNT YOU HAVE PAID TO THEPage 2 ASSOCIATION FOR YOUR MEMBERSHIP. ALL CLAIMS MADE BY YOU HEREUNDER MUST BE MADE WITHIN ONE YEAR OF THE ACTION TO WHICH SUCH CLAIM RELATES OR FOREVER BE BARRED</p>
                        </li>
                        <li class="protection-window__list-item">
                          <p class="protection-window__text"><b>6. MISCELLANEOUS</b></p>
                          <p class="protection-window__text">A. If any part of this Agreement is found by a court of competent jurisdiction to be unlawful, void, or unenforceable, that part will be deemed severable and will not affect the validity and enforceability of any remaining provisions.</p>
                          <p class="protection-window__text">B. This Agreement constitutes the entire agreement among the parties relating to the subject matter hereof, and supersede all prior or contemporaneous communications and proposals, whether electronic, oral, or written between you and the Association with respect to the Handbook.</p>
                          <p class="protection-window__text">C. This Agreement shall be governed by the laws of North Carolina without regard to any conflicts of laws rules. All claims between you and the Association arising out of this Agreement shall be decided in a court of competent jurisdiction in Cumberland County, North Carolina</p>
                        </li>
                      </ul>
                      <form class="protection-window__form" action="" onsubmit="hideProtect(); return false;">
                        <input class="protection-window__checkbox" type="checkbox" id="protect" name="" value="" required/>
                          <label class="protection-window__lable" for="">Accept the agreement</label>
                          <input type="submit" class="btn btn__main protection-window__btn" value="Continue">
                      </form>
										</div>
										
									<div class="custom-pdf-preloader"></div>
									<?php the_content() ?>
								</div>

                </div>

								<div class="blogs-content__links">
									<?php if ( ($terms = get_the_terms( get_the_ID(), 'board_tag' )) && !is_wp_error( $terms ) ): ?>
									<div class="user-info user-info-interest">
										<?php foreach ( $terms as $term ) : ?>
										<div class="user-info__item user-info__item-interest">
											<a href="<?= awir_message_board::url( $term ) ?>" class="user-info__tag"><?= esc_html( $term->name ) ?></a>
										</div>
										<?php endforeach ?>
									</div>
									<?php endif ?>
									<div class="blogs-content__activity">
										<a href="javascript:void(0);" class="blogs-content__activity-vievs"><?php echo do_shortcode('[post-views]'); ?></a>
										<a href="javascript:void(0);" class="blogs-content__activity-messages"><?= get_comments_number() ?></a>
									</div>
								</div>

							</div>
						</div>
					</div>

					<?php comments_template() ?>

					<?php get_template_part('inc/related') ?>

				</div>

			</div>
		</div>
		</section>


<?php get_footer();
