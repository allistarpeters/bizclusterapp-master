<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="advocacy-policy-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline">
		<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

<section class="sheet-section">

	<div class="pages-hexset-wrap-bottom advocacy-become-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/advocacy_3_1.png" alt="" class="advocacy-policy-bott-hex">
	</div>

	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
	</div>

	<div class="sheet-container advocacy-page">
		<div id="policy" class="sheet">

			<div class="advocacy sheet-text-wrap">
				<?php the_content() ?>
			</div>

			<?php for ( $i = 0; have_rows('sections'); $i++ ): the_row() ?>

			<?php 	if ( get_sub_field('image') ): ?>
			<div class="advocacy-policy<?= $i%2 ? ' advocacy-policy-reverse' : '' ?>">
				<div class="advocacy-side1">
					<div class="advocacy sheet-text-wrap">
            		<lightbox class="map-desktop" src="<?php the_sub_field('image')?>">
					  	<img src="<?php the_sub_field('image')?>">
					</lightbox>
					<?php the_sub_field('content') ?>
					<lightbox class="map-mobile" src="<?php the_sub_field('image')?>">
					  	<img src="<?php the_sub_field('image')?>">
					</lightbox>
					</div>
				</div>
			</div>
			<?php 	else: ?>

			<div class="advocacy sheet-text-wrap">
				<?php the_sub_field('content') ?>
			</div>

			<?php 	endif ?>

			<?php endfor ?>

		</div>
	</div>
</section>



<?php get_footer();
