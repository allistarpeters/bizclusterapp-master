<?php

get_header();

the_post();

$events = tribe_get_events([
	'eventDisplay'   => 'past',
	'posts_per_page' => 100,
	//'featured'       => true,
//	'tax_query' => [
//		[
//			'taxonomy' => 'tribe_events_cat',
//			'field' => 'slug',
//			'terms' => ['national', 'local', 'global-colleagues'],
//		]
//	]
]);

?>

<!-- Pages Hero Section -->
<div class="calendar-archive">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

	<div class="pages-headline">
    <a href="<?= home_url('events/') ?>" class="main-text pages-headline__category">Meetings /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>
  <?= get_template_directory_uri() ?>
		<section class="sheet-section calendar-section">

		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set8">
		</div>

		<div class="sheet-container">
			<div class="sheet">
				<div class="meetings-list">
					<div class="meetings-calendar js-sticky">
							<div id="calendar">
								<div class="__vev_calendar-wrapper">
									<div class="cal-wrapper">
										<div class="cal-header"><div class="title">Archive</div></div>
											<div class="cal-body">
												<div class="archive-img"></div>
											</div>
									</div>
								<div class="cal-past cal-previous">
									<a href="<?=home_url('events/')?>" class="past-title">BACK TO CALENDAR</a>
								</div>
							</div>
						</div>
					</div>

					<?php foreach ( $events as $post ): setup_postdata( $post ); ?>

					<!-- Archive meeting-->
					<div class="meetings-slider-archive">
						<!--slide 1-->
						<div class="meetings-slider__slide archive1">
							<div class="meetings-slider__overlay meetings-slider__overlay-archive"></div>
							<div class="meetings-slider__wrap">
								<div class="meetings-slider__content-wrap">
									<h3 class="meetings-slider__title"><?php the_title() ?></h3>
									<p class="meetings-slider__text"><?php awir_theme::strict_excerpt() ?></p>

									<div class="meetings-slider__info">
										<p class="meetings-slider__date meetings-slider__text meetings-slider__date-line"><?php echo tribe_events_event_schedule_details() ?></p>
										<p class="meetings-slider__address meetings-slider__text meetings-slider__place-line"><?= awir_theme::event_address() ?></p>
									</div>
								</div>
							</div>
							<a href="javascript:void(0);" class="meetings-slider__link meeting-archive-btn">Full information</a>
						</div><!--// slide 1-->
						<div class="full-info">
							<div class="full-info__content-wrap">
								<div class="full-info__text-wrap">
										<?php the_content() ?>
								</div>

								<?php get_template_part('inc/event-sponsors') ?>
								</div>
							<div class="collapse">
								<p class="collapse__text">Collapse</p>
								<img src="<?= get_template_directory_uri() ?>/assets/img/icons/arrow-black.svg" alt="">
							</div>
						</div>
					</div>

					<?php endforeach ?>

				</div><!-- End meeting list -->
			</div>
		</div>
	</section>

<?php get_footer();
