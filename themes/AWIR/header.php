<?php
	get_template_part('inc/head');

	if ( awir::is_member_area() )
		get_template_part('inc/header', 'member');

	else
		get_template_part('inc/header');
