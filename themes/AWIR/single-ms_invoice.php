<?php

get_header();

the_post();
?>

<!-- Exhibitors Hero Section -->
<div class="exhibitors-header">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

	<!-- Large screen bg -->
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

</div>


<div class="pages-headline exhibitors">
	<?php if ( $parent = awir_theme::page_parent() ): ?>
	<p class="main-text pages-headline__category"><?= $parent ?></p>
	<?php endif ?>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

<section class="sheet-section advocacy-page">

	<div class="sheet-container">
		<div class="sheet">

			<div class="advocacy sheet-text-wrap">
				<?php
				global $post;

				$attr = array( 'post_id' => $post->ID );
				$scode = MS_Plugin::instance()->controller->controllers['membership_shortcode'];
				echo $scode->membership_invoice( $attr );
				?>
				<?php //the_content() ?>
			</div>

		</div>
	</div>
</section>

<?php get_footer();
