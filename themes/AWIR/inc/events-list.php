<?php
$events = tribe_get_events([
	'eventDisplay' => 'upcomming',
	'posts_per_page' => -1,
]);

if ( !$events )
	return;

$term_slug = function()
{
	$terms = wp_get_post_terms( get_the_ID(), 'tribe_events_cat' );
	if ( !$terms || is_wp_error( $terms ) )
		return '';

	return $terms[0]->slug;
};

$month = '';
$year = '';

$first = true;

foreach ( $events as $post ):

	setup_postdata( $post );

	if ( $first )
	{
		$first = false;
		if ( $term_slug() == 'national' )
			continue;
	}

	$pid = get_the_ID();

	$show_date_row = null;
	$event_month = tribe_get_start_date( null, false, 'F' );
	$event_mon = tribe_get_start_date( null, false, 'M' );
	$event_year = tribe_get_start_date( null, false, 'Y' );

	if ( $event_year != $year || $event_month != $month )
	{
		$year = $event_year;
		$month = $event_month;
		if ( $year != date('Y') )
			$show_date_row = "$month&nbsp;$year";
		else
			$show_date_row = $month;
	}

	$isAllDay = tribe_event_is_all_day($pid);
	$title = get_the_title();

	if($isAllDay === false){
		$startTime =tribe_get_start_date( $pid, false, 'Ymd\\THi00\\Z' );
		$endTime =tribe_get_end_date( $pid, false, 'Ymd\\THi00\\Z' );
		$timezone = awir_theme::get_string_event_date( do_shortcode('[tribe_formatted_event_date id='.$pid.' format="e"]') );
	}else{
		$startTime = tribe_get_start_date( $pid, false, 'Ymd' );

		$stop_date = new DateTime( tribe_get_start_date( $pid, false, 'Y-m-d H:i:s' ) );
		$stop_date->modify('+1 day');
		$endTime = $stop_date->format('Ymd');
	}

	$location = awir_theme::event_address();
	$content = get_the_content();
?>

<?php if ( $show_date_row ): ?>
<!-- Divider-->
<div class="month-divider">
	<p class="month-divider__text"><?= $show_date_row ?></p>
</div>
<?php endif ?>

<div class="meeting meeting-main meeting-<?= $term_slug() ?>" data-date="<?= tribe_get_start_date( null, false, 'Y/n/j' ) ?>">
	<div class="meeting__top">
		<div class="meeting__date">
			<p class="meeting__number"><?= tribe_get_start_date( null, false, 'j' ) ?></p>
			<p class="meeting__month"><?= $event_mon ?></p>
			<p class="meeting__add-event">
				<a target="_blank" href="http://www.google.com/calendar/event?
					action=TEMPLATE
					<?= ($isAllDay == false ? '&ctz='.$timezone : '') ?>
					&text=<?= $title ?>
					&dates=<?= $startTime.'/'.$endTime ?>
					&details=<?= sanitize_text_field( $content ) ?>
					&location=<?= $location ?>
					&trp=false
					&sprop=
					&sprop=name:" rel="nofollow">add event</a>
			</p>
		</div>
		<div class="meeting__subject">
			<div class="meeting__title"><?php the_title() ?></div>
			<div class="meeting__info">
				<p class="meetings-slider__date meetings-slider__text meetings-slider__date-line">
				<?php
				if($isAllDay === true){
					// if all day
					echo 'All Day';
				}else{
          //echo awir_theme::get_string_event_date( do_shortcode('[tribe_formatted_event_date id='.$pid.' format="g:i A T"]') );
          echo tribe_events_event_schedule_details();
				}
				?>
				</p>
				<p class="meetings-slider__address meetings-slider__text meetings-slider__place-line"><?= $location ?></p>
			</div>
		</div>
	</div>

	<div class="meeting__full-info">

		<div class="full-info__text-wrap">
			<?php the_content() ?>
		</div>

		<?php get_template_part('inc/event-sponsors') ?>

    <?php $event_meta = get_post_meta($post->ID) ?>

    <?php if ( get_field('event_additional_info') ): ?>
    <?php the_field('event_additional_info') ?>
    <?php endif ?>
		<?php if ( get_field('custom_registration_link') ): ?>
		<a href="<?= the_field('custom_registration_link') ?>" class="btn btn__register meeting__register btn__register-custom_link btn__inline" target="_blank">Register</a>
		<?php else: ?>
      <?php if($event_meta['_RTECregistrationsDisabled'][0] != 1): ?>
		    <a href="javascript:void(0);" class="btn btn__register meeting__register btn__inline" data-event_info="<?=esc_attr(json_encode(awir_theme::event_reg_data()))?>">Register</a>
		  <?php endif ?>
		<?php endif ?>

	</div>

</div>


<?php
endforeach;
