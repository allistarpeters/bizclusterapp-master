<?php
	if ( !have_rows('sponsors') )
		return
?>
<div class="full-info__text-wrap">
	<h4><?php the_field('sponsors_title') ?></h4>
</div>

<div class="sponsors">

	<?php while ( have_rows('sponsors') ): the_row(); $type = get_sub_field('type') ?>
	<div class="sponsors__item sponsors__item-<?=$type['value']?>">
		<h4><?= $type['value'] == 'other' ? get_sub_field('other_type') : esc_html($type['label']) ?></h4>
		<div class="sponsors__wrap">
			<?php the_sub_field( 'content' ) ?>
		</div>
	</div>
	<?php endwhile ?>

</div>
