<?php while ( have_rows('section_who') ): the_row() ?>
<section class="s-who">
	<div class="who-wrap">

		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/Deskt.svg" alt="" class="hex-set-who hex-set-who__3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/Mobile.svg" alt="" class="hex-set-who hex-set-who__5">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-who.svg" alt="" class="hex-set-who hex-set-who__4">

		<img src="<?= get_template_directory_uri() ?>/assets/img/home/who-bg-back.svg" alt="" class="who-bg who-bg__bott">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/who-bg-mid.svg" alt="" class="who-bg who-bg__mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/who-bg-top.png" alt="" class="who-bg who-bg__top">

		<div class="who">
			<h2 class="title-text who__title event-underline"><?php the_sub_field('title') ?></h2>
			<div class="who__info">
				<p class="main-text who__text"><?php the_sub_field('text') ?></p>
			</div>

		</div>
		<div class="who-look">
			<div class="who-look__video">

				<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-plain.svg" alt="" class="who-look__video-corners who-look__video-corners-1">
				<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-shadow.png" alt="" class="who-look__video-corners who-look__video-corners-2">
				<?php if ( get_sub_field('video') ): ?>
				<!-- Responsive iFrame -->
				<div class="flexible-container who-look__placeholder">
					<?php the_sub_field('video') ?>
					<!-- <iframe src="https://fast.wistia.net/embed/iframe/26sk4lmiix?dnt=1#?secret=ZXzEbPmfjn" frameborder="0" style="border:0"></iframe> -->
				</div>
				<?php elseif ( ($img_id = get_sub_field('fallback_image')) && ($image = awir_theme::image_url($img_id, 'video_fallback')) ): ?>
				<img src="<?= $image ?>" alt="" class="who-look__placeholder">
				<?php endif ?>

				<!-- <a href="javascript:void(0)" class="who-look__play"><img src="<?= get_template_directory_uri() ?>/assets/img/home/play-btn.svg" alt="" class="who-look__play"></a> -->

			</div>
		</div>
	</div>

</section><!-- //s-who -->
<?php endwhile ?>