<?php 
$events = tribe_get_events([
	'eventDisplay' => 'upcomming',
	'posts_per_page' => 1,
	'tax_query' => [
		[
			'taxonomy' => 'tribe_events_cat',
			'field' => 'slug',
			'terms' => 'national',
		]
	],
]);

if($events && count($events) > 0) :
	foreach ( $events as $post ):
		setup_postdata( $post );
		$pid = get_the_ID();
?>
<!--	Events Section	-->
<section class="s-events">
	<div class="event-wrap">

		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-events.svg" alt="" class="hex-set-events hex-set-events__1">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-events-2.svg" alt="" class="hex-set-events hex-set-events__2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-events.svg" alt="" class="hex-set-events hex-set-events__3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-events-3.svg" alt="" class="hex-set-events hex-set-events__4">

		<img src="<?= get_template_directory_uri() ?>/assets/img/home/event-bg3.svg" alt="" class="event-bg event-bg__bott">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/event-bg2.svg" alt="" class="event-bg event-bg__mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/event-bg1.png" alt="" class="event-bg event-bg__top">


		<div class="hone-event">
			<h2 class="title-text hone-event__title event-underline">Upcoming Events</h2>
			<h3 class="title-text hone-event__subtitle"><?= get_the_title() ?></h3>
			<div class="hone-event__info">
				<p class="main-text hone-event__place"><span><?= esc_html( awir_theme::event_address( null, 'full') )?></span></p>
				<p class="main-text hone-event__date"><span><?= tribe_events_event_schedule_details() ?></span></p>
			</div>
			<?php $url = get_sub_field('lerm_more_url') ?: home_url('/events/').'#event-'.$pid ?>
			<a href="<?= $url ?>" class="btn btn__main btn__main-on_dark hone-event__btn"><span class="btn__text">Learn More</span></a>

		</div>
		<?php
			if ( $img_id = get_sub_field('image') )
				$image = awir_theme::image_url( $img_id, 'event_homepage');
			elseif ( has_post_thumbnail() )
				$image = get_the_post_thumbnail_url( $pid, 'event_homepage' );
			else
				$image = '';
		?>
		<?php if ( $image ): ?>
		<div class="event-look">
			<div class="event-look__photo-wrap">
				<svg class="event-look__svg-mask" version="1.1" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 519.6152422706632 600" style="filter: drop-shadow(rgba(0, 0, 0, 0.3) 0px 12px 12px);">
					 <defs>
						 <clipPath id="shape">
							 <path fill="#fff" d="M259.8076211353316 0L519.6152422706632 150L519.6152422706632 450L259.8076211353316 600L0 450L0 150Z"></path>
						 </clipPath>
					 </defs>
					 <image height="100%" width="100%" xlink:href='<?= $image ?>' class='photo_rectangle_inverse'/>
				 </svg>
			</div>
		</div>
		<?php endif ?>
	</div>
</section><!-- //s-events -->
<?php endforeach ?>
<?php endif ?>
<?php wp_reset_postdata() ?>
