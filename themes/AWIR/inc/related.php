<?php
$ids = (array)get_crp_posts_id();

if ( !$ids )
	return;


$ids = array_map( function( $id ){
	if ( is_object( $id ) )
		$id = $id->ID;

	return $id;
}, $ids);

$query = new WP_Query([
	'post__in' => $ids,
	'ignore_sticky_posts' => true,
	'post_type' => 'any',
	'post_status' => 'publish',
	'posts_per_page' => 3,
]);

if ( !$query->have_posts() )
	return;

?>
<h3 class="related__main-title">Related Articles</h3>
<div class="related">
	<?php for ( $i = 0; $query->have_posts() && $i < 3; $i++ ): $query->the_post() ?>
	<div class="related__item">
		<?php if ( has_post_thumbnail() ): ?>
		<img src="<?php the_post_thumbnail_url('related_thumb') ?>" alt="" class="related__image">
		<?php else: ?>
		<img src="<?= get_template_directory_uri() ?>/customs/article-mask.png" alt="" class="related__image">
		<?php endif ?>
		<a href="<?php the_permalink() ?>"><p class="related__title"><?php the_title() ?></p></a>
		<p class="related__text"><?php awir_theme::the_excerpt( 120 ) ?></p>
	</div>
	<?php endfor ?>
</div>