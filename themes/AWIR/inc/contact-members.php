<?php for ( $i = 0; have_rows('members'); $i++ ): the_row() ?>

<div class="member member-centered contactor">
	<div class="member__photo photo-back-hex<?= $i % 2 ? '-second' : '' ?>">
		<img src="<?= awir_theme::image_url( get_sub_field('image'), 'avatar') ?>" alt="AWIR" class="member__photo-img">
	</div>
	<div class="member__info">
		<div class="member__data">
			<p class="main-text member__name"><?php the_sub_field('name') ?></p>
			<p class="main-text member__degree"><?php the_sub_field('degree') ?></p>
		</div>
	</div>
</div>

<?php endfor ?>
