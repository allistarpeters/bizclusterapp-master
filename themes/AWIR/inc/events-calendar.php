<?php
$events = tribe_get_events([
        'eventDisplay' => 'upcomming',
        'posts_per_page' => -1,
    ]);
    $rez = [];
    $term_slug = function( $id )
    {
        $terms = wp_get_post_terms( $id, 'tribe_events_cat' );
        if ( !$terms || is_wp_error( $terms ) )
            return '';
        return $terms[0]->slug;
    };
    foreach ( $events as $post )
    {
        $rez[] = [
            'id' => $post->ID,
            'title' => $post->post_title,
            'date' => tribe_get_start_date( $post->ID, false, 'Y/n/j' ),
            'date_end' => tribe_get_end_date( $post->ID, false, 'Y/n/j' ),
            'category' => $term_slug( $post->ID ),
        ];
    }
?>

<div class="meetings-calendar js-sticky">
    <div id="calendar">
      <vue-event-calendar :events="activeEvents" @day-changed="handleDayChanged">
      </vue-event-calendar>

      <div class="cal-legend" v-if="eventCategoryList.length > 1">
        <form action="">
          <ul class="legend-list">
          <li v-if="eventCategoryList.indexOf('national') !== -1" class="legend-item legend-item--national">
            <li class="legend-item legend-item--national">
              <input v-model="filterNational" type="checkbox" id="filter-national" name="calendarFilter" value="national">
              <label for="filter-national">National</label>
            </li>
            <li v-if="eventCategoryList.indexOf('global-colleagues') !== -1" class="legend-item legend-item--global">
              <input v-model="filterGlobal" type="checkbox" id="filter-global" name="calendarFilter" value="global">
              <label for="filter-global">Global (Colleagues)</label>

     		<li v-if="eventCategoryList.indexOf('summits') !== -1" class="legend-item legend-item--summits">
              <input v-model="filterSummits" type="checkbox" id="filter-summits" name="calendarFilter" value="summits">
              <label for="filter-summits">Summits</label>

     		<li v-if="eventCategoryList.indexOf('local') !== -1" class="legend-item legend-item--local">
              <input v-model="filterLocal"  type="checkbox" id="filter-local" name="calendarFilter" value="local">
              <label for="filter-local">Local</label>
            <li v-if="eventCategoryList.indexOf('endorsed') !== -1" class="legend-item legend-item--endorsed">
              <input v-model="filterEndorsed"  type="checkbox" id="filter-endorsed" name="calendarFilter" value="endorsed">
              <label for="filter-endorsed">Endorsed</label>
          </ul>
        </form>
      </div>

      <div class="cal-past">
        <a href="<?= awir::user_page_url('events-archive')?>" class="past-title">PAST EVENTS</a>
      </div>
    </div>
</div>
<script>
window.awir_events = <?= json_encode( $rez, JSON_PRETTY_PRINT ) ?>;
</script>
