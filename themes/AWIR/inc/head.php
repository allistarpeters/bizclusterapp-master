<!DOCTYPE html>
<html <?php //language_attributes(); ?> class="no-js home-page" onclick = "void(0)">

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">

	<script>
		window.ajaxurl = <?=json_encode(admin_url('admin-ajax.php'))?>;
		window.currentPage = <?=json_encode(awir_theme::current_page())?>;
	</script>

	<?php wp_head(); ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M8V82BJ');</script>
    <!-- End Google Tag Manager -->
  <style>
    #preloader-pane {
      width: 100vw;
      height: 100vh;
      position: fixed;
      padding: 0px;
      margin: 0px;
      top: 0px;
      left: 0px;
      background-color: #FFF;
      z-index: 999999;
      transition: opacity 0.7s linear;
      opacity: 1;
      /* display: none; */
    }
    #preloader-pane
    .preloader-wrap {
      width: 100%;
      height: 100vh;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      background-color: #FFF;
    }

    .pre_dot {
      position: relative;
      margin: 0 auto;
      height: 100px;
      width: 100px;
    }

    @media screen and (max-width: 767px) {
      .pre_dot {
        margin-bottom: 30%;
      }
    }

  </style>
</head>

<body <?php body_class(); ?>>
<div id="preloader-pane">
  <div class="preloader-wrap">
    <img class="pre_dot" src="<?= get_template_directory_uri().'/assets/img/awir_preloader.svg' ?>" alt="">
  </div>
</div>

