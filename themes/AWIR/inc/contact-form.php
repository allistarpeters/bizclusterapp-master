<?php 
$isContact = is_page('contact');
?>

<?php while ( have_rows('contact_form') ): the_row() ?>

<?php 	if (!$isContact && get_sub_field('title') ): ?>
<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_sub_field('title') ?></h2>
<?php 	endif ?>

<?php if ( !get_sub_field('banner_text') && have_rows('members') ): ?>
<div class="member-wrap">
	<?php get_template_part('inc/contact-members') ?>
</div>
<?php endif ?>


<!-- Contacts -->
<div class="contacts">
	<div class="contacts-form-side">
		<?php 	if ($isContact === true && get_sub_field('title') ): ?>
		<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_sub_field('title') ?></h2>
		<?php 	endif ?>

		<?php if ( get_sub_field('banner_text') ): ?>
			<?php get_template_part('inc/contact-members') ?>
		<?php endif ?>

		<?php if ($isContact === true && get_sub_field('email') ): ?>
		<div class="contacts-data__address data-email">
			<div class="data-address__text-wrap">
				<p class="main-text sheet__text data-address__text"><?php the_sub_field('email') ?></p>
			</div>
		</div>
		<?php endif ?>
		<div class="subscribe__form message-form">
			<?= do_shortcode( sprintf('[contact-form-7 id="%d"]', get_sub_field('form')) ) ?>
		</div>
	</div><!--// contacts-form-side-->

	<div class="contacts-data">
		<?php if($isContact === true): ?>
		<div id="contacts-support" class="contacts-support">
			<h3>Something doesn’t work?</h3>
			<div class="contacts-data__address data-email">
				<div class="data-address__text-wrap">
					<p class="main-text sheet__text data-address__text">support@awirgroup.org</p>
				</div>
			</div>
			<div class="message-form">
				<?= do_shortcode( '[contact-form-7 title="Support"]' ) ?>
			</div>
		</div>
		<?php endif; ?>

		<?php if ( get_sub_field('banner_text') ): ?>
		<div class="contacts-data__lable">
			<p class="main-text sheet__small-title contacts-data__lable-text"><?php the_sub_field('banner_text') ?></p>
		</div>
		<?php endif ?>

		<div class="contacts-data__info-box">
			<?php if ( !$isContact && get_sub_field('email') ): ?>
			<div class="contacts-data__address data-email">
				<div class="data-address__text-wrap">
					<p class="main-text sheet__text data-address__title">Email</p>
					<p class="main-text sheet__text data-address__text"><?php the_sub_field('email') ?></p>
				</div>
			</div>
			<?php endif ?>			

			<?php while ( have_rows('emails') ): the_row() ?>
			<div class="contacts-data__address data-email">
				<div class="data-address__text-wrap">
					<p class="main-text sheet__text data-address__title"><?= esc_html( get_sub_field('title') ?: 'Email' ) ?></p>
					<p class="main-text sheet__text data-address__text"><?php the_sub_field('email') ?></p>
				</div>
			</div>
			<?php endwhile ?>

		</div>

	</div><!--// contacts-data -->
</div><!--// contacts-->
<?php endwhile ?>