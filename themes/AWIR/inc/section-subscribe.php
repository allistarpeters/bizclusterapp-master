<?php $campaigns = awir_mailchimp::get_campaigns(); ?>
<!--	Subscribe Section	-->
<section class="s-subscribe">

	<div class="subscribe-wrap">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-subscribe.svg" alt="" class="subscribe-bg subscribe-bg__hex1">

		<div class="subscribe">

			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-subscribe-3.svg" alt="" class="subscribe-bg subscribe-bg__hex3">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-subscribe-4.svg" alt="" class="subscribe-bg subscribe-bg__hex4">

			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-subscribe.svg" alt="" class="subscribe-bg subscribe-bg__hex2">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/subscribe-bg-bott.svg" alt="" class="subscribe-bg subscribe-bg__bott">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/subscribe-bg-mid.svg" alt="" class="subscribe-bg subscribe-bg__mid">

			<img src="<?= get_template_directory_uri() ?>/assets/img/home/subscribe-bg-top.png" alt="" class="subscribe-bg subscribe-bg__top">

			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="previous__free-element-2">

			<h2 class="title-text subscribe__title title-underline"><?php the_field('mailchimp_block_title') ?></h2>

			<div class="subscribe__form">
				<?= do_shortcode(sprintf('[mc4wp_form id="%d"]', get_field('maichimp_form'))) ?>
			</div>

		</div>
		<div class="previous">
			<div class="hexagon">
				<div class="previous__buttons">
					<p class="previous__title"><?php the_field('previous_newsletters_title') ?></p>
					<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-shadow-2.png" alt="" class="previous__free-element-1">
					<?php
						$i = 0;
						foreach ( $campaigns as $campaign )
						{
							$i++;
					?>
					<a href="<?=esc_url($campaign['url'])?>" class="btn btn__main btn__main-previous" target="_blank"><?= esc_html( $campaign['title'] ) ?></a>
					<?php
							if ( $i >= 4 )
								break;
						} ?>
					<a href="<?= awir::user_page_url('newsletter/archive') ?>" class="previous__cta"><?php the_field('archive_link_title') ?></a>

				</div>
				<span></span>
			</div>

		</div>

	</div>
</section><!-- //s-subscribe -->
