<?php while ( have_rows('downloads') ): the_row() ?>

	<?php if ( get_sub_field('title') ): ?>
	<div class="sheet-text-wrap exhibitors-text-wrap">
		<h1>Downloads</h1>
	</div>
	<?php endif ?>

	<div class="downloads">

	<?php for ( $i = 0; have_rows('files'); $i++ ): the_row() ?>

		<?php if ( $i % 2 == 0 ): ?>
		<div class="downloads__item-wrap">
		<?php endif ?>
			<div class="downloads__item">
				<a href="<?php the_sub_field('file') ?>" class="btn btn__main btn__download" target="_blank"><?php the_sub_field('title') ?>
					<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/btn-blur.svg" alt="" class="downloads__btn-blur">
				</a>
				<p><?php the_sub_field('description') ?></p>
			</div>
		<?php if ( $i % 2 ): ?>
		</div>
		<?php endif ?>

	<?php endfor ?>

	<?php if ( $i % 2 ): ?>
			<div class="downloads__item"></div>
		</div>
	<?php endif ?>

	</div>

	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg4.png" alt="" class="downloads-side-bg">

<?php endwhile ?>
