<?php
if( !have_rows('leaders') )
	return;

?>

<div class="local-leaders">
	<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_field('leaders_title')?></h2>
	<h2 class="title-text sheet__medium-title local-leaders__medium-title"><?php the_field('leaders_sub_title')?></h2>

	<div class="local-places" >
		<div class="places-wrap">

			<?php while ( have_rows('leaders') ): the_row() ?>
            <?php $lat_lng = ' data-lat="'.get_sub_field('lat').'" data-lng="'.get_sub_field('lng').'"' ?>
            <?php $countries = get_field_object('field_5cd1a46735714');
            $cur_code = get_sub_field('country');
            $cur_coutry = $countries['choices'][$cur_code]; ?>
			<div class="place type-chapter" data-label="<?php echo $cur_coutry ?>" data-country="<?php the_sub_field('country') ?>" <?php echo $lat_lng ?>>
                <img src="<?php the_sub_field('logo')?>" alt="" class="partners__logo partner__logo-r pull-left global-partners-logo">
				<h2 class="title-text sheet__medium-title"><?php the_sub_field('title') ?></h2>
				<p class="main-text sheet__text sheet__text place__text"><?php the_sub_field('sub_title') ?></p>
				<div class="place__mails">
                    <?php
                    $url = get_sub_field('url');
                    if($url){
                        echo "<a href='{$url}' class='main-text sheet__text place__link'>".get_sub_field('contact_person')."</a>";
                    }
                    ?>
                    <?php
                    $mail = get_sub_field('email');
                    if($mail){
                        echo "<a href='mailto:{$mail}' class='main-text sheet__text place__mail'>".get_sub_field('contact_person')."</a>";
                    }
                    ?>
                    <?php
						while ( have_rows('contacts') ): the_row()
					?><a href="mailto:<?php the_sub_field('email') ?>" class="main-text sheet__text place__mail"><?php the_sub_field('name') ?></a><?php
						endwhile ?>

				</div>
			</div>
			<?php endwhile ?>

            <h2 class="title-text title-underline title-underline-left sheet__title-main margins">Global Partners</h2>

            <?php while ( have_rows('partners') ): the_row() ?>
                <?php $lat_lng = ' data-lat="'.get_sub_field('lat').'" data-lng="'.get_sub_field('lng').'"' ?>
                <?php $countries = get_field_object('field_5cd1a46735714');
                $cur_code = get_sub_field('country');
                $cur_coutry = $countries['choices'][$cur_code]; ?>
			<div class="place type-partner" data-label="<?php echo $cur_coutry ?>" data-country="<?php the_sub_field('country') ?>"  <?php echo $lat_lng ?>>
                <img src="<?php the_sub_field('logo')?>" alt="" class="partners__logo partner__logo-r pull-left global-partners-logo">
				<h2 class="title-text sheet__medium-title"><?php the_sub_field('title') ?></h2>
				<p class="main-text sheet__text sheet__text place__text"><?php the_sub_field('sub_title') ?></p>
				<div class="place__mails">
                    <?php
                    $url = get_sub_field('url');
                    if($url){
                        echo "<a href='{$url}' class='main-text sheet__text place__link'>".get_sub_field('contact_person')."</a>";
                    }
                    ?>
                    <?php
                    $mail = get_sub_field('email');
                    if($mail){
                        echo "<a href='mailto:{$mail}' class='main-text sheet__text place__mail'>".get_sub_field('contact_person')."</a>";
                    }
                    ?>
                    <?php
						while ( have_rows('contacts') ): the_row()
					?><a href="mailto:<?php the_sub_field('email') ?>" class="main-text sheet__text place__mail"><?php the_sub_field('name') ?></a><?php
						endwhile ?>

				</div>
			</div>
			<?php endwhile ?>

		</div>

		<div class="select-map global" data-sticky-container>
			<div class="js-sticky" data-sticky-wrap>
                <div class="mapcontainer">
                    <div class="map">world map</div>
                    <div class="pri-tooltip">
                        <span class="text"></span>
                        <span class="corner"></span>
                    </div>
                </div>
                <div class="map-controls">
                    <button id="zoom-out" class="zoom-button">-</button>
                    <span class="current-zoom"></span>
                    <button id="zoom-in" class="zoom-button">+</button>
                </div>
            </div>
		</div>

	</div>

</div>
