<?php
$profile = new awir_profile();
?>
<div class="register-popup">
	<div class="register-popup__window">
		<div class="close-btn">
			<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
		</div>
		<div class="meeting meeting-open meeting-popup meeting-part">
			<div class="meeting__top">
				<div class="meeting__date">
					<p class="meeting__number">12</p>
					<p class="meeting__month">April</p>
				</div>
				<div class="meeting__subject">
					<div class="meeting__title">Immunology Bootcamp</div>
					<div class="meeting__info">
						<p class="meetings-slider__date meetings-slider__text meetings-slider__date-line">
							<img src="<?= get_template_directory_uri() ?>/assets/img/icons/meeting-date.svg"  class="meetings-slider__info-icon meetings-slider__info-icon-clock" alt="AWIR">
							<span class="meeting__time">4:30 PM PST</span>
						</p>
						<p class="meetings-slider__address meetings-slider__text">
							<img src="<?= get_template_directory_uri() ?>/assets/img/icons/meeting-place.svg"  class="meetings-slider__info-icon meetings-slider__info-icon-place" alt="AWIR">
							<span class="meeting__place">San Francisco, Ca</span>
						</p>
					</div>
				</div>
			</div>

			<div class="meeting__content">
				<div class="meeting__success">
					<div class="success-hex success-hex-1"></div>
					<div class="success-hex success-hex-2"></div>
					<div class="success-hex success-hex-3"></div>
					<div class="success-hex success-hex-4"></div>
					<div class="success-hex success-hex-5"></div>
					<div class="success-img"></div>
					<p>You have successfully<br>registered for this event</p>
				</div>

				<div class="meeting__error">
					<div class="error-hex error-hex-1"></div>
					<div class="error-hex error-hex-2"></div>
					<div class="error-hex error-hex-3"></div>
					<div class="error-hex error-hex-4"></div>
					<div class="error-hex error-hex-5"></div>
					<div class="error-img"></div>
					<p><strong>Error</strong><br>Please try again</p>
				</div>

				<form action="" class="subscribe__form message-form" method="post">
					<input type="hidden" name="action" value="rtec_process_form_submission" />
					<?php echo wp_nonce_field( 'rtec_form_nonce', '_wpnonce', true, false ); ?>
					<input type="hidden" name="rtec_email_submission" value="1" />
					<input type="hidden" name="rtec_event_id" value="" id="rtec_event_id"/>

					<div class="subscribe__input-wrap">
						<input type="text" placeholder="Your name" autocomplete="nope" required name="rtec_first" value="<?=esc_attr($profile->name)?>" />
					</div>

					<div class="subscribe__input-wrap">
						<input type="email" placeholder="Email address" autocomplete="nope" required name="rtec_email" value="<?=esc_attr($profile->email)?>">
					</div>

					<div class="subscribe__input-wrap">
						<input type="tel" placeholder="Phone (Format: 000-000-0000)" autocomplete="nope" required name="rtec_other" value="<?=esc_attr($profile->phone)?>" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
					</div>

					<button type="submit" class="btn btn-centered btn__main btn__register reg-none"><span class="btn__text reg-none">Registration</span></button>
				</form>

				<button id="reg-finish" type="submit" class="btn btn-centered btn__main btn__register reg-success"><span class="btn__text">Thank you!</span></button>
				<button id="reg-reset" type="submit" class="btn btn-centered btn__main btn__register reg-error"><span class="btn__text">Try again</span></button>

			</div>
		</div>

	</div>
</div>
