<?php
	$links = paginate_links([
		'type' => 'array',
	]);

	if ( !$links )
		return;
?>
<div class="pagination">
	<nav data-pagination>
		<p class="title">Pages</p>
		<?= paginate_links([
			'type' => 'list',
			'next_text' => '',
      'prev_text' => '',
      'mid_size' => '1',
			]) ?>
	</nav>
</div>
