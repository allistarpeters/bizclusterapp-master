<?php
if ( !function_exists('get_field') || !have_rows('sections') )
	return;

	echo '!!!';

while ( have_rows('sections') )
{
	the_row();
	$type = get_sub_field('section_type');

	$classes = ['acf-section', 'acf-section-'.$type];
	if ( $class = get_sub_field('css_class') )
		$classes[] = $class;
	$classes = array_map('esc_attr', $classes);
	$classes = implode(' ', $classes);

	$id = (string)get_sub_field('css_id');
	?>
<div class="<?= $classes ?>"<?php if ( $id ): ?> id="<?=esc_attr($id)?>"<?php endif ?>>
	<?php if ( $type == 'media' ): ?>
	<div>
		<?php if ( get_sub_field('title') ): ?>
		<h1><?= the_sub_field('title') ?></h1>
		<?php endif ?>
		<?php if ( get_sub_field('content') ): ?>
		<div>
			<?php the_sub_field('content') ?>
		</div>
		<?php endif ?>
		<?php if ( get_sub_field('button_text') ): ?>
		<a class="button" href="<?php the_sub_field('button_url') ?>"><?php the_sub_field('button_text')?></a>
		<?php endif ?>
		<?php if ( get_sub_field('below_button') ): ?>
		<div>
			<?php the_sub_field('below_button') ?>
		</div>
		<?php endif ?>
	</div>
	<div>
		<?php if ( get_sub_field('media_type') == 'image' && get_sub_field('image') ): ?>
		<img src="<?php the_sub_field('image') ?>" />
		<?php elseif ( get_sub_field('media_type') == 'video' ): ?>
			<?php the_sub_field('video') ?>
		<?php endif ?>
	</div>
	<?php else: ?>
	<?php 	if ( get_sub_field('title') ): ?>
	<h1><?php the_sub_field('title') ?></h1>
	<?php 	endif; ?>
	<?php 	if ( get_sub_field('content') ): ?>
	<div>
		<?php the_sub_field('content') ?>
	</div>
	<?php 	endif;?>
	<?php 	if ( have_rows('icons') ): ?>
	<div>
		<?php while ( have_rows('icons') ): the_row() ?>
		<div class="<?php the_sub_field('icons-size') //'half'/'third' ?>">
			<a href="<?= esc_attr(get_sub_field('icon_link'))?>">
				<?php the_sub_field('icon') ?>
				<?php if ( get_sub_field('title') ): ?>
				<h5><?php the_sub_field('title') ?></h5>
				<?php endif ?>
			</a>
			<?php if ( get_sub_field('description') ): ?>
			<div>
				<?php the_sub_field('description') ?>
			</div>
			<?php endif ?>
			<?php if ( $url = get_sub_field('url') ): ?>
			<div>
				<a href="<?= esc_attr( $url ) ?>"><?= esc_html($url) ?></a>
			</div>
			<?php endif ?>
		</div>
		<?php endwhile ?>
	</div>
	<?php 	endif;?>

	<?php if ( get_sub_field('button_text') ): ?>
	<a class="button" href="<?php the_sub_field('button_url') ?>"><?php the_sub_field('button_text')?></a>
	<?php endif ?>
	<?php if ( get_sub_field('below_button') ): ?>
	<div>
		<?php the_sub_field('below_button') ?>
	</div>
	<?php endif ?>

	<?php endif ?>
</div>
	<?php
}