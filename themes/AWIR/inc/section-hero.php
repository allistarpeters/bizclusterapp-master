<?php while ( have_rows('section_hero') ): the_row() ?>
<!--	Hero Section	-->
<section class="s-hero">
	<div class="container">
		<div class="main-headline">
			<h1 class="title-text main-headline__title"><?php the_sub_field('title') ?></h1>
			<p class="main-text main-headline__subtitle"><?php the_sub_field('sub_title') ?></p>

		<a href="<?= get_permalink( get_page_by_path( 'covid19' ) )?>" class="btn-covid"><span class="btn-text">Click Here for More Information <br>on COVID-19</span></a>


        <?php if ( !is_user_logged_in() ): ?>
          <a href="<?= get_sub_field('button_url') ?: awir::user_page_url('register')?>" class="btn btn__main btn__main-headline"><span class="btn__text">Register</span></a>
        <?php 	else: ?>
          <a href="<?= awir::user_page_url('memberships')?>" class="btn btn__main btn__main-headline"><span class="btn__text">Register</span></a>
        <?php endif ?>

			<p class="main-text main-headline__more">Learn more about<a href="<?= get_sub_field('benefits_url') ?: awir::user_page_url('become_member') ?>" class="main-headline__more-link">Membership benefits</a></p>
		</div>
		<!-- //main-headline -->

		<!-- <div class="hero-imgset">
			<img srcset="<?= get_template_directory_uri() ?>/assets/img/home/hero-img.png 1x,
										<?= get_template_directory_uri() ?>/assets/img/home/hero-img@2x.png 2x"
										src="<?= get_template_directory_uri() ?>/assets/img/home/hero-img.png"
										alt="AWIR Association of Women in Rheumatology" class="hero-imgset__front">

			<img srcset="<?= get_template_directory_uri() ?>/assets/img/home/hero-back-img.png 1x,
										<?= get_template_directory_uri() ?>/assets/img/home/hero-back-img@2x.png 2x"
										src="<?= get_template_directory_uri() ?>/assets/img/home/hero-back-img.png"
										alt="AWIR Association of Women in Rheumatology"
										class="hero-imgset__back">


			<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex1.svg" alt="" class="hero-imgset__hex1">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex2.svg" alt="" class="hero-imgset__hex2">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hero-imgset__hex3">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-texture1.png" alt="" class="hero-imgset__hex4">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-texture1.png" alt="" class="hero-imgset__hex5">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hero-imgset__hex6">

		</div> -->

		<!-- New COVID start-->
		<div class="new-hero-imgset">

			<img srcset="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hero-back-img.png 1x,
										<?= get_template_directory_uri() ?>/assets/img/home/new/new-hero-back-img@2x.png 2x"
										src="<?= get_template_directory_uri() ?>/assets/img/new/new-home/hero-back-img.png"
										alt="AWIR Association of Women in Rheumatology"
										class="new-hero-imgset__back">


			<a href="<?= get_permalink( get_page_by_path( 'covid19' ) )?>">
			<img srcset="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hero-img.png 1x,
										<?= get_template_directory_uri() ?>/assets/img/home/new/new-hero-img@2x.png 2x"
										src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hero-img.png"
										alt="AWIR Association of Women in Rheumatology" class="new-hero-imgset__front">
										</a>
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex1.svg" alt="" class="new-hero-imgset__hex1">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex2.svg" alt="" class="new-hero-imgset__hex2">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex-main.svg" alt="" class="new-hero-imgset__hex3">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex-main-texture1.png" alt="" class="new-hero-imgset__hex4">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex-main-texture2.png" alt="" class="new-hero-imgset__hex5">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="new-hero-imgset__hex6">
			<img src="<?= get_template_directory_uri() ?>/assets/img/home/new/new-hex-main1.svg" alt="" class="new-hero-imgset__hex7">
			<p class="hext-text">Click Here for <br>More Information <br>on COVID-19</p>

		</div>
	</div>
	<!-- New COVID end-->

</section><!-- //s-hero -->
<?php endwhile ?>
