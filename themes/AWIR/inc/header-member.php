<div class="user-page">
	<?php get_template_part('inc/header') ?>
</div>
<!-- User-pages background -->
<div class="user-pages-header">
	<div class="pages-hexset-wrap-top user-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/my-profile/group-15-copy-25.png" alt="" class="user-set1">
		<img src="<?= get_template_directory_uri() ?>/assets/img/my-profile/group-15-copy-25.png" alt="" class="user-set2">
	</div>
	<img src="<?= get_template_directory_uri() ?>/assets/img/contact/contact_2.png" alt="" class="user-pages-header__bg user-pages-header__bg-bot">
	<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="user-pages-header__bg user-pages-header__bg-mid">
	<img src="<?= get_template_directory_uri() ?>/assets/img/my-profile/bg_blog-top.png" alt="" class="user-pages-header__bg user-pages-header__bg-top">
</div>
<!-- End user-pages background -->

<?php
	$user_id = apply_filters('header_user_id', get_current_user_id() );
?>
<?php if ( !$user_id || !($user = get_userdata( $user_id )) ): ?>

  <div class="pages-headline exhibitors">
    <!-- <a href="http://192.168.31.128:3306/events/" class="main-text pages-headline__category" data-wpel-link="internal">Meetings /</a> -->
		<h1 class="title-text pages-headline__title">Meeting Content</h1>
	</div>

<?php else: ?>

<div class="user-wrapper user-<?= $user->ID ?>">
	<div class="user-top">
		<img src="<?= awir_theme::avatar( $user->ID ) ?>" alt="" class="user-photo">
		<div class="user-data">
			<h1 class="user-data__name"><?= esc_html($user->display_name) ?></h1>
			<div class="user-data__member">Member since: <?= awir::member_since( $user->ID ) ?></div>
			<?php /*
			<div class="user-data__tags-wrap">
				<?php $i = 0; foreach ( awir_message_board::user_tags( $user->ID ) as $tag ): if ( $i >= 4 ) break; ?>
				<a href="<?= awir_message_board::url( $tag ) ?>" class="user-data__tag"><?= esc_html($tag->name)?></a>
				<?php $i++; endforeach; ?>
			</div>
			*/ ?>
		</div>
	</div>

	<div class="profile-menu">
		<a id="profile-menu" href="javascript:void(0);" class="user-menu__link profile-menu__button">My Profile</a>
	</div>
	<div id="user-menu"  class="user-menu">
		<div class="user-menu__content">
			<a href="<?= awir_message_board::url() ?>" class="user-menu__link<?php if ( is_post_type_archive('board') || is_singular('board') ) echo ' page-active' ?>">Message Board</a>
			<a href="<?= get_post_type_archive_link('post') ?>" class="user-menu__link<?php if ( awir::is_meeting_content() ) echo ' page-active' ?>">Meeting Content</a>
			<a href="<?= awir::user_page_url('messages') ?>" class="user-menu__link<?php if ( is_page('messages') ) echo ' page-active' ?><?php if ( awir::have_messages() ) echo ' new-messages-marker' ?>">Direct Messages</a>
		</div>
		<div class="user-menu__profile">
			<a href="<?= awir::user_page_url() ?>" class="user-menu__link<?php if ( is_page('user') && $user->ID == get_current_user_id() ) echo ' page-active' ?>">My Profile</a>
		</div>
	</div>
</div>

<?php endif ?>
