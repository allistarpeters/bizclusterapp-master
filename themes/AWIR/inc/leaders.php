<?php
if( !have_rows('leaders') )
	return;

?>

<div class="local-leaders">
	<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_field('leaders_title')?></h2>
	<h2 class="title-text sheet__medium-title local-leaders__medium-title"><?php the_field('leaders_sub_title')?></h2>

	<div class="local-places" >
		<div class="places-wrap">
			<?php while ( have_rows('leaders') ): the_row() ?>
			<div class="place" data-state="<?php the_sub_field('state') ?>">
				<h2 class="title-text sheet__medium-title"><?php the_sub_field('title') ?></h2>
				<p class="main-text sheet__text sheet__text place__text"><?php the_sub_field('sub_title') ?></p>
				<div class="place__mails">
					<a href="mailto:<?php the_sub_field('email') ?>" class="main-text sheet__text place__mail"><?php the_sub_field('contact_person') ?></a><?php
						while ( have_rows('contacts') ): the_row()
					?><a href="mailto:<?php the_sub_field('email') ?>" class="main-text sheet__text place__mail"><?php the_sub_field('name') ?></a><?php
						endwhile ?>
				</div>
			</div>
			<?php endwhile ?>

		</div>

		<div class="select-map" data-sticky-container>
			<div class="js-sticky" data-sticky-wrap>
        <?php include get_template_directory().'/inc/map.svg' ?>
      </div>
		</div>

	</div>

</div>
