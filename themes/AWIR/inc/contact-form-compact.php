<?php 
$isContact = is_page('contact');
?>

<?php while ( have_rows('contact_form') ): the_row() ?>

<?php 	if (!$isContact && get_sub_field('title') ): ?>
<h2 class="title-text title-underline title-underline-left sheet__title"><?php the_sub_field('title') ?></h2>
<?php 	endif ?>

<?php if ( !get_sub_field('banner_text') && have_rows('members') ): ?>
<div class="member-wrap">
	<?php get_template_part('inc/contact-members') ?>
</div>
        <div class="contacts-data__info-box no-padding">
            <?php if ( !$isContact && get_sub_field('email') ): ?>
                <div class="contacts-data__address data-email no-margin">
                    <div class="data-address__text-wrap">
                        <p class="main-text sheet__text data-address__title">Email</p>
                        <p class="main-text sheet__text data-address__text"><?php the_sub_field('email') ?></p>
                    </div>
                </div>
            <?php endif ?>

            <?php while ( have_rows('emails') ): the_row() ?>
                <div class="contacts-data__address data-email">
                    <div class="data-address__text-wrap">
                        <p class="main-text sheet__text data-address__title"><?= esc_html( get_sub_field('title') ?: 'Email' ) ?></p>
                        <p class="main-text sheet__text data-address__text"><?php the_sub_field('email') ?></p>
                    </div>
                </div>
            <?php endwhile ?>

        </div>
<?php endif ?>


<!-- Contacts -->
<div class="contacts no-margin full-width">

    <?= do_shortcode( sprintf('[contact-form-7 id="%d"]', get_sub_field('form')) ) ?>


</div><!--// contacts-->
<?php endwhile ?>