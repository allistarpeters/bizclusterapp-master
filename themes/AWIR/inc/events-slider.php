<?php
	//Slider

$events = tribe_get_events([
	'eventDisplay' => 'upcomming',
	'posts_per_page' => 10,
	//'start_date' => date( 'Y-m-d' ),
	//'featured'       => true,
	'tax_query' => [
		[
			'taxonomy' => 'tribe_events_cat',
			'field' => 'slug',
			'terms' => 'national',
		]
	],
]);

if ( !$events )
	return;

?>
<!--Slider-->
<div id="meeting-slider" class="meetings-slider">

	<?php $i=0; foreach ( $events as $post ): setup_postdata( $post ); $i++ ?>

	<!--slide 1-->
	<div class="meetings-slider__slide" <?php if ( has_post_thumbnail() ): ?> style="background-image: url(<?=get_the_post_thumbnail_url(null,'full')?>);"<?php endif ?> >
		<div class="meetings-slider__overlay"></div>
		<div class="meetings-slider__wrap">
			<div class="meetings-slider__content-wrap">
				<h3 class="meetings-slider__title"><?php the_title() ?></h3>
				<p class="meetings-slider__text"><?php awir_theme::strict_excerpt() ?></p>

				<div class="meetings-slider__info">
					<p class="meetings-slider__date meetings-slider__text meetings-slider__date-line"><?php echo tribe_events_event_schedule_details() ?></p>
					<p class="meetings-slider__address meetings-slider__text meetings-slider__place-line"><?= awir_theme::event_address() ?></p>
					<?php if ( $email = get_field('event_email') ): ?>
					<p class="meetings-slider__email meetings-slider__text meetings-slider__email-line">
						<a href="mailto:<?=esc_attr($email)?>"><?=esc_html($email)?></a>
					</p>
					<?php endif ?>
				</div>
			</div>
		</div>
		<a href="javascript:void(0);" class="meetings-slider__link slider-open-info">Full information</a>
	</div>

	<?php endforeach ?>
	<?php foreach ( $events as $post ): setup_postdata( $post ); ?>

	<div class="full-info">
		<div class="full-info__content-wrap">
			<?php if ( get_field('custom_registration_link') ): ?>
			<a href="<?= the_field('custom_registration_link') ?>" class="btn btn__register meeting__register btn__register-custom_link full-info__register btn__inline" target="_blank">Register</a>
			<?php else: ?>
			<a href="javascript:void(0);" class="btn btn__register meeting__register full-info__register btn__inline" data-event_info="<?=esc_attr(json_encode(awir_theme::event_reg_data()))?>">Register</a>
			<?php endif ?>

      <?php if ( get_field('custom_pdf_file') ): ?>
        <a href="<?= the_field('custom_pdf_file') ?>" class="btn btn__register meeting__register btn__register-custom_link full-info__register btn__inline no-margin" target="_blank"><?php the_field('pdf_button_text'); ?></a>
      <?php endif ?>


			<div class="full-info__divider"></div>

			<?php get_template_part('inc/event-content', 'slider') ?>

			<div class="full-info__text-wrap">
				<?php the_content() ?>
			</div>

			<?php get_template_part('inc/event-sponsors', 'slider') ?>
      <?php if ( get_field('event_additional_info') ): ?>
        <?php the_field('event_additional_info') ?>
      <?php endif ?>
			<?php if ( get_field('custom_registration_link') ): ?>
			<a href="<?= the_field('custom_registration_link') ?>" class="btn btn__register meeting__register btn__register-custom_link full-info__register btn__inline" target="_blank">Register</a>
			<?php else: ?>
			<a href="javascript:void(0);" class="btn btn__register meeting__register full-info__register slider-close-info btn__inline" data-event_info="<?=esc_attr(json_encode(awir_theme::event_reg_data()))?>">Register</a>
			<?php endif ?>

		</div>
		<div class="collapse slider-close-info">
			<p class="collapse__text">Collapse</p>
			<img src="<?= get_template_directory_uri() ?>/assets/img/icons/arrow-black.svg" alt="">
		</div>
	</div>

	<?php endforeach; ?>

	<div class="meetings-slider__nav slider-nav" <?php if ( count( $events ) < 2 ) : ?> style="display:none" <?php endif ?>>
		<div class="slider-nav__prev"><img src="<?= get_template_directory_uri() ?>/assets/img/icons/slider-naw-arrow.svg" alt="">Prev</div>
		<div class="slider-nav__dots">
			<?php for ( $i = 1; $i <= count( $events ); $i++ ): ?>
			<div class="slider-nav__dot" data-id="<?=$i?>"></div>
			<?php endfor ?>
		</div>
		<div class="slider-nav__next">Next<img src="<?= get_template_directory_uri() ?>/assets/img/icons/slider-naw-arrow.svg" alt=""></div>
	</div>

</div><!--End Slider-->

<?php foreach ( $events as $post ): setup_postdata( $post ); ?>
<!-- floating event -->
		<div class="float-event">
			<div class="float-event__wrap">
				<div class="float-event__overlay"></div>
				<div class="float-event__content">
						<h3 class="float-event__title"><?php the_title() ?></h3>
				</div>
				<a href="javascript:void(0);" class="float-event__link" data-target="sheet">Full information</a>
			</div>
		</div>
	<?php break; ?>
<?php endforeach ?>

<?php wp_reset_postdata(); ?>
