<?php while ( have_rows('section_mission') ): the_row() ?>
<!-- Mission Section	-->
<section class="s-mission">

	<div class="hex-set-wrap">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex-set1.svg" alt="" class="hex-set1">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex-set4.svg" alt="" class="hex-set4">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex-set2.svg" alt="" class="hex-set2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex-set3.svg" alt="" class="hex-set3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/home/hex-set5.svg" alt="" class="hex-set5">

	</div>

	<div class="hexset-mission-wrap">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-gray.svg" alt="" class="hexset-mission hexset-mission__1">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-gray.svg" alt="" class="hexset-mission hexset-mission__2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-border.svg" alt="" class="hexset-mission hexset-mission__3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hexset-mission hexset-mission__4">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-gray.svg" alt="" class="hexset-mission hexset-mission__5">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hexset-mission hexset-mission__6">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hexset-mission hexset-mission__7">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-gray.svg" alt="" class="hexset-mission hexset-mission__8">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-gray.svg" alt="" class="hexset-mission hexset-mission__10">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main-border.svg" alt="" class="hexset-mission hexset-mission__11">
		<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-main.svg" alt="" class="hexset-mission hexset-mission__12">
	</div>

	<div class="container">
		<div class="mission">
			<h2 class="title-text mission__title title-underline"><?php the_sub_field('title') ?></h2>
      <p class="main-text mission__text"><?php the_sub_field('text') ?></p>

      <a href="<?= awir::user_page_url('about') ?>" class="btn btn__main btn__main-mission"><span class="btn__text"><?php the_sub_field('button_text') ?></span></a>

      <div class="home-lable-wrap">
        <div class="home-lable">
          <h3 style="text-align: center;">THE ROAD TO RHEUMATOLOGY:</h3>
          <p>A Quick-Start Guide for Non-Physician Providers</p>
          <h1 style="text-align: center;">
            <?php if ( is_user_logged_in() ): ?>
              <a href="/board/quickstart-pa-np-handbook/" data-wpel-link="internal">READ</a>
            <?php 	else: ?>
              <a href="javascript:void(0);" id="sing-in-pdf" data-wpel-link="internal">Log In</a>
            <?php endif ?>
          </h1>
        </div>
    </div>

		</div>
	</div>

</section><!-- //s-mission -->
<?php endwhile ?>
