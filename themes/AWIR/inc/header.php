<header class="section s-header top-menu">

	<div class="header-container">

		<a href="<?= home_url() ?>" class="logo">
			<img class="logo__part1" src="<?= get_template_directory_uri() ?>/assets/img/logos/awir-logo.svg" alt="">
			<p class="logo__part2">Association of Women in Rheumatology</p>
		</a>

		<!-- Menu Icon for mobile -->
		<div class="btn-menu is-hidden-large" id="toggle-nav-btn" onclick="openMobileMenu('toggle-nav-btn','main-nav');">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>

		<nav class="header-nav" id="main-nav">

			<ul class="main-nav">
				<?php $menu = awir_theme::get_menu_tree('top') ?>

				<?php foreach ( $menu as $item ): ?>
				<?php 	if ( $item['children'] ): ?>

				<li onmouseover="addActive('<?=$item['slug']?>_submenu');" touchend="removeActive('<?=$item['slug']?>_submenu');" onmouseout="removeActive('<?=$item['slug']?>_submenu');" class="main-nav__link" onclick = "void(0)">
					<a class="main-nav__expander"><?=$item['title']?></a>
					<ul id="<?=$item['slug']?>_submenu" class="main-nav__second-level">
						<?php foreach ( $item['children'] as $child ): ?>
						<li class="main-nav__second-link"><a class="main-nav__for-touch nav-item-<?=$child['slug']?>" href="<?=$child['url']?>"><?=$child['title']?></a></li>
						<?php endforeach ?>
					</ul>

				</li>

				<?php 	else: ?>

				<li class="main-nav__link"><a class="nav-item-<?=$item['slug']?>" href="<?=$item['url']?>"><?=$item['title']?></a></li>

				<?php 	endif ?>
				<?php endforeach ?>
			</ul>

			<?php if ( !is_user_logged_in() ): ?>
			<ul class="pages-nav">
				<li><a id="sing-in-btn" href="javascript:void(0);" class="btn btn__nav btn__nav-transparent">Sign in</a></li>
				<li><a href="<?= awir::user_page_url('become_member') ?>" class="btn btn__nav btn__nav-white">Become a Member</a></li>
			</ul>

			<?php endif ?>
		</nav>
		<?php if ( is_user_logged_in() ): ?>
		<div class="user-nav">
			<a href="<?= awir::user_page_url('messages') ?>" class="user-nav__message user-nav__message-unread<?php if ( awir::have_messages() ) echo ' new-messages-marker' ?>"></a>
			<div class="user-nav__avatar-wrap">
				<img src="<?= awir_theme::avatar() ?>" alt="" class="user-nav__avatar">
			</div>
			<div class="user-nav__popup-menu user-popup-menu" style="display: block; opacity: 0;">
				<ul class="user-popup-menu__list">
					<li class="user-popup-menu__item user-popup-menu__item-message">
						<a href="<?= get_post_type_archive_link( 'board' ) ?>">Message Board</a>
					</li>
					<li class="user-popup-menu__item user-popup-menu__item-meeting">
						<a href="<?= get_post_type_archive_link('post') ?>">Meeting Content</a>
					</li>
					<li class="user-popup-menu__item user-popup-menu__item-direct">
					<a href="<?= awir::user_page_url('messages') ?>">Direct Messages</a>
					</li>
					<li class="user-popup-menu__item user-popup-menu__item-profile">
						<a href="<?= awir::user_page_url() ?>">My Profile</a>
					</li>
					<div class="user-popup-menu__divider"></div>
					<li class="user-popup-menu__item user-popup-menu__item-logout">
						<a href="<?= wp_logout_url() ?>">Log Out</a>
					</li>
				</ul>
			</div>
		</div>
		<?php endif ?>	</div>

</header>
