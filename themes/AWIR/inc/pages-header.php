<section class="pages-header" <?php if ( $image = get_field('page_header_image') ): ?> style="background-image:url(<?=$image?>);" <?php endif ?>>
	<div class="pages-header__overlay"></div>
	<div class="pages-header__wave"></div>
</section>

