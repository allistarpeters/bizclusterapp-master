<?php while ( have_rows('section_partners') ): the_row() ?>
<section class="s-partners">
	<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-events-4.svg" alt="" class="hex-set-partners">
	<div class="container">
			<h2 class="title-text partners__title"><?php the_sub_field('title') ?></h2>
			<div class="partners">

				<?php while ( have_rows('partners') ):
						the_row();

						if ( ($url = get_sub_field('url')) && !preg_match('~^[a-z]+://~i', $url ) )
							$url = 'http://'.$url;
				?>
				<div class="partners__item">
					<a target="_blank"<?php if ( $url ): ?> href="<?= $url ?>"<?php endif ?>><img src="<?php the_sub_field('logo')?>" alt="" class="partners__logo partner__logo-r"></a>
					<p class="main-text partners__name"><?php the_sub_field('name') ?></p>
					<p class="main-text partners__info"><?php the_sub_field('info') ?></p>
				</div>
				<?php endwhile ?>

			</div>
	</div>

</section><!-- //partners -->
<?php endwhile ?>