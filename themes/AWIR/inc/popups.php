<?php
	if ( is_user_logged_in() )
		return;
?>
<div class="popups">

	<div id="popup-singin" class="popup-main popup-main-middle">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<h2 class="popup-main__title">Sign In</h2>
					<form action="" class="subscribe__form message-form popup-main__form">
						<div class="subscribe__input-wrap">
							<input type="text" placeholder="Enter your login" name="log" required>
						</div>
						<div class="subscribe__input-wrap">
							<input type="password" placeholder="Password" autocomplete="nope" name="pwd" required>
						</div>

						<div class="subscribe__ckeck-wrap">
							<input type="checkbox" id="check" name="rememberme" value="forever">
							<label for="check">Remember me</label>
						</div>

						<button type="submit" class="btn btn-centered btn__main"><span class="btn__text">Login</span></button>
						<a href="javascript:void(0);" id="forgot" class="popup-main__forgot">Forgot password</a>

					</form>
				</div>
		</div>
		<a href="<?= awir::user_page_url('become_member') ?>" id="no-account" class="popup-main__bottom-btn">Do not have an account?</a>
	</div>

	<div id="popup-forgot" class="popup-main popup-main-middle">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<h2 class="popup-main__title">Forgot your password?</h2>
					<p class="popup-main__text">Enter your email address and we’ll send you a link to reset password</p>
					<form action="" class="subscribe__form message-form popup-main__form">
						<div class="subscribe__input-wrap">
							<input type="text" placeholder="Email address" required name="user_login">
						</div>
						<button type="submit" class="btn btn-centered btn__main"><span class="btn__text">Restore</span></button>
					</form>
				</div>
		</div>
		<a href="javascript:void(0);" id="to-singin3" class="popup-main__bottom-btn">Back to sign in</a>
	</div>

	<div id="popup-error" class="popup-main popup-main-middle popup-main-error">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<h2 class="popup-main__title">Error</h2>
					<p class="popup-main__text popup-main__text-error">Account with such email not found</p>
					<p class="popup-main__text">Please re-enter your email</p>
					<form action="" class="subscribe__form message-form popup-main__form">
						<div class="subscribe__input-wrap">
							<input type="email" placeholder="Email address" required>
						</div>
						<button type="submit" class="btn btn-centered btn__main"><span class="btn__text">Request reset link</span></button>
					</form>
				</div>
		</div>
		<a href="javascript:void(0);" id="to-singin4" class="popup-main__bottom-btn">Back to sign in</a>
	</div>

	<div id="popup-success" class="popup-main popup-main-middle popup-main-success">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<h2 class="popup-main__title">Success!</h2>
					<p class="popup-main__text">Instructions for changing the password have been sent to the specified email</p>
				</div>
		</div>
		<a href="javascript:void(0);" id="to-singin2" class="popup-main__bottom-btn">Back to sign in</a>
	</div>

<?php if ( !awir::is_member() && !awir_memberships::check_meeting_password() ): ?>
	<div id="popup-guest" class="popup-main popup-main-middle">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<h2 class="popup-main__title">Guest Area</h2>
					<p class="popup-main__text">Please enter the password below.</p>
					<form action="<?= awir::user_page_url('meeting-password') ?>" method="post" class="subscribe__form message-form popup-main__form">
						<div class="subscribe__input-wrap">
							<input type="password" placeholder="Password" name="meeting_password" required>
						</div>
						<button type="submit" class="btn btn-centered btn__main"><span class="btn__text">Go</span></button>
					</form>
				</div>
		</div>
		<a href="javascript:void(0);" class="close-popup popup-main__bottom-btn">Back</a>
	</div>

	<script>
	jQuery( function($) {
		$('.nav-item-page-meeting-content').click( function(e) {
			e.preventDefault();

			$('#popup-guest').addClass('active');
		} );
	} );
	</script>
<?php endif ?>

</div>
