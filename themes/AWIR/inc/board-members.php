<?php
if ( !have_rows('members') )
	return;
?>
<div class="members-wrap">

	<div class="awr">
		<h2>Board Members</h2>
	</div>

<?php while ( have_rows('members') ): the_row() ?>
	<div class="member">
		<div class="member__photo">
			<img src="<?= awir_theme::image_url( get_sub_field('image'), 'avatar') ?>" alt="AWIR" class="member__photo-img">
		</div>
		<div class="member__info member__info-underline">

			<div class="member__data">
				<p class="main-text member__name"><?php the_sub_field('name') ?></p>
				<p class="main-text member__degree"><?php the_sub_field('med_degree') ?></p>
				<p class="main-text member__position"><?php the_sub_field('role') ?></p>
			</div>

			<p class="main-text sheet__text member__description"><?php the_sub_field('description') ?></p>

			<?php if ( get_sub_field('text') ): ?>
			<a href="javascript:void(0);" class="main-text sheet__text member__more">Read more<span>&gt;</span></a>

			<div class="member__more-content">
				<div class="membership-text-wrap sheet-text-wrap">
					<?php the_sub_field('text') ?>
				</div>
			</div>

			<?php endif ?>
		</div>
	</div>
<?php endwhile ?>
</div>
