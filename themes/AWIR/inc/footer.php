<?php
	$settings = (array)get_field( 'footer', 'options' );
	$settings += [
		'address_1' => '',
		'address_2' => '',
		'copy' => '',
	];
?>
<footer class="s-footer">
	<div class="footer-container">
		<div class="footer-row">
			<div class="footer-logo">
				<img src="<?= get_template_directory_uri() ?>/assets/img/logos/footer-logo-white.svg" alt="" class="footer-logo__part1">
				<p class="footer-logo__part2">Association of Women<br>in Rheumatology</p>

        <?php if ( !is_user_logged_in() ): ?>
          <a class="btn btn__main btn__main-on_dark footer__btn" href="<?= get_sub_field('button_url') ?: awir::user_page_url('membership')?>">Become a Member</a>
        <?php 	else: ?>
          <a class="btn btn__main btn__main-on_dark footer__btn" href="<?= awir::user_page_url('memberships')?>">Become a Member</a>
        <?php endif ?>

				<div class="footer-credits">
					<div class="footer-text-main footer-small-text footer-credits__text"><?=$settings['copy']?></div>
					<div class="footer-text-main footer-small-text footer-credits__text"><?=$settings['address_1']?></div>
					<div class="footer-text-main footer-small-text footer-credits__text"><?=$settings['address_2']?></div>
				</div>

			</div>

			<div class="footer-menu-wrap">
				<div class="footer-menu">
					<?php foreach ( awir_theme::get_menu_tree('footer') as $menu ): ?>
					<ul class="footer-menu__item">
						<li class="footer-text-title footer-menu__title"><?= $menu['title'] ?></li>
						<?php foreach ( $menu['children'] as $item ): ?>
						<li class="footer-text-main footer-menu__text "><a class="footer-menu__link nav-item-<?=$item['slug']?>" href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
						<?php endforeach ?>
					</ul>
					<?php endforeach ?>
				</div>
				<div class="footer-support">Something doesn’t work?<br> <a href="<?= get_page_link( get_page_by_path( 'contact', OBJECT, 'page' ) ) ?>#contacts-support">Contact Support</a></div>
				<div class="footer-subscribe">
					<p class="footer-text-title footer-subscribe-title">Subscribe to Our Newsletter</p>
					<p class="footer-text-main footer-subscribe-text">Receive the latest in updates our newsletters.</p>
					<form action="" class="footer-form">
						<div class="footer-form__mail-box">
							<input type="email" placeholder="Enter your email address">
						</div>
						<div id="footer-subscribe" class="footer-form__input-box">
							<a href="javascript:void(0);" class="footer-form__submit">Subscribe</a>
						</div>
					</form>
				</div>

				<?php if ( have_rows('social_icons', 'options') ): ?>
				<ul class="f-social">
					<?php while ( have_rows('social_icons', 'options') ): the_row() ?>
					<?php if ( !get_sub_field('icon') ) continue ?>
					<li class="f-social__item"><a target="_blank" href="<?php the_sub_field('url') ?>"><img src="<?php the_sub_field('icon') ?>" alt="" class="f-social__icon"></a></li>
					<?php endwhile ?>
				</ul>
				<?php endif ?>

			</div>

			<!-- Facebook page-->
			<div class="f-facebook-page">
				<div class="fb-page" data-href="https://www.facebook.com/awirgroup.org/" data-tabs="timeline" data-height="405" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/awirgroup.org/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/awirgroup.org/">Association of Women in Rheumatology</a></blockquote></div>
			</div>

		</div>
	</div>

	<div class="popup-main popup-main-middle popup-footer">
		<div class="popup-main__window">
			<div class="close-btn">
				<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
			</div>
			<div class="popup-main__inner">

				<h2 class="popup-main__title">Subscribe</h2>
				<div class="subscribe__form popup-main__form">
					<?= do_shortcode(sprintf('[mc4wp_form id="%d"]', get_field('subscribe_form', 'option'))) ?>
				</div>
			</div>
		</div>
	</div>

</footer>

<?php get_template_part('inc/popups') ?>

<?php
add_action('wp_footer', function()
{
	?>

<div id="fb-root"></div>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M8V82BJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<script>
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

  function hideProtect() {
    $('.pdf_protected .protection-window').fadeOut();
    $('.protection-overlay').fadeOut();
  }
</script>
<?php
}, 999 );
?>
