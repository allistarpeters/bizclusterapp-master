<?php

get_header();

the_post();
?>

	<!-- Pages Hero Section -->
	<div class="clinical-header">
		<?php get_template_part('inc/pages-header') ?>
	</div>
	<!-- End Pages Hero Section -->

	<div class="pages-headline pages-headline__alone">
		<!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

<section class="sheet-section">

	<div class="pages-hexset-wrap-bottom clinicals-hexset">
		<img src="<?= get_template_directory_uri() ?>/assets/img/clinical-trials/clinical_trial_4.png" alt="" class="clinicals-bott-hex">
	</div>

	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
	</div>

	<div class="sheet-container advocacy-page">
		<div class="sheet">

			<?php get_template_part('inc/contact-form') ?>

			<?php while( have_rows('clinical_trials') ): the_row() ?>

			<?php 	if ( get_sub_field('banner') ): ?>
			<div class="clinical-lable">
				<?php the_sub_field('banner') ?>
			</div>
			<?php 	endif ?>

			<?php 	if ( get_sub_field('title') ): ?>
			<div class="sheet-text-wrap">
				<h1><?php the_sub_field('title') ?></h1>
			</div>
			<?php 	endif ?>

			<div class="clinical-list">

				<?php while ( have_rows('trials') ): the_row() ?>
				<div class="clinical-item">
					
					<div class="clinical-item__info clinical-item__info-underline">
  					<p class="clinical-item__subtitle"><?php the_sub_field('upper_title') ?></p>
  					<?php if ( get_sub_field('url') ): ?>
  					<a href="<?php the_sub_field('url') ?>" target="_blank">
						<p class="clinical-item__title"><?php the_sub_field('title') ?></p>
					</a>
  					<?php else: ?>
						<p class="clinical-item__title"><?php the_sub_field('title') ?></p>
  					<?php endif ?>
						<p class="clinical-item__description"><?php the_sub_field('description') ?></p>

						<a href="mailto:<?php the_sub_field('contact_email') ?>" class="main-text sheet__text clinical-item__contact"><?php the_sub_field('contact_name') ?></a>
					</div>
				</div>
				<?php endwhile ?>

			</div>

			<?php endwhile ?>

		</div>
	</div>
</section>


<?php get_footer();
