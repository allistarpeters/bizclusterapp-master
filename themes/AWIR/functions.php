<?php

if ( class_exists( 'awir_singleton') ):

class awir_theme extends awir_singleton
{
	protected function __construct()
	{
		parent::__construct();

		$this->add_action( 'wp_enqueue_scripts' );
		$this->add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_late', 999 );
		$this->add_action( 'pre_get_posts', 'board_quick_start_guide' );
		$this->add_action('upload_mimes', 'awir_custom_upload_mimes');

		if ( function_exists('acf_add_options_page') )
			acf_add_options_page();

		$this->add_action('after_setup_theme');

		remove_filter('the_content', 'wptexturize');
		remove_filter('acf_the_content', 'wptexturize');
	}

	public function init()
	{

		$this->ajax_action('comment');
		$this->ajax_action('upload_avatar');
		$this->ajax_nopriv_action('upload_avatar');
		$this->add_filter('image_resize_dimensions', null, 10, 6);
		$this->add_filter( 'wp_get_attachment_image_src', null, 10, 4 );
		$this->add_action('acf/save_post', 'pri_sort_partners_alphabetically', 1000);
	}

	public function after_setup_theme()
	{
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		register_nav_menus(
			array(
				'top'    => __( 'Top Menu', 'awir' ),
				'footer'    => __( 'Footer Menu', 'awir' ),
				//'social' => __( 'Social Links Menu', 'twentyseventeen' ),
			)
		);

		add_image_size( 'event_homepage', 520, 600, true );
		add_image_size( 'video_fallback', 736, 463, true );
		//add_image_size( 'event_slider', 500, 300, true );
		add_image_size( 'related_thumb', 260, 160, true );
		add_image_size( 'avatar', 100, 100, true );

		remove_shortcode('caption');
		add_shortcode('caption', array( $this, 'caption_shortcode' ) );

		//add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );


	}

	public function wp_enqueue_scripts() {
		global $wp_query;

		$dir = get_template_directory_uri();

		wp_enqueue_style( 'google-fonts-sans', 'https://fonts.googleapis.com/css?family=Open+Sans' );
		wp_enqueue_style( 'google-fonts-karla', 'https://fonts.googleapis.com/css?family=Karla' );
		//wp_enqueue_style( 'normalize', get_template_directory_uri().'/assets/css/normalize.css' );
		wp_enqueue_style( 'fotorama', $dir.'/assets/css/fotorama.css' );
		wp_enqueue_style( 'awir-main', $dir.'/assets/css/styles.css', [], self::ftime('assets/css/styles.css') );

		wp_enqueue_style( 'awir-custom', get_stylesheet_uri(), [], self::ftime('style.css') );


		wp_deregister_script('jquery');
		wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.3.1.min.js", [], '3.1.1' );
		wp_enqueue_script( 'jquery-isInViewport', $dir.'/assets/js/isInViewport.min.js', ['jquery'], false, true );
		wp_enqueue_script( 'fotorama', $dir.'/assets/js/fotorama.js', ['jquery'], false, true );
		wp_enqueue_script( 'svg-pan-zoom', '//ariutta.github.io/svg-pan-zoom/dist/svg-pan-zoom.js', ['jquery'], false, true );
		wp_enqueue_script( 'main', $dir.'/assets/js/scripts.min.js', ['jquery'], self::ftime('assets/js/scripts.min.js'), true );
		wp_enqueue_script( 'pages', $dir.'/assets/js/app.min.js', ['jquery'], false, true );
		wp_enqueue_script( 'custom', $dir.'/customs/custom.js', ['jquery'], self::ftime('customs/custom.js') );

		if( is_single() || is_archive() || $wp_query->is_posts_page){
			wp_enqueue_script( 'pdf-loader', $dir.'/assets/js/pdf-loader.min.js', ['jquery'], false, true );
		}
	}

	public function wp_enqueue_scripts_late()
	{
		wp_deregister_style('ms-styles');
		wp_deregister_style('wpmu-wpmu-html-3-min-css');
		wp_deregister_style('wpmu-wpmu-ui-3-min-css');
		wp_deregister_style('wpmu-select2-3-min-css');
	}

	public function board_quick_start_guide( $query ) {
		if ( $query->is_post_type_archive('board') && $query->is_main_query() ) {
			$queried_object = get_queried_object();

			if(isset($queried_object->term_id)){
				// if select custom category and its a 'quickstart-guide'
				if($queried_object->slug === 'quickstart-guide'){
					$query->set( 'tax_query', array(
						array(
							'taxonomy' => 'board_tag',
							'field'    => 'slug',
							'terms'    => array( 'quickstart-guide' ),
						),
					) );
				}
			}else{
				// if select all categories
				$query->set( 'tax_query', array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'board_tag',
						'field'    => 'slug',
						'terms'    => array( 'quickstart-guide' ),
					),
				) );
			}
		}
	}

	public function pri_sort_partners_alphabetically($post_id){
    $values = get_fields($post_id);
	  if ($post_id == 8636){
      $leaders = $values['leaders'];
      usort( $leaders, function ( $a, $b ) {
        $_res = strcasecmp($b['title'], $a['title']);
        return $_res >= 0 ? -1 : 1;
      } );
      update_field('leaders', $leaders, $post_id);
    }
    if ($post_id == 760){
      $chaps = get_field('rep_chapters',$post_id);
      usort( $chaps, function ( $a, $b ) {
        $_res = strcasecmp($b['country'], $a['country']);
        return $_res >= 0 ? -1 : 1;
      } );
      update_field('rep_chapters', $chaps, $post_id);
    }
    if ($post_id == 759){
      $leaders = $values['leaders'];
      usort( $leaders, function ( $a, $b ) {
        $_res = strcasecmp($b['title'], $a['title']);
        return $_res >= 0 ? -1 : 1;
      } );
      update_field('leaders', $leaders, $post_id);
    }

  }

	private static function ftime( $path )
	{
		return filemtime( get_template_directory().'/'.$path );
	}

	public static function get_menu_tree( $location )
	{
		$locations = get_nav_menu_locations();
		$menu = [];

		if ( empty( $locations[ $location ] ) )
			return $menu;

		$slug = $locations[ $location ];
		$items = wp_get_nav_menu_items( $slug );
		if ( !$items )
			return $menu;

		foreach ( $items as $item )
		{
			$menuitem = [
				'title' => $item->title,
				'url' => $item->url,
				'slug' => $item->post_name,
				'children' => [],
			];

			if ( $item->object == 'page' && $item->object_id && ($post = get_post($item->object_id)) )
				$menuitem['slug'] = 'page-'.$post->post_name;

			if ( empty( $item->menu_item_parent ) )
				$menu[ $item->ID ] = $menuitem;
			elseif ( !empty( $menu[ $item->menu_item_parent ] ) )
				$menu[ $item->menu_item_parent ]['children'][ $item->ID ] = $menuitem;
		}

		return $menu;
	}

	public static function event_address( $post_id = null, $type = 'short' )
	{
		if ( $post_id === null )
			$post_id = get_the_ID();

		$venue_id = tribe_get_venue_id( $post_id );

		if ( $type == 'full' )
			$address = [ tribe_get_address($venue_id), tribe_get_city($venue_id), tribe_get_region($venue_id), tribe_get_country($venue_id), tribe_get_zip($venue_id) ];

		elseif ( $type == 'short' && $venue = get_post($venue_id) )
			$address = [ $venue->post_title ];

		else
			$address = [ tribe_get_city( $venue_id ), tribe_get_region( $venue_id ) ];

		//tribe_get_country tribe_get_zip tribe_get_address

		$address = implode( ', ', array_filter( $address ) );

		return $address;
	}

	public static function strict_excerpt( $post = null )
	{
		$post = get_post( $post );
		$excerpt = $post->post_excerpt;

		$excerpt = esc_html( $excerpt );
		$excerpt = nl2br( $excerpt );
		echo $excerpt;
	}

	public static function the_excerpt( $length = 300 )
	{
		echo self::get_the_excerpt( null, $length );
	}

	public static function get_the_excerpt( $post = null, $length = 300 )
	{
		$post = get_post( $post );

		$excerpt = get_the_excerpt( $post );

		if ( mb_strlen( $excerpt ) > $length )
		{
			$excerpt = mb_substr( $excerpt, 0, $length );
			$excerpt = preg_replace('/\s[^\s]*$/', '', $excerpt );
			$excerpt .= '...';
		}
		//$excerpt = preg_replace('/[\r\n]+/', "\n", $excerpt );

		return $excerpt;
	}

	public static function get_string_event_date($time){
		$dom = new DOMDocument;
		$dom->loadHTML($time);
		return $dom->getElementsByTagName('span')->item(0)->nodeValue;
	}

	public static function event_reg_data( $post_id = null )
	{
		if ( $post_id === null )
			$post_id = get_the_ID();

		// get formatted event date
		$time = self::get_string_event_date( do_shortcode('[tribe_formatted_event_date id='.$post_id.' format="g:i A T"]') );

		$data = [
			'id' => $post_id,
			'day' => tribe_get_start_date( $post_id, false, 'j' ),
			'month' => tribe_get_start_date( $post_id, false, 'M' ),
			'time' => $time,
			'title' => get_the_title(),
			'place' => self::event_address( $post_id ),
		];

		return $data;
	}

	public function caption_shortcode( $attr, $content = '' )
	{
		if ( ! isset( $attr['caption'] ) )
		{
			if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) )
			{
				$content = $matches[1];
				$attr['caption'] = trim( $matches[2] );
			}
		}
		elseif ( strpos( $attr['caption'], '<' ) !== false )
			$attr['caption'] = wp_kses( $attr['caption'], 'post' );

		return sprintf ('
			<div class="figure">
				%s
				<p class="wp-caption-text">%s</p>
			</div>',
			do_shortcode( $content ), $attr['caption']
		);

	}

	public function ajax_upload_avatar()
	{
		$attachment_id = awir_profile::upload_avatar( 'image', $error );

		if ( !$attachment_id )
			self::ajax_error( $error );

		$data = [
			'id' => $attachment_id,
			'url' => self::image_url( $attachment_id, 'avatar' ),
		];

		self::ajax_success( $data );
	}

	public static function image_url( $attachment_id, $size = 'full' )
	{
		$image = wp_get_attachment_image_src( $attachment_id, $size );
		return $image ? $image[0] : '';
	}

	public static function avatar( $user_id = null, $size = 96 )
	{
		$user_id = self::current_user_if_null( $user_id );
		if ( $user_id )
			$avatar_url = get_avatar_url( (int)$user_id, [ 'size' => $size ] );

		if ( empty($avatar_url) )
			$avatar_url = get_template_directory_uri().'/customs/default-avatar.png';

		return $avatar_url;
	}

	public function wp_get_attachment_image_src( $image, $attachment_id, $size, $icon )
	{
		static $resized = [];

		if ( $size == 'full' || !$image || !empty($image[3]) || !empty($resized[ $attachment_id ]) )
			return $image;

		$resized[ $attachment_id ] = true;

		$fullsizepath = get_attached_file( $attachment_id );

		if ( false === $fullsizepath || !file_exists( $fullsizepath ) )
			return $image;

		require_once ABSPATH . 'wp-admin/includes/image.php';
		@set_time_limit( 120 );

		$metadata = wp_generate_attachment_metadata( $attachment_id, $fullsizepath );

		if ( is_wp_error( $metadata ) || !$metadata )
			return $image;

		wp_update_attachment_metadata( $attachment_id, $metadata );

		return wp_get_attachment_image_src( $attachment_id, $size, $icon );
	}

	function image_resize_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop)
	{
		if ( !$crop )
			return $default;

		$aspect_ratio = $orig_w / $orig_h;
		$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);

		$s_x = floor( ($orig_w - $crop_w) / 2 );
		$s_y = floor( ($orig_h - $crop_h) / 2 );

		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}

	public static function is_posts_archive()
	{
		return is_home() || ( is_archive() && in_array(get_query_var('post_type'), ['post','']) );
	}

	public static function current_page()
	{
		if ( is_home() )
			return 'blog';

		if ( is_archive() )
		{
			$post_type = get_query_var('post_type');
			if ( in_array(get_query_var('post_type'), ['post','']) )
				return 'blog';

			return $post_type;
		}

		if ( is_page() && ($page = get_post()) )
			return $page->post_name;

		if ( is_singular() && ( $post = get_post() ) )
			return 'single-'.$post->post_type;

		return '';
	}

	public static function comments_tree( $post = null, $parent = 0, $level = 0, &$all_comments = [] )
	{
		$post = get_post( $post );

		$args = [
			'post_id' => $post->ID,
			'parent' => $parent,
			'ststus' => 'approve',
			'type' => 'comment',
			'orderby' => 'comment_date_gmt',
			'order' => 'DESC',
		];

		$comments = get_comments( $args );
		foreach ( $comments as $comment )
		{
			$comment->level = $level;
			if ( $comment->user_id && ($user = get_userdata( $comment->user_id )) )
			{
				$comment->comment_author = $user->display_name;
				$comment->comment_author_url = awir::user_page_url('user', [ 'user'=>$user->user_login ]);
			}

			$all_comments[] = $comment;
			if ( $level + 1 < (int)get_option('thread_comments_depth') )
				self::comments_tree( $post, $comment->comment_ID, $level + 1, $all_comments );
		}

		return $all_comments;
	}

	public function ajax_comment()
	{
		//$this->debug = true
		if ( !awir::is_member() )
			user_page_redirect('become_member');

		if ( empty($_POST['post_id']) || !($post = get_post($_POST['post_id'])) )
		{
			wp_redirect( home_url() );
			exit;
		}

		if ( !check_ajax_referer('comment_'.$_POST['post_id']) || !($comment_content = trim( $_POST['comment_content'] )) )
		{
			wp_redirect( get_permalink( $post ) );
			exit;
		}

		if ( !empty( $_POST['parent'] ) && ($parent = (int)$_POST['parent']) > 0 )
		{
			$parent_id = $parent;
			$depth = 0;
			$log = [];

			while ( $parent_id )
			{
				$parent_comment = get_comment( $parent_id );

				if ( !$parent_comment )
				{
					wp_redirect( get_permalink( $post ) );
					exit;
				}

				$log[] = $parent_comment->comment_ID;
				$parent_id = $parent_comment->comment_parent;
			}

			$max_log_count = max( 1, (int)get_option('thread_comments_depth') - 1 );

			if ( count($log) > $max_log_count )
				$parent = $log[ count($log) - $max_log_count ];

			//var_dump( $parent, $log, $max_lo
		}

		$profile = new awir_profile();

		$comment = [
			'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
			'comment_approved' => 1,
			'comment_author' => $profile->name,
			'comment_author_email' => $profile->email,
			'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
			'comment_author_url' => '',
			'comment_content' => $comment_content,
			'comment_parent' => $parent,
			'comment_post_ID' => $post->ID,
			'user_id' => $profile->ID,
		];

		$id = wp_insert_comment( $comment );
		wp_redirect( get_permalink( $post->ID ).'#comment-'.$id );
		exit;
	}

	public static function page_parent( $post = null )
	{
		$post = get_post( $post );
		if ( $parent = apply_filters( 'awir_page_parent', '', $post ) )
			return $parent;

		if ( $post->post_parent && ($parent_post = get_post( $post->post_parent )) )
			$parent = esc_html( $parent_post->post_title ).' /';

		return $parent;
	}

	public function awir_custom_upload_mimes($mimes = array()) {
		$mimes['rar'] = "application/x-rar";
		$mimes['xls|xlsx'] = 'application/vnd.ms-excel';

		return $mimes;
	}
}

awir_theme::getInstance();

endif;

function debug_print($s){
	echo '<pre>';
	print_r($s);
	echo '</pre>';
}

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) {
	$confFiles = get_user_meta( $user->ID, 'confirmation_letters', true );

	if(!empty($confFiles)):
		$list = explode(',', $confFiles);
	?>
    <table class="form-table">
		<tr>
			<th>Rheumatology Fellowship Program Confirmation Letter</th>
			<td>
				<ol style="padding: 0; margin: 0; margin-left: 15px">
				<?php
				foreach($list as $attach_id){
					$url = wp_get_attachment_url( $attach_id );
					?>
					<li><a href="<?= esc_attr( $url ) ?>" target="_blank"><?= esc_html( basename( $url ) ) ?></a></li>
					<?php
				}
				?>
				</ol>
			</td>
		</tr>
    </table>
	<?php
	endif;
}


add_filter('manage_users_columns', 'add_users_comm_column', 4);
function add_users_comm_column( $columns ){
	$columns['awir_username'] = 'Username';

	$n_columns = array();
	$move = 'awir_username'; // what to move
	$before = 'username'; // move before this
	foreach($columns as $key => $value) {
		if ($key==$before){
			$n_columns[$move] = $move;
		}
		$n_columns[$key] = $value;
	}
	unset($n_columns['username']);

	return $n_columns;
}

add_action('manage_users_custom_column', 'fill_users_comm_column', 5, 3);
function fill_users_comm_column( $out, $column_name, $user_id ) {
	global $wpdb;

	$userdata = get_userdata( $user_id );

	if( $column_name === 'awir_username' ){
		$editLink = get_edit_user_link( $user_id );
		$delLink = wp_nonce_url( "users.php?action=delete&amp;user={$user_id}", 'bulk-users' );
		$out =
			get_avatar( $user_id, 32 ) .
			'<strong><a href="'. $editLink .'">'. esc_attr( $userdata->user_nicename ) .'</a></strong><br>' .
			'<div class="row-actions">
				<span class="edit"><a href="' . $editLink . '">Edit</a> | </span>
				<span class="delete"><a class="submitdelete" href="'.$delLink.'">Delete</a></span>
			</div>';
	}

	return $out;
}


add_action('admin_head', 'my_admin_custom_styles');
function my_admin_custom_styles() {
    $output_css = '<style type="text/css">
		.column-awir_username img {
			float: left;
			margin-right: 10px;
			margin-top: 1px;
		}
    </style>';
    echo $output_css;
}

/* Make column sortable */
add_filter('manage_users_sortable_columns', 'add_users_comm_sortable_column');
function add_users_comm_sortable_column($sortable_columns){
	$sortable_columns['awir_username'] = 'login';

	return $sortable_columns;
}
