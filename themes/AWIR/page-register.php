<?php

$profile = new awir_profile();

if ( $profile->process_form() )
{
	$user = $profile->user;

	if ( !is_user_logged_in() )
	{
		wp_set_current_user($user->ID, $user->user_login);
		wp_set_auth_cookie( $user->ID );

		do_action( 'wp_login', $user->user_login, $user );
	}

	awir::user_page_redirect('checkout');
}

get_header();

?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">
	</div>

<div class="pages-headline exhibitors">
	<!-- <p class="main-text pages-headline__category">Advocacy /</p> -->
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

	<section class="sheet-section">
		<div class="pages-hexset-wrap-bottom register-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
		</div>
		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
		</div>

		<div class="sheet-container registration-page">
			<div class="sheet">
				<form id="registerForm" action="<?php the_permalink() ?>" autocomplete="nope" class="main-form" method="post" enctype="multipart/form-data">

					<label class="inline-upload-avatar">
						<div class="member add-photo">
							<a class="member__photo photo-back-hex edit-photo">
								<img src="<?= $profile->avatar ? awir_theme::image_url($profile->avatar, 'avatar') : awir_theme::avatar() ?>" alt="AWIR" class="member__photo-img">
							</a>
							<div class="member__data">
								<a class="main-text member__edit">Add photo</a>
							</div>
						</div>
						<?php $profile->the_field('avatar') ?>
					</label>

						<!-- reg form row 1 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap top_fix">
								<?php $profile->the_field('email', ['placeholder'=>'Email *']) ?>
							</div>
							<div class="main-form__input-wrap top_fix">
								<?php $profile->the_field('email_verify', ['placeholder'=>'Verify Email *']) ?>
							</div>
							<div class="main-form__input-wrap top_fix">
								<div class="select-box select-box-small ms-select_type">
									<?php $profile->the_field('membership', ['placeholder'=>'Select a Membership *']) ?>
								</div>
							</div>
						</div>

						<!-- reg form row password -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<?php $profile->the_field('password', ['placeholder'=>'Password *']) ?>
							</div>
							<div class="main-form__input-wrap">
								<?php $profile->the_field('password_verify', ['placeholder'=>'Verify Password *']) ?>
							</div>
						</div>

						<!-- reg form row 2 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<?php $profile->the_field('first_name', ['placeholder'=>'First Name *']) ?>
							</div>
							<div class="main-form__input-wrap">
								<?php $profile->the_field('last_name', ['placeholder'=>'Last Name *']) ?>
							</div>
						</div>

						<!-- reg form row 3 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<p class="main-form__input-title">Country</p>
								<div class="select-box">
									<?php $profile->the_field('country',['placeholder'=>'Select a country *']) ?>
								</div>
							</div>
							<div class="main-form__multiple-input">
								<div class="main-form__input-wrap">
									<p class="main-form__input-title">State</p>
									<div class="select-box">
										<?php $profile->the_field('state',['placeholder'=>'Select a state *']) ?>
									</div>
								</div>
								<div class="main-form__input-wrap">
									<p class="main-form__input-title">City</p>
									<div class="select-box no-select">
										<?php $profile->the_field('city', ['placeholder'=>'City *']) ?>
									</div>
								</div>
						</div>
					</div>

						<!-- reg form row 4 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<?php $profile->the_field('address', ['placeholder'=>'Address Line *']) ?>
							</div>
							<div class="main-form__input-wrap">
								<?php $profile->the_field('address_line_2') ?>
							</div>
						</div>

						<!-- reg form row 5 -->
						<div class="main-form__row">
							<div class="main-form__input-wrap">
								<div class="select-box no-select">
									<?php $profile->the_field('phone', ['placeholder'=>'Cell Phone *']) ?>
								</div>
							</div>
							<div class="main-form__multiple-input birth-date">
								<div class="main-form__input-wrap">
								<p class="main-form__input-title">Date of Birth *</p>
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[day]">
											<?php $day = (int)$profile->birth_date('d') ?>
											<?php for ( $i = 1; $i <= 31; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $day ) echo 'selected ' ?>><?php printf('%02d', $i) ?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
								<div class="main-form__input-wrap">
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[month]">
											<?php $month = (int)$profile->birth_date('m') ?>
											<?php for ( $i = 1; $i <= 12; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $month ) echo 'selected ' ?>><?= date('F', mktime(0,0,0,$i,15)) ?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
								<div class="main-form__input-wrap">
									<div class="select-box">
										<select name="<?=$profile->form_prefix?>[year]">
											<?php $profile_year = (int)$profile->birth_date('Y') ?>
											<?php $current_year = date('Y') ?>
											<?php for ( $i = $current_year-100; $i <= $current_year-16; $i++ ): ?>
											<option value="<?=$i?>" <?php if ( $i == $profile_year ) echo 'selected ' ?>><?=$i?></option>
											<?php endfor ?>
										</select>
									</div>
								</div>
						</div>
					</div>

					<!-- reg form row 6 -->
					<div class="main-form__row">


						<div class="main-form__radio-wrap">
							<div class="main-form__title-wrap">
								<p class="main-form__input-title">Gender *</p>
								<?php $profile->the_field('gender') ?>
							</div>
						</div>

						<div class="main-form__radio-wrap">
							<div class="main-form__title-wrap">
								<p class="main-form__input-title">Are you a member of ACR *</p>
								<?php $profile->the_field('acr_member') ?>
							</div>
						</div>
					</div>

					<!-- reg form row 7 -->
					<div class="main-form__row">
						<div class="main-form__input-wrap">
							<div class="select-box select-box-small">
								<?php $profile->the_field('race_ethnicity', ['placeholder'=>'Race/Ethnicity *']) ?>
							</div>
						</div>
						<div class="main-form__input-wrap">
							<div class="select-box select-box-small">
								<?php $profile->the_field('med_degree', ['placeholder'=>'What is your Medical Degree? *']) ?>
							</div>
						</div>
						<div class="main-form__input-wrap ms-type ms-type-<?=awir_memberships::TYPE_SPECIAL?>">
							<div class="select-box select-box-small">
								<?php $profile->the_field('allied_speciality', ['placeholder'=>'What is your Allied Specialty? *']) ?>
							</div>
						</div>
					</div>

					<div class="main-form__row ms-type ms-type-<?=awir_memberships::TYPE_FREE?>">
						<div class="main-form__input-wrap">
							<p class="main-form__input-title">What is the name of your Fellowship Training Program?</p>
							<?php $profile->the_field('fellowship_name') ?>
						</div>
						<div class="main-form__input-wrap">
							<p class="main-form__input-title">What is the name of your Program Director?</p>
							<?php $profile->the_field('fellowship_director') ?>
						</div>
					</div>

					<div class="main-form__row ms-type ms-type-<?=awir_memberships::TYPE_FREE?>">
						<div class="main-form__input-wrap">
							<p class="main-form__input-title">Rheumatology Fellowship Program Confirmation Letter</p>	
							<input type="hidden" name="profile[fellowship_confirmation_files]" <?= !empty($profile->__get('fellowship_confirmation_files')) ? 'value="'.$profile->__get('fellowship_confirmation_files').'"' : '' ?>>						
							<input id="profile_confirmation_letters" type="file" name="confirmation_letters[]" multiple>
							<p class="main-form__input-wrap-sign">Please create a zip file or use Ctrl/Cmd+select to select multiple files</p>
							<?php 
							$attachList = $profile->get_conf_letters_id_list();

							if(!empty($attachList)){
								$len = count($attachList);
								$maxLen = 4; // max chars in title

								echo '<div class="main-form__input-file-selected">';
								for($i=0; $i < $len; $i++){
									$fileLink = get_attached_file( $attachList[$i] );
									$name = pathinfo( $fileLink, PATHINFO_FILENAME );
									$baseName = basename($fileLink);
									$isLongName = (strlen($name) > $maxLen ? true : false);

									echo '<span title="'.$baseName.'">('.($isLongName === true ? substr($name, 0, $maxLen) . '...' : $name).')'.($i < $len - 1 ? ', ' : '').'</span>';
								}
								echo '</div>';
							}

							$errorsList = $profile->__get('errors');

							if(isset($errorsList['confirmation_letters'])){
								?>
								<label class="register-error active" for="profile_confirmation_letters"><?=esc_html($errorsList['confirmation_letters'])?></label>
								<?php
							}
							?>
						</div>
					</div>
					
					<div class="registration-divider"></div>

					<div class="your-interests">
						<h3><strong>What are you Primary Areas of Interest</strong> (check all that apply)</h3>
						<div class="your-interests__area">
							<?php $profile->the_field('interests', [
								'wrap' => ['<div class="your-interests__item">', '</div>'],
							]) ?>
						</div>

						<h3><strong>What are your Primary Professional Activities. These may be different from your «Primary Areas of Interest»</strong> (check all that apply)</h3>
						<div class="your-interests__area">
							<?php $profile->the_field('activities', [
								'wrap' => ['<div class="your-interests__item">', '</div>'],
							]) ?>
						</div>
					</div>

					<div class="consent">
						<p class="consent__title">VIDEO AND MEDIA CONSENT AND RELEASE</p>
						<div class="main-form__radio-wrap main-form__consent">
							<div class="main-form__title-wrap">
								<?php $profile->the_field('media_consent') ?>
							</div>
						</div>
						<a href="" class="consent__details-btn">Details</a>
					</div>

					<?php $profile->nonce_field() ?>
					<button type="submit" class="btn btn-centered btn__main registration-btn" onsubmit="this.submit();"><span class="btn__text">Sign Up</span></button>

				</form>
			</div><!--End Sheet -->
		</div>
	</section>
	<?php
		$types = [];
		foreach ( awir_memberships::get_list() as $id => $ms )
			$types[ $id ] = awir_memberships::get_type( $id );
	?>
	<script>
	var types = <?= json_encode( $types, JSON_FORCE_OBJECT ) ?>;
	jQuery( function($) {
		$('.ms-select_type select').change( function(){
			var type = types[ $(this).val() ];

			$('.ms-type').not('.ms-type-' + type ).hide();
			$('.ms-type-' + type ).fadeIn();

			return true;
		} ).change();
		$('.consent__details-btn').click(function(e){
			e.preventDefault();
			$('#popup-consent').addClass('active');
		});


		// update state field
		var SELECT_COUNTRY = $('select[name="profile[country]"]');
		var SELECT_STATE = $('select[name="profile[state]"]');

		function updateState(){
			var val = SELECT_COUNTRY.val();

			if(val === 'United States of America'){
				SELECT_STATE.prop('disabled', false);
			}else{
				SELECT_STATE.prop('selectedIndex', 0);
				SELECT_STATE.prop('disabled', true);
			}
		}

		SELECT_COUNTRY.change(updateState);
		updateState();
	} );
	</script>

	<div id="popup-consent" class="popup-main popup-main-middle popup-large ">
		<div class="popup-main__window">
				<div class="close-btn">
					<img src="<?= get_template_directory_uri() ?>/assets/img/icons/close-popup.svg" alt="" class="close-btn__icon">
				</div>
				<div class="popup-main__inner">
					<div class="sheet-text-wrap">
						<?php wp_reset_query() ?>
						<?php the_content() ?>
					</div>
				</div>
		</div>
	</div>

<?php get_footer();
