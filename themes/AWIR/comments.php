<?php

$comments = awir_theme::comments_tree();

?>
<div class="comments-wrap">
	<?php if ( $comments ): ?>
	<p class="comments-wrap__title">Comments <span>(<?=count($comments)?>)</span></p>
	<?php 	foreach ( $comments as $comment ): ?>
	<div class="comment<?php if ( $comment->level ) echo ' comment-reply comment-reply--level'.$comment->level ?>" id="comment-<?=$comment->comment_ID?>" data-id="<?=$comment->comment_ID?>">

		<div class="comment__content-wrap">
			<img src="<?= awir_theme::avatar($comment->user_id) ?>" alt="" class="comment__image">
			<div class="comment__content">
				<div class="comment__autor">
					<a href="<?= esc_url($comment->comment_author_url) ?>">
						<p class="comment__autor-name"><?= esc_html( $comment->comment_author) ?> </p>
					</a>
					<p class="comment__date"><?= date('j F, Y H:i', strtotime( $comment->comment_date_gmt.' GMT' ) ) ?></p>
				</div>
				<p class="comment__message"><?= nl2br( esc_html( trim( $comment->comment_content ) ) ) ?></p>
				<a href="javascript:void(0)" class="comment__reply">Reply</a>
			</div>
		</div>


	</div>
	<?php 	endforeach ?>
	<?php endif ?>

	<div class="add-comment" id="add-comment">
		<p class="add-comment__title">Add comment</p>
		<div class="add-comment__inner">
			<img src="<?= awir_theme::avatar() ?>" alt="" class="add-comment__avatar">
			<form action="<?= admin_url('admin-ajax.php') ?>" class="add-comment__form" method="post">
				<input type="hidden" name="action" value="awir_comment" />
				<input type="hidden" name="parent" value="0" />
				<input type="hidden" name="post_id" value="<?= get_the_ID() ?>" />
				<?php wp_nonce_field('comment_'.get_the_ID()) ?>
				<textarea type="text" placeholder="Type your comment here…" name="comment_content"></textarea>
				<button type="submit" class="btn btn__main add-comment__btn"><span class="btn__text">Send</span></button>
			</form>
		</div>
	</div>

	<div class="add-comment" id="add-comment-reply" style="display:none">
		<div class="add-comment__inner">
			<form action="<?= admin_url('admin-ajax.php') ?>" class="add-comment__form" method="post">
				<input type="hidden" name="action" value="awir_comment" />
				<input type="hidden" name="parent" value="0" />
				<input type="hidden" name="post_id" value="<?= get_the_ID() ?>" />
				<?php wp_nonce_field('comment_'.get_the_ID()) ?>
				<textarea type="text" placeholder="Type your comment here…" name="comment_content"></textarea>
				<button type="submit" class="btn btn__main add-comment__btn"><span class="btn__text">Send</span></button>
			</form>
		</div>
	</div>

</div>
