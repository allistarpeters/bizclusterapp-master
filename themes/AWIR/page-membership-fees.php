<?php


get_header();

the_post();

?>

	<section class="sheet-section user-sheet">

		<div class="membership-hexset-top">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5-copy.png" alt="" class="hexset-middle">
		</div>
		<div class="membership-hexset-middle">
			<img src="<?= get_template_directory_uri() ?>/assets/img/user-pages/message/group-5.png" alt="" class="hexset-middle">
		</div>

		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/hex-set-chapters-bottom.png" alt="" class="pages-set4">
		</div>
		<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
			<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
		</div>

		<div class="sheet-container message-board">
			<div class="sheet">

				<div id="side-menu" class="blogs-sidebar internal-sidebar">
					<a href="<?= get_post_type_archive_link('post') ?>" class="blogs-sidebar__back"><span>Back</span></a>
					<div class="blogs-sidebar__social-list">
						<?php while ( have_rows('social_icons', 'options') ): the_row() ?>
						<?php if ( !get_sub_field('icon_alt') ) continue ?>
						<a href="<?php the_sub_field('url') ?>" class="blogs-sidebar__social"><img src="<?php the_sub_field('icon_alt') ?>" alt="" class="blogs-sidebar__icon"></a>
						<?php endwhile ?>
					</div>
				</div>

				<div class="blogs-wrap internal-content">
					<div class="internal-content-wrap">
						<div class="blogs-content">
							<div class="blogs-content__item">
								<p class="blogs-content__subtitle"><?= get_the_date( 'F j, Y' ) ?></p>
								<h2 class="title-text sheet__title blogs-content__title"><?php the_title() ?></h2>
								<?php if ( has_post_thumbnail() ): ?>
								<img src="<?php the_post_thumbnail_url('full') ?>" alt="" class="blogs-content__image">
								<?php endif ?>
								<div class="sheet-text-wrap blogs-text-wrap <?php if ( get_field('pdf_protected') ):?> pdf-protected<?php endif ?>">
									<?php the_content() ?>
								</div>

								<div class="blogs-content__links">
									<?php if ( ($terms = get_the_terms( get_the_ID(), 'board_tag' )) && !is_wp_error( $terms ) ): ?>
									<div class="user-info user-info-interest">
										<?php foreach ( $terms as $term ) : ?>
										<div class="user-info__item user-info__item-interest">
											<a href="<?= awir_message_board::url( $term ) ?>" class="user-info__tag"><?= esc_html( $term->name ) ?></a>
										</div>
										<?php endforeach ?>
									</div>
									<?php endif ?>
									<div class="blogs-content__activity">
										<a href="javascript:void(0);" class="blogs-content__activity-vievs"><?php echo do_shortcode('[post-views]'); ?></a>
										<a href="javascript:void(0);" class="blogs-content__activity-messages"><?= get_comments_number() ?></a>
									</div>
								</div>

							</div>
						</div>
					</div>

					<?php comments_template() ?>

					<?php get_template_part('inc/related') ?>

				</div>

			</div>
		</div>


<?php get_footer();
