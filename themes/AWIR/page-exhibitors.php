<?php

get_header();

the_post();

?>

	<!-- Exhibitors Hero Section -->
	<div class="exhibitors-header">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg-hex.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhib-top-hex.svg" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex2">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/mobile-top.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-hex3">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg3.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg12.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/exhibitors-bg11.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top">

		<!-- Large screen bg -->
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-bott-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-bot-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-mid-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-mid-large">
		<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/bg-top-large.png" alt="" class="exhibitors-header__bg exhibitors-header__bg-top-large">

	</div>

	<!-- End Pages Hero Section -->

	<div class="pages-headline exhibitors">
    <a href="<?= home_url('events/') ?>" class="main-text pages-headline__category">Meetings /</a>
		<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
	</div>

	<!-- page content-->

	<section class="sheet-section exhibitors-section">

		<div class="pages-hexset-wrap-bottom exhibitors-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/group-14.svg" alt="" class="pages-set7">
		</div>
		<div class="pages-hexset-wrap-bottom callendar-hexset">
			<img src="<?= get_template_directory_uri() ?>/assets/img/hexagons/hex-set-calendar.svg" alt="" class="pages-set7">
		</div>

		<div class="sheet-container exhibitors">
			<div class="sheet">

				<?php
				global $lvlChoices;
				$lvlChoices = get_field_object('field_5c7fecdafbb22')['choices'];
				$lvlChoices = array_reverse($lvlChoices);
				foreach ($lvlChoices as $key => $name):
					$lvlChoices[$key] = [
						'name' => $name,
						'list' => [],
					];
				endforeach;

				/** Reset list for choice lvl */
				function resetLvlChoices(){
					global $lvlChoices;

					foreach ($lvlChoices as $key => $name):
						$lvlChoices[$key]['list'] = [];
					endforeach;
				}
				?>

				<?php while( have_rows('exhibitors') ): the_row() ?>

				<?php 	if ( get_sub_field('title') ): ?>
				<div class="sheet-text-wrap exhibitors-text-wrap">
					<h1><?php the_sub_field('title') ?></h1>
				</div>
				<?php 	endif ?>

				<?php 	while ( have_rows('groups') ): the_row() ?>

				<div class="corporate-members">
					<p class="corporate-members__title"><?php the_sub_field('title') ?></p>

					<div class="corporate-members__logos">
						<?php
						resetLvlChoices();

						while( have_rows('icons') ): the_row();
							$lvlChoices[get_sub_field('lvl')]['list'][] = [
								'url' => get_sub_field('url'),
								'image' => get_sub_field('image'),
							];
						endwhile;

						foreach ($lvlChoices as $key => $lvl):
							?>
							<div class="corporate-members__lvl">
								<?php  if($key !== 'none' && count($lvl['list']) > 0): ?>
								<div class="title"><?= $lvl['name'] ?></div>
								<?php endif; ?>
								<?php if(count($lvl['list']) > 0): ?>
									<div class="list">
									<?php foreach ($lvl['list'] as $member): ?>
										<div class="corporate-members__item">
											<a href="<?= $member['url'] ?>" target="_blank"><img src="<?= $member['image'] ?>" alt=""></a>
										</div>
									<?php endforeach ?>
									</div>
								<?php endif; ?>

							</div>
						<?php endforeach ?>

						<?php //while ( have_rows('icons') ): the_row() ?>
						<!-- <div class="corporate-members__item">
							<a href="<?php //the_sub_field('url') ?>" target="_blank"><img src="<?php //the_sub_field('image') ?>" alt=""></a>
						</div> -->
						<?php //endwhile ?>
					</div>
				</div>

				<?php 	endwhile ?>

				<?php endwhile ?>

				<div class="sheet-text-wrap exhibitors-text-wrap">
					<?php the_content() ?>
				</div>

				<?php if ( get_field('banner_content') ): ?>
				<div class="cta-lable">
					<img src="<?= get_template_directory_uri() ?>/assets/img/exhibitors/16-layers.svg" alt="" class="cta-lable__bg2">
					<?php the_field('banner_content') ?>
				</div>
				<?php endif ?>

				<?php get_template_part('inc/downloads') ?>

			</div><!--End Sheet -->
		</div>
	</section>


	<!-- About page content-->

<?php get_footer();
