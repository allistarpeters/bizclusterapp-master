<?php

get_header();

the_post();
?>

<!-- Pages Hero Section -->
<div class="advocacy-policy-header">
	<?php get_template_part('inc/pages-header') ?>
</div>
<!-- End Pages Hero Section -->

<div class="pages-headline">
	<a href="<?= home_url('advocacy/') ?>" class="main-text pages-headline__category">Advocacy /</a>
	<h1 class="title-text pages-headline__title"><?php the_title() ?></h1>
</div>

<section class="sheet-section">

	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-resourses">
		<img src="<?= get_template_directory_uri() ?>/assets/img/advocacy/advocacy_hex.png" alt="" class="pages-set4">
	</div>
	<div class="pages-hexset-wrap-bottom pages-hexset-wrap-bottom-chapters chapters-phone">
		<img src="<?= get_template_directory_uri() ?>/assets/img/local-chapters/mobile-bottom.png" alt="" class="pages-set5">
	</div>

	<div class="sheet-container resources-page">
		<div class="sheet">

			<?php while ( have_rows('partner_items') ): the_row() ?>
			<div class="membership-text-wrap sheet-text-wrap resources-item">
					<?php the_sub_field('content') ?>
			</div>
			<?php endwhile ?>

			<?php if ( get_field('partner_banner') ): ?>
			<div class="pages-lable resources-label">
				<?php the_field('partner_banner') ?>
			</div>
			<?php endif ?>

		</div>
	</div>
</section>


<?php get_footer();
