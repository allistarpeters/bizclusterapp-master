# Haymarket ALTITUDE PRO THEME

* Customization for McKnights Awards Site
  * Plugins:
    * Haymarket Events Clock
    * Genesis Simple Edits

## Setup

* Theme - Activate Haymarket Altitude Pro theme
* Appearance -> Customize -> Header Image: upload McKnight's logo
* Pages
  * Create Page called "Home Top"
  * Create Page called "Home Middle"
  * Create Page called "Home Bottom"
* Appearance -> Customize -> Front Page Background Images
  * add background image for Featured Section 1
  * remove all other background images
* Menu
  * Create menu with name "Primary" and add items. Display location is Header Menu
* Sponsors
  * Create a post for each sponsor category of Sponsor. Add custom html block with image and text.
* Widgets
  * Primary Sidebar:
    * Add 3 widgets: Image (logo), Genesis - Featured Posts (Sponsors category and Content Type: Show Content), Custom HTML (Countdown clock shortcode)
  * Front Page 1
    * Add Genesis - Featured Page widget and point to Home Top, check checkbox for Show Page Content
    * Add Content and HTML to Home Top
  * Front Page 2
    * Add Genesis - Featured Page widget and point to Home Middle, check checkbox for Show Page Content
    * Add Content and HTML to Home Middle
  * Front Page 3
    * Add Genesis - Featured Posts widget to show Sponsor Categories. Choose Show Content for Content Type
* Footer
  * In Genesis->Simple Edits change Footer Credits Text
