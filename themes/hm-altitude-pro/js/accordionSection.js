(function($){
    $(document).ready(function(){
      $('.enter-now__link').on('click', function(event){
        event.preventDefault();
        if($(this).hasClass('open')){
          $(this).removeClass('open');
        } else {
          $(this).addClass('open');
        }
  
        $(this)
          .parent()
          .find('.enter-now__track-copy')
          .slideToggle('slow');
      });
    })
  })(jQuery)