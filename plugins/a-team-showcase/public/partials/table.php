<?php
    use TeamBuilder\App\Model\Employer;
    use TeamBuilder\App\Shortcode;

    $base = LA_Team_Builder::$plugin['base'];
    $layout = 'table';
    $hover_class = '';
    $layout_css = $this->data->custom_css ?: '';
    $preview = $this->data->preview;
    $styles = $this->data->styles[$layout] ?: $this->data->custom_fields['styles'][$layout];
    $panel = $styles['panel'];
    $panel_animation = $styles['panel_animation'];

    include($base . 'public/partials/parts/buttons_css.php');

    $hover = $styles['hover'];
    if ($hover != 'off') {
        $hover_class = 'ats-effect-' . $hover;
    }

    $gaps = 'padding: ' . Shortcode::get_size($styles['card_gaps']);
    $blocks_order = $styles['blocks_order'];
    $border_color = $styles['card_border_color'];
    $i = 1;

    $even_css = '#ats-layout-' . esc_attr( $this->data->ID ) . ' .employers-box li.even_background_color{background-color:' . esc_attr( $styles['card_even_row_color'] ) . ';}';

    echo Shortcode::$ad;
    echo '<div style="display:none" class="ats-layout-table ats-layout ' . esc_attr( $hover_class ) . '" id="ats-layout-' . esc_attr( $this->data->ID ) . '">';
    echo '<style type="text/css">' . $layout_css . $even_css . '</style>';

    include($base . 'public/partials/parts/team-information.php');
    include($base . 'public/partials/parts/filter.php');
    if($styles['filter'] != '1') {
        include($base.'public/partials/parts/search.php');
    }

    echo "<ul team-id='" . esc_attr( $this->data->ID ) . "' style='border-top: 1px solid " . esc_attr( $border_color ) . ";' class='filter-container employers-box'>";
    if (count($this->data->employers) > 0):
        $employers = $this->data->employers ?: $this->data->custom_fields['employers'];
        foreach ($employers as $employer):
            $id = $employer->ID;
            $terms = wp_get_post_terms($id, Employer::getTaxonomy(), array('fields' => 'ids'));
            $terms = array_map(function ($term) {
                return 'awesome-filter-' . $term;
            }, $terms);
            $terms = implode(' ', $terms);

            $clicable = !empty($employer->profile) || $panel ? 'ats-profile' : '';
            $profile = esc_html($employer->profile);
            $profile_title = !empty($employer->profile) ? 'Open profile' : '';
            $name = esc_html($employer->post_title);

            echo "<li class='" . esc_attr( $terms ) . " filter-item' style='
                        " . esc_attr( $gaps ) . ";
                        border-bottom: 1px solid " . esc_attr( $border_color ) . ";' employer-id='" . esc_attr( $id ) . "' data-title='" . esc_attr( $name ) . "'>";

            echo "<div $clicable data-profile='" . esc_attr( $profile ) . "'
                        class='sortable-box'
                        title='" . esc_attr( $profile_title ) . "'
                        style=''
                        >";
            foreach ($blocks_order as $block) {
                $visible = $styles[$block . '_visible'];
                if (($visible == '1' || $preview)) {
                    Shortcode::include_block(
                        array(
                            'block' => $block,
                            'layout' => $layout,
                            'styles' => $styles,
                            'preview' => $preview,
                            'employer' => $employer
                        )
                    );
                }
            }
            echo '</div>';

            if ($panel && !$preview) {
                $panel_theme = $styles['panel_theme'];
                include($base . 'public/partials/parts/panels/' . $panel_theme . '.php');
            }

            echo '</li>';
        endforeach;
    endif;
    echo '</ul>';
    echo '</div>';
    echo Shortcode::$ad;
    ?>
    <?php if (!$preview): ?>
        <script type="text/javascript">
            // paint all even && visible rows with background color
            jQuery(document).ready(function ($) {
                ATS.paint_rows(<?php echo esc_attr( $this->data->ID ) ?>);
                $('[team-id="' + <?php echo esc_attr( $this->data->ID ) ?> + '"].employers-box > li').on('cssClassChanged', function (e) {
                    ATS.paint_rows(<?php echo esc_attr( $this->data->ID ) ?>);
                });
            });
        </script>
    <?php endif; ?>
    <?php if (!$panel && !$preview): ?>
        <script type="text/javascript">
            // handle click on employer to open profile url
            jQuery(document).ready(function ($) {
                ATS.bind_profiles(<?php echo esc_attr( $this->data->ID )?>);
            });
        </script>
    <?php endif; ?>
    <?php if (!$preview): ?>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    ATS.calc_rating_icons_size(<?php echo esc_attr( $this->data->ID )?>);
                    ATS.calc_social_icons_size(<?php echo esc_attr( $this->data->ID )?>);
                });
            }(jQuery));
        </script>
    <?php endif; ?>
    <?php if ($panel == 1 && !$preview): ?>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    ATS.init_panel(<?php echo esc_attr( $this->data->ID ) ?>, '<?php echo esc_attr( $panel_animation ); ?>');
                });
            }(jQuery));
        </script>
    <?php endif; ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#ats-layout-<?php echo esc_attr( $this->data->ID ); ?>').show();
    });
</script>
