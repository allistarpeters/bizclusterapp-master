<?php

if ($preview) {
    return;
}

if ($styles['filter'] == '1') {
    $terms = $this->data->filter_terms;
    $active_term = $this->data->filter_default;

    $layout_align = 'text-align: left';
    if (isset($styles['align'])) {
        $layout_align = 'text-align: ' . $styles['align'];
    }

    $filter_font_size = 'font-size:' . $styles['filter_font_size'];
    $filter_bold = $styles['filter_bold'] == '1' ? 'font-weight: ' . 'bold' : 'font-weight: ' . 'normal';
    $filter_italic = $styles['filter_italic'] == '1' ? 'font-style: ' . 'italic' : 'font-style: ' . 'normal';
    $filter_text_transform = 'text-transform: ' . $styles['filter_text_transform'];

    if (count($terms) > 0) {
        echo "<ul team-id='" . esc_attr( $this->data->ID ) . "' class='filter-controls' style='
            " . esc_attr( $layout_align ) . ";
	        " . esc_attr( $filter_font_size ) . ";
            " . esc_attr( $filter_bold ) . ";
            " . esc_attr( $filter_italic ) . ";
            " . esc_attr( $filter_text_transform ) . ";
            '>";
        echo '<li class="filter-item">';
        echo '<div class="ats-button" data-filter="*">' . esc_html__('Everyone', LA_Team_Builder::$plugin['name']) . '</div>';
        echo '</li>';
        foreach ($terms as $term) {
            echo '<li class="filter-item">';
            echo '<div class="ats-button" data-filter="' . esc_attr( $term->term_id ) . '">' . esc_html( $term->name ) . '</div>';
            echo '</li>';
        }
        echo '<li class="filter-item">';
        include($base . 'public/partials/parts/search.php');
        echo '</li>';
        echo '</ul>';
    }
    ?>
    <script type="text/javascript">
        (function ($) {
            $(function () {
                ATS.init_filter(<?php echo esc_attr( $this->data->ID )?>, "<?php echo esc_html( $active_term ); ?>");
            });
        }(jQuery));
    </script>
    <?php
}