<?php
$id = $this->data->ID;
$button_color = $styles['button_color'];
$button_hover_color = $styles['button_hover_color'];
$button_text_color = $styles['button_text_color'];
$button_text_hover_color = $styles['button_text_hover_color'];
$layout_css .= "#ats-layout-" . esc_attr( $id ) . " .ats-button{background-color:" . esc_attr( $button_color ) .";color:" . esc_attr( $button_text_color ) . ";}";
$layout_css .= "#ats-layout-" . esc_attr( $id ) . " .ats-search input{background-color:" . esc_attr( $button_color ) .";color:" . esc_attr( $button_text_color ) . ";}";
$layout_css .= "#ats-layout-" . esc_attr( $id ) . " .ats-search input:focus{border-color:" . esc_attr( $button_hover_color ) . ";}";
$layout_css .= "#ats-layout-" . esc_attr( $id ) . " .ats-button.ats-button-active,#ats-layout-" . esc_attr( $id ) . " .ats-button:hover{background-color:" . esc_attr( $button_hover_color ) . ";color:" . esc_attr( $button_text_hover_color ) . ";}";