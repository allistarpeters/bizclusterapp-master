<ul id="action-add-buttons-tooltip">
    <li data-action="add-team">
        <?php echo esc_html( $labels['team']['add_new'] ); ?>
    </li>
    <li class="a-divider"></li>
    <li data-action="add-employer">
        <?php echo esc_html( $labels['employer']['add_new'] ); ?>
    </li>
</ul>