��    0      �              "     ;   @     |     �     �  
   �     �  3   �     �  .   �  /   .     ^     l  
   |     �     �  #   �     �     �  "   �  2        J     ^  	   k     u  (   �     �     �     �  3   �       .   (     W     ^  1   u     �  %   �  1   �  5     A   E     �     �     �     �     �     �     �  �    (   �
  ?        B     R     g     �     �  ?   �     �  7   �  9        O     a     o     x     �  -   �     �  !   �  *     M   1          �  
   �  $   �  E   �     $     3     B  >   X  
   �  ;   �  	   �  *   �  =        Q  @   d  ;   �  ?   �  W   !     y     �     �     �     �  
   �     �   %d item(s) generated successfully. Adjust this setting if you are experiencing timeout errors. Administrators Company Name Generator Custom field Date range Default Download random photos from https://picsum.photos/. Duration Enter YouTube video IDs separated with commas. Enter dummy data for this social media account. Featured date Field generator Field name Generate Now Generating... Item #%d could not be generated: %s Last active date range Last edited date range Max number of items to be assigned Max number of random %s to fetch from the database Max number of users Newest users No author Number of item to generate Number of records to process per request Number range Payment Plans Picsum Photo Generator Please configure additional options for each field. Prioirty Probability of generating value for this field Random Select files to attach Select images/files from WordPress media manager. Select manually Select the fields to generate for %s. Select the range of possible dates for the field. Select the range of possible durations for the field. Select the range of possible number of view counts for the field. Specific users Text length range View count range Vote count range YouTube videos generate contentGenerate — Select — Project-Id-Version: unnamed project
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=n != 1;
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-12-07 15:48+0000
PO-Revision-Date: 2019-12-08 17:00+0000
Last-Translator: Marcus Schröder <kommentar@salzsau-panorama.de>
Language-Team: Deutsch
Language: de_DE
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3 %d Artikel wurden erfolgreich generiert. Passen Sie diese Einstellung an, wenn Timeout-Fehler auftreten. Administratoren Firmenname Generator Benutzerdefiniertes Feld Datumsbereich Standard Laden Sie zufällige Fotos von https://picsum.photos/ herunter. Dauer Geben Sie durch Kommas getrennte YouTube-Video-IDs ein. Geben Sie Dummy-Daten für dieses Social Media-Konto ein. Empfohlenes Datum Feldgenerator Feldname Jetzt generieren Generieren ... Artikel #%d konnte nicht generiert werden:% s Letzter aktiver Datumsbereich Zuletzt geänderter Datumsbereich Maximale Anzahl der zuzuweisenden Elemente Maximale Anzahl zufälliger %s, die aus der Datenbank abgerufen werden sollen Maximale Anzahl von Benutzern Neueste Benutzer Kein Autor Nummer des zu generierenden Elements Anzahl der Datensätze, die pro Anforderung verarbeitet werden sollen Nummernbereich Zahlungspläne Picsum Foto Generator Bitte konfigurieren Sie zusätzliche Optionen für jedes Feld. Priorität Wahrscheinlichkeit, ein Wert für dieses Feld zu generieren Zufällig Wählen Sie die anzuhängenden Dateien aus Wählen Sie Bilder / Dateien aus dem WordPress Media Manager. Manuell auswählen Wählen Sie die Felder aus, die für %s generiert werden sollen. Wählen Sie den Bereich möglicher Daten für das Feld aus. Wählen Sie den Bereich der möglichen Dauer für das Feld aus. Wählen Sie den Bereich der möglichen Anzahl von Bewertungszählern für das Feld aus. Bestimmte Benutzer Textlängenbereich Zählbereich anzeigen Stimmenzahlbereich Youtube Videos Generieren — Wähle — 