# Creates Products CPT

*Dependencies*
* must use van11y-accessible-simple-tooltip-aria https://van11y.net/accessible-simple-tooltip/
* gulp moves the js to an available folder

*Development*
* currently gulp only copies the tooltip plugin javascript (from node_modules) into a dist folder
* css is not currently compiled with gulp