<?php
/**
 * shortcode will add enable-tooltip class to text 
 * and add content of Product Term to title
 *
 */
function product_terms_tooltip_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
			'text' => '',
			'id'    => '',
		),
		$atts,
		'product_term_tooltip'
	);
	 global $post;
	$post = get_post($atts['id']);
	$output = '<span class="js-simple-tooltip" data-simpletooltip-text="';
	$output .= '<strong>'.strip_tags($post->post_title).'</strong>';
	$output .= '<br>';
	$output .= '<p>'.strip_tags($post->post_content).'</p>';
	$output .= '" >';
	$output .= $atts['text'];
	$output .= '</span>';
	return $output;
}

add_shortcode( 'product_term_tooltip', 'product_terms_tooltip_shortcode' );