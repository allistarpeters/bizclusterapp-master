<?php
/**
 * Extends Custom_Post_Type_Controller in 020-cpt-controller.php 
 *
 * @package Haymarket
 */

 class productTermsCPT extends Custom_post_Type_Controller {

	const NAME                = 'Product Terms';
	const SINGULAR_NAME       = 'Product Term';
	const DESCRIPTION         = 'Haymarket Product Terms CPT';
	const PUBLICLY_QUERYABLE  = true;
	const EXCLUDE_SEARCH      = false;
	const ADD_TO_REST_API     = true;
	const MENU_POSITION       = 20;
	const ARCHIVE_PAGE        = true;
	const EXPORTABLE          = true;
	const DEBUG               = false;

	public function set_support_args() {
		$this->support_args = array(
			'title',
			'editor',
			'author',
			'custom-fields',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		);
	}
	public function set_lc_name() {
		return( $this->lc_name = 'product-terms' );
	}
	public function set_lc_singular_name() {
		return( $this->lc_singular_name = 'product-term' );
	}
 }
 $productTermsCPT = new productTermsCPT();
 
 add_action( 'init', array( $productTermsCPT, 'init' ), 0 );