<?php
/*
Plugin Name: Haymarket Product Terms Manager
Version: 1.0
Description: Creates Product Terms CPT and adds tooltip capability
Author: Theresa Newman
Text Domain: hm-product-terms-manager
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2014, Theresa Newman
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

include( 'inc/product-terms-cpt.php' );
include( 'inc/product-terms-shortcode.php' );

class HM_Product_Terms_Manager extends WP_Base {
	const VERSION               = '1.0';
	const FILE_SPEC             = __FILE__;
	const PRODUCT_TERMS_SCRIPTS_SLUG     = 'product-terms-scripts';
	const PRODUCT_TERMS_SCRIPTS_URL      = '/dist/all.js';
	const TOOLTIP_STYLE_SLUG   = 'product-terms-tooltip-style';
	const TOOLTIP_STYLE_URL    = '/css/tooltip.css';
	
	protected function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_styles') );
	}
	public function enqueue_scripts() {

        wp_register_script(
			self::PRODUCT_TERMS_SCRIPTS_SLUG,
			$this->get_asset_url(self::PRODUCT_TERMS_SCRIPTS_URL),
			array(),
			self::VERSION,
			self::IN_FOOTER
		);

        wp_enqueue_script( self::PRODUCT_TERMS_SCRIPTS_SLUG );
	}
	public function enqueue_styles() {

        wp_register_style(
			self::TOOLTIP_STYLE_SLUG,
			$this->get_asset_url(self::TOOLTIP_STYLE_URL),
			array(),
			self::VERSION,
			self::IN_HEADER
		);

        wp_enqueue_style( self::TOOLTIP_STYLE_SLUG );
	}

	
	
	protected function activation_actions() {}

	protected function deactivation_actions() {}

	public function init() {
		register_activation_hook( __FILE__, array( 'HM_Product_Terms_CPT', 'activator' ) );
		register_activation_hook( __FILE__, array( 'HM_Product_Terms_CPT', 'deactivator' ) );
	}

}

$hm = HM_Product_Terms_Manager::get_instance();