<?php

class Registration_Manager_Admin {
	const PAGE_TITLE    = 'Registration Manager';
	const MENU_SLUG     = 'registration-manager';
	const METHOD_PREFIX = 'registration_manager';
	const MAX_WIDTH     = '100%'; // the % in the format causes issues
    const LINE_WIDTH    = '700px';
	const FIELD_FMT     = '<input type="text" id="%s" name="%s" value="%s" style="max-width:%s; width: %s;" />';

	private $script_urls;
	
	public $fields= array(
			'HMI Manifest URL' => 'hmi-manifest-url',
			'HMI Vendor URL'   => 'hmi-vendor-url',
			'HMI Bundle URL'   => 'hmi-bundle-url',
			'HMI Callback URL' => 'hmi-callback-url',
	);

	public function __construct() {
		$this->get_options();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}
	
	/**
	 * @return array|null
	 */
	public function get_script_urls() {
		if ( is_array( $this->script_urls ) && ! empty( $this->script_urls ) ) {
			return( $this->script_urls );
		}
		return null;
	}
	
	/**
	 * Register the admin page and setup the menu in WordPress
	 */
	public function admin_settings() {
		add_options_page(
			'Registration Manager',
			self::PAGE_TITLE,
			'manage_options',
			self::MENU_SLUG,
			array( $this, 'show_admin_page' )
		);
	}
	
	/**
	 * Render the admin page in the WordPress CMS
	 */
	public function show_admin_page() {
		?>
		<div class="wrap">
			<h1><?php static::PAGE_TITLE ?> Admin</h1>
			<form method="post" action="options.php">
				<?php
				settings_fields( static::METHOD_PREFIX . '_options' );
				do_settings_sections( static::METHOD_PREFIX . '_settings' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}
	
	/**
	 * Reads the options stored in WordPress
	 */
	public function get_options() {
		foreach ($this->fields as $field) {
			$this->script_urls[$field] = get_option( static::MENU_SLUG . '-' . $field );
		}
	}
	
	/**
	 * Renders the input fields with the stored data (if any) retrieved from WordPress
	 */
	public function add_fields() {
		foreach ( $this->fields as $key => $field ) {
			add_settings_field(
				static::MENU_SLUG . '-' . $field, // ID
				$key, // Title
				array( $this, 'url_validator' ), // Callback
				static::METHOD_PREFIX . '_settings', // Page
				static::METHOD_PREFIX . '_main' // Section
			);
		}
	}
	
	/**
	 * Initializes the admin page assigning the fields and labels
	 */
	public function admin_page_init() {
		foreach ( $this->fields as $key => $field ) {
			register_setting(
				static::METHOD_PREFIX . '_options', // Option group
				static::MENU_SLUG . '-' . $field // Option name
			);
	
			add_settings_section(
				static::METHOD_PREFIX . '_main', // ID
				static::PAGE_TITLE . ' Settings', // Title
				array($this, 'print_section_info'), // Callback
				static::METHOD_PREFIX . '_settings' // Page
			);
			
			add_settings_field(
				static::MENU_SLUG . '-' . $field, // ID
				$key, // Title
				array( $this, 'url_validator' ), // Callback
				static::METHOD_PREFIX . '_settings', // Page
				static::METHOD_PREFIX . '_main', // Section
				$field
			);
		}
	}
	
	/**
	 * Prints the admin page section heading
	 */
	public function print_section_info() {
		$msg = '<b>Note</b> Only add override URLs to this section for testing. Also all registration system JavaScript URLs will be enqueued as deferred in the footer.';
		echo wpautop( $msg );
	}
	
	/**
     * Validates the stored/user inputted field data (if any) and outputs it in the field in the CMS
	 * @param $field
	 */
	public function url_validator( $field ) {
		$option = isset( $this->script_urls[$field] ) ? esc_attr( $this->script_urls[$field] ) : '';
		printf(
            static::FIELD_FMT,
           static::MENU_SLUG . '-' . $field,
            static::MENU_SLUG . '-' . $field,
            $option,
            static::MAX_WIDTH,
            static::LINE_WIDTH
		);
	}
}
