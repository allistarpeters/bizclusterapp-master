<?php
/*
Plugin Name: Registration Manager
Version: 1.0.2
Description: Allows that overriding of the three JavaScript assets related to the Haymarket Registration system.
Author:
Author URI: https://www.mikelking.com
Plugin URI: https://www.jafdip.com
Text Domain: hm-registration
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

  Copyright (c) 2017, Mikel King (mikel.king@olivent.com)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.

	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.

	* Neither the name of the {organization} nor the names of its
	  contributors may be used to endorse or promote products derived from
	  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

include( 'inc/registration-manager-admin.php' );

class Registration_Manager extends WP_Base {
	const VERSION              = '1.0.2';
	const PRIORITY             = 10;
	const FILTER_NAME          = 'hmi-registration-scripts';

	private $script_urls;
	private $default_script_urls = array(
		'hmi-manifest-url' => 'https://s3.amazonaws.com/haymarket-reg-js/develop/production/hmi-registration-ui.manifest.js',
		'hmi-vendor-url'   => 'https://s3.amazonaws.com/haymarket-reg-js/develop/production/hmi-registration-ui.vendor.js',
		'hmi-bundle-url'   => 'https://s3.amazonaws.com/haymarket-reg-js/develop/production/hmi-registration-ui.bundle.js',
		'hmi-callback-url' => 'http://subapi.haymarketmedia.com/subscriber',
    );

	public $registration_manager;

	public function __construct() {
		parent::__construct();
		$this->registration_manager = new Registration_Manager_Admin();

		add_filter( static::FILTER_NAME, array( $this, 'get_scripts_filter' ), self::PRIORITY, 1 );
	}

	/**
	 *
	 * @return array
	 */
	public function get_scripts_filter( array $scripts = null ) {

		$this->script_urls = $this->registration_manager->get_script_urls();
		$this->verify_script_urls( $scripts );

		return $this->script_urls;
	}

	/**
	 * Check to see that we are not dealing with an empty hand/
	 */
	public function verify_script_urls( $scripts ) {
		if ( empty( $this->script_urls ) || in_array( '', $this->script_urls ) ) {
			$this->script_urls = $scripts;
			return( true );
		}
		return( false );
	}

	public function get_script_urls() {
		return $this->script_urls;
	}
}

Registration_Manager::get_instance();
