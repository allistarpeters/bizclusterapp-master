<?php
/*
Plugin Name: Haymarket Feathr Tool
Version: 1.2
Description: Allows that addition of the Feathr JavaScript inserted deferred asynchronously into the footer.
Author:
Author URI: https://www.mikelking.com
Plugin URI: https://www.jafdip.com
Text Domain: hm-feathr-tool
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

  Copyright (c) 2019, Mikel King (mikel.king@olivent.com)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.

	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.

	* Neither the name of the {organization} nor the names of its
	  contributors may be used to endorse or promote products derived from
	  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


include( 'inc/feathr-manager-admin.php' );

class HM_Feathr_tool extends WP_Base {
	const VERSION     = '1.2';
	const FILE_SPEC   = __FILE__;
	const SCRIPT_SLUG = 'hm-feathr-tool';
	const SCRIPT_URL  = 'js/feather-tool.js';
	const LOCAL_ID    = 'hm_feathr';
	
	public $feathr_settings;
	
	public function __construct() {
		parent::__construct();
		$feathr = new Feathr_Manager_Admin();
		$this->feathr_settings = $feathr->get_options();
		$vb = new Variant_Base();
		if ( ! $vb->is_ad_blocked() ) {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), self::PRIORITY );
			add_filter( 'script_loader_tag', array( __CLASS__, 'async_filter_tag' ), self::PRIORITY, 3 );
		}
	}

	public function enqueue_scripts() {
		wp_register_script(
			self::SCRIPT_SLUG,
			$this->get_asset_url(self::SCRIPT_URL),
			array(),
			self::VERSION,
			self::IN_FOOTER
		);
		
		
		wp_localize_script(
			self::SCRIPT_SLUG,
			self::LOCAL_ID,
			$this->feathr_settings
		);
		
		Base_Plugin::set_async_assets( self::SCRIPT_SLUG );
		wp_enqueue_script( self::SCRIPT_SLUG );
	}
}

HM_Feathr_tool::get_instance();
