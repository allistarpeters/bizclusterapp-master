<?php

class Feathr_Manager_Admin extends Base_Manager_Admin {
	const PAGE_TITLE    = 'Feathr Manager';
	const MENU_SLUG     = 'feathr-manager';
	const METHOD_PREFIX = 'feathr_manager';
    const LINE_WIDTH    = '210px';
	const SECTION_INFO  = '<b>Note</b> Enter the value of the Feathr Tool API key relevant to this site.';

	public $options;

	public $fields= array(
			'Feathr API Key' => 'Feathr_API_Key',
	);

	public function __construct() {
		$this->get_admin_options();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}
}
