<?php
/*
Plugin Name: Haymarket Charbeat
Version: 1.0
Description: Installs Chartbeat auto-generated site specific (MM&M ONLY) JavaScript analytics beacon.
Author:
Author URI: https://www.mikelking.com
Plugin URI: https://www.jafdip.com
Text Domain: hm-chartbeat
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

  Copyright (c) 2019, Mikel King (mikel.king@olivent.com)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.

	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.

	* Neither the name of the {organization} nor the names of its
	  contributors may be used to endorse or promote products derived from
	  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


class HM_Chartbeat extends WP_Base {
	const VERSION          = '1.0';
	const FILE_SPEC        = __FILE__;
	const FILTER_TAG       = '<script ';
	const ASYNC_FILTER_TAG = '<script async ';
	const DEFER_FILTER_TAG = '<script defer ';
	const CHARTBEAT_SLUG = 'hm_Chartbeat';
	const CHARTBEAT_URL  = 'js/chartbeat.js';

	public function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), self::PRIORITY );
        add_filter( 'script_loader_tag', array( __CLASS__, 'async_filter_tag' ), self::PRIORITY, 3 );
	}

	public function enqueue_scripts() {
		wp_register_script(
			self::CHARTBEAT_SLUG,
			$this->get_asset_url(self::CHARTBEAT_URL),
			array(),
			self::VERSION,
			self::IN_HEADER
		);
		Base_Plugin::set_async_assets( self::CHARTBEAT_SLUG );
		wp_enqueue_script( self::CHARTBEAT_SLUG );
	}
}

HM_Chartbeat::get_instance();
