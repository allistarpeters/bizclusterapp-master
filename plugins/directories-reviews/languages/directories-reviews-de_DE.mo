��    2      �      <      <     =  	   F  
   P     [     k  .   �     �  
   �     �     �     �     �     �     �     �       C        a     t     |     �  	   �     �     �     �      �      �     �  3        B     b     o     {     �     �     �     �     �     �  
   �                    ,     I  @   i     �     �     �  �  �     �	     �	     �	     �	  #   �	  2   �	     /
     @
     L
  
   R
     ]
  	   c
     m
     |
     �
     �
  d   �
  ,        <     H     V     \  	   l     v     �  )   �  +   �     �  >     '   B     j     {     �  #   �     �  	   �     �                ;     I     U     f  0   �  3   �  R   �     >     D     ]   %s (asc) %s review %s reviews 0 (no decimals) A new review has been submitted A new review has been submitted for your %1$s. All Reviews Bar height Color Comments Date Dear %s, Decimals Default Display format Enable reviews Enter the character used to separate the rating criteria and value. Hide if no reviews Listing Overall rating Photos Published Rating Rating Stars Rating bars Rating bars (by number of stars) Rating bars (by rating criteria) Rating criteria Rating criteria slugs must be 1-40 characters long. Rating criteria/value separator Rating stars Rating step Recalculate review ratings Render bar labels inline Render labels inline Review Review Rating Review Ratings Review Settings Review: %s Reviews Search reviews Show review count Show review count with label Shows aggregated review ratings This tool will recalculate review ratings for each content item. Title Write a Review slug Project-Id-Version: unnamed project
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=n != 1;
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-12-08 11:21+0000
PO-Revision-Date: 2019-12-08 17:12+0000
Last-Translator: Marcus Schröder <kommentar@salzsau-panorama.de>
Language-Team: Deutsch
Language: de_DE
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3 %s (asc) %s Bewertung % s Bewertungen 0 (keine Kommastellen) Eine neue Bewertung wurde abgegeben Eine neue Bewertung wurde für ihre %1$s abgegeben Alle Bewertungen Balkenhöhe Farbe Kommentare Datum Lieber %s Dezimalstellen Standard Anzeigeformat Bewertungen aktivieren Geben Sie das Zeichen ein, mit dem die Bewertungskriterien und der Wert voneinander getrennt werden. Ausblenden, wenn keine Bewertungen vorliegen Verzeichnis Gesamtwertung Fotos Veröffentlicht Bewertung Bewertungssterne Bewertungsbalken Bewertungsbalken (nach Anzahl der Sterne) Bewertungsbalken (nach Bewertungskriterien) Bewertungskritierien Slugs für Bewertungskriterien müssen 1-40 Zeichen lang sein. Bewertungskriterien / Wertetrennzeichen Bewertungssterne Bewertungsschritt Bewertungen neu berechnen Balkenbeschriftungen inline rendern Beschriftungen inline rendern Bewertung Renzension bewerten Bewertungen überprüfen Einstellungen überprüfen Bewertung: %s Bewertungen Bewertung suchen Anzahl der Bewertungen anzeigen Anzahl der Bewertungen mit Beschriftung anzeigen Zeigt die aggregierten Bewertungsüberprüfungen an Dieses Tool berechnet die Überprüfungsbewertungen für jedes Inhaltselement neu. Titel Eine Rezension schreiben slug 