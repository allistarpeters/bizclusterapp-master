��    1      �  C   ,      8     9  	   B  
   L     W     g  
   s     ~     �     �     �     �     �     �  C   �                    #     *     7      C      d     �  3   �     �     �     �               6     K     R     `     o  
        �     �  	   �     �     �     �     �  @        [     a  9   p     �     �    �     �
     �
     �
     �
          (     <     D     Q  
   V     a     m     �  Z   �     �     �       
        "     7  ,   L  0   y     �  C   �  ,        2     I  *   ^  $   �  %   �     �     �     �          7     G  +   T  	   �     �  #   �  3   �  $   �  S   "     v     |  9   �     �     �                      (   $       *       
   .       "   &                    ,         /      )      +                    !      #       %   1          0                               -                               '               	       %s (asc) %s review %s reviews 0 (no decimals) All Reviews Bar height Color Comments Date Decimals Default Display format Enable reviews Enter the character used to separate the rating criteria and value. Listing Overall rating Photos Rating Rating Stars Rating bars Rating bars (by number of stars) Rating bars (by rating criteria) Rating criteria Rating criteria slugs must be 1-40 characters long. Rating criteria/value separator Rating stars Rating step Recalculate review ratings Render bar labels inline Render labels inline Review Review Rating Review Ratings Review Settings Review: %s Reviews Reviews add-on for Directories. SabaiApps Search reviews Show review count Show review count with label Shows aggregated review ratings This tool will recalculate review ratings for each content item. Title Write a Review https://codecanyon.net/user/onokazu/portfolio?ref=onokazu https://directoriespro.com/ slug Plural-Forms: nplurals=2; plural=(n > 1);
Project-Id-Version: Directories - Reviews
PO-Revision-Date: 2019-02-21 15:37-0500
Language-Team: jean-pierre michaud <i18n@wpenhancer.agency>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: directories-reviews.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: jean-pierre michaud <i18n@wpenhancer.agency>
Language: fr_FR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %s (asc) %s évaluation %s évaluations 0 (pas de décimale) Toutes les Évaluations Hauteur de la barre Couleur Commentaires Date Décimales Par défaut Format d’affichage Activer les évaluations Entrer les caractère utilisé pour séparer les critères de classement et leurs valeurs. Annonce Classement global Photos Classement Étoiles de notation Barres de Classement Barres de classement (par nombre d'étoiles) Barres de classement (par critères de notation) Critère de classement Le plug des critères de classement doit être de 1-40 caractères. Critère de Classement/séparateur de valeur Étoiles de Classement Étape de Classement Recalculer les classements d’évaluation Afficher les étiquettes à la suite Afficher les évaluations à la suite Évaluation Classement de l’Évaluation Classements d’Évaluation Paramètres des Évaluations Évaluation: %s Évaluations Évaluation d’articles pour Répertoires. SabaiApps Recherche des évaluations Afficher le nombre d’évaluations Afficher le nombre d’évaluations avec étiquette Affiche les évaluations regroupées Cet outil recalculera les classements d’évaluation pour chaque article proposé. Titre Rédiger une évaluation https://codecanyon.net/user/onokazu/portfolio?ref=onokazu https://directoriespro.com/ slug 