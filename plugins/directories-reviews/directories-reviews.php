<?php
/**
 * Plugin Name: Directories - Reviews
 * Plugin URI: https://directoriespro.com/
 * Description: Reviews add-on for Directories.
 * Author: SabaiApps
 * Author URI: https://codecanyon.net/user/onokazu/portfolio?ref=onokazu
 * Text Domain: directories-reviews
 * Domain Path: /languages
 * Version: 1.3.12
 */

add_filter('drts_core_component_paths', function ($paths) {
    $paths['directories-reviews'] = [__DIR__ . '/lib/components', '1.3.12'];
    return $paths;
});

if (is_admin()) {
    register_activation_hook(__FILE__, function () {
        if (!function_exists('drts')) die('The directories plugin needs to be activated first before activating this plugin!');
    });
    add_action('in_plugin_update_message-directories-reviews/directories-reviews.php', function () {
        printf(
            ' ' . esc_html__('Make sure to bulk update all Directories plugins at once. See %s for more details.', 'directories-reviews'),
            '<a href="https://directoriespro.com/documentation/updating-and-uninstalling/updating-directories-pro.html#automatic-update" target="_blank">Updating Directories Pro</a>'
        );
    });
}
