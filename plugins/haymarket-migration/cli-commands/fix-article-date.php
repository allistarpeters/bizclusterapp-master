<?php

class FixArticleDate extends WP_CLI_Command {

	public $articles_fixed = [];

	/**
	 * Fix article publish date in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @return void
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );

		if ( isset( $legacy_data->Date ) ) {
			update_post_meta( $post_id, 'hm-article-date', date( 'n/j/Y g:i A', $legacy_data->Date ) );
			$this->articles_fixed[] = $post_id;
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'post',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
			// \Haymarket\Migration\Base\stop_the_insanity();
		}
		wp_cli::success( count( $this->articles_fixed ) . ' article dates were fixed' );

		wp_reset_postdata();
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-article-date', 'FixArticleDate' );
