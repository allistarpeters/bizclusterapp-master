<?php

class FixArticleDSM extends WP_CLI_Command {

	public $articles_fixed = [];

	/**
	 * Fix article article DMS legacy Ids
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate fix-dms 6
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			$args,
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @return void
	 */
	public function update_posts( $row ) {
		$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $row[1], 'any', 'hm-dsm-id' );

		if ( $post_id && has_term( 'dsm', 'hm-article-type', $post_id ) ) {
			update_post_meta( $post_id, '_hmm-mig-id', filter_var( $row[0], FILTER_SANITIZE_NUMBER_INT ) );
			$this->articles_fixed[] = $post_id;
		}
	}

	/**
	 * Invokes a callback function per each post
	 *
	 * @param string $callback   Name of the callback function.
	 * @param array  $args       sites option.
	 * @param array  $assoc_args attional args
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $args, $assoc_args = false ) {

		if ( false !== ( $handle = fopen( HMM_PATH . 'dsm-mapping.csv', 'r' ) ) ) {
			$is_headers = true;
			while ( false !== ( $row = fgetcsv( $handle, 1000, ',' ) ) ) {

				if ( $is_headers ) {
					$is_headers = false;
					continue;
				}

				if ( (int) $row[2] === (int) $args[0] ) {
					call_user_func( $callback, $row );
				}
			}
			fclose( $handle );
		}

		wp_cli::success( count( $this->articles_fixed ) . ' article legacy ids were fixed' );

		wp_reset_postdata();
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-dms', 'FixArticleDSM' );
