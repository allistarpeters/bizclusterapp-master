<?php
class ImageCleanup extends WP_CLI_Command {

	/**
	 * Get images/attachments in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, isset( $assoc_args['featured'] ) ? 'update_description' : 'update_alt_text' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Update featured image description
	 *
	 * @param $query
	 */
	public function update_description( $query ) {
		global $post;
		$query->the_post();

		$legacy_data = get_post_meta( $post->ID, '_hmm-mig-legacy-data', true );

		if ( ! empty( $legacy_data ) && ! empty( $legacy_data->Image ) && ! empty( $legacy_data->Image->Description ) ) {
			$image_url = $legacy_data->Image->DynamicSource;

			// Remove query parameters from URL.
			$image_url = strtok( $image_url, '?' );

			$existing = new \WP_Query( [
				'post_type'              => 'attachment',
				'post_mime_type'         => 'image',
				'post_status'            => 'inherit',
				'meta_query'             => [
					[
						'key' => 'legacy_url',
						'value' => $image_url,
					]
				],
				'fields'                 => 'ids',
				'posts_per_page'         => 1,
				'no_found_rows'          => true,
				'update_post_term_cache' => false,
			] );

			if ( $existing->have_posts() ) {
				$image_id = reset( $existing->posts );
				$image = get_post( $image_id );

				if ( empty( $image->post_content ) ) {
					wp_update_post(
						[
							'ID' => $image_id,
							'post_content' => sanitize_text_field( $legacy_data->Image->Description )
						]
					);
				}
			}
		}
	}

	/**
	 * Update alt text based on caption
	 *
	 * @param $query
	 */
	public function update_alt_text( $query ) {
		global $post;
		$query->the_post();

		$alt_text = get_post_meta( $post->ID, '_wp_attachment_image_alt', true );
		if ( ( empty( $alt_text ) || '1' === trim( $alt_text ) ) && ! empty( $post->post_excerpt ) ) {
			update_post_meta( $post->ID, '_wp_attachment_image_alt', sanitize_text_field( $post->post_excerpt ) );
		} elseif ( '1' === trim( $alt_text ) ) {
			update_post_meta( $post->ID, '_wp_attachment_image_alt', '' );
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => isset( $assoc_args['featured'] ) ? 'post' : 'attachment',
				'post_status'    => 'any',
				'offset'         => $offset,
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Getting ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();

		WP_CLI::success( 'Done cleaning up' );
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate image-cleanup', 'ImageCleanup' );
