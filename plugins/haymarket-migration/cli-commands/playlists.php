<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users;

class Playlists extends WP_CLI {
	/**
	 * Migrates Playlists
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate playlists 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {

		$playlists = $this->get_playlists( (int) $args[0] );
		$report = $this->create_playlists( $playlists );
		$final_report = ( $report ) ? $report : [];
		$count = count( $playlists );
		Reporting\report( $final_report, $count, 'Playlists' );
	}

	/**
	 * Creates playlist post types from api playlist items.
	 *
	 * @method create_playlists
	 * @param  array $results A results array from api response.
	 * @return array|void
	 */
	public function create_playlists( $results ) {

		// Will return array with status message regardless, so we need to check both results and an item in the array.
		if ( empty( $results ) || empty( $results[0]->{'Playlist ID'} ) ) {
			return;
		}

		$count  = is_array( $results ) || $results instanceof Countable ? count( $results ) : 0;
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Playlists', 'haymarket' ), $count );
		$report = [];

		foreach( $results as $playlist ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $playlist->{'Playlist ID'}, 'hm-playlist' );
			if ( ! empty( $existing_id ) ) {
				$report['Existing Playlist'][] = 0;
				$progress->tick();
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $playlist->{$api_key} ) ) {
					continue;
				}
				if ( 'Images' === $api_key ) {
					continue;
				}
				$api_value                = $playlist->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_title'   => $playlist->Name ? $playlist->Name : '',
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $playlist->CreateDate ) ),
					'post_type'    => 'hm-playlist',
					'post_status'  => 'publish',
					'post_author'  => Users\get_default_user_id( $playlist->CreatedBy ),
					'post_excerpt' => $playlist->Deck ? $playlist->Deck : '',
				]
			);

			if ( empty( $post_id ) ) {
				$report['Create Failed'][] = 0;
				$progress->tick();
				continue;
			}

			// generate the playlist items.
			$playlist_items = [];
			$video_order = [];

			foreach ( $playlist->PlaylistMedia as $media ) {
				$playlist_items[] = [
					'name'        => ( $media->MediaDo->Name ) ? $media->MediaDo->Name : '',
					'description' => ( $media->MediaDo->Description ) ? $media->MediaDo->Description : '',
					'video'       => ( $media->MediaDo->Video ) ? $media->MediaDo->Video : '',
				];
				$video_order[ $media->Id ] = $media->Position;
				// Used for doing redirect look up
				add_post_meta( $post_id, HMM_PREFIX . 'vid-id', $media->Id );
			}

			if( ! empty( $video_order ) ){
				// Used for doing redirect chapter
				add_post_meta( $post_id, HMM_PREFIX . 'vid-order', $video_order );
			}

			if ( ! empty( $playlist_items ) ) {
				$added = add_post_meta( $post_id, 'hm-playlist', $playlist_items );
				if( ! empty( $added ) ) {
					$report[ 'playlist-items' ][] = count( $playlist_items );
				}
			}

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			foreach( $playlist->PlaylistTags as $playlist_tag ){

				// set tags to Topics
				wp_set_object_terms(
					$post_id,
					$playlist_tag->TagId,
					'post_tag',
					 true
				);
			}

			if( ! empty( $playlist->PrimarySectionDo ) ){
				// set category to Section
				wp_set_object_terms(
					$post_id,
					$playlist->PrimarySectionDo->Name,
					'category'
				);
			}

			$report['Playlists Imported'][] = $post_id;

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $playlist );
			$progress->tick();
		}
		$progress->finish();

		return $report;
	}

	/**
	 * Query API for newsletters
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_playlists( int $pub_id = 0 ) {

		if ( empty( $pub_id ) ) {
			return;
		}

		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/playlists", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Playlist ID'                 => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'Name'            => [
				'name' => 'hm-name',
				'type' => 'string',
			],
			'IsForWeb'    => [
				'name' => 'hm-visible-web',
				'type' => 'bool',
			],
			'IsForMobile'  => [
				'name' => 'hm-visible-mobile',
				'type' => 'bool',
			],
			'SuppressSocialSharing'        => [
				'name' => 'hm-hide-social',
				'type' => 'bool',
			],
			'MinimumAccessLevel' => [
				'name' => 'hm-minimum-acces-level',
				'type' => 'int',
			],
		];
	}

}

WP_CLI::add_command( 'haymarket-migrate playlists', 'Playlists' );
