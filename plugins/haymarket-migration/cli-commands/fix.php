<?php

class Fix extends WP_CLI_Command {

	public $print_issues_assigned = [];
	public $print_sections_assigned = [];
	public $images_updated = [];
	public $article_types_assigned = [];
	public $sponsors_assigned = [];
	/**
	 * Regenerates media attachments in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @param \WP_Query $query WordPress query object.
	 *
	 * @uses Update_Categories->set_primary_category()
	 *
	 * @return void
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$print_issue = '';
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );
		$print_issue = isset( $legacy_data->PrintIssue->Name ) ? $legacy_data->PrintIssue->Name : '';
		$print_section = isset( $legacy_data->PrintSection->Name ) ? $legacy_data->PrintSection->Name : '';
		$article_type = isset( $legacy_data->Type ) ? $legacy_data->Type : '';
		$sponsordId = isset( $legacy_data->SponsorId ) ? $legacy_data->SponsorId : '';

		// set sponsors by legacy ID lookup.
		if ( ! empty( $sponsordId ) ) {
			$term_id = Haymarket\Migration\Helpers\get_term_by_legacy_id( $sponsordId, 'hm-sponsor' );
			if ( ! empty( $term_id ) ) {
				$added = wp_set_object_terms(
					$post_id,
					$sponsordId,
					'hm-sponsor'
				);

				if	( ! is_wp_error( $added ) ) {
					$this->sponsors_assigned[] = $added;
				}
			}
		}

		if ( $print_issue ) {
			// set tags to Topics.
			$added = wp_set_object_terms(
				$post_id,
				$print_issue,
				'hm-print-issue'
			);
			if	( ! is_wp_error( $added ) ) {
				$this->print_issues_assigned[] = $added;
			}
		}

		if ( $print_section ) {
			// set tags to Topics.
			$added = wp_set_object_terms(
				$post_id,
				$print_section,
				'hm-print-section'
			);
			if	( ! is_wp_error( $added ) ) {
				$this->print_sections_assigned[] = $added;
			}
		}

		if ( $article_type ) {
			// set tags to Topics.
			$added = wp_set_object_terms(
				$post_id,
				$article_type,
				'hm-article-type'
			);
			if	( ! is_wp_error( $added ) ) {
				$this->article_types_assigned[] = $added;
			}
		}

		// get first 50 characters of post content and check for image tag - images are often nested in other tags, so can't check just beginnning of text
		if ( false === \Haymarket\Migration\Helpers\is_featured_image_inline( get_the_content() ) ) {
			// update post meta to uncheck box.
			delete_post_meta( $post_id, 'hm-hide-featured-image' );
			$this->images_updated[] = $post_id;
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query          = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page'         => $posts_per_page,
				'post_type'              => 'post',
				'post_status'            => 'any',
				'offset'                 => $offset,
				'orderby'                => 'ID',
				'order'                  => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
			// \Haymarket\Migration\Base\stop_the_insanity();
		}
		wp_cli::success( count( $this->print_issues_assigned ) . ' print issues were assigned' );
		wp_cli::success( count( $this->print_sections_assigned ) . ' print sections were assigned' );
		wp_cli::success( count( $this->images_updated ) . ' images metas were updated' );
		wp_cli::success( count( $this->article_types_assigned ) . ' articles types were updated' );
		wp_cli::success( count( $this->sponsors_assiged ) . ' sponsors were updated' );

		wp_reset_postdata();

	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix', 'Fix' );
