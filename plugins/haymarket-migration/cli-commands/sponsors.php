<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Images as Images;

class Sponsors extends WP_CLI {
	/**
	 * Migrates Sponsors
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration sponsors
	 */
	public function __invoke() {
		$sponsors = array_reverse( $this->get_sponsors() );
		$count    = count( $sponsors );
		$report   = [];
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Sponsors', 'haymarket' ), $count );

		foreach ( $sponsors as $sponsor ) {
			$existing_id = Helpers\get_term_by_legacy_id( (int) $sponsor->SponsorID, 'hm-sponsor' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}
			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $sponsor->{$api_key} ) ) {
					continue;
				}
				$api_value                = $sponsor->{$api_key};
				$type                     = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $type );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			// If this is a duplicate term, cache for later meta insert
			$duplicate_term = get_term_by( 'name', $sponsor->Name, 'hm-sponsor' );

			$term = wp_insert_term(
				$sponsor->Name,
				'hm-sponsor',
				[
					'description' => $sponsor->Description,
					'slug'        => sanitize_title( "{$sponsor->Name}-{$sponsor->SponsorID}" ),
				]
			);

			if ( is_wp_error( $term ) ) {
				wp_cli::debug( $term->get_error_message() . ' - ' . $sponsor->Name );
				$progress->tick();
				continue;
			}

			// Record duplicate terms for later reporting
			if ( $duplicate_term ) {
				$key            = 'duplicate-term';
				$duplicate_meta = add_term_meta( $term['term_id'], $key, true );
				if ( $duplicate_meta ) {
					$report[ $key ][] = $key;
				}
			}

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_term_meta( $term['term_id'], $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}
			// Sponsor Type.
			if ( ! empty( $sponsor->{'Sponsor Type'} ) ) {
				$key        = 'hm-sponsor-type';
				$value      = $this->sponsor_type_map()[ $sponsor->{'Sponsor Type'} ];
				$meta_added = add_term_meta( $term['term_id'], $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $value;
				}
			}
			// Event Sponsor Type.
			if ( ! empty( $sponsor->{'Event Sponsor Type'} ) ) {
				$key        = 'hm-event-sponsor-types';
				$value      = $this->sponsor_event_type_map()[ $sponsor->{'Event Sponsor Type'} ] ?? '';
				if( ! empty( $value ) ){
					$meta_added = add_term_meta( $term['term_id'], $key, $value );
					if ( $meta_added ) {
						$report[ $key ][] = $value;
					}
				}
			}

			// Images.
			if ( ! empty( $sponsor->{'Image Group'} ) ) {
				$image_id = Images\upload_remote_image( $sponsor->{'Image Group'}->Images[0]->Url );
				if ( $image_id ) {
					$key        = 'image';
					$value      = (int) $image_id;
					$meta_added = add_term_meta( $term['term_id'], $key, $value );
					if ( $meta_added ) {
						$report[ $key ][] = $value;
					}
				}
			}

			update_term_meta( $term['term_id'], HMM_PREFIX . 'legacy-data', $sponsor );

			$report['Sponsors Imported'][] = $term['term_id'];
			$progress->tick();
		} // End foreach().
		$progress->finish();

		update_term_meta( $term->term_id, HMM_PREFIX . 'legacy-data', $sponsor );

		Reporting\report( $report, $count, 'Sponsors' );
	}

	/**
	 * Query API for newsletters
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_sponsors() {
		$request = wp_remote_get( API_V2_URL . 'sponsors', [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'SponsorID' => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'URL'       => [
				'name' => 'hm-sponsor-url',
				'type' => 'url',
			],
		];
	}

	/**
	 * Get sponsor type ids by name
	 */
	function sponsor_type_map() {
		return [
			'NativeAd' => 1,
			'Event'    => 2,
		];
	}

	/**
	 * Get Sponsor event type IDs by name
	 *
	 * @return array
	 */
	function sponsor_event_type_map() {
		return [
			'Headline'                        => 6,
			'Platinum'                        => 1,
			'Gold'                            => 2,
			'Silver'                          => 3,
			'Partners'                        => 4,
			'Exhibitors'                      => 5,
			'Bronze'                          => 7,
			'Panel'                           => 8,
			'PRWeek Partners'                 => 12,
			'MM&M Partners'                   => 13,
			'Cocktail Reception'              => 14,
			'DMN Partners'                    => 15,
			'Sponsor'                         => 17,
			'Cocktail Reception & Photobooth' => 18,
			'Digital & Social Media'          => 19,
			'Partner'                         => 16,
		];
	}

	/**
	 * Prints a table of terms that match existing terms
	 *
	 * @return void
	 */
	public function get_duplicate_sponsors() {
		$terms  = get_terms( [
			'taxonomy'   => 'hm-sponsor',
			'hide_empty' => false,
			'meta_query' => [
				[
					'key'     => 'duplicate-term',
					'value'   => true,
					'compare' => '=',
				],
			],
		] );
		$report = [];
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$report[] = [
					'name' => $term->name,
					'id'   => $term->term_id
				];
			}
		}

		WP_CLI\Utils\Format_Items( 'table', $report, array_keys( $report[0] ) );
	}
}

WP_CLI::add_command( 'haymarket-migrate sponsors', 'Sponsors' );

