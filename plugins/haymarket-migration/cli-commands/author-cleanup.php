<?php

use Haymarket\Migration\Helpers as Helpers;

class AuthorCleanup extends WP_CLI_Command {

	/**
	 * @var array List of duplicate authors
	 */
	private $list = array();

	/**
	 * Get duplicate authors in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'get_authors' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Populate $list with duplicate guest authors
	 *
	 * @param $query
	 */
	public function get_authors( $query ) {
		$query->the_post();

		$post_id   = get_the_ID();
		$user_name = get_the_title( $post_id );
		if ( isset( $this->list[ $user_name ] ) ) {
			$this->list[ $user_name ][] = $post_id;
		} else {
			$this->list[ $user_name ] = [ $post_id ];
		}
	}

	/**
	 * Return the linked users from the list of duplicates.
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function maybe_get_linked_account( $ids ) {
		$real_user = false;
		foreach ( $ids as $id ) {
			$linked_account = get_post_meta( $id, 'cap-linked_account', true );
			if ( $linked_account ) {
				$real_user = $id;
				break;
			}
		}

		return $real_user;
	}

	/**
	 * Reassign one users post to another. Also update the post author if the
	 * user has a linked account
	 *
	 * @param      $primary_user
	 * @param      $duplicate_user
	 * @param bool $linked
	 *
	 * @return bool
	 */
	public function reassign_posts( $primary_user, $duplicate_user, $linked = false ) {
		$primary_term   = get_the_terms( $primary_user, 'author' );
		$duplicate_term = get_the_terms( $duplicate_user, 'author' );
		$linked_account = get_post_meta( $primary_user, 'cap-linked_account', true );

		if ( $primary_term && $duplicate_term ) {
			$query = new \WP_Query( array(
				'posts_per_page' => - 1,
				'post_type'      => array( 'post', 'hm-slideshow', 'hm-webcast', 'hm-product-review', 'hn-group-test' ),
				'post_status'    => 'any',
				'orderby'        => 'ID',
				'tax_query'      => array(
					array(
						'taxonomy' => 'author',
						'field'    => 'slug',
						'terms'    => $duplicate_term[0]->slug,
					),
				),
			) );

			$primary_name   = get_the_title( $primary_user );
			$duplicate_name = get_the_title( $duplicate_user );
			WP_CLI::log( "$query->found_posts post were merged from $duplicate_name-$duplicate_user $primary_name-$primary_user - linked account: $linked" );

			if ( $query->have_posts() ) {

				while ( $query->have_posts() ) {
					$query->the_post();
					wp_set_post_terms( get_the_ID(), $primary_term[0]->slug, 'author' );
					if ( $linked && $linked_account ) {
						$user = get_user_by( 'slug', $linked_account );
						if ( $user ) {
							$arg = array(
								'ID'          => get_the_ID(),
								'post_author' => $user->ID,
							);
							wp_update_post( $arg );
						}
					}
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * Loop over the duplicate authors and consolidate them
	 */
	public function clean_up_duplicates() {

		foreach ( $this->list as $ids ) {
			if ( count( $ids ) > 1 ) {
				$real_user          = $this->maybe_get_linked_account( $ids );
				$previous_id        = false;
				$has_post_thumbnail = false;
				$previous_count     = 0;

				foreach ( $ids as $id ) {
					$term = get_the_terms( $id, 'author' );

					//if the user has no post and does not have a connected accounts lest delete them
					if ( $term[0]->count === 0 && $real_user !== $id ) {
						$previous_count     = 0;
						$has_post_thumbnail = get_post_thumbnail_id( $id );
						wp_trash_post( $id );
					} elseif ( $real_user && $real_user !== $id ) {
						//We are going to reassign any guest authors to the guest author that is actually
						//linked to an account
						$updated = $this->reassign_posts( $real_user, $id, true );

						if ( empty( $has_post_thumbnail ) ) {
							$has_post_thumbnail = get_post_thumbnail_id( $id );
						}

						if ( $updated ) {
							if ( ! empty( $has_post_thumbnail ) ) {
								set_post_thumbnail( $real_user, $has_post_thumbnail );
							}
							wp_trash_post( $id );
						}
					} elseif ( $previous_id && $real_user !== $id ) {
						//if the previous duplicate user has more post than the current duplicate user
						//merge the duplicate into the previous else reverse the merge
						if ( $previous_count > $term[0]->count ) {
							$updated = $this->reassign_posts( $previous_id, $id );
							if ( empty( $has_post_thumbnail ) ) {
								$has_post_thumbnail = get_post_thumbnail_id( $id );
							}

							if ( $updated ) {
								if ( ! empty( $has_post_thumbnail ) ) {
									set_post_thumbnail( $previous_id, $has_post_thumbnail );
								}
								wp_trash_post( $id );
							}
						} else {
							$updated = $this->reassign_posts( $id, $previous_id );
							if ( empty( $has_post_thumbnail ) ) {
								$has_post_thumbnail = get_post_thumbnail_id( $previous_id );
							}

							if ( $updated ) {
								if ( ! empty( $has_post_thumbnail ) ) {
									set_post_thumbnail( $id, $has_post_thumbnail );
								}
								wp_trash_post( $previous_id );
							}
						}
					} else {
						$previous_id    = $id;
						$previous_count = $term[0]->count;

						if ( empty( $has_post_thumbnail ) ) {
							$has_post_thumbnail = get_post_thumbnail_id( $id );
						} else {
							set_post_thumbnail( $id, $has_post_thumbnail );
						}
					}

				}
			}
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'guest-author',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'post_title',
				'order'          => 'ASC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Getting ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();

		$this->clean_up_duplicates();

		WP_CLI::success( 'Done cleaning up' );
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate author-cleanup', 'AuthorCleanup' );
