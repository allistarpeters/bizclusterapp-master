<?php

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Images as Images;

class HealthdayCleanup extends WP_CLI_Command {

	/**
	 * Get Healthday articles in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		if ( ! empty( $assoc_args['start'] ) && empty( strtotime( $assoc_args['start'] ) ) ) {
			\WP_CLI::error( 'Invalid start date' );
		}

		$this->paginated_query(
			[ $this, 'import_images' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Import images for a healthday article
	 *
	 * @param $query
	 */
	public function import_images( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );

		if ( empty( $legacy_data ) ) {
			return;
		}

		/*
		//Don't update post content so we don't have to run fix internal links
		$post_content = $legacy_data->Body ? Helpers\prepare_post_body( $post_id, $legacy_data->Body ) : '';

		$post_args    = [
			'ID'           => $post_id,
			'post_content' => $post_content
		];

		$post_id = wp_update_post( $post_args );*/

		// upload and attach images.
		if ( ! empty( $legacy_data->Image ) && ! empty( $legacy_data->Image->DynamicSource ) ) {
			$attachment_id = Images\upload_remote_image(
				$legacy_data->Image->DynamicSource,
				$post_id,
				[
					'post_date'    => date( 'Y-m-d H:i:s', $legacy_data->Date ),
					'post_title'   => $legacy_data->Image->Name,
					'post_excerpt' => $legacy_data->Image->AltText,
				]
			);
			set_post_thumbnail( $post_id, $attachment_id );

			$altText = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );

			// Fallback image caption for pre migrated images
			if ( $altText !== $legacy_data->Image->AltText && empty( $legacy_data->ImageCaptionOverride ) && ! empty( $legacy_data->Image->AltText ) ) {
				update_post_meta( $post_id, 'hm-image-caption-override', sanitize_text_field( $legacy_data->Image->AltText ) );
			}
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'post',
				'post_status'    => 'any',
				'offset'         => $offset,
				'tax_query'      => array(
					array(
						'taxonomy' => 'hm-article-type',
						'field'    => 'slug',
						'terms'    => 'healthday',
					),
				),
				'date_query' => array(
					array(
						'after'     => empty( $assoc_args['start'] ) ? '2018-01-01' : $assoc_args['start'],
						'inclusive' => true,
					),
				),
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Getting ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();

		WP_CLI::success( 'Done cleaning up' );
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate healthday-cleanup', 'HealthdayCleanup' );
