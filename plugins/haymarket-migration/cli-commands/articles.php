<?php
/**
 * Migrates Article Posts
 *
 */

use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Users as Users;
use Haymarket\Migration\Shortcodes as Shortcodes;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Helpers as Helpers;

class Articles extends WP_CLI {


	/**
	 * Migrates Articles
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 * <start_date>
	 * : 2004-01-01 format
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate articles 6 2018-01-01
	 *
	 * @param array $args Positional arguments.
	 *
	 * @throws Exception
	 */
	public function __invoke( $args ) {
		\Haymarket\Base\paginated_query(
			'article',
			[
				'publicationid' => $args[0],
				'IncludeBody'   => true,
			],
			[ $this, 'create_posts' ],
			$args[1]
		);
	}

	/**
	 * Insert Posts
	 *
	 * @param array $results Items from API call.
	 */
	public function create_posts( $results ) {
		// Will return array with status message regardless, so we need to check both results and articles
		if ( empty( $results ) || empty( $results->Articles ) ) {
			return;
		}

		$count  = is_array( $results->Articles ) || $results->Articles instanceof Countable ? count( $results->Articles ) : 0;
		$report = [];

		foreach ( $results->Articles as $result ) {

			$existing_id = Helpers\get_post_by_legacy_id( (int) $result->Id );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			//Skip Two Minute Medicine articles
			if ( 'TwoMinuteMedicine' === $result->Type ) {
				continue;
			}

			$post_args = [
				'post_author'   => Users\get_default_user_id( $result->Authors ?? array() ),
				'post_title'    => $result->Title ?? '',
				'post_status'   => 'publish',
				'post_date'     => date( 'Y-m-d H:i:s', $result->ArticlePublishDate ),
				'post_modified' => date( 'Y-m-d H:i:s', $result->ModifiedDate ),
				'post_excerpt'  => $result->Deck ?? '',
				'post_type'     => 'post',
			];

			$post_id = wp_insert_post( $post_args );

			if ( empty( $post_id ) ) {
				continue;
			}

			//Insert post content after WordPress post is created so that we can upload inline images
			$post_content = $result->Body;

			//If article is Healthday remove inline images so they don't get uploaded to WordPress
			if ( 'Healthday' === $result->Type ) {
				$post_content = \Haymarket\Post_Meta\Article_Settings\remove_inline_images( $post_content, true );
			}

			$post_content = $result->Body ? Helpers\prepare_post_body( $post_id, $post_content ) : '';

			$post_args    = [
				'ID'           => $post_id,
				'post_content' => $post_content
			];

			$post_id = wp_update_post( $post_args );

			if ( empty( $post_id ) ) {
				continue;
			}

			$media_prepend = false;

			if ( ! empty( $result->Media ) ) {
				$media_prepend = Helpers\get_attachment_from_media_object( $result->Media, $post_id );
			}

			if ( $media_prepend ) {
				$link = get_permalink( $post_id );
				// if we run migration in --debug mode, this will spit out examples for us to QA.
				wp_cli::debug( "Prepended media to $link" );
			}

			$report['count'][] = $post_id;

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {

				if ( ! isset( $result->{$api_key} ) ) {
					continue;
				}
				$api_value                = $result->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			//Hide featured image if image is included inline
			if ( Helpers\is_featured_image_inline( $post_content ) ) {
				$meta_values['hm-hide-featured-image'] = 'on';
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $meta_added;
				}
			}

			// Coauthors
			$author_data = [];
			if ( isset( $result->Authors ) && ! empty( $result->Authors ) ) {
				$author_data = array_map( function ( $author ) {
					//Attempt to get author by legacy author id
					if ( ! empty( $author->AuthorId ) && $author->AuthorId > 0 ) {
						$user = Helpers\get_user_by_legacy_id( $author->AuthorId );
						if ( $user ) {
							$user = get_user_by( 'id', $user );
							if ( $user ) {

							}
							$author_login = $user->user_login;
						}
					}

					if ( empty( $author_login ) ) {
						//attempt to find author by full name search
						global $coauthors_plus;
						$user = $coauthors_plus->search_authors( $author->Name );
						if ( ! empty( $user ) && is_array( $user ) ) {
							$user = reset( $user );
							//grab first user found
							$author_login = $user->user_login;
						} else {
							$author_login = sanitize_title( "{$author->Name}-{$author->AuthorId} " );
						}
					}

					$coauthor = Users\get_coauthor_by_display_name( $author_login );
					if ( is_object( $coauthor ) ) {
						return $coauthor->user_nicename; // format CAP add-to-post function expects
					} else { // article is attributed to author not existent in User API endpoint. create a co-author on the spot.
						return Users\create_coauthor( $author->Name, $author->AuthorId );
					}
				}, $result->Authors );
			} else {
				//hide author byline
				$meta_added = add_post_meta( $post_id, 'hm-hide-author', 'on' );
				if ( $meta_added ) {
					$report['hm-hide-author'][] = $meta_added;
				}
			}

			// set coauthors terms.
			wp_set_object_terms(
				$post_id,
				$author_data,
				'author'
			);

			// upload and attach images.
			if ( ! empty( $result->Image->DynamicSource ) && 'Healthday' !== $result->Type ) {
				$attachment_id = Images\upload_remote_image(
					$result->Image->DynamicSource,
					$post_id,
					[
						'post_date'    => date( 'Y-m-d H:i:s', $result->Date ),
						'post_title'   => $result->Image->Name,
						'post_excerpt' => $result->Image->AltText,
					]
				);
				set_post_thumbnail( $post_id, $attachment_id );

				$altText = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );

				//Fallback image caption for pre migrated images
				if ( $altText !== $result->Image->AltText && empty( $result->ImageCaptionOverride ) && ! empty( $result->Image->AltText ) ) {
					update_post_meta( $post_id, 'hm-image-caption-override', sanitize_text_field( $result->Image->AltText ) );
				}
			}

			// set tags to Topics.
			wp_set_object_terms(
				$post_id,
				$result->Topics,
				'post_tag'
			);
			// set category to Section.
			wp_set_object_terms(
				$post_id,
				$result->Section->Name,
				'category'
			);
			// set article type to Type.
			wp_set_object_terms(
				$post_id,
				$result->Type,
				'hm-article-type'
			);
			// set Print Section type to PrintSection.
			if ( ! empty( $result->PrintSection ) ) {
				wp_set_object_terms(
					$post_id,
					$result->PrintSection->Name,
					'hm-print-section'
				);
			}
			// set Print Issue type to PrintIssue.
			if ( ! empty( $result->PrintIssue ) ) {
				wp_set_object_terms(
					$post_id,
					$result->PrintIssue->Name,
					'hm-print-issue'
				);
			}

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $result );

			$report['Articles Imported'][] = $post_id;

		} // End foreach().

		// paginated query will handle compiling reports.
		return $report;
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Id'                      => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'URL'                     => [
				'name' => HMM_PREFIX . 'url',
				'type' => 'string',
			],
			'Date'                    => [
				'name' => 'hm-article-date',
				'type' => 'date-linux',
			],
			'Annotation'              => [
				'name' => 'hm-annotation',
				'type' => 'post_content',
			],
			'CuratedPublisher'        => [
				'name' => 'hm-curated-publisher',
				'type' => 'string',
			],
			'CuratedURL'              => [
				'name' => 'hm-curated-url',
				'type' => 'string',
			],
			'ContentIsVisibleOn'      => [
				'name' => 'hm-visible-on',
				'type' => 'content-visible',
			],
			'CompanyId'               => [
				'name' => 'hm-company',
				'type' => 'string',
			],
			'DisplayArticleUpdate'    => [
				'name' => 'hm-display-update-time',
				'type' => 'bool',
			],
			'DisplayArticleTime'      => [
				'name' => 'hm-display-time',
				'type' => 'bool',
			],
			'DisableComments'         => [
				'name' => 'hm-disable-comments',
				'type' => 'bool',
			],
			'DisableContinuousScroll' => [
				'name' => 'hm-disable-continuous-scroll',
				'type' => 'bool',
			],
			'ExpirationDate'          => [
				'name' => 'hm-expiration-date',
				'type' => 'date-timestamp',
			],
			'HideAdColumn'            => [
				'name' => 'hm-hide-sidebar',
				'type' => 'bool',
			],
			'IsNativeAd'              => [
				'name' => 'hm-native-ad',
				'type' => 'bool',
			],
			'ImageCaptionOverride'    => [
				'name' => 'hm-image-caption-override',
				'type' => 'string',
			],
			'Media'                   => [
				'name' => 'hm-media',
				'type' => 'none',
			],
			'MetaDescription'         => [
				'name' => '_yoast_wpseo_metadesc',
				'type' => 'string',
			],
			'MetaKeywords'            => [
				'name' => '_yoast_wpseo_focuskw',
				'type' => 'string',
			],
			'MinimumAccessLevel'      => [
				'name' => 'hm-minimum-access-level',
				'type' => 'int',
			],
			'RelatedArticles'         => [
				'name' => 'hm-related-articles',
				'type' => 'json',
			],
			'RelatedSlideShows'       => [
				'name' => 'hm-related-slideshows',
				'type' => 'json',
			],
			'RelatedLinks'            => [
				'name' => 'hm-related-links',
				'type' => 'json',
			],
			'ShortTitle'              => [
				'name' => 'hm-short-title',
				'type' => 'string',
			],
			'SlideShow'               => [
				'name' => 'hm-slideshow',
				'type' => 'none',
			],
			'SyndicatedSpecialties'   => [
				'name' => 'hm-syndicated-specialties',
				'type' => 'none',
			],
			'SyndicatedTopics'        => [
				'name' => 'hm-syndicated-topics',
				'type' => 'none',
			],
			'ClonedArticleId'         => [
				'name' => '_hm-cloned-article-id',
				'type' => 'int',
			],
			'OriginalPublicationName' => [
				'name' => '_hm-cloned-publication-name',
				'type' => 'string',
			],
			'OriginalUrl'             => [
				'name' => '_yoast_wpseo_canonical',
				'type' => 'url',
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate articles', 'Articles' );