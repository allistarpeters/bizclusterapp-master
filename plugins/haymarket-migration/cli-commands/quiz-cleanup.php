<?php

use Haymarket\Migration\Helpers as Helpers;

class FixQuizzes extends WP_CLI_Command {

	/**
	 * Set un published post to draft
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @param \WP_Query $query WordPress query object.
	 *
	 * @return void
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );

		//Set post to draft when IsPublished = false
		if ( isset( $legacy_data->isPublished ) && false === $legacy_data->isPublished && 'publish' === get_post_status( $post_id ) ) {
			wp_update_post( array(
				'ID'          => $post_id,
				'post_status' => 'draft'
			) );
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'hm-quiz',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-quizzes', 'FixQuizzes' );
