<?php

class Migrate_Categories extends WP_CLI_Command {

	public $post_updated = [];

	/**
	 * Set new primary category for all post with a specific existing category
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_post_category' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Update inline links and ids in related meta data
	 *
	 * @param $post
	 */
	public function update_post_category( $post, $old_category, $category ) {
		$terms = wp_get_object_terms( $post, 'category', array( 'fields' => 'slugs' ) );
		if ( empty( $terms ) || is_wp_error( $terms ) ) {
			wp_set_object_terms( $post, $category, 'category' );
			$this->post_updated[] = $post;
		} else {
			$terms = array_flip( $terms );
			if ( array_key_exists( $old_category, $terms ) ) {
				wp_set_object_terms( $post, $category, 'category' );
				$this->post_updated[] = $post;
			}
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return voidcç
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		$post_type = 'any';

		if ( ! empty( $assoc_args['post-type'] ) ) {
			$post_type = $assoc_args['post-type'];
		}

		$category     = 'uncategorized';
		$new_category = '';

		if ( ! empty( $assoc_args['cat'] ) ) {
			$cat = get_category_by_slug( $assoc_args['cat'] );
			if ( $cat ) {
				$category = $assoc_args['cat'];
			}
		}

		if ( ! empty( $assoc_args['new-cat'] ) ) {
			$cat = get_category_by_slug( $assoc_args['new-cat'] );
			if ( $cat ) {
				$new_category = $assoc_args['new-cat'];
			} else {
				wp_cli::error( 'A existing new category has to be set' );
			}
		} else {
			wp_cli::error( 'A new category has to be set' );
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => $post_type,
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
				'fields'         => 'ids',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				foreach ( $query->posts as $post ) {
					call_user_func( $callback, $post, $category, $new_category );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );
		}
		wp_cli::success( count( $this->post_updated ) . ' post were updated' );

		wp_reset_postdata();

	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate migrate-categories', 'Migrate_Categories' );
