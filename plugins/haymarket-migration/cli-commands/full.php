<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

 namespace Haymarket\Migration;

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Images as Images;

class Full extends \WP_CLI {
	/**
	 * Run Full Migration Sequence
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * [--empty]
	 * : Whether to empty the site before importing
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate full 6
	 *
	 * @when after_wp_load
	 */
	public function __invoke( $args, $assoc_args ) {
		$url = get_site_url();

		// Make sure we're providing a url so we don't overwrite main site if we forget to supply one.
		if ( network_site_url() === trailingslashit( $url ) ) {
			\wp_cli::error( __( 'Please provide a site URL using the --url parameter.', 'haymarket' ) );
		}

		$commands = [
			// Users.
			'authors',

			// Taxonomy Terms.
			'sections',
			'sponsors',
			'newsletters',

			// Post Types.
			'webcasts',
			'product-reviews',
			'group-tests',
			'quizzes',
			'playlists',
			'slideshows',
			'articles',
			//'companies', -- This will be brought over by the drug database for medical sites
		];

		if ( isset( $assoc_args['empty'] ) ) {
			// todo   Use passed-in --url argument.
			\wp_cli::runcommand( 'site empty --yes --uploads' );
		}

		array_map( function( $command ) use ( $args ) {
			 \wp_cli::runcommand( "haymarket-migrate $command {$args[0]}" );
		}, $commands );

	}
}


\WP_CLI::add_command( 'haymarket-migrate full', __NAMESPACE__ . '\\Full' );
