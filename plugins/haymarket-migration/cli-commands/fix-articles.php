<?php

use Haymarket\Migration\Helpers as Helpers;

class FIXArticles extends WP_CLI {

	/**
	 * Clean up Articles
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 * <start_date>
	 * : 2004-01-01 format
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate fix-articles 6 2018-01-01
	 *
	 * @param array $args Positional arguments.
	 *
	 * @throws Exception
	 */
	public function __invoke( $args ) {
		\Haymarket\Base\paginated_query(
			'article',
			[
				'publicationid' => $args[0],
				'IncludeBody'   => true,
			],
			[ $this, 'update_posts' ],
			$args[1]
		);
	}

	/**
	 * Update Posts
	 *
	 * @param array $results Items from API call.
	 */
	public function update_posts( $results ) {
		// Will return array with status message regardless, so we need to check both results and articles
		if ( empty( $results ) || empty( $results->Articles ) ) {
			return;
		}

		$report = [];

		foreach ( $results->Articles as $result ) {

			$post_id = Helpers\get_post_by_legacy_id( (int) $result->Id );

			if ( empty( $post_id ) ) {
				continue;
			}

			$report['count'][] = $post_id;

			//fix iframes
			/*if ( false !== strpos( $result->Body, 'iframe' ) ) {
				$post_content = $result->Body ? Helpers\prepare_post_body( $post_id, $result->Body ) : '';
				$post_args    = [
					'ID'           => $post_id,
					'post_content' => $post_content
				];

				$post_id = wp_update_post( $post_args );
			}*/

			// update featured image caption/alt text
			if ( ! empty( $result->Image->DynamicSource ) ) {
				$thumbnail_id = get_post_thumbnail_id( $post_id );
				if ( $thumbnail_id ) {
					$altText        = $result->Image->AltText;
					$thumbnail_args = [
						'ID'           => intval( $thumbnail_id ),
						'post_excerpt' => $altText
					];

					$thumbnail_id = wp_update_post( $thumbnail_args );

					update_post_meta( $thumbnail_id, '_wp_attachment_image_alt', sanitize_text_field( $altText ) );
				}
			}

			update_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $result );

			$report['Articles Imported'][] = $post_id;

		} // End foreach().

		// paginated query will handle compiling reports.
		return $report;
	}
}

WP_CLI::add_command( 'haymarket-migrate fix-articles', 'FIXArticles' );