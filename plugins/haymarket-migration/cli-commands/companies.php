<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users;

class Companies extends WP_CLI {
	/**
	 * Migrates Companies
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration companies 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$pub_id     = $args[0];
		$companies  = $this->get_companies( $pub_id );
		$count      = count( $companies );
		$report     = [];
		$progress   = \WP_CLI\Utils\make_progress_bar( __( 'Importing Companies', 'haymarket' ), $count );

		foreach ( $companies as $company ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $company->{'Company ID'}, 'hm-company' );
			if ( ! empty( $existing_id ) ) {
				$report['Companies Skipped'][] = $existing_id;
				$progress->tick();
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $company->{$api_key} ) ) {
					continue;
				}
				$api_value                = $company->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_title'   => $company->{'Company Name'} ? $company->{'Company Name'} : '00 noname',
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $company->{'CreateDate'} ) ),
					'post_type'    => 'hm-company',
					'post_status'  => 'publish',
					'post_author'  => Users\get_default_user_id(),
					'post_content' => $company->{'Description'} ? Helpers\prepare_post_body( $post_id, $company->{'Description'} ) : '',
				]
			);

			if ( empty( $post_id ) ) {
				$report['Creation Failed'][] = $company->{'Company ID'};
				$progress->tick();
				continue;
			}

			// TODO: Handle featured image. Check 'Image Group' key.

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			$report['Companies Imported'][] = $post_id;
			$progress->tick();

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $company );
		} // End foreach().
		$progress->finish();

		Reporting\report( $report, $count, 'Companies' );
	}

	/**
	 * Query API for companies
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_companies( $pub_id ) {

		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/companies", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Company ID'                 => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'FirstName'    => [
				'name' => 'hm-first-name',
				'type' => 'string',
			],
			'LastName'    => [
				'name' => 'hm-last-name',
				'type' => 'string',
			],
			'Title'    => [
				'name' => 'hm-title',
				'type' => 'string',
			],
			'Address1'  => [
				'name' => 'hm-address-1',
				'type' => 'string',
			],
			'Address2'        => [
				'name' => 'hm-address-2',
				'type' => 'string',
			],
			'Web Address'    => [
				'name' => 'hm-url',
				'type' => 'string',
			],
			'City'    => [
				'name' => 'hm-city',
				'type' => 'string',
			],
			'State'  => [
				'name' => 'hm-state',
				'type' => 'string',
			],
			'Zip'        => [
				'name' => 'hm-zip',
				'type' => 'string',
			],
			'Country'    => [
				'name' => 'hm-country',
				'type' => 'string',
			],
			'Telephone'    => [
				'name' => 'hm-telephone',
				'type' => 'string',
			],
			'Fax'  => [
				'name' => 'hm-fax',
				'type' => 'string',
			],
			'Email'        => [
				'name' => 'hm-email',
				'type' => 'string',
			],
			'MinimumAccessLevel'    => [
				'name' => 'hm-minimum-access-level',
				'type' => 'string',
			],
			'Available as Article Metadata'    => [
				'name' => 'hm-available-as-article-metadata',
				'type' => 'bool',
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate companies', 'Companies' );
