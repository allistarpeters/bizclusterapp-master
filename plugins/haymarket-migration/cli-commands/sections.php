<?php
/**
 * Migrates Sections
 */

use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;

class Sections extends WP_CLI {

	/**
	 * Holds report information
	 *
	 * @var array $report
	 */
	public $report = [];

	/**
	 * Migrates Sections
	 *
	 * ## OPTIONS
	 *
	 * <site>
	 * : Publication ID of the site from which to grab sections
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migrate sections 6
	 *
	 * @when after_wp_load
	 */
	public function __invoke( $args, $assoc_args ) {
		$pub_id   = $args[0];
		$sections = $this->get_sections( $pub_id );
		$this->create_sections( $sections );
		$this->set_section_parents();
		Reporting\report( $this->report, 0, 'Sections' );
	}

	/**
	 * Create sections from API data
	 *
	 * @param array $sections
	 */
	function create_sections( array $sections = [] ) {
		if ( empty( $sections ) ) {
			wp_cli::warn( 'No sections passed' );
		}

		$progress = \WP_CLI\Utils\make_progress_bar( 'Importing sections', count( $sections ) );

		foreach ( $sections as $section ) {
			$existing_id = Helpers\get_term_by_legacy_id( (int) $section->Section->Id, 'category' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			if ( empty( $section->Section->Name ) ) {
				wp_cli::debug( 'Not a section!' );
				continue;
			}
			$term = wp_insert_term(
				$section->Section->Name,
				'category',
				[
					'description' => $section->Section->Description ?? '',
				]
			);

			$progress->tick();

			if ( is_wp_error( $term ) ) {
				continue;
			};

			$term_id = $term['term_id'];

			$this->report['Sections Created'][] = $term_id;

			$int_fields = [
				HMM_PREFIX . 'id'        => $section->Section->Id ?? null,
				HMM_PREFIX . 'parent-id' => $section->Section->ParentSectionId ?? null,
				'image'                  => $section->Section->ImageGroup ? $this->get_section_image( $section->Section->ImageGroup ) : null,
				'minimum-access-level'   => $section->Section->MinimumAccessLevel ?? null,
				'visible-on'             => $section->Section->DefaultVisiblePlatform ?? null,
			];

			foreach ( $int_fields as $key => $value ) {
				$data = filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
				if ( ! empty( $data ) ) {
					$meta_created = update_term_meta( $term_id, $key, $data );
					if ( $meta_created ) {
						$this->report[ $key ][] = $term_id;
					}
				} else {
					delete_term_meta( $term_id, $key );
				}
			}

			$string_fields = [
				'hide-sidebar'       => $section->Section->HideAdColumn ? 'on' : null,
				'hide-header-footer' => $section->Section->HideHeader ? 'on' : null,
				'gated-section'      => $section->Section->IsGatedByPrimarySection ? 'on' : null,
			];

			foreach ( $string_fields as $key => $value ) {
				$data = filter_var( $value, FILTER_SANITIZE_STRING );
				if ( ! empty( $data ) ) {
					$meta_created = update_term_meta( $term_id, $key, $data );
					if ( $meta_created ) {
						$this->report[ $key ][] = $term_id;
					}
				} else {
					delete_term_meta( $term_id, $key );
				}
			}

			$seo_metas = [
				'wpseo_title'   => $section->Section->MetaTitle,
				'wpseo_desc'    => $section->Section->MetaDescription,
				'wpseo_focuskw' => $section->Section->MetaKeywords,
			];

			WPSEO_Taxonomy_Meta::set_values( $term_id, 'category', $seo_metas );

			update_term_meta( $term_id, HMM_PREFIX . 'legeacy-data', $section );

			$added[]     = $term_id;
			$added_count = count( $added );
		}

		$progress->finish();
	}

	/**
	 * Request sections from API
	 *
	 * @param $pub_id
	 *
	 * @todo this is duplicate code from base.php with minor alterations. Refactor base to be more reuseable.
	 * @return object|WP_Error
	 */
	private function get_sections( int $pub_id ) {
		$request = wp_remote_get( API_V3_URL, [
			'body'    => [
				'methodid'      => 2,
				'publicationid' => $pub_id,
				'countrycode'   => 'us'
			],
			'timeout' => 30,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::warning( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Get section image from API data
	 *
	 * @param $image_group
	 *
	 * @return int
	 */
	public function get_section_image( $image_group ) {
		if ( empty( $image_group ) ) {
			return;
		}
		$url = $image_group->Images[0]->URL;

		return Images\upload_remote_image( $url );
	}

	/**
	 * Assign parent term
	 *
	 * @return string
	 */
	public function set_section_parents() {
		$term_ids = get_terms( [
			'taxonomy'   => 'category',
			'hide_empty' => false,
			'fields'     => 'ids'
		] );

		if ( is_wp_error( $term_ids ) ) {
			return $term_ids->get_error_message();
		}

		$progress = \WP_CLI\Utils\make_progress_bar( 'Designating Term Parents', count( $term_ids ) );

		foreach ( $term_ids as $term_id ) {
			$legacy_parent_id = get_term_meta( $term_id, HMM_PREFIX . 'parent-id', true );
			if ( ! $legacy_parent_id ) {
				$progress->tick();
				continue;
			}
			$parent_id = Helpers\get_term_by_legacy_id( $legacy_parent_id, 'category' );

			if ( $legacy_parent_id ) {
				$term_updated = wp_update_term( $term_id, 'category', [
					'parent' => $parent_id,
				] );

				if ( $term_updated ) {
					$this->report[ 'Parents Assigned' ][] = $term_id;
				}
			}

			$progress->tick();
		}

		$progress->finish();
	}
}

WP_CLI::add_command( 'haymarket-migrate sections', 'Sections' );
