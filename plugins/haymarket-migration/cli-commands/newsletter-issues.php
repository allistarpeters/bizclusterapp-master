<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users as Users;

class NewsletterIssues extends WP_CLI {
	/**
	 * Migrates Newsletters
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration newsletters 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$pub_id   = $args[0];
		$issues   = $this->get_newsletters( $pub_id );
		$count    = count( $issues );
		$report   = [];
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Newsletter Issues', 'haymarket' ), $count );

		foreach ( $issues as $issue ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $issue->Id, 'hm-newsletter-issue' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $issue->{$api_key} ) ) {
					continue;
				}
				$api_value                = $issue->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_title'   => $issue->Name ?? '',
					'post_content' => $issue->HtmlAssets[0]->Body ?? '',
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $issue->PublishDate ) ),
					'post_type'    => 'hm-newsletter-issue',
					'post_status'  => 'publish',
					'post_author'  => Users\get_default_user_id(),
				]
			);

			if ( empty( $post_id ) ) {
				continue;
			}

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			$legacy_newsletter_id = $issue->NewsletterId;
			$newsletter_id = Helpers\get_term_by_legacy_id( $legacy_newsletter_id, 'hm-newsletter' );
			if ( $newsletter_id ) {
				wp_set_object_terms(
					$post_id,
					$newsletter_id,
					'hm-newsletter'
				);
			}

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $issue );

			$report['Newsletters Imported'][] = $post_id;
			$progress->tick();
		} // End foreach().
		$progress->finish();

		Reporting\report( $report, $count, 'Newsletter Issues' );
	}

	/**
	 * Query API for newsletters
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_newsletters( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/newsletteritems", [ // @codingStandardsIgnoreLine.
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::warning( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Id'        => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'HideBannerAd'        => [
				'name' => 'hm-hide-banner-ad',
				'type' => 'bool',
			],
			'HideOtherAds'        => [
				'name' => 'hm-hide-other-ads',
				'type' => 'bool',
			],
			'HeaderText'          => [
				'name' => 'hm-header-text',
				'type' => 'string',
			],
			'TextVersion'         => [
				'name' => 'hm-text-version',
				'type' => 'string',
			],
			'PreHeaderText'       => [
				'name' => 'hm-pre-header-text',
				'type' => 'string',
			],
			// currently not exposed in the API.
//			'Subject'            => [
//				'name' => 'hm-subject',
//				'type' => 'string',
//			],
			'CustomUrlParameters' => [
				'name' => 'hm-url-params',
				'type' => 'string',
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate newsletter-issues', 'NewsletterIssues' );

