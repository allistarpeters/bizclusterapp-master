<?php

use Haymarket\Migration\Helpers as Helpers;

class FixProducts extends WP_CLI_Command {

	/**
	 * Clean up products in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Update product ratings
	 *
	 * @param $query
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );
		$meta_values = array();

		foreach ( $this->get_meta_keys() as $api_key => $data ) {
			if ( ! isset( $legacy_data->{$api_key} ) ) {
				continue;
			}
			$api_value                = $legacy_data->{$api_key};
			$value                    = $data['type'];
			$meta_key                 = $data['name'];
			$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
		}

		$meta_input = array_filter( $meta_values, function ( $value ) {
			return ! empty( $value );
		} );

		foreach ( $meta_input as $key => $value ) {
			update_post_meta( $post_id, $key, $value );
		}
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'FeatureRating'       => [
				'name' => 'hm-rating-features',
				'type' => 'rating',
			],
			'PerformanceRating'   => [
				'name' => 'hm-rating-performance',
				'type' => 'rating',
			],
			'EaseOfUseRating'     => [
				'name' => 'hm-rating-easeofuse',
				'type' => 'rating',
			],
			'SupportRating'       => [
				'name' => 'hm-rating-support',
				'type' => 'rating',
			],
			'DocumentationRating' => [
				'name' => 'hm-rating-documentation',
				'type' => 'rating',
			],
			'ValueOfMoneyRating'  => [
				'name' => 'hm-rating-valueformoney',
				'type' => 'rating',
			],
			'OverallRating'       => [
				'name' => 'hm-rating-overall',
				'type' => 'rating',
			],
		];
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'hm-product-review',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();
		WP_CLI::success( 'Done cleaning up' );
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-products', 'FixProducts' );
