<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users;

class Slideshows extends WP_CLI {
	/**
	 * Migrates Slideshows
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 * <start_date>
	 * : The date to start fetching from in YYYY-MM-DD format
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration slideshows 6 2018-01-01
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$this->paginated_query(
			'slideshow',
			[
				'publicationid' => $args[0],
				'IncludeBody'   => true,
			],
			[ $this, 'create_slideshow' ],
			$args[1]
		);

	}

	public function paginated_query( $api = 'slideshow', array $args = [], $callback = '', $start_date ) {
		if ( empty( $callback ) ) {
			return;
		}

		$api_start_date  = API_START_DATE;
		if ( isset( $start_date ) ) {
			if ( date( 'Y-m-d', strtotime( $start_date ) ) !== $start_date ) {
				\wp_cli::error( esc_html_e( 'Please use Y-m-d date format.', 'haymarket' ) );
			} else {
				$api_start_date = $start_date;
			}
		}

		$per_page = 25;

		$period   = $this->get_days_since( $api_start_date );
		$count    = iterator_count( $period );
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Slideshows', 'haymarket' ), $count );

		$i = 0;
		$final_report = [];
		// for every day, query the api and provide data to callback.
		foreach ( $period as $key => $value ) {
			// get array of timestamps for range of day.
			$day_range = \Haymarket\Base\get_day_range( $value->format( 'Y-m-d' ) );

			// pass range to callback function.
			$results = $this->query_api( $day_range, $args );

			// provide returned data to callback.
			$report = call_user_func( $callback, $results ) ?? [];

			$final_report = Reporting\add_to_report( $final_report, $report );

			// placeholder for memory clear function.
			if ( 0 === $i % $per_page ) {
				\Haymarket\Base\stop_the_insanity();
			}
			$progress->tick();
			$i ++;
		}
		$progress->finish();

		$count = count( $final_report['count'] );
		Reporting\report( $final_report, 0, 'Slideshows' );
	}

	/**
	 * Get all the days since a given time
	 *
	 * @param string $since
	 *
	 * @return \DatePeriod|void
	 * @throws \Exception
	 */
	function get_days_since( $since = '' ) {
		if ( empty( $since ) ) {
			return;
		}

		$period = new \DatePeriod(
			new \DateTime( $since ),
			new \DateInterval( 'P1D' ),
			new \DateTime()
		);

		return $period;
	}

	/**
	 * Make remote request to API
	 *
	 * @param array $day_range
	 * @param array $args
	 *
	 * @return array|mixed|void
	 */
	public function query_api( array $day_range = [], array $args = [] ) {
		if ( empty( $day_range ) || empty( $args ) ) {
			return;
		}

		$args['StartDate'] = $day_range['start_date'];
		$args['EndDate']   = $day_range['end_date'];

		$get_url = API_V2_URL . "pub/{$args['publicationid']}/slideshows?from={$args['StartDate']}&to={$args['EndDate']}";

		$request = wp_remote_get( $get_url, [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	public function create_slideshow( $slideshows ) {
		foreach ( $slideshows as $slideshow ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $slideshow->Id, 'hm-slideshow' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $slideshow->{$api_key} ) ) {
					continue;
				}
				if ( 'Images' === $api_key ) {
					continue;
				}
				$api_value                = $slideshow->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$images_meta              = $this->prepare_images_metadata( $slideshow->Images ?? array() );
			$meta_values['hm-slides'] = $images_meta;

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_title'   => $slideshow->Title ?? '',
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $slideshow->PublishDate ) ),
					'post_type'    => 'hm-slideshow',
					'post_status'  => 'publish',
					'post_excerpt' => $slideshow->Deck ?? '',
					'post_author'  => Users\get_default_user_id(),
				]
			);

			if ( empty( $post_id ) ) {
				continue;
			}

			// upload and attach images.
			if ( ! empty( $slideshow->{'Cover Image Group'} ) ) {
				$attachment_id = \Haymarket\Migration\Images\upload_remote_image(
					$slideshow->{'Cover Image Group'}->DynamicSource,
					$post_id,
					[
						'post_title'   => $slideshow->{'Cover Image Group'}->Name,
						'post_excerpt' => $slideshow->{'Cover Image Group'}->Description,
					]
				);
				set_post_thumbnail( $post_id, $attachment_id );
			} elseif( !empty( $images_meta ) && is_array( $images_meta )) {
				//set featured image to the first image in the slideshow
				$attachment_id = $images_meta[0]['id'];
				set_post_thumbnail( $post_id, $attachment_id );
			}

			//Insert post content after WordPress post is created so that we can upload inline images
			$post_args = [
				'ID'     => $post_id,
				'post_content'  => $slideshow->Description ? Helpers\auto_link_urls( Helpers\prepare_post_body( $post_id, $slideshow->Description ) ) : '',
			];

			$post_id = wp_update_post( $post_args );

			if ( empty( $post_id ) ) {
				continue;
			}

			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					// $report[ $key ][] = $key;
				}
			}

			// Check for questions attached to slideshows and make them a quiz.
			// NOTE: $slideshow->HasExam was initially always false so relying on detection the actual questions here.
			if( ! empty( $slideshow->ExamQuestions ) ) {
				$quiz_id = $this->make_exam_into_quiz( $slideshow, $post_id );
				if ( ! empty( $quiz_id ) ) {
					add_post_meta( $post_id, 'hm-related-quiz', $quiz_id );
				}
			}

			// set tags to Topics
			foreach( $slideshow->Topics as $topic ) {
				wp_set_object_terms(
					$post_id,
					$topic,
					'post_tag',
					 true
				);
			}

			if( ! empty( $slideshow->PrimarySection ) ){
				// set category to Section
				wp_set_object_terms(
					$post_id,
					$slideshow->PrimarySection->Name,
					'category'
				);
			} else {
				if( ! empty( $slideshow->ExamQuestions ) ){
					// set category to Section
					wp_set_object_terms(
						$post_id,
						'Clinical Quiz',
						'category'
					);
				} else {
					// set category to Section
					wp_set_object_terms(
						$post_id,
						'Slides',
						'category'
					);
				}

			}

			// $report['Slideshows Imported'][] = $post_id;
			// $progress->tick();

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $slideshow );
		}

	}

	/**
	 * Prepare meta data for images object
	 *
	 * @param array $slide_data
	 *
	 * @return array
	 */
	public function prepare_images_metadata( array $slide_data ) {
		$slides = [];

		if( ! empty( $slide_data ) ){
			foreach ( $slide_data as $slide ) {
				$image_url = $slide->ImageGroupDo->Images[0]->Url;
				$image_id  = Haymarket\Migration\Images\upload_remote_image( $image_url, 0 ,
					[
						'post_title'   => $slide->ImageGroupDo->Images[0]->Name,
						'post_excerpt' => $slide->ImageGroupDo->Images[0]->Description,
					] );

				$slide_title = filter_var( $slide->ImageGroupDo->Name, FILTER_SANITIZE_STRING );

				if ( preg_match( '/\.(jpg|jpeg|png|gif)(?:[\?\#].*)?$/i', strtolower( $slide_title ), $matches ) ) {
					$slide_title = '';
				}

				$slides[]  = [
					'title'       => $slide_title,
					'description' => filter_var( $slide->ImageGroupDo->Description, FILTER_CALLBACK, array(
						'options' => 'wp_kses_post'
					) ),
					'image'       => filter_var( wp_get_attachment_image_src( $image_id, 'thumbnail' )[0], FILTER_SANITIZE_URL ),
					'id'          => (int) $image_id,
				];
			}
		}

		return $slides;
	}

	/**
	 * Takes exam questions from a slideshow and convert them to a quiz CPT.
	 *
	 * @method make_exam_into_quiz
	 * @param  [type] $slideshow [description]
	 * @return int|bool
	 */
	public function make_exam_into_quiz( $slideshow, $slideshow_id ) {

		$quiz_id = false;
		$questions = [];
		foreach ( $slideshow->ExamQuestions as $index => $question ) {
			$answers = [];
			foreach ( $question->Answers as $answer ) {
				$answers[] = [
					'text'    => $answer->Answer,
					'correct' => $answer->CorrectAnswer,
					'id'      => (int) $answer->Position,
				];
			}
			$questions[] = [
				'type'        => 'question',
				'question'    => $question->Question,
				'explanation' => '', // NOTE: There is no explanation attached to slideshows I checked.
				'answers'     => $answers,
				'id'          => (int) ( $index + 1 )
			];
		}
		if ( ! empty( $questions ) ) {
			$args = [
				'post_title'   => $slideshow->Title ? $slideshow->Title : esc_html__( 'Exam: ', 'haymarket' ) . $slideshow->Id,
				'post_date'    => date( 'Y-m-d H:i:s', strtotime( $slideshow->PublishDate ) ),
				'post_type'    => 'hm-quiz',
				'post_status'  => 'publish',
				'post_excerpt' => $slideshow->Deck ?? '',
				'post_author'  => Users\get_default_user_id(),
			];

			$quiz_id = wp_insert_post(
				$args
			);

			if( $quiz_id ){

				// set category to Section
				if( ! empty( $slideshow->PrimarySection ) ){
					wp_set_object_terms(
						$quiz_id,
						$slideshow->PrimarySection->Name,
						'category'
					);
				} else {
					wp_set_object_terms(
						$quiz_id,
						'Clinical Quiz',
						'category'
					);
				}

				// set tags to Topics
				foreach( $slideshow->Topics as $topic ) {
					wp_set_object_terms(
						$quiz_id,
						$topic,
						'post_tag',
						true
					);
				}

				if( ! empty( $slideshow->Explanation ) ){
					//Insert post content after WordPress post is created so that we can upload inline images
					$post_content = $slideshow->Explanation ? Helpers\prepare_post_body( $quiz_id, $slideshow->Explanation ) : '';
					$post_args    = [
						'ID'           => $quiz_id,
						'post_content' => $post_content
					];

					$quiz_id = wp_update_post( $post_args );
				}

				if( ! empty( $slideshow->QuizId ) ){
					add_post_meta( $quiz_id, '_hm_quiz_syndication_quiz_id', (int) $slideshow->QuizId );
				}
				add_post_meta( $quiz_id, 'hm-questions', $questions );
				add_post_meta( $quiz_id, 'hm-display-graph', 'on' );
				add_post_meta( $quiz_id, HMM_PREFIX . 'legacy-data', $slideshow );

				// Store origin
				add_post_meta( $quiz_id, '_associated-slideshow', $slideshow_id );
			}

		}

		return $quiz_id;
	}

	/**
	 * Query API for newsletters
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_slideshows( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/slideshows", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Id'                 => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'HasExam'            => [
				'name' => 'hm-has-exam',
				'type' => 'bool',
			],
			'RelatedArticles'    => [
				'name' => 'hm-related-articles',
				'type' => 'json',
			],
			'RelatedSlideshows'  => [
				'name' => 'hm-related-slideshows',
				'type' => 'json',
			],
			'AdFrequency'        => [
				'name' => 'hm-ad-frequency',
				'type' => 'int',
			],
			'MinimumAccessLevel' => [
				'name' => 'hm-minimum-acces-level',
				'type' => 'int',
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate slideshows', 'Slideshows' );
