<?php

use Haymarket\Migration\Helpers as Helpers;

class FixWebcasts extends WP_CLI_Command {

	/**
	 * Regenerates media attachments in a paginated manner to avoid memory issues.
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @param \WP_Query $query WordPress query object.
	 *
	 * @uses Update_Categories->set_primary_category()
	 *
	 * @return void
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );

		// set sponsors by legacy ID lookup.
		if ( ! empty( $legacy_data->WebCastIdSponsorId ) ) {
			$term_id = Haymarket\Migration\Helpers\get_term_by_legacy_id( $legacy_data->WebCastIdSponsorId[0]->SponsorID, 'hm-sponsor' );
			if ( ! empty( $term_id ) ) {
				$created = wp_set_object_terms(
					$post_id,
					$term_id,
					'hm-sponsor'
				);
			}
		}

		// Webcast Topics.
		if ( ! empty( $legacy_data->WebCastIdTopicId ) ) {
			// set category to Section.
			foreach ( $legacy_data->WebCastIdTopicId as $topic ) {
				$created = wp_set_object_terms(
					$post_id,
					$topic->Name,
					'hm-webcast-topic'
				);
			}
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query          = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page'         => $posts_per_page,
				'post_type'              => 'hm-webcast',
				'post_status'            => 'any',
				'offset'                 => $offset,
				'orderby'                => 'ID',
				'order'                  => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-webcasts', 'FixWebcasts' );
