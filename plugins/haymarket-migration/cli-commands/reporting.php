<?php
/**
 * Outputs Reporting
 *
 * @package Haymarket
 */

use Haymarket\Migration\Reporting as Reporting;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Migrates Articles
 *
 * ## OPTIONS
 *
 * <report_type>
 * : The type of report to produce
 *
 * [--legacy_url=<legacy_url>]
 * : URL used for legacy reference in spreadsheet
 *
 * ## EXAMPLES
 *
 *     wp haymarket-migration articles 6
 *
 * @param array $args Positional arguments.
 * @throws Exception
 */
class Report extends WP_CLI {

	/**
	 * Instance of PhpOffice\PhpSpreadsheet\Spreadsheet.
	 *
	 * @var object $spreadsheet \
	 */
	public $spreadsheet;

	public function __invoke( $args, $assoc_args ) {

		$this->spreadsheet = new Spreadsheet();

		$legacy_url = isset( $assoc_args['legacy_url'] ) ? $assoc_args['legacy_url'] : '';

		$sheets = [
			// Users.
			'user',

			// Taxonomy Terms.
			/*'category',
			'hm-sponsor',
			'hm-newsletter',
			'hm-therapeutic-category',
			'hm-generic',
			'hm-disease',*/

			// Post Types.
			'hm-quiz',
			'hm-slideshow',
			'hm-webcast',
			'hm-product-review',
			'hm-group-test',
			'hm-company',
			'hm-drug',
			'hm-playlist',
			'post',
		];
		$i = 0;

		foreach ( $sheets as $sheet ) {
			$report = Reporting\random_content_types( $sheet, $legacy_url );
			if ( empty( $report ) ) {
				\wp_cli::warning( "Report name $sheet did not produce results" );
				continue;
			}
			$this->add_sheet( $i, $report, $sheet );
			$i++;
		}

		$title = 'site-' . get_current_blog_id() . '-' . date( 'n-j-Y-g:i-A' );

		$writer = new Xlsx( $this->spreadsheet );
		$writer->save( HMM_PATH . "reports/$title.xlsx" );
	}

	/**
	 * Add A sheet to the excel document
	 *
	 * @param int   $index The index of the sheet to be added.
	 * @param array $data Asssociative array of data to add to sheet.
	 */
	public function add_sheet( int $index, $data, $name ) {
		// Create a new worksheet.
		$worksheet = new Worksheet( $this->spreadsheet, $name );
		$this->spreadsheet->addSheet( $worksheet, $index );

		// Set spreadsheet's active tab.
		$sheet = $this->spreadsheet->setActiveSheetIndex( $index );

		// Set headers based on array_keys.
		$sheet->fromArray( array_keys( $data[0] ), null, 'A1' );

		// Add a row per line of data.
		array_walk( $data, function( $row, $index ) use ( $sheet, $data ) {
			$this->add_row( $sheet, $data[ $index ] );
		} );
	}

	/**
	 * Add an associative array of data as a row.
	 * @param object $sheet Instance of phpspreadsheet.
	 * @param array  $array Associative array of data.
	 * @param string $col   Start column.
	 * @param int    $row   Start row.
	 */
	public function add_row( $sheet, $data, $col = 'A', $row = null ) {
		// if we're not specifying an index, find the last row and add 1 to determine index.
		if ( null === $row ) {
			$row = $sheet->getHighestRow() + 1;
		}
		$sheet->fromArray( array( $data ), null, "$col$row" );
		$row++;
	}


}

WP_CLI::add_command( 'haymarket-migrate report', 'Report' );