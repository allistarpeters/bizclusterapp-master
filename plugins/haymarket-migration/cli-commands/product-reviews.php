<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Users;

class ProductReviews extends WP_CLI {
	/**
	 * Migrates Product Reviews
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration product-reviews 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args, $assoc_args ) {
		$pub_id   = $args[0];
		$reviews  = $this->get_reviews( $pub_id );
		$products = $this->get_products( $pub_id );
		$count    = count( $reviews );
		$report   = [];
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Product Reviews', 'haymarket' ), $count );

		foreach ( $reviews as $review ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $review->{'Review ID'}, 'hm-product-review' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}
			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $review->{$api_key} ) ) {
					continue;
				}
				$api_value                = $review->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_title'   => $review->Title ?? null,
					'post_content' => Helpers\sanitize( $review->Body, 'post_content' ) ?? null,
					'post_excerpt' => $review->Deck ?? null,
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $review->PublishDate ) ) ?? null,
					'post_type'    => 'hm-product-review',
					'post_status'  => 'publish',
					'post_author'  => Users\get_default_user_id( ),
				]
			);

			// skip the rest if we didn't import a post.
			if ( empty( $post_id ) ) {
				continue;
			}

			//Insert post content after WordPress post is created so that we can upload inline images
			$post_args = [
				'ID'     => $post_id,
				'post_content'  => $review->Body ? Helpers\prepare_post_body( $post_id, $review->Body ) : '',
			];

			$post_id = wp_update_post( $post_args );

			if ( empty( $post_id ) ) {
				continue;
			}

			// Coauthors
			$author_data = [];
			if ( isset( $review->ReviewAuthors ) && ! empty( $review->ReviewAuthors ) ) {

				$author_data = array_map( function ( $author ) use ( $post_id ) {
					$author_id = 0; //Author id is not expose
					$author_login = sanitize_title( "{$author->Name}-{$author_id}" );
					$coauthor = Users\get_coauthor_by_display_name( $author_login );
					if ( is_object( $coauthor ) ) {
						return $coauthor->user_nicename; // format CAP add-to-post function expects
					} else { // article is attribute to author not existent in User API endpoint. create a co-author on the spot.
						return Users\create_coauthor( $author->Name, $author_id );
					}
				}, $review->ReviewAuthors );
			}

			// Product Info.
			$product_id = $review->ProductId;
			$product = array_filter( $products, function( $item ) use ( $product_id ) {
				return $item->Id ===  $product_id;
			} );

			if ( ! empty( $product ) ) {
				$key = array_keys( $product )[0];
				$product_info = sprintf(
					'Name: %1$s
Description: %2$s
Price: %3$s', // wonky formatting to avoid whitespace in textarea.
					filter_var( $product[ $key ]->Name ?? '', FILTER_SANITIZE_STRING ),
					filter_var( $product[ $key ]->Description ?? '', FILTER_SANITIZE_STRING ),
					filter_var( $product[ $key ]->Price ?? '', FILTER_SANITIZE_STRING )
				);
				$product_info = trim( $product_info );
				$meta_input['hm-product-info'] = $product_info;
			}

			// Meta fields.
			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			// Featured Image.
			if ( ! empty( $review->ImageGroupDo->Images ) ) {
				$attachment_id = Images\upload_remote_image( $review->ImageGroupDo->Images[0]->Url );
				if ( $attachment_id ) {
					set_post_thumbnail( $post_id, $attachment_id );
					$report['Featured Image'][] = $attachment_id;
				}
			}

			$print_sections = $review->PrintSection ?? [];
			if ( ! empty( $print_sections ) ) {
				// set Print Section type to PrintSection.
				wp_set_object_terms(
					$post_id,
					$print_sections,
					'hm-print-section'
				);
			}

			$print_issues = $review->PrintIssue ?? [];
			if ( ! empty( $print_issues ) ) {
				foreach ( $print_issues as $print_issue ) {
					// set Print Section type to PrintSection.
					wp_set_object_terms(
						$post_id,
						$print_issue->Name,
						'hm-print-issue'
					);
				}
			}

			$review_tags = $review->ReviewTags ?? [];
			if ( ! empty( $review_tags ) ) {
				foreach ( $review_tags as $review_tag ) {
					wp_set_object_terms(
						$post_id,
						$review_tag,
						'post_tag'
					);
				}
			}

			//set product categories as categories
			if ( ! empty( $product ) ) {
				$key = array_keys( $product )[0];
				$review_categories = $product[ $key ]->ProductCategories ?? [];
				if ( ! empty( $review_categories ) ) {
					foreach ( $review_categories as $review_category ) {
						wp_set_object_terms(
							$post_id,
							$review_category->Name,
							'category'
						);
					}
				}
			}

			$report['Product Reviews Imported'][] = $post_id;
			$progress->tick();
			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $review );
		} // End foreach().
		$progress->finish();


		Reporting\report( $report, $count, 'Product Reviews' );
	}

	/**
	 * Query API for product reviews
	 *
	 * @param int $pub_id Publication ID.
	 *
	 *
	 * @return array|mixed|object
	 */
	public function get_reviews( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/reviews", [
			'timeout' => 60,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Query the API to grab Product content type.
	 *
	 * @param int $pub_id Id of publication to retrieve products from.
	 *
	 * @return array
	 */
	function get_products( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/products", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Review ID'                  => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'ProductId' => [
				'name' => HMM_PREFIX . '-product-id',
				'type' => 'int',
			],
			'Strength'            => [
				'name' => 'hm-strength',
				'type' => 'string',
			],
			'Weaknesses'          => [
				'name' => 'hm-weakness',
				'type' => 'string',
			],
			'Verdict'             => [
				'name' => 'hm-verdict',
				'type' => 'string',
			],
			'FeatureRating'       => [
				'name' => 'hm-rating-features',
				'type' => 'rating',
			],
			'PerformanceRating'   => [
				'name' => 'hm-rating-performance',
				'type' => 'rating',
			],
			'EaseOfUseRating'     => [
				'name' => 'hm-rating-easeofuse',
				'type' => 'rating',
			],
			'SupportRating'       => [
				'name' => 'hm-rating-support',
				'type' => 'rating',
			],
			'DocumentationRating' => [
				'name' => 'hm-rating-documentation',
				'type' => 'rating',
			],
			'ValueOfMoneyRating'  => [
				'name' => 'hm-rating-valueformoney',
				'type' => 'rating',
			],
			'OverallRating'       => [
				'name' => 'hm-rating-overall',
				'type' => 'rating',
			],
			'IsBestBuy'           => [
				'name' => 'hm-rating-bestbuy',
				'type' => 'bool',
			],
			'IsRecommend'         => [
				'name' => 'hm-rating-recommended',
				'type' => 'bool',
			],
			'IsLabApproved'       => [
				'name' => 'hm-rating-labapproved',
				'type' => 'bool',
			],
			'IsPickOfTheLitter'   => [
				'name' => 'hm-rating-pickofthelitter',
				'type' => 'bool',
			],
			'MinimumAccessLevel'  => [
				'name' => 'hm-minimum-access-level',
				'type' => 'int',
			],
			'ReviewType' => [
				'name' => 'hm-review-type',
				'type' => 'review-type',
			],
			'ReviewDate' => [
				'name' => 'hm-review-date',
				'type' => 'date-ymd',
			],
			'MinimumAccessLevel' => [
				'name' => 'hm-minimum-acces-level',
				'type' => 'int',
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate product-reviews', 'ProductReviews' );
