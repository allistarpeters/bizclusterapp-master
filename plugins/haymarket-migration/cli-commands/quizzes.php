<?php
/**
 * Migrate Newsletter Issues
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users;

class Quizzes extends WP_CLI {
	/**
	 * Migrates Quizzes
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration quizzes 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$pub_id     = $args[0];
		$quizzes = $this->get_quizzes( $pub_id );
		$count      = count( $quizzes );
		$report     = [];
		$progress   = \WP_CLI\Utils\make_progress_bar( __( 'Importing Quizzes', 'haymarket' ), $count );
		foreach ( $quizzes as $quiz ) {

			$existing_id = Helpers\get_post_by_legacy_id( (int) $quiz->quizId, 'hm-quiz' );
			if ( ! empty( $existing_id ) ) {
				// tick even when not importing.
				$progress->tick();
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $quiz->{$api_key} ) ) {
					continue;
				}
				$api_value                = $quiz->{$api_key};
				$value                    = $data['type'];
				$meta_key                 = $data['name'];
				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $value );
			}

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$quiz_status = $this->get_quiz_published_status( $quiz );
			// NOTE: Some quizes don't have publish dates set. Use createdDate in that case.
			$datestring = ( ! empty( $quiz->publishDate ) ) ? date( 'Y-m-d H:i:s', strtotime( $quiz->publishDate ) ) : date( 'Y-m-d H:i:s', strtotime( $quiz->createdDate ) );

			$insert_args = [
				'post_title'   => $quiz->title ? $quiz->title : '',
				'post_date'    => $datestring,
				'post_type'    => 'hm-quiz',
				'post_status'  => $quiz_status,
				'post_author'  => Users\get_default_user_id( $quiz ), // NOTE: how to get author id
				'post_excerpt' => $quiz->deck ? wp_kses_post( $quiz->deck ) : '',
			];

			$post_id = wp_insert_post(
				$insert_args
			);

			if ( empty( $post_id ) ) {
				// tick even when not importing.
				$progress->tick();
				continue;
			}

			//Insert post content after WordPress post is created so that we can upload inline images
			$post_args = [
				'ID'     => $post_id,
				'post_content'  => $quiz->description ? Helpers\prepare_post_body( $post_id, $quiz->description ) : '',
			];

			$post_id = wp_update_post( $post_args );

			//Set quiz featured image
			if ( ! empty( $quiz->imageUrl ) ) {
				$attachment_id = \Haymarket\Migration\Images\upload_remote_image(
					$quiz->imageUrl,
					$post_id,
					[
						'post_title'   => $quiz->title,
					]
				);
				set_post_thumbnail( $post_id, $attachment_id );
			}

			if ( isset( $quiz->contentTypes ) ) {

				$questions = $this->get_questions_formatted( $quiz->contentTypes );
				if( ! empty( $questions ) ) {
					$qs_set = add_post_meta( $post_id, 'hm-questions', $questions );
					if ( $qs_set ) {
						$report[ 'questions' ][] = $questions;
					}
				}
			}

			// sort the other meta for quiz post types.
			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			// set one additional meta that clashes with a key used in the loop above.
			add_post_meta( $post_id, HMM_PREFIX . 'id', $quiz->quizId );

			foreach( $quiz->topicTags as $tag ) {
				wp_set_object_terms(
					$post_id,
					$tag->tagName,
					'post_tag',
					true
				);
			}

			if( ! empty( $quiz->primarySectionId ) ){
				$category = (int) \Haymarket\Legacy_Helpers\get_term_by_legacy_id( $quiz->primarySectionId, 'category' );

				if( ! empty( $category ) ){
					wp_set_object_terms(
						$post_id,
						$category,
						'category'
					);
				} else {
					wp_set_object_terms(
						$post_id,
						'Clinical Quiz',
						'category'
					);
				}
			} else {
				wp_set_object_terms(
					$post_id,
					'Clinical Quiz',
					'category'
				);
			}

			// Coauthors
			$author_data = [];
			if ( isset( $quiz->authors ) && ! empty( $quiz->authors  ) ) {
				$author_data = array_map( function ( $author ) {
					//Attempt to get author by legacy author id
					if( ! empty( $author->id ) && $author->id > 0){
						$user = Helpers\get_user_by_legacy_id( $author->id  );
						if( $user ){
							$user = get_user_by( 'id', $user );
							if( $user ){

							}
							$author_login = $user->user_login;
						}
					}

					if( empty( $author_login ) ){
						//attempt to find author by full name search
						global $coauthors_plus;
						$user = $coauthors_plus->search_authors( $author->name );
						if( ! empty( $user ) && is_array( $user ) ){
							$user = reset( $user );
							//grab first user found
							$author_login = $user->user_login;
						} else {
							$author_login = sanitize_title( "{$author->name}-{$author->id} " );
						}
					}

					$coauthor     = Users\get_coauthor_by_display_name( $author_login );
					if ( is_object( $coauthor ) ) {
						return $coauthor->user_nicename; // format CAP add-to-post function expects
					} else { // article is attributed to author not existent in User API endpoint. create a co-author on the spot.
						return Users\create_coauthor( $author->name, $author->id );
					}
				}, $quiz->authors );

				// set coauthors terms.
				wp_set_object_terms(
					$post_id,
					$author_data,
					'author'
				);
			}

			$report['Quizzes Imported'][] = $post_id;
			$progress->tick();

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $quiz );
		} // End foreach().
		$progress->finish();


		Reporting\report( $report, $count, 'Quizzes' );
	}

	/**
	 * Query API for quizzes
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_quizzes( $pub_id ) {

		// TODO: What is the quiz url?
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/quiz/getallquizzes", [
			'timeout' => 45,
		] );
		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		// NOTE: Is this all the post meta values needed for a quiz?
		return [
			'id' => [
				'name' => '_hm_quiz_syndication_id',
				'type' => 'string',
			],
			// NOTE: This array can't have 2 items with quizId key. This first one is also set at a different point but left here so it's easy to see what keys we're using.
			'quizId'               => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'quizId' => [
				'name' => '_hm_quiz_syndication_quiz_id',
				'type' => 'int',
			],
			'displayResult'            => [
				'name' => 'hm-display-results',
				'type' => 'bool',
			],
			'displayGraph'    => [
				'name' => 'hm-display-graph',
				'type' => 'bool',
			],
			'submitOnEveryQuestion'    => [
				'name' => 'hm-display-submit',
				'type' => 'bool',
			],
			'displayScore'    => [
				'name' => 'hm-display-score',
				'type' => 'bool',
			],
			'allQuestionsRequired'  => [
				'name' => 'hm-all-questions-required',
				'type' => 'bool',
			],
			'isGated'        => [
				'name' => 'hm-gated',
				'type' => 'bool',
			],
		];
	}

	public function get_quiz_published_status( $published ) {
		// NOTE: May need to account for future posts here too!
		// NOTE: This may need to have other date passed in because not all posts have dates in their published fields.
		$status = 'draft';
		if ( $published ) {
			$status = 'publish';
		}
		return $status;
	}

	public function get_questions_formatted( $content_types ) {
		$questions = [];
		foreach( $content_types as $content ) {
			// NOTE: ContentTypeId 1 = a question, 2 = an ad, 3 = ASSUMING a page brake.
			if ( 1 === $content->contentTypeId ) {

				$answers_arr = [];

				foreach ( $content->question->answers as $answer ) {
					$answers_arr[] = [
						'id'      => intval( $answer->answerId ),
						'text'    => wp_kses_post( $answer->answerText ),
						'correct' => boolval( $answer->isCorrect ),
					];
				}
				$questions[] = [
					'id'          => intval( $content->question->questionId ),
					'type'        => 'question',
					'question'    => wp_kses_post( $content->question->questionText ),
					'explanation' => wp_kses_post( $content->question->explanationText ),
					'answers'     => $answers_arr,
				];

			} elseif ( 2 === $content->contentTypeId ) {
				$questions[] = [
					'type' => 'ad',
				];
			} elseif ( 3 === $content->contentTypeId ) {
				$questions[] = [
					'type' => 'pagebreak',
				];
			}
		}
		return $questions;
	}

}

WP_CLI::add_command( 'haymarket-migrate quizzes', 'Quizzes' );
