<?php
/**
 * Migrate Group Tests
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users as Users;

class GroupTests extends WP_CLI {
	/**
	 * Migrates Group Tests
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration group-tests 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$pub_id   = $args[0];
		$group_tests = $this->get_group_tests( $pub_id );
		$count    = count( $group_tests );
		$report   = [];
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Group Tests', 'haymarket' ), $count );

		foreach ( $group_tests as $group_test ) {

			$existing_id = Helpers\get_post_by_legacy_id( (int) $group_test->{'Group Test Id'}, 'hm-group-test' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			$meta_values = [];
			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $group_test->{$api_key} ) ) {
					continue;
				}
				$api_value = $group_test->{$api_key};
				$type      = $data['type'];
				$meta_key  = $data['name'];

				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $type );
			}

			// Map product reviews based on legacy ID.
			$meta_values['hm-selected-reviews'] = array_map( function ( $item ) {
				$legacy_id = Helpers\get_post_by_legacy_id( $item->Id, 'hm-product-review' );
				return $legacy_id;
			}, $group_test->{'Selected Reviews(array of IDs)'} );

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );

			$post_id = wp_insert_post(
				[
					'post_author'  => Users\get_default_user_id(),
					'post_title'   => $group_test->Title ?? null,
					'post_excerpt' => $group_test->Deck ?? null,
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $group_test->PublishDate ) ) ?? null,
					'post_type'    => 'hm-group-test',
					'post_status'  => 'publish',
				]
			);

			if ( empty( $post_id ) ) {
				$progress->tick();
				continue;
			}

			//Insert post content after WordPress post is created so that we can upload inline images
			$post_content = $group_test->Body ? Helpers\prepare_post_body( $post_id, $group_test->Body ) : '';
			$post_args = [
				'ID'           => $post_id,
				'post_content' => $post_content
			];

			$post_id = wp_update_post( $post_args );

			// Meta Fields.
			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}

			$report['Group Tests Imported'][] = $post_id;
			$progress->tick();
			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $group_test );
		} // End foreach().
		$progress->finish();


		Reporting\report( $report, $count, 'Group Tests' );
	}

	/**
	 * Query API for Group Tests
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_group_tests( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/group-tests", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Group Test Id' => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int'
			],
			'Selected Reviews(array of IDs)' => [
				'name' => 'hm-selected-reviews',
				'type' => 'json'
			],
			'Summary' => [
				'name' => 'hm-summary',
				'type' => 'string',
			],
			'MinimumAccessLevel' => [
				'name' => 'hm-minimum-access-level',
				'type' => 'int',
			]
		];
	}

	/**
	 * Get section image from API data
	 *
	 * @param object $image_group
	 *
	 * @return int
	 */
	public function get_featured_image( $image_group ) {
		if ( empty( $image_group ) ) {
			return;
		}
		$url = $image_group->Images[0]->Url;

		return Images\upload_remote_image( $url );
	}
}

WP_CLI::add_command( 'haymarket-migrate group-tests', 'GroupTests' );

