<?php
/**
 * Migrate Webcasts
 *
 * @package Haymarket/Migration
 */

use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;
use Haymarket\Migration\Users;

class Webcasts extends WP_CLI {
	/**
	 * Migrates Webcasts
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration webcasts 6
	 *
	 * @param array $args Positional arguments.
	 */
	public function __invoke( $args ) {
		$pub_id   = $args[0];
		$webcasts = $this->get_webcasts( $pub_id );
		$count    = count( $webcasts );
		$report   = [];
		$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Webcasts', 'haymarket' ), $count );

		foreach ( $webcasts as $webcast ) {
			$existing_id = Helpers\get_post_by_legacy_id( (int) $webcast->{'Webcast ID'}, 'hm-webcast' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}
			$post_id = wp_insert_post(
				[
					'post_title'   => Helpers\sanitize( $webcast->Title, 'string' ) ?? null,
					'post_content' => Helpers\sanitize( $webcast->Description, 'post_content' ) ?? null,
					'post_excerpt' => Helpers\sanitize( $webcast->ShortDescription, 'string' ) ?? null,
					'post_date'    => date( 'Y-m-d H:i:s', strtotime( $webcast->PublishDate ) ) ?? null,
					'post_type'    => 'hm-webcast',
					'post_status'  => 'publish',
					'post_author'  => Users\get_default_user_id( ),
				]
			);

			if ( empty( $post_id ) ) {
				$progress - tick();
				continue;
			}

			$meta_values = [];

			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! isset( $webcast->{$api_key} ) ) {
					continue;
				}
				$api_value = $webcast->{$api_key};
				$type      = $data['type'];
				$meta_key  = $data['name'];

				// Nested value.
				if ( 'Webcast Type' === $api_key ) {
					$api_value = $api_value->Id;
				}

				$meta_values[ $meta_key ] = Helpers\sanitize( $api_value, $type );
			}

			$meta_values['hm-webcast-date-query'] = date( 'Y-m-d', strtotime( $webcast->WebCastDate ) );

			$meta_input = array_filter( $meta_values, function ( $value ) {
				return ! empty( $value );
			} );
			// All webcasts are ET.
			$meta_input['hm-webcast-timezone'] = 'et';


			// Meta Fields.
			foreach ( $meta_input as $key => $value ) {
				$meta_added = add_post_meta( $post_id, $key, $value );
				if ( $meta_added ) {
					$report[ $key ][] = $key;
				}
			}



			// set sponsors by legacy ID lookup.
			if ( ! empty( $webcast->WebCastIdSponsorId ) ) {
				$term_id = Haymarket\Migration\Helpers\get_term_by_legacy_id( $webcast->WebCastIdSponsorId[0]->SponsorID, 'hm-sponsor' );
				if ( ! empty( $term_id ) ) {
					$created = wp_set_object_terms(
						$post_id,
						$term_id,
						'hm-sponsor'
					);

					if ( is_array( $created ) && ! empty( $created ) ) {
						$report['sponsors'][] = $created;
					}
				}
			}

			// Webcast Topics.
			if ( ! empty( $webcast->WebCastIdTopicId ) ) {
				// set category to Section.
				foreach ( $webcast->WebCastIdTopicId as $topic ) {
					$created = wp_set_object_terms(
						$post_id,
						$topic->Name,
						'hm-webcast-topic'
					);
					if ( is_array( $created ) && ! empty( $created ) ) {
						add_term_meta( $created[0], HMM_PREFIX . 'id', $topic->Id );
						$report['Webcasts Topics'][] = $post_id;
					}
				}
			}

			// Featured Image.
			$img_added = set_post_thumbnail( $post_id, $this->get_featured_image( $webcast->Images ) );
			if ( $img_added ) {
				$report['Featured Image'][] = $img_added;
			}

			$report['Webcasts Imported'][] = $post_id;
			$progress->tick();

			add_post_meta( $post_id, HMM_PREFIX . 'legacy-data', $webcast );
		} // End foreach().
		$progress->finish();


		Reporting\report( $report, $count, 'Webcasts' );
	}

	/**
	 * Query API for webcasts
	 *
	 * @param int $pub_id Publication ID.
	 *
	 * @return array|mixed|object
	 */
	public function get_webcasts( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/webcasts", [
			'timeout' => 45,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::error( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Webcast ID'   => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'Webcast Type' => [
				'name' => 'hm-webcast-type',
				'type' => 'int',
			],
			'WebCastDate'  => [
				'name' => 'hm-webcast-date',
				'type' => 'date',
			],
			'RegisterUrl'  => [
				'name' => 'hm-register-url',
				'type' => 'url',
			],
		];
	}

	/**
	 * Get section image from API data
	 *
	 * @param object $image_group
	 *
	 * @return int
	 */
	public function get_featured_image( $image_group ) {
		if ( empty( $image_group ) ) {
			return;
		}
		$url = $image_group->Images[0]->Url;

		return Images\upload_remote_image( $url );
	}
}

WP_CLI::add_command( 'haymarket-migrate webcasts', 'Webcasts' );

