<?php
/**
 * Migrate Newsletter Taxonomy Terms
 */
use Haymarket\Migration\Images as Images;
use Haymarket\Migration\Helpers as Helpers;
use Haymarket\Migration\Reporting as Reporting;


class Newsletter extends WP_CLI {
	/**
	 * Migrates Newsletters
	 *
	 * ## OPTIONS
	 *
	 * <pub_id>
	 * : The ID of the publication to migrate from
	 *
	 * ## EXAMPLES
	 *
	 *     wp haymarket-migration newsletters 6
	 *
	 * @when after_wp_load
	 */
	public function __invoke( $args ) {
		$pub_id          = $args[0];
		$newsletters     = $this->get_newsletters( $pub_id );
		$count           = count( $newsletters );
		$report          = [];
		$progress        = \WP_CLI\Utils\make_progress_bar( __( 'Importing Newsletters', 'haymarket' ), $count );

		foreach ( $newsletters as $newsletter ) {
			$existing_id = Helpers\get_term_by_legacy_id( (int) $newsletter->Id, 'hm-newsletter' );
			if ( ! empty( $existing_id ) ) {
				continue;
			}

			$term = wp_insert_term( $newsletter->Name, 'hm-newsletter', [
				'description' => Helpers\sanitize( $newsletter->Description, 'string' ),
			] );

			if ( is_wp_error( $term ) ) {
				$report['term-creation-errors'][] = $newsletter->Name;
				$progress->tick();
				continue;
			}

			$report['terms-created'][] = $newsletter->Name;

			foreach ( $this->get_meta_keys() as $api_key => $data ) {
				if ( ! empty( $newsletter->{$api_key} ) ) {
					$value = $newsletter->{$api_key};
					if ( 'HeaderLogo' === $api_key ) {
						$value = Images\upload_remote_image( $newsletter->{$api_key} );
					}
					$meta = add_term_meta(
						$term['term_id'],
						$data['name'],
						Helpers\sanitize( $value, $data['type'] )
					);
					if ( $meta && ! is_wp_error( $meta ) ) {
						$report[ $data['name'] ][] = $meta;
					}
				}
			}

			update_term_meta( $term['term_id'], HMM_PREFIX . 'legacy-data', $newsletter );

			$progress->tick();
		}
		$progress->finish();

		Reporting\report( $report, $count, 'Newsletters' );

	}

	/**
	 * Query API for newsletters
	 *
	 * @param $pub_id
	 *
	 * @return array|mixed|object
	 */
	public function get_newsletters( $pub_id ) {
		$request = wp_remote_get( API_V2_URL . "pub/$pub_id/newsletters", [
			'timeout' => 30,
		] );

		if ( is_wp_error( $request ) ) {
			wp_cli::warning( $request->get_error_message() );
		}

		return json_decode( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 * @return array
	 */
	public function get_meta_keys() {
		return [
			'Id' => [
				'name' => HMM_PREFIX . 'id',
				'type' => 'int',
			],
			'AdName'                          => [
				'name' => 'hm-ad-name',
				'type' => 'string',
			],
			'CSS' => [
				'name' => 'hm-css',
				'type' => 'string',
			],
			'DisplayName'                     => [
				'name' => 'hm-display-name',
				'type' => 'string',
			],
			'FooterText'                      => [
				'name' => 'hm-footer-text',
				'type' => 'string',
			],
			'FromAddress'                     => [
				'name' => 'hm-from-address',
				'type' => 'email',
			],
			'HeaderLogo'                      => [
				'name' => 'hm-logo',
				'type' => 'string',
			],
			'HeaderLogoLink'                  => [
				'name' => 'hm-url',
				'type' => 'string',
			],
			'LiveIntentSectionIdForLargeAds'  => [
				'name' => 'hm-live-intent-728-90',
				'type' => 'string',
			],
			'LiveIntentSectionIdForSmallAds'  => [
				'name' => 'hm-live-intent-300-250',
				'type' => 'string',
			],
			'NewsletterProviderAPIParameter1' => [
				'name' => 'hm-api-parameter-1',
				'type' => 'string',
			],
			'NewsletterProviderAPIParameter2' => [
				'name' => 'hm-api-parameter-2',
				'type' => 'string',
			],
			'NewsletterProviderAPIParameter3' => [
				'name' => 'hm-api-parameter-3',
				'type' => 'string',
			],
			'NewsletterProviderAPIParameter4' => [
				'name' => 'hm-api-parameter-4',
				'type' => 'string',
			],
			'NewsletterProviderAPIParameter5' => [
				'name' => 'hm-api-parameter-5',
				'type' => 'string',
			],
			'ReplyTo'                         => [
				'name' => 'hm-reply-to-address',
				'type' => 'email',
			],
			'Tracking'                        => [
				'name' => 'hm-tracking',
				'type' => 'string'
			],
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate newsletters', 'Newsletter' );

