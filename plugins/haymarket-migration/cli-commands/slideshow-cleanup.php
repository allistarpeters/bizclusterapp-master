<?php

use Haymarket\Migration\Helpers as Helpers;

class FixSlideshows extends WP_CLI_Command {

	/**
	 * Set un published post to draft and fix related slideshows
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Callback function to update posts.
	 *
	 * @param \WP_Query $query WordPress query object.
	 *
	 * @return void
	 */
	public function update_posts( $query ) {
		$query->the_post();

		$post_id     = get_the_ID();
		$legacy_data = get_post_meta( $post_id, '_hmm-mig-legacy-data', true );

		if ( ! empty( $legacy_data->RelatedSlideShows ) ) {
			$related_slideshows = array();

			foreach ( $legacy_data->RelatedSlideShows as $slideShow ){
				$id = Helpers\get_post_by_legacy_id( $slideShow, 'hm-slideshow' );
				if( $id ){
					$related_slideshows[] = $id;
				}
			}

			if ( ! empty( $related_slideshows ) ) {
				update_post_meta( $post_id, 'hm-related-slideshows', array_filter( $related_slideshows ) );
			}
		}

		//Set post to draft when IsPublished = false
		if ( false === $legacy_data->IsPublished && 'publish' === get_post_status( $post_id ) ) {
			wp_update_post( array(
				'ID'          => $post_id,
				'post_status' => 'draft'
			) );
		}

		$quiz_id = get_post_meta( $post_id, 'hm-related-quiz', true );
		// Set slideshow quiz post_status to `publish-slideshow`
		if ( ! empty( $quiz_id ) && ! empty( $quiz = get_post( $quiz_id ) ) && 'publish' === $quiz->post_status ) {
			wp_update_post( [
				'ID'          => $quiz_id,
				'post_status' => 'publish-slideshow'
			] );
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 50;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'hm-slideshow',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					call_user_func( $callback, $query );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );

			// Avoid running out of memory.
		}

		wp_reset_postdata();
	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate fix-slideshows', 'FixSlideshows' );
