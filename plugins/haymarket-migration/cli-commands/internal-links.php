<?php

class Internal_Links extends WP_CLI_Command {

	public $post_updated = [];

	/**
	 * Search and replace internal urls and ids
	 *
	 * @param array $args       Args passed in order.
	 * @param array $assoc_args Args passed as flags.
	 */
	public function __invoke( $args, $assoc_args ) {
		$this->paginated_query(
			[ $this, 'update_posts' ],
			null,
			$assoc_args
		);
	}

	/**
	 * Update inline links and ids in related meta data
	 *
	 * @param $post
	 */
	public function update_posts( $post ) {
		$updated = false;
		$post_content     = $post->post_content;

		preg_match_all( '/href="(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))/', $post_content, $links, PREG_SET_ORDER );

		foreach ( $links as $link ) {
			if ( ! empty( $link[1] ) ) {

				preg_match( '%.?.+?\/(article|newsletter\/item|slideshow|product-review|webcast|author|newsletter|section|drug|manufacturer)\/([0-9]+)%', $link[1], $matches );

				if ( empty( $matches ) ) {
					continue;
				}

				$type      = $matches[1];
				$legacy_id = $matches[2];

				switch ( $type ) {
					case 'article':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'post' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'webcast':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-webcast' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'product-review':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-product-review');
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'webcast':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-webcast' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'slideshow':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-slideshow' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'manufacturer':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-company' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'drug':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-drug' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'newsletter':
					case 'section':
						// determine taxonomy based on content type name.
						switch ( $type ) {
							case 'newsletter':
								$taxonomy = 'hm-newsletter';
								break;
							case 'section':
								$taxonomy = 'category';
								break;
							default:
								break;
						}
						// query for legacy id.
						$term_id = \Haymarket\Legacy_Helpers\get_term_by_legacy_id( $legacy_id, $taxonomy );
						if ( ! empty( $term_id ) ) {
							$url = get_term_link( $term_id );
						}
						break;
					case 'author':
						$user_id = \Haymarket\Legacy_Helpers\get_user_by_legacy_id( $legacy_id );
						if ( ! empty( $user_id ) ) {
							$url = get_author_posts_url( $user_id );
						}
						break;
					default:
						$redirect = null;
				}

				if ( ! empty( $url ) ) {
					$updated = true;
					$post_content = str_replace( $link[1], $url, $post_content );
				}
			}
		}

		//fix JSON urls
		preg_match_all( '/href="(\/([-a-zA-Z0-9@:%_\+.~#$?&\/\/=]*))/', $post_content, $links, PREG_SET_ORDER );

		foreach ( $links as $link ) {
			if ( ! empty( $link[1] ) ) {
				preg_match( '%.?.+?\/(article|slideshow|drugs)\/([0-9-]+).json%', $link[1], $matches );

				if ( empty( $matches ) ) {
					continue;
				}

				$type      = $matches[1];
				$legacy_id = $matches[2];

				switch ( $type ) {
					case 'article':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'post' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'slideshow':
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-slideshow' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					case 'drugs':
						$legacy_id_parts = explode('-', $legacy_id );

						if( count( $legacy_id_parts ) > 1 ){
							$legacy_id = $legacy_id_parts[1];
						}
						$post_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $legacy_id, 'hm-drug' );
						if ( ! empty( $post_id ) ) {
							$url = get_permalink( $post_id );
						}
						break;
					default:
						$redirect = null;
				}

				if ( ! empty( $url ) ) {
					$updated = true;
					$post_content = str_replace( $link[1], $url, $post_content );
				}
			}
		}

		if ( $updated ) {
			$post_args = [
				'ID'           => $post->ID,
				'post_content' => $post_content,
			];

			$this->post_updated[] = $post->ID;

			wp_update_post( $post_args );
		}

		if ( 'post' === $post->post_type ) {
			$articles = get_post_meta( $post->ID, 'hm-related-articles', true );
			if ( ! empty( $articles ) ) {
				if ( ! is_array( $articles ) ) {
					$articles = json_decode( $articles );
				}
				if( is_array( $articles ) ){
					foreach ( $articles as $key => $article ) {
						$new_article_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $article->id );
						if ( $new_article_id ) {
							$articles[ $key ] = array(
								'id'    => $new_article_id,
								'title' => $article->title,
								'url'   => get_the_permalink( $new_article_id )
							);
						}
					}
					update_post_meta( $post->ID, 'hm-related-articles', json_encode( $articles ) );
				}
			}


			$slideshows = get_post_meta( $post->ID, 'hm-related-slideshows', true );
			if ( ! empty( $slideshows ) ) {
				if ( ! is_array( $slideshows ) ) {
					$slideshows = json_decode( $slideshows );
				}
				foreach ( $slideshows as $key => $slideshow ) {
					$new_slideshow_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $slideshow->id );
					if ( $new_slideshow_id ) {
						$slideshows[ $key ] = array(
							'id'    => $new_slideshow_id,
							'title' => $slideshow->title,
							'url'   => get_the_permalink( $new_slideshow_id ),
							'image' => has_post_thumbnail( $new_slideshow_id ) ? wp_get_attachment_image_url( get_post_thumbnail_id( $new_slideshow_id ) ) : ''
						);
					}
				}
				update_post_meta( $post->ID, 'hm-related-slideshows', json_encode( $articles ) );
			}
		}

		if ( 'hm-slideshow' === $post->post_type ) {
			$articles = get_post_meta( $post->ID, 'hm-related-articles', true );
			if ( ! empty( $articles ) ) {
				if ( ! is_array( $articles ) ) {
					$articles = json_decode( $articles );
				}
				$article_ids = array();
				foreach ( $articles as $key => $article ) {
					$new_article_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $article->id );
					if ( $new_article_id ) {
						$article_ids[] = $article_ids;
					}
				}
				update_post_meta( $post->ID, 'hm-related-articles', implode( ',', $article_ids ) );
			}


			$slideshows = get_post_meta( $post->ID, 'hm-related-slideshows', true );
			if ( ! empty( $slideshows ) ) {
				if ( ! is_array( $slideshows ) ) {
					$slideshows = json_decode( $slideshows );
				}
				$slidshow_ids = array();
				foreach ( $slideshows as $key => $slideshow ) {
					$new_slideshow_id = \Haymarket\Legacy_Helpers\get_post_by_legacy_id( $slideshow->id );
					if ( $new_slideshow_id ) {
						$slidshow_ids[] = $new_slideshow_id;
					}
				}
				update_post_meta( $post->ID, 'hm-related-slideshows', implode( ',', $slidshow_ids ) );
			}
		}
	}

	/**
	 * Invokes a callback function per each post in a paginated query.
	 *
	 * @param string $callback Name of the callback function.
	 * @param int    $per_page Number of posts to grab per page.
	 *
	 * @return void
	 */
	public function paginated_query( $callback, $per_page = 0, $assoc_args = false ) {
		$posts_per_page = ! empty( $per_page ) ? absint( $per_page ) : 5;

		if ( ! empty( $assoc_args['offset'] ) ) {
			$offset = absint( $assoc_args['offset'] );
		} else {
			$offset = 0;
		}

		// set up query and re-use to avoid memory issues.
		$query = new WP_Query();

		while ( true ) {
			$args = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'post',
				'post_status'    => 'any',
				'offset'         => $offset,
				'orderby'        => 'ID',
				'order'          => 'DESC',
			);
			$query->query( $args );
			if ( $query->have_posts() ) {
				foreach ( $query->posts as $post ) {
					call_user_func( $callback, $post );
				}
			} else {
				break;
			}
			WP_CLI::log( 'Processed ' . ( $query->post_count + $offset ) . '/' . $query->found_posts . ' entries. . .' );

			// set offset for next page.
			$offset += $posts_per_page;

			// Take a breath.
			usleep( 500 );
		}
		wp_cli::success( count( $this->post_updated ) . ' internal links were updated' );

		wp_reset_postdata();

	}
}

// Add the command.
WP_CLI::add_command( 'haymarket-migrate replace-internal-links', 'Internal_Links' );
