<?php
/**
 * Migrates Users
 */

use Haymarket\Migration\Users as User;
use Haymarket\Migration\Reporting as Reporting;

class Authors extends WP_CLI {
	// todo cli docblocks
	public function __invoke( $args ) {
		if ( ! isset( $args[0] ) ) {
			WP_CLI::error( 'Please provide a site ID' );
		} else {
			$pub_id = $args[0];
		}

		global $coauthors_plus;

		if ( ! class_exists( 'CoAuthors_Plus' ) ) {
			WP_CLI::error( 'Co-Authors Plus is not active.' );
		}

		$request = wp_remote_get( API_V2_URL . "pub/{$pub_id}/authors", [
				'timeout' => 15
			]
		);
		if ( is_wp_error( $request ) ) {
			WP_CLI::error( $request->get_error_message() );
		}

		$authors   = json_decode( wp_remote_retrieve_body( $request ) );
		$count     = ( is_array( $authors ) || $authors instanceof Countable ) ? count( $authors ) : 0;
		$users     = [];
		$coauthors = [];
		$progress  = \WP_CLI\Utils\make_progress_bar( 'Generating users', count( $authors ) );

		if ( $user_id = username_exists( 'haymarket' ) ) {
			add_user_to_blog( get_current_blog_id(), $user_id, 'editor' );
		} else {
			// Insert default author.
			wp_insert_user( [
				'user_login'     => 'haymarket',
				'display_name'   => 'Haymarket' . ' ' . 'Media',
				'first_name'     => 'Haymarket',
				'last_name'      => 'Media',
				'user_pass'      => null,
				'user_email'     => 'haymarket@haymarketmedia.com',
				'role'           => 'editor',
				'user_registerd' => date( 'Y-m-d H:i:s' ),
			] );
		}

		foreach ( $authors as $author ) {
			$legacy_id  = $author->Id ?? 0;
			$user_login = sanitize_title( $author->FirstName . ' ' . $author->LastName );
			$is_wp_user = ! empty( $author->EmailAddress ) && $author->IsActive;

			// create wp user or coauthor.
			if ( $is_wp_user ) {
				$user = \Haymarket\Migration\Helpers\get_user_by_legacy_id( $legacy_id );

				if ( $user ) {
					$user_id = $user;
				} else {
					$user_id = wp_insert_user(
						[
							'user_login'     => $user_login,
							'display_name'   => $author->FirstName . ' ' . $author->LastName,
							'first_name'     => $author->FirstName,
							'last_name'      => $author->LastName,
							'user_pass'      => null,
							'user_email'     => $author->EmailAddress,
							'role'           => 'editor',
							'user_registerd' => date( 'Y-m-d H:i:s' ),
						]
					);
				}

				if ( ! empty( $user_id ) && ! is_wp_error( $user_id ) ) {
					$users[] = $user_id;
					if ( ! empty( $legacy_id ) ) {

						$meta_values = [];
						foreach ( $this->get_meta_keys() as $api_key => $data ) {

							if ( ! isset( $author->{$api_key} ) ) {
								continue;
							}
							$api_value                = $author->{$api_key};
							$value                    = $data['type'];
							$meta_key                 = $data['name'];
							$meta_values[ $meta_key ] = \Haymarket\Migration\Helpers\sanitize( $api_value, $value );
						}

						$meta_input = array_filter( $meta_values, function ( $value ) {
							return ! empty( $value );
						} );

						foreach ( $meta_input as $key => $value ) {
							add_user_meta( $user_id, $key, $value );
						}

						//Upload user avatar
						if ( isset( $author->ImageGroup ) && ! empty( $author->ImageGroup->Images ) ) {
							if ( ! empty( $author->ImageGroup->Images[0] ) && ! empty( $author->ImageGroup->Images[0]->Url ) ) {
								$avatar_id = \Haymarket\Migration\Images\upload_remote_image( $author->ImageGroup->Images[0]->Url );
								if ( $avatar_id ) {
									$meta_value             = array();
									$meta_value['media_id'] = $avatar_id;
									$meta_value['full']     = wp_get_attachment_url( $avatar_id );
									update_user_meta( $user_id, 'simple_local_avatar', $meta_value );
								}
							}
						}

						update_user_meta( $user_id, HMM_PREFIX . 'id', (int) $legacy_id );
						update_user_meta( $user_id, HMM_PREFIX . 'legacy-data', $author );
					}
				}
			} else {
				$coauthor_exists = $coauthors_plus->get_coauthor_by( 'user_login', $user_login, true );

				if ( ! $coauthor_exists ) {
					$coauthor = $coauthors_plus->guest_authors->create(
						[
							'user_login'   => $user_login,
							'display_name' => $author->FirstName . ' ' . $author->LastName,
							'first_name'   => $author->FirstName,
							'last_name'    => $author->LastName,
						]
					);

					if ( ! empty( $coauthor ) ) {
						$meta_values = [];
						foreach ( $this->get_meta_keys() as $api_key => $data ) {
							if ( ! isset( $author->{$api_key} ) ) {
								continue;
							}
							$api_value                         = $author->{$api_key};
							$value                             = $data['type'];
							$meta_key                          = $data['name'];
							$meta_values[ 'cap-' . $meta_key ] = \Haymarket\Migration\Helpers\sanitize( $api_value, $value );
						}

						$meta_values['cap-user_email'] = \Haymarket\Migration\Helpers\sanitize( $author->EmailAddress, 'email' );

						$meta_input = array_filter( $meta_values, function ( $value ) {
							return ! empty( $value );
						} );

						foreach ( $meta_input as $key => $value ) {
							add_post_meta( $coauthor, $key, $value );
						}

						//Upload user avatar
						if ( isset( $author->ImageGroup ) && ! empty( $author->ImageGroup->Images ) ) {
							if ( ! empty( $author->ImageGroup->Images[0] ) && ! empty( $author->ImageGroup->Images[0]->Url ) ) {
								$avatar_id = \Haymarket\Migration\Images\upload_remote_image( $author->ImageGroup->Images[0]->Url );
								if ( $avatar_id ) {
									set_post_thumbnail( $coauthor, $avatar_id );
								}
							}
						}

						update_post_meta( $coauthor, HMM_PREFIX . 'legacy-data', $author );
					}

					if ( ! empty( $coauthor ) && ! empty( $legacy_id ) ) {
						update_post_meta( $coauthor, HMM_PREFIX . 'id', (int) $legacy_id );
					}
					$coauthors[] = $coauthor;
				} else {
					WP_CLI::debug( $user_login . 'Already Exists' );
				}
			}
			$progress->tick();
		}
		$progress->finish();

		Haymarket\Migration\Reporting\report(
			[
				'Users created'     => $users,
				'Coauthors created' => $coauthors,
			], $count, 'Authors'
		);

		WP_CLI::runcommand( 'co-authors-plus create-guest-authors', [
			'return' => true,
		] );
	}

	/**
	 * Returns associative array of meta keys and slug names/data types
	 *
	 * @return array
	 */
	public function get_meta_keys() {

		return [
			'Bio'                => [
				'name' => 'description',
				'type' => 'string',
			],
			'Title'              => [
				'name' => 'profile-title',
				'type' => 'string',
			],
			'GooglePlusUrl'      => [
				'name' => 'googleplus',
				'type' => 'string',
			],
			'TwitterUrl'         => [
				'name' => 'twitter',
				'type' => 'string',
			],
			'Video'              => [
				'name' => 'video',
				'type' => 'string',
			],
			'Rss'                => [
				'name' => 'rss',
				'type' => 'string',
			],
			'Institution'        => [
				'name' => 'profile-institution',
				'type' => 'string',
			],
			'IsSpeaker'          => [
				'name' => 'profile-is-speaker',
				'type' => 'string',
			],
			'MetaTagDescription' => [
				'name' => 'wpseo_metadesc',
				'type' => 'string',
			]
		];
	}
}

WP_CLI::add_command( 'haymarket-migrate authors', 'Authors' );
