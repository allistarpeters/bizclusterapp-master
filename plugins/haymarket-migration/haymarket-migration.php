<?php
/**
 * Plugin Name:     Haymarket Migration
 * Description:     Haymarket Plugin Utilities
 * Author:          10up
 * Author URI:      https://10up.com
 * Text Domain:     haymarket-migration
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Haymarket_Migration
 */

// Useful global constants.
define( 'HMM_VERSION', '0.1.0' );
define( 'HMM_PATH', dirname( __FILE__ ) . '/' );
define( 'HMM_INC', HMM_PATH . 'inc/' );
define( 'CLI_INC', HMM_PATH . 'cli-commands/' );
define( 'HMM_PREFIX', '_hmm-mig-' );
define( 'API_V1_URL', 'http://api.haymarketmedia.com/pages/hmicmsapi.aspx' );
define( 'API_V2_URL', 'http://migratewebapi.haymarketmedia.com/api/' );
define( 'API_V3_URL', 'http://migrateapi.haymarketmedia.com/pages/hmicmsapi.aspx' );
define( 'API_START_DATE', '2018-06-01' );

// Cli Commands.
if ( defined( 'WP_CLI' ) && WP_CLI ) {
	require_once CLI_INC . 'articles.php';
	require_once CLI_INC . 'authors.php';
	require_once CLI_INC . 'full.php';
	require_once CLI_INC . 'group-tests.php';
	require_once CLI_INC . 'newsletters.php';
	require_once CLI_INC . 'newsletter-issues.php';
	require_once CLI_INC . 'product-reviews.php';
	require_once CLI_INC . 'reporting.php';
	require_once CLI_INC . 'sections.php';
	require_once CLI_INC . 'slideshows.php';
	require_once CLI_INC . 'sponsors.php';
	require_once CLI_INC . 'webcasts.php';
	require_once CLI_INC . 'fix.php';
	require_once CLI_INC . 'fix-newsletter-issues.php';
	require_once CLI_INC . 'fix-webcasts.php';
	require_once CLI_INC . 'fix-products.php';
	require_once CLI_INC . 'fix-articles.php';
	require_once CLI_INC . 'author-cleanup.php';
	require_once CLI_INC . 'internal-links.php';
	require_once CLI_INC . 'migrate-categories.php';
	require_once CLI_INC . 'fix-article-date.php';
	require_once CLI_INC . 'quizzes.php';
	require_once CLI_INC . 'playlists.php';
	require_once CLI_INC . 'companies.php';
	require_once CLI_INC . 'slideshow-cleanup.php';
	require_once CLI_INC . 'quiz-cleanup.php';
	require_once CLI_INC . 'healthday-cleanup.php';
	require_once CLI_INC . 'fix-dsm-articles.php';
	require_once CLI_INC . 'image-cleanup.php';
}

require_once HMM_INC . 'base.php';
require_once HMM_INC . 'debugging.php';
require_once HMM_INC . 'helpers.php';
require_once HMM_INC . 'images.php';
require_once HMM_INC . 'shortcodes.php';
require_once HMM_INC . 'reporting.php';
require_once HMM_INC . 'users.php';

if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require_once 'vendor/autoload.php';
}

Haymarket\Migration\Debugging\load();

// Debugging.
add_filter( 'is_protected_meta', '__return_false' );
