<?php
/**
 * Various functions related to Users/Coauthors
 *
 * @package Haymarket
 */

namespace Haymarket\Migration\Users;
use function Haymarket\Migration\Helpers\get_user_by_legacy_id;

/**
 * Get coauthor by display name
 *
 * @param string $user
 *
 * @return string|void
 */
function get_coauthor_by_display_name( string $user ) {
	global $coauthors_plus;
	if ( empty( $user ) ) {
		return 'Empty User';
	}

	$user_slug = sanitize_title( $user );
	$user      = $coauthors_plus->guest_authors->get_guest_author_by( 'user_login', $user_slug );

	return $user;

}

/**
 * Create a coauthor given a display name
 *
 * @param string $name
 * @param int    $id
 *
 * @return string|void
 */
function create_coauthor( string $name = '', int $id ) {
	global $coauthors_plus;

	if ( empty( $name ) ) {
		return;
	}

	$first_last = explode( " ", wp_strip_all_tags( $name ), 2 );

	if ( ! is_array( $first_last ) ) {
		return;
	}
	$user_login = sanitize_title( "$name-$id" );

	$args = [
		'user_login'   => $user_login,
		'display_name' => $first_last[0] . ' ' . $first_last[1],
		'first_name'   => $first_last[0],
		'last_name'    => $first_last[1],
	];

	$coauthor_exists = $coauthors_plus->get_coauthor_by( 'user_login', $user_login, true );

	if ( $coauthor_exists ) {
		return;
	}

	$coauthors_plus->guest_authors->create( $args );

	return $args['user_login'];
}

/**
 * Get ID of the main author or default
 *
 * @return int
 */
function get_default_user_id( $authors = array() ) {
	$user_id = 0;
	$user    = get_user_by( 'login', 'haymarket' );

	if ( is_a( $user, '\WP_User' ) ) {
		$user_id = $user->ID;
	}

	if ( ! empty( $authors ) && is_array( $authors ) ) {
		if( ! empty( $authors[0]->AuthorId ) ){
			$user = get_user_by_legacy_id( $authors[0]->AuthorId  );
			if( $user ){
				$user_id = $user;
			}
		}
	}
	return $user_id;
}

