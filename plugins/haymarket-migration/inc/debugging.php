<?php
/**
 * Debugging hooks and functions
 *
 * @package Haymarket
 */

namespace Haymarket\Migration\Debugging;

function load() {
	add_filter( 'wp_footer', __NAMESPACE__ . '\\show_debugging' );
	add_filter( 'debug_bar_panels', __NAMESPACE__ . '\\debug_bar_panel' );
}

function show_debugging( $content ) {
	if ( ! is_single()  ) {
		return;
	}
	if ( isset( $_GET['show_debug'] ) && is_user_logged_in() ) {
		?> <h2>Legacy URL</h2> <?php
		var_dump( get_post_meta( get_the_ID(), HMM_PREFIX . 'url', true ) );
		?> <h2>Legacy Data</h2> <?php
		var_dump( get_post_meta( get_the_ID(), HMM_PREFIX . 'legacy-data', true ) );
		?> <h2>Post Meta</h2> <?php
		var_dump( get_post_meta( get_the_ID() ) );
	}

	return $content;
}


function debug_bar_panel( $panels ) {
	require_once( dirname( __FILE__ ) . '/debug-bar.php' );
	$panels[] = new \Migration_Debug_Bar();
	return $panels;
}