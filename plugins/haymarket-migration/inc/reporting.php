<?php
/**
 * Migration Reporting
 */

namespace Haymarket\Migration\Reporting;
use wp_cli;

/**
 * Generate an import report. Takes an associative array of
 * 'key' => ( array of created content type ids) e.g post_ids, term_ids..
 *
 * @param array  $report Array of report items.
 * @param int    $count Total number of original API entries.
 * @param string $type Name of report section
 *
 * @return void Outputs a WP_CLI ASCII table
 */
function report( array $report, $count, $type = '' ) {
	$imported_count = array_map( function ( $key, $value ) {
		return [
			'key'   => $key,
			'count' => count( $value ),
		];
	}, array_keys( $report ), $report );

	array_unshift( $imported_count, [
		'key'   => 'original-term-count',
		'count' => $count,
	] );

	\wp_cli\Utils\format_items( 'table', $imported_count, array_keys( $imported_count[0] ) );
}

/**
 * Add report results to the existing report. Useful
 * on date-paginated query where we need to repeatedly add
 * results from a callback.
 *
 * @param array $report The original report array- can be empty.
 * @param array $arr    The results to add to the report
 *
 * @return array The modified report.
 */
function add_to_report( array $report, array $arr ) {
	if ( empty( $arr ) ) {
		return $report;
	}
	// loop through each key in the new array.
	foreach ( $arr as $key => $value ) {
		// if the key matches a key exisiting in the current array.
		if ( isset( $report[ $key ] ) ) {
			// add the to the key's array.
			$report[ $key ][] = $value;
		} else {
			// else add the key/value to the original array.
			$report[ $key ] = $value;
		}
	}

	return $report;
}

/**
 * Export a csv of random posts for QA
 *
 * @todo allow for specific content type, currently only does post.
 */
function random_content_types( $content_type = 'user', $legacy_url = 'https://www.mmm-online.com/' ) {
	$report = [];

	$base_url = trailingslashit( $legacy_url );

	switch ( $content_type ) {
		case 'user':
			$query = new \WP_User_Query( [
				'role' => 'editor',
			] );

			$authors = $query->get_results();

			if ( ! empty( $authors ) ) {
				foreach ( $authors as $author ) {
					$user_id       = $author->data->ID;
					$user_info     = get_userdata( $user_id );

					$legacy_id     = get_user_meta( $user_id, HMM_PREFIX . 'id', true );
					$wp_link       = get_author_posts_url( $user_id );
					$wp_admin_link = admin_url( "user-edit.php?user_id=$user_id" );

					$report[]      = [
						'User Name'    => $user_info->last_name . ', ' . $user_info->first_name,
						'HM Front-end' => "=HYPERLINK(\"{$base_url}author/$legacy_id\", \"Front End\")",
						'WP Front-end' => "=HYPERLINK(\"$wp_link\", \"Backend\")",
						'WP Edit Screen' => "=HYPERLINK(\"$wp_admin_link\", \"HM\")",
					];
				}
			}
			break;
		case 'post':
		case 'hm-webcast':
		case 'hm-product-review':
		case 'hm-slideshow':
		case 'hm-newsletter-issue':
		case 'hm-group-test':
		case 'hm-quiz':
		case 'hm-company':
		case 'hm-drug':
		case 'hm-playlist':
			$url_fronts = [
				'hm-slideshow'        => 'slideshow',
				'hm-product-review'   => 'product-review',
				'hm-webcast'          => 'webcast',
				'hm-newsletter-issue' => 'newsletter/item',
				'hm-group-test'       => 'group-test',
				'hm-quiz'             => 'quiz',
				'hm-company'          => 'manufacture',
				'hm-drug'             => 'drug',
				'hm-playlist'         => 'playlist',
			];

			$query = new \WP_Query( [
				'post_type'              => $content_type,
				'orderby'                => 'rand',
				'posts_per_page'         => 50,
				'update_term_meta_cache' => false,
				'update_post_meta_cache' => false,
				'no_found_rows'          => true,
			] );

			if ( $query->have_posts() ) {
				while( $query->have_posts() ) {
					$query->the_post();
					$post_id    = get_the_ID();
					$legacy_id  = get_post_meta( $post_id, HMM_PREFIX . 'id', true );
					$legacy_url = get_post_meta( $post_id, HMM_PREFIX . 'url', true );
					$legacy_url = $legacy_url ? $legacy_url : "$base_url{$url_fronts[$content_type]}/$legacy_id";
					$title      = html_entity_decode( get_the_title() );
					$url        = get_the_permalink();
					$admin_url  = admin_url( "post.php?post=$post_id&action=edit" );
					$report[] = [
						'Name' => esc_html( $title ),
						'HM Front-end' => "=HYPERLINK(\"$legacy_url\", \"Link\" )",
						'WP Front-end' => "=HYPERLINK(\"$url\", \"Link\" )",
						'WP Edit Screen' => "=HYPERLINK(\"$admin_url\",  \"Link\" )",
					];
				}
			}
			break;
		case 'category':
		case 'hm-sponsor':
		case 'hm-newsletter':

			$url_front = [
				'category' => 'section',
				'hm-sponsor' => 'sponsor',
				'hm-newsletter' => 'newsletter',
			];
			// Get all terms
			$terms = get_terms( array(
				'taxonomy'      => $content_type,
				'hide_empty'    => false,
			) );

			shuffle( $terms );
			$random_terms = array_slice( $terms, 0, 25 );

			foreach ( $random_terms as $term ) {
				$title = $term->name;
				$legacy_id = get_term_meta( $term->term_id, HMM_PREFIX . 'id', true );
				$legacy_url = "$base_url{$url_front[$content_type]}/$legacy_id";
				$url = get_term_link( $term->term_id, $content_type );
				$admin_url = admin_url(  "edit-tags.php?taxonomy=$content_type" );
				$report[] = [
					'Term Name'      => html_entity_decode( $title ),
					'HM Front-end'   => "=HYPERLINK(\"$legacy_url\", \"Link\" )",
					'WP Front-end'   => "=HYPERLINK(\"$url\", \"Link\" )",
					'WP Edit Screen' => "=HYPERLINK(\"$admin_url\",  \"Link\" )",
				];
			}
			break;
	} // End switch().

	return $report;
}