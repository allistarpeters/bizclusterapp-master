<?php
/**
 * Helper Functions
 */

namespace Haymarket\Migration\Helpers;

use Haymarket\Migration\Images;
use function Haymarket\Migration\Shortcodes\convert_legacy_shortcodes;

/**
 * Find a term by it's legacy ID term meta
 *
 * @param int    $id
 * @param string $taxonomy
 *
 * @return string|int
 */
function get_term_by_legacy_id( int $id = 0, string $taxonomy = 'category' ) {
	if ( empty( $id ) ) {
		return;
	}

	$terms = get_terms( [
		'taxonomy'   => $taxonomy,
		'hide_empty' => false,
		'meta_query' => array(
			array(
				'key'     => HMM_PREFIX . 'id',
				'value'   => $id,
				'compare' => '=',
			)
		)
	] );

	if ( is_wp_error( $terms ) || empty( $terms ) ) {
		return false;
	}

	return $terms[0]->term_id;
}

function get_user_by_legacy_id( int $id = 0 ) {
	if ( empty( $id ) ) {
		return;
	}

	$user = new \WP_User_Query( [
		'meta_query' => array(
			array(
				'key'     => HMM_PREFIX . 'id',
				'value'   => $id,
				'compare' => '=',
			)
		),
		'fields'     => 'ID',
		'number'     => '1',
	] );

	if ( is_wp_error( $user ) && empty( $user ) ) {
		return;
	}

	$user = $user->get_results();

	return reset( $user );
}

/**
 * Find a post by it's legacy ID term meta
 *
 * @param int    $id
 * @param string $post_type
 *
 * @return int
 */
function get_post_by_legacy_id( int $id = 0, $post_type = 'post' ) {
	if ( empty( $id ) ) {
		return;
	}

	$query = new \WP_Query( [
		'post_type'              => $post_type,
		'posts_per_page'         => 1,
		'no_found_rows'          => true,
		'post_status'            => array( 'publish', 'future' ),
		'update_post_term_cache' => false,
		'fields'                 => 'ids',
		'meta_query'             => [
			[
				'key'     => HMM_PREFIX . 'id',
				'value'   => $id,
				'compare' => '=',
			],
		],
	] );

	if ( $query->have_posts() ) {
		return $query->get_posts()[0];
	}

	return false;

}

/**
 * Sanitize a value by it's data type
 *
 * @param        $value
 * @param string $type
 *
 * @return mixed|string
 */
function sanitize( $value, string $type = '' ) {
	if ( empty( $type ) || empty( $value ) ) {
		return '';
	}
	switch ( $type ) {
		case 'string':
			$sanitized = filter_var( $value, FILTER_SANITIZE_STRING );
			break;
		case 'post_content':
			$sanitized = wp_kses_post( $value );
			break;
		case 'url':
			$sanitized = filter_var( $value, FILTER_SANITIZE_URL );
			break;
		case 'email':
			$sanitized = filter_var( $value, FILTER_SANITIZE_EMAIL );
			break;
		case 'int':
			$sanitized = filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
			break;
		case 'bool':
			$sanitized = ! empty( $value ) ? 'on' : null;
			break;
		case 'json':
			// lowercase array keys to match expected format.
			$encoded   = json_encode( $value );
			$decoded   = json_decode( $encoded );
			$final     = array_map( function ( $item ) {
				$item = (array) $item;
				// rename LinkText key to match expected format.
				rename_array_key( 'LinkText', 'text', $item );

				return array_change_key_case( $item, CASE_LOWER );
			}, $decoded );
			$sanitized = wp_json_encode( $final );
			break;
		case 'rating':
			$sanitized = number_format( $value, 2, '.', '' );
			break;
		case 'date':
			$date_string = filter_var( $value, FILTER_SANITIZE_STRING );
			$sanitized   = date( 'n/j/Y g:i A', strtotime( $date_string ) );
			break;
		case 'date-ymd':
			$date_string = filter_var( $value, FILTER_SANITIZE_STRING );
			$sanitized   = date( 'Y-m-d', strtotime( $date_string ) );
			break;
		case 'date-linux':
			$date_string = filter_var( $value, FILTER_SANITIZE_STRING );
			$sanitized   = date( 'n/j/Y g:i A', $date_string );
			break;
		case 'date-timestamp':
			$date_string = filter_var( $value, FILTER_SANITIZE_STRING );
			$sanitized   = strtotime( $date_string );
			break;
		case 'review-type':
			$value     = sanitize_title( $value );
			$sanitized = '';
			if ( in_array( $value, [ 'product-review', 'first-look' ], true ) ) {
				$sanitized = filter_var( $value, FILTER_SANITIZE_STRING );
			}
			break;
		case 'content-visible':
			$options   = array(
				'WebAndMobileApp' => 1,
				'MobileAppOnly'   => 2,
				'WebOnly'         => 3,
				'None'            => 4
			);
			$sanitized = isset( $options[ $value ] ) ? $options[ $value ] : 1;
			break;
		case 'none':
			$sanitized = $value;
			break;
		default:
			$sanitized = '';
			break;
	}

	return $sanitized;
}


/**
 * Helper function to rename array keys.
 *
 * @return bool Whether the key was updated
 */
function rename_array_key( $oldkey, $newkey, array &$arr ) {
	if ( array_key_exists( $oldkey, $arr ) ) {
		$arr[ $newkey ] = $arr[ $oldkey ];
		unset( $arr[ $oldkey ] );

		return true;
	} else {
		return false;
	}
}

/**
 * Helper function to get attachments from article media objects
 *
 * @param object $media_object Media object from HM API
 * @param int    $post_id      Post ID
 *
 * @return bool Whether or not the media was processed/added.
 */
function get_attachment_from_media_object( $media_object = [], $post_id = 0 ) {
	if ( empty( $media_object ) || empty( $post_id ) ) {
		return false;
	}
	$added = false;
	// convert nested objects to associative array.
	$data = json_decode( json_encode( $media_object ), true );
	// get first element.
	reset( $data );
	// get array key.
	$key = key( $data );

	switch ( key( $data ) ) {
		case 'Document':
		case 'Archive':
			// upload attachment
			$url   = $data[ $key ]['URL'];
			$title = $data[ $key ]['Name'];

			$attach_id = Images\upload_remote_image( $url, 0, [
				'post_title' => esc_html( $title ),
			] );

			$pdf_url = wp_get_attachment_url( $attach_id );

			if ( ! empty( $pdf_url ) ) {
				$prepend_string   = "<a href=\"$pdf_url\" class=\"download-document\">$title</a>";
				$post_content     = get_post_field( 'post_content', $post_id, 'raw' );
				$post_content_new = $prepend_string . $post_content;
				wp_update_post( [
					'ID'           => $post_id,
					'post_content' => $post_content_new,
				] );
				$added = true;
			}
			break;
		case 'Video':
			$embed = $data[ $key ]['EmbeddedCode'] ?? null;
			if ( ! empty( $embed ) ) {
				$added = update_post_meta( $post_id, 'hm-video-embed', $embed ); // not sure how to sanitize this.
			}
			break;
		case 'Audio':
			// @todo need example audio response to for key reference.
			$prepend_string = '<audio src="{mp3-url}" controls="controls"> Your browser does not support the audio element. </audio>';
			break;
	}

	return $added;
}

/**
 * Check to see if the featured image is also included inline in the post content
 *
 * @param $content
 *
 * @return bool
 */
function is_featured_image_inline( $content ) {
	$substr = substr( strip_tags( $content, '<img>' ), 0, 50 );
	if ( false === strpos( $substr, '<img' ) ) {
		return false;
	}

	return true;
}

/**
 * Upload inline assets
 *
 * @param $content
 */
function upload_inline_assets( $post_id, $content ) {
	$images = array();
	preg_match_all( '#<img(.*?)src="(.*?)"(.*?)>#', $content, $images, PREG_SET_ORDER );

	foreach ( $images as $image ) {

		$image_id = Images\upload_remote_image( $image[2], $post_id );

		$src = wp_get_attachment_url( $image_id );

		if ( $src ) {
			$content = str_replace( $image[2], $src, $content );
		} else {
			\WP_CLI::line( "Error: $image[2] not changed in post content." );
		}
	}

	$docs = array();
	preg_match_all( '/<a(.*?)href="([\/].*?.pdf)(.*?)"(.*?)>/', $content, $docs, PREG_SET_ORDER );

	foreach ( $docs as $doc ) {
		$doc_id = Images\upload_remote_image( $doc[2], $post_id );

		$src = wp_get_attachment_url( $doc_id );

		if ( $src ) {
			$content = str_replace( $doc[2], $src, $content );
		} else {
			\WP_CLI::line( "Error: $doc[2] not changed in post content." );
		}
	}

	return $content;
}

/**
 * Prepare post body by converting shortcodes and uploading inline assets
 *
 * @param $post_id
 * @param $content
 *
 * @return mixed
 */
function prepare_post_body( $post_id, $content ) {
	$content = clean_up_embeds( $post_id, $content );
	$content = convert_legacy_shortcodes( $content );
	$content = upload_inline_assets( $post_id, $content );

	$content = str_replace( array( "\n\r", "\n", "\r" ), '', $content );

	//Clean up brightcove embed video tags
	if ( false !== strpos( $content, '//players.brightcove.net' ) && false !== strpos( $content, 'data-account="77368774001"' ) ) {
		$scrips = get_post_meta( $post_id, '_hm-post-scripts', true );
		$scrips .= PHP_EOL;
		$scrips .= '(function(){
	var script = document.createElement(\'script\');
	script.type = \'text/javascript\';
	script.src = \'//players.brightcove.net/77368774001/default_default/index.min.js\';    
	document.getElementsByTagName(\'head\')[0].appendChild(script);
})();';
		update_post_meta( $post_id, '_hm-post-scripts', $scrips );
		$content = str_replace( 'max-width: 650px', 'max-width: 100%', $content );
	}
	$content = wp_kses_post( $content );

	//Content has spaces sometimes that end up in empty P tags
	$content = str_replace( "<p> </p>", '', $content );

	return $content;
}

/**
 * Auto link urls found in content
 *
 * @param $value
 */
function auto_link_urls( $value ) {

	$links = array();

	// Extract existing links and tags
	$value = preg_replace_callback( '~(<a .*?>.*?</a>|<.*?>)~i', function ( $match ) use ( &$links ) {
		return '<' . array_push( $links, $match[1] ) . '>';
	}, $value );

	$value = preg_replace_callback( '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ( $match ) use ( &$links ) {
		$protocol = 'https';
		if ( $match[1] ) {
			$protocol = $match[1];
		}
		$link = $match[2] ?: $match[3];

		return '<' . array_push( $links, "<a href=\"$protocol://$link\">$link</a>" ) . '>';
	}, $value );

	return preg_replace_callback( '/<(\d+)>/', function ( $match ) use ( &$links ) {
		return $links[ $match[1] - 1 ];
	}, $value );

}

/**
 * Move inline styles and scripts to post meta and replace Iframes with the iframe shortcode
 *
 * @param $post_id
 * @param $content
 *
 * @return mixed
 */
function clean_up_embeds( $post_id, $content ) {

	//collect inline js to store in meta
	$all_scripts = array();
	preg_match_all( '/<script[^>]*>(.*?)<\/script>/is', $content, $scripts, PREG_SET_ORDER );
	foreach ( $scripts as $script ) {
		if ( ! empty( $script[1] ) ) {
			$all_scripts[] = $script[1];
			$content       = str_replace( $script[0], '', $content );
		}
	}

	if ( ! empty( $all_scripts ) ) {
		update_post_meta( $post_id, '_hm-post-scripts', implode( PHP_EOL, $all_scripts ) );
	}

	//collect inline css to store in meta
	$all_styles = array();
	preg_match_all( '/<style[^>]*>(.*?)<\/style>/is', $content, $styles, PREG_SET_ORDER );
	foreach ( $styles as $style ) {
		if ( ! empty( $style[1] ) ) {
			$all_styles[] = $style[1];
			$content      = str_replace( $style[0], '', $content );
		}
	}

	if ( ! empty( $all_styles ) ) {
		update_post_meta( $post_id, '_hm-post-styles', implode( PHP_EOL, $all_styles ) );
	}

	//Replace inline Iframe with shortcode
	preg_match_all( '/<iframe(.*?)>[^>]*<\/iframe>/is', $content, $iframes, PREG_SET_ORDER );
	foreach ( $iframes as $iframe ) {
		if ( ! empty( $iframe[1] ) ) {
			$content = str_replace( $iframe[0], '[hm-iframe ' . $iframe[1] . ']', $content );
		}
	}

	return $content;
}