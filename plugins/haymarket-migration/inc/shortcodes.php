<?php
/**
 * Utilities related to data mapping
 */
namespace Haymarket\Migration\Shortcodes;

/**
 * List all shortcodes from legacy CMS
 *
 * @return array
 */
function get_legacy_shortcodes() {
	return [
		'[HMICMS RELATED ARTICLES]',
		'[HMICMS RELATED TOPICS]',
		'[HMICMS RELATED SLIDESHOWS]',
		'[HMICMS RELATED LINKS]',
		'[HMICMS RELATED RESOURCE CENTER]',
		'[HMICMS RELATED NEWS BY THERAPEUTIC CATEGORY]',
		'[HMICMS RELATED THERAPEUTIC CATEGORY]',
		'[HMICMS AD]',
	];
}

/**
 * Replace legacy shortcodes with WP style shortcodes
 *
 * @param string $content
 *
 * @return string
 */
function convert_legacy_shortcodes( $content = '' ) {
	if ( empty( $content ) ) {
		return;
	}
	foreach ( get_legacy_shortcodes() as $shortcode ) {
		$replace = str_replace( ' ', '_', $shortcode );
		$replace = strtolower( $replace );
		$content = str_replace( $shortcode, $replace, $content );
	}

	//Account for pagebreaks
	$content = str_replace( array( '<--pagebreak-->', '&lt;--pagebreak--&gt;' ), '<!--nextpage-->', $content );

	//account for ad placeholders
	$content = str_replace( array( '<--adholder-->', '&lt;--adholder--&gt;' ), '[hmicms-ad]', $content );

	return $content;
}

