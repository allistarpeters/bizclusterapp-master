<?php

class Migration_Debug_Bar extends Debug_Bar_Panel {
	/**
	 * Panel menu title
	 */
	public $title;

	/**
	 * Initial debug bar stuff
	 */
	public function init() {
		$this->title( esc_html__( 'Haymarket Migration', 'debug-bar' ) );
	}

	/**
	 * Show the menu item in Debug Bar.
	 */
	public function prerender() {
		$this->set_visible( true );
	}

	/**
	 * Show the contents of the panel
	 */
	public function render() {
		?>
		<h2>Legacy ID</h2>
		<pre style="display: block;background: white;clear: both;margin: 0;">
			<?php
			if ( is_tax() || isset( $_GET['tag_ID'] ) ) {
				$id = isset( $_GET['tag_ID'] ) ? $_GET['tag_ID'] : get_queried_object_id();
				var_dump( get_term_meta( $id, HMM_PREFIX . 'id', true ) );
			} elseif ( is_singular() || isset( $_GET['post'] ) ) {
				$id = isset( $_GET['post'] ) ? $_GET['post'] : get_the_ID();
				var_dump( get_post_meta( $id, HMM_PREFIX . 'id', true ) );
			} elseif ( is_author() || isset( $_GET['user_id'] ) ) {
				$id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : get_queried_object_id();
				var_dump( get_user_meta( $id, HMM_PREFIX . 'id', true ) );
			}
			?>
		</pre>
		<h2>Legacy URL</h2>
		<pre style="display: block;background: white;clear: both;margin: 0;">
			<?php
			if ( is_tax() || isset( $_GET['tag_ID'] ) ) {
				$id = isset( $_GET['tag_ID'] ) ? $_GET['tag_ID'] : get_queried_object_id();
				var_dump( get_term_meta( $id, HMM_PREFIX . 'url', true ) );
			} elseif ( is_singular() || isset( $_GET['post'] ) ) {
				$id = isset( $_GET['post'] ) ? $_GET['post'] : get_the_ID();
				var_dump( get_post_meta( $id, HMM_PREFIX . 'url', true ) );
			} elseif ( is_author() || isset( $_GET['user_id'] ) ) {
				$id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : get_queried_object_id();
				var_dump( get_user_meta( $id, HMM_PREFIX . 'url', true ) );
			}
			?>
		</pre>

		<h2>Legacy Data</h2>
		<pre style="display: block;background: white;clear: both;margin: 0;">
			<?php
			if ( is_tax() || isset( $_GET['tag_ID'] ) ) {
				$id = isset( $_GET['tag_ID'] ) ? $_GET['tag_ID'] : get_queried_object_id();
				var_dump( get_term_meta( $id, HMM_PREFIX . 'legacy-data', true ) );
			} elseif ( is_singular() || isset( $_GET['post'] ) ) {
				$id = isset( $_GET['post'] ) ? $_GET['post'] : get_the_ID();
				var_dump( get_post_meta( $id, HMM_PREFIX . 'legacy-data', true ) );
			} elseif ( is_author() || isset( $_GET['user_id'] ) ) {
				$id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : get_queried_object_id();
				var_dump( get_user_meta( $id, HMM_PREFIX . 'legacy-data', true ) );
			}
			?>
		</pre>
		<h2>Post Meta</h2>
		<pre style="display: block;background: white;clear: both;margin: 0;">
			<?php
			if ( is_tax() || isset( $_GET['tag_ID'] ) ) {
				$id = isset( $_GET['tag_ID'] ) ? $_GET['tag_ID'] : get_queried_object_id();
				var_dump( get_term_meta( $id ) );
			} elseif ( is_singular() || isset( $_GET['post'] ) ) {
				$id = isset( $_GET['post'] ) ? $_GET['post'] : get_the_ID();
				var_dump( get_post_meta( $id ) );
			} elseif ( is_author() || isset( $_GET['user_id'] ) ) {
				$id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : get_queried_object_id();
				var_dump( get_user_meta( $id ) );
			}
			?>
		</pre>
		<?php
	}
}