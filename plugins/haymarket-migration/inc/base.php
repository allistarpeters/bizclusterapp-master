<?php
/**
 * Paginated query based on day-by-day
 */

namespace Haymarket\Base;

use Haymarket\Migration\Reporting as Reporting;
/**
 * Loops through individual days since given date,
 * passes unix date range to callback function
 *
 * @param string $api API to query
 * @param array $args
 * @param string $callback
 *
 * @throws \Exception
 *
 * @return void
 */
function paginated_query( $api = 'article', array $args = [], $callback = '', $start_date ) {
	if ( empty( $callback ) ) {
		return;
	}

	$api_start_date  = API_START_DATE;
	if ( isset( $start_date ) ) {
		if ( date( 'Y-m-d', strtotime( $start_date ) ) !== $start_date ) {
			\wp_cli::error( esc_html_e( 'Please use Y-m-d date format.', 'haymarket' ) );
		} else {
			$api_start_date = $start_date;
		}
	}

	$args = wp_parse_args(
		$args,
		api_params()[ $api ]
	);

	$per_page = 25;

	$period   = get_days_since( $api_start_date );
	$count    = iterator_count( $period );
	$progress = \WP_CLI\Utils\make_progress_bar( __( 'Importing Articles', 'haymarket' ), $count );

	$i = 0;
	$final_report = [];
	// for every day, query the api and provide data to callback.
	foreach ( $period as $key => $value ) {
		// get array of timestamps for range of day.
		$day_range = get_day_range( $value->format( 'Y-m-d' ) );

		// pass range to callback function.
		$results = query_api( $day_range, $args );

		// provide returned data to callback.
		$report = call_user_func( $callback, $results ) ?? [];

		$final_report = Reporting\add_to_report( $final_report, $report );

		// placeholder for memory clear function.
		if ( 0 === $i % $per_page ) {
			stop_the_insanity();
		}
		$progress->tick();
		$i ++;
	}
	$progress->finish();

	$count = count( $final_report['count'] );
	Reporting\report( $final_report, 0, 'Articles' );
}

/**
 * Get all the days since a given time
 *
 * @param string $since
 *
 * @return \DatePeriod|void
 * @throws \Exception
 */
function get_days_since( $since = '' ) {
	if ( empty( $since ) ) {
		return;
	}

	$period = new \DatePeriod(
		new \DateTime( $since ),
		new \DateInterval( 'P1D' ),
		new \DateTime()
	);

	return $period;
}

/**
 * Get start/end range in unix timestamps of a given day
 *
 * @param string $date Value to be passed to DateTime class
 *
 * @return array Array of start and end unix time values of given date
 */
function get_day_range( $date = '' ) {
	if ( empty( $date ) ) {
		return;
	}

	$datetime = new \DateTime( $date );

	// start of day (0:0:0)
	$start_time      = $datetime->setTime( 0, 0, 0 );
	$start_timestamp = $start_time->getTimeStamp();
	$start_date      = $start_time->format('Y-m-d');

	// end of day (23:59:59)
	$end_time      = $datetime->setTime( 23, 59, 59 );
	$end_timestamp = $end_time->getTimeStamp();

	// end of next day.
	$end_time      = $datetime->add( new \DateInterval('P1D') );
	$end_date      = $end_time->format('Y-m-d');

	return [
		'start_unix' => $start_timestamp,
		'start_date' => $start_date,
		'end_unix'   => $end_timestamp,
		'end_date'   => $end_date,
	];
}

/**
 * Make remote request to API
 *
 * @param array $day_range
 * @param array $args
 *
 * @return array|mixed|void
 */
function query_api( array $day_range = [], array $args = [] ) {
	if ( empty( $day_range ) || empty( $args ) ) {
		return;
	}

	$args['StartDate'] = $day_range['start_unix'];
	$args['EndDate']   = $day_range['end_unix'];

	$request = wp_remote_get( API_V3_URL, [
		'body'    => $args,
		'timeout' => 30,
	] );

	if ( is_wp_error( $request ) ) {
		return $request;
	}

	return json_decode( wp_remote_retrieve_body( $request ) );
}

/**
 * Get default query parameters for Articles
 *
 * @return array
 */
function api_params() {
	return [
		'article' => [
			'publicationid' => 6,
			'ArticleTypeId' => null,
			'SectionId'     => null,
			'Count'         => null,
			'Preview'       => false,
			'IncludeBody'   => true,
			'StartDate'     => null,
			'EndDate'       => null,
		],
	];
}

/**
 * Resets some values to reduce memory footprint.
 * Ripped from Elasticpress
 */
function stop_the_insanity() {
	global $wpdb, $wp_object_cache, $wp_actions, $wp_filter;
	$wpdb->queries = array();
	if ( is_object( $wp_object_cache ) ) {
		$wp_object_cache->group_ops = array();
		$wp_object_cache->stats = array();
		$wp_object_cache->memcache_debug = array();
		// Make sure this is a public property, before trying to clear it
		try {
			$cache_property = new \ReflectionProperty( $wp_object_cache, 'cache' );
			if ( $cache_property->isPublic() ) {
				$wp_object_cache->cache = array();
			}
			unset( $cache_property );
		} catch ( ReflectionException $e ) {
		}
		/**
		 * In the case where we're not using an external object cache, we need to call flush on the default
		 * WordPress object cache class to clear the values from the cache property
		 */
		if ( ! wp_using_ext_object_cache() ) {
			wp_cache_flush();
		}
		if ( is_callable( $wp_object_cache, '__remoteset' ) ) {
			call_user_func( array( $wp_object_cache, '__remoteset' ) ); // important
		}
	}
	// Prevent wp_actions from growing out of control
	$temporary_wp_actions = array();
	$wp_actions = $temporary_wp_actions;
	// WP_Query class adds filter get_term_metadata using its own instance
	// what prevents WP_Query class from being destructed by PHP gc.
	//    if ( $q['update_post_term_cache'] ) {
	//        add_filter( 'get_term_metadata', array( $this, 'lazyload_term_meta' ), 10, 2 );
	//    }
	// It's high memory consuming as WP_Query instance holds all query results inside itself
	// and in theory $wp_filter will not stop growing until Out Of Memory exception occurs.
	if ( isset( $wp_filter['get_term_metadata'] ) ) {
		/*
			* WordPress 4.7 has a new Hook infrastructure, so we need to make sure
			* we're accessing the global array properly
			*/
		if ( class_exists( 'WP_Hook' ) && $wp_filter['get_term_metadata'] instanceof WP_Hook ) {
			$filter_callbacks   = &$wp_filter['get_term_metadata']->callbacks;
		} else {
			$filter_callbacks   = &$wp_filter['get_term_metadata'];
		}
		if ( isset( $filter_callbacks[10] ) ) {
			foreach ( $filter_callbacks[10] as $hook => $content ) {
				if ( preg_match( '#^[0-9a-f]{32}lazyload_term_meta$#', $hook ) ) {
					unset( $filter_callbacks[10][ $hook ] );
				}
			}
		}
	}
}
