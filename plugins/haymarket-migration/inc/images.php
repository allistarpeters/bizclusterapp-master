<?php

namespace Haymarket\Migration\Images;

use Haymarket\Migration\Helpers as Helpers;

require_once( ABSPATH . 'wp-admin/includes/image.php' );

/**
 * Upload a remote image from URL
 *
 * @param string $image_url Url of image.
 * @param int    $parent_id Optional. ID of post to attach to.
 *
 * @return bool|int|\WP_Error
 */
function upload_remote_image( $image_url = '', $parent_id = 0, $args = [] ) {
	if ( empty( $image_url ) ) {
		return;
	}

	// Remove query parameters from URL.
	$image_url = strtok( $image_url, '?' );

	$existing = new \WP_Query( [
		'post_type'              => 'attachment',
		'post_mime_type'         => 'image',
		'post_status'            => 'inherit',
		'meta_query'             => [
			[
				'key' => 'legacy_url',
				'value' => $image_url,
			]
		],
		'fields'                 => 'ids',
		'posts_per_page'         => 1,
		'no_found_rows'          => true,
		'update_post_term_cache' => false,
	] );

	$existing = $existing->get_posts();
	if( is_array( $existing ) && ! empty( $existing ) ) {
		// this image already exists.
		return $existing[0];
	}

	// Download image.
	$get = wp_remote_get( $image_url, array(
		'timeout' => 20,
		'sslverify'   => false,
	) );
	if ( is_wp_error( $get ) ) {
		\wp_cli::warning( sprintf( __( '%s could not be imported for post id:%s', 'haymarket' ), $image_url, $parent_id ) );
	}

	$mime_type = wp_remote_retrieve_header( $get, 'content-type' );
	// Bail if no file.
	if ( ! $mime_type ) {
		return;
	}

	// Upload image to local uploads.
	$uploaded = wp_upload_bits( basename( $image_url ), '', wp_remote_retrieve_body( $get ) );

	if ( empty( $uploaded ) || ! isset( $uploaded['file'] ) ) {
		//Lets try and download it again
		sleep(30);
		$uploaded = wp_upload_bits( basename( $image_url ), '', wp_remote_retrieve_body( $get ) );

		if ( empty( $uploaded ) || ! isset( $uploaded['file'] ) ) {
			\wp_cli::warning( sprintf( __( '%s could not be upload for post id:%s', 'haymarket' ), $image_url, $parent_id ) );

			return;
		}
	}

	$attachment_defaults = [
		'post_title'     => basename( $image_url ),
		'post_mime_type' => esc_html( $mime_type ),
		'post_date'      => '',
		'post_content'   => '',
		'post_excerpt'   => '',
	];
	$attachment          = wp_parse_args( $args, $attachment_defaults );

	// Insert attachment.
	$attach_id = wp_insert_attachment( $attachment, $uploaded['file'], $parent_id );


	// Generate attachment crops, meta...etc.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded['file'] );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	if( ! empty( $attachment['post_excerpt'] ) ){
		update_post_meta( $attach_id, '_wp_attachment_image_alt', sanitize_text_field( $attachment['post_excerpt'] ) );
	}

	// store the original url location of the file for detecting duplicates later.
	$meta = update_post_meta( $attach_id, 'legacy_url', $image_url );
	return $attach_id;

}
