<?php
/**
 * Publishes customizer options.
 *
 * @package genesis-design-pro
 */

namespace DPP\Customizer;

/**
 * Class Publish
 */
class Publish {

	/**
	 * Array of options used to build CSS.
	 *
	 * @var Data
	 */
	public $data = array();

	/**
	 * Preview constructor.
	 */
	public function __construct() {
		$this->data = new Data();

		$this->save_css();
	}

	/**
	 * Add filters, build css, and generate file opts.
	 */
	public function save_css() {
		add_filter( 'gppro_css_builder_data', array( $this, 'gppro_css_builder_data' ) );

		$build  = \GP_Pro_Builder::build_css();
		$build .= $this->data->get_custom();

		\Genesis_Palette_Pro::generate_file( $build );

		remove_filter( 'gppro_css_builder_data', array( $this, 'gppro_css_builder_data' ) );
	}

	/**
	 * Replaces the DPP settings with theme mod.
	 *
	 * @param array $data The original settings.
	 *
	 * @return array
	 */
	public function gppro_css_builder_data( $data ) {
		return empty( $this->data->get_data() ) ? $data : $this->data->get_data();
	}
}
