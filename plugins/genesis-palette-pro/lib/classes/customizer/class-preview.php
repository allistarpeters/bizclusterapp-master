<?php
/**
 * Builds Preview Output
 *
 * @package genesis-design-pro
 */

namespace DPP\Customizer;

/**
 * Builds preview output.
 */
class Preview {

	/**
	 * Array of options used to build preview CSS.
	 *
	 * @var Data
	 */
	public $data = array();

	/**
	 * Preview constructor.
	 */
	public function __construct() {
		add_action( 'wp_head', array( $this, 'preview_styles' ), 99 );
	}

	/**
	 * Do preview output
	 */
	public function preview_styles() {
		$this->data = new Data();

		echo '<style type="text/css" id="dpp-customize-preview-css">';
		$this->do_sections();
		echo '</style>';
	}

	/**
	 * Setup the data for the build_css method.
	 */
	public function do_sections() {
		add_filter( 'gppro_css_builder_data', array( $this, 'gppro_css_builder_data' ) );

		echo \GP_Pro_Builder::build_css(); // WPCS: xss ok.
		echo $this->data->get_custom(); // WPCS: xss ok.

		remove_filter( 'gppro_css_builder_data', array( $this, 'gppro_css_builder_data' ) );
	}

	/**
	 * Replaces the DPP settings with theme mod.
	 *
	 * @param array $data The original settings.
	 *
	 * @return array
	 */
	public function gppro_css_builder_data( $data ) {
		return empty( $this->data->get_data() ) ? $data : $this->data->get_data();
	}
}
