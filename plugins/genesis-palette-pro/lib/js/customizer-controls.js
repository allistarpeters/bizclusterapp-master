/**
 * Handes custom controls for DPP.
 *
 * @package smart-passive-income-pro
 */

( function( $ ) {

	'use strict';

	var DPP_Range = {

		/**
		 * DOM elements used.
		 */
		el: {},

		/**
		 * The current val.
		 */
		val: '',

		/**
		 * Initialize all the things.
		 */
		init: function() {
			DPP_Range.bind_actions();
		},

		/**
		 * Binds the actions.
		 */
		bind_actions: function() {
			jQuery('body').on( 'input', '.customize-control-dpp-range input', DPP_Range.change );
		},

		/**
		 * Changes the range value.
		 */
		change: function() {
			DPP_Range.el.input = $( this );
			DPP_Range.val      = DPP_Range.el.input.val();

			DPP_Range.el.input.next( '.dpp-range-output' ).find( 'span' ).text( DPP_Range.val );
			DPP_Range.el.input.val( val );
		}
	};

	$( document ).ready( function() {
		DPP_Range.init();
	} );

} )( jQuery );
