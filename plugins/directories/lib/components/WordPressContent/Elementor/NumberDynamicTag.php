<?php
namespace SabaiApps\Directories\Component\WordPressContent\Elementor;

use Elementor\Controls_Manager;
use Elementor\Core\DynamicTags\Tag;
use ElementorPro\Modules\DynamicTags\Module;

class NumberDynamicTag extends Tag
{
    use DynamicTagTrait;

    public function get_name()
    {
        return 'drts-number';
    }

    public function get_title()
    {
        return 'Directories' . ' - ' .  __('Numeric Field', '-sabai_plugin_name');
    }

    public function get_categories()
    {
        return [
            Module::NUMBER_CATEGORY,
            Module::POST_META_CATEGORY,
        ];
    }

    public function render()
    {
        if ((!$field_key = $this->get_settings('field'))
            || (!$entity = $this->_getEntity())
            || (!$field = $this->_getDynamicTagField($entity, $field_key))
            || (!$value = $entity->getSingleFieldValue($field->getFieldName()))
            || !is_numeric($value)
        ) return;

        echo wp_kses_post($value);
    }

    protected function _register_controls()
    {
        $this->add_control(
            'field',
            [
                'label' => __('Field name', 'directories'),
                'type' => Controls_Manager::SELECT,
                'groups' => $this->_getDynamicTagFields(['number']),
            ]
        );
    }
}