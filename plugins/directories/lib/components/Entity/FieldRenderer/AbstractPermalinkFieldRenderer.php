<?php
namespace SabaiApps\Directories\Component\Entity\FieldRenderer;

use SabaiApps\Directories\Component\Entity;
use SabaiApps\Directories\Component\Field;

abstract class AbstractPermalinkFieldRenderer extends Field\Renderer\AbstractRenderer
{
    protected $_isDefault = true;

    protected function _fieldRendererInfo()
    {
        return [
            'label' => $this->_isDefault ? null : __('Permalink', 'directories'),
            'field_types' => $this->_getFieldTypes(),
            'default_settings' => [
                'link' => 'post',
                'link_field' => null,
                'link_target' => '_self',
                'link_rel' => null,
                'link_custom_label' => null,
                'max_chars' => 0,
                'show_count' => false,
                'content_bundle_type' => null,
            ],
            'inlineable' => true,
        ];
    }

    /**
     * @return array
     */
    protected function _getFieldTypes()
    {
        return [$this->_name];
    }

    protected function _fieldRendererSettingsForm(Field\IField $field, array $settings, array $parents = [])
    {
        if ($settings['link'] === 'field_post') $settings['link'] = 'field'; // backward compat with 1.2.x
        $form = array(
            'link' => array(
                '#type' => 'select',
                '#title' => __('Link type', 'directories'),
                '#options' => $this->_getTitleLinkTypeOptions(),
                '#default_value' => $settings['link'],
                '#weight' => 1,
            ),
            'link_target' => array(
                '#title' => __('Open link in', 'directories'),
                '#type' => 'select',
                '#options' => $this->_getLinkTargetOptions(),
                '#default_value' => $settings['link_target'],
                '#weight' => 4,
                '#states' => array(
                    'invisible' => array(
                        sprintf('select[name="%s[link]"]', $this->_application->Form_FieldName($parents)) => array('type' => 'value', 'value' => ''), 
                    ),
                ),
            ),
            'link_custom_label' => array(
                '#title' => __('Custom link label', 'directories'),
                '#type' => 'textfield',
                '#default_value' => $settings['link_custom_label'],
                '#weight' => 5,
                '#states' => array(
                    'invisible' => array(
                        sprintf('select[name="%s[link]"]', $this->_application->Form_FieldName($parents)) => array('type' => 'value', 'value' => ''),
                    ),
                ),
            ),
        );
        
        // Allow linking title to another field if any linkable field exists
        $bundle = $this->_application->Entity_Bundle($field->bundle_name);
        if ($bundle_fields = $this->_application->Entity_Field($bundle->name)) {
            $linkable_fields = $this->_application->Filter('entity_linkable_fields', array(
                'url' => '',
            ), array($bundle));
            if (!empty($linkable_fields)) {
                $link_field_options = [];
                foreach ($bundle_fields as $bundle_field_name => $bundle_field) {
                    $bundle_field_type = $bundle_field->getFieldType();
                    if (isset($linkable_fields[$bundle_field_type])) {
                        $link_field_options[$bundle_field_name . ',' . $linkable_fields[$bundle_field_type]] = $bundle_field->getFieldLabel() . ' - ' . $bundle_field_name;
                    }
                }
                if (!empty($link_field_options)) {
                    asort($link_field_options);
                    $form['link']['#options'] += $this->_getTitleLinkTypeExtraOptions();
                    $form['link_field'] = array(
                        '#type' => 'select',
                        '#default_value' => $settings['link_field'],
                        '#options' => $link_field_options,
                        '#weight' => 2,
                        '#required' => function($form) use ($parents) { return in_array($form->getValue(array_merge($parents, array('link'))), ['field']); },
                        '#states' => array(
                            'visible' => array(
                                sprintf('select[name="%s[link]"]', $this->_application->Form_FieldName($parents)) => array('type' => 'one', 'value' => ['field']),
                            ),
                        ),
                    );
                    $form['link_rel'] = array(
                        '#title' => __('Add to "rel" attribute', 'directories'),
                        '#weight' => 3,
                        '#type' => 'checkboxes',
                        '#options' => $this->_getLinkRelAttrOptions(),
                        '#default_value' => $settings['link_rel'],
                        '#states' => array(
                            'visible' => array(
                                sprintf('select[name="%s[link]"]', $this->_application->Form_FieldName($parents)) => array('type' => 'one', 'value' => ['field']),
                            ),
                        ),
                    );
                }
            }
        }
        
        $form['max_chars'] = [
            '#title' => __('Max number of characters', 'directories'),
            '#type' => 'slider',
            '#integer' => true,
            '#min_value' => 0,
            '#max_value' => 500,
            '#min_text' => __('Unlimited', 'directories'),
            '#step' => 10,
            '#weight' => 10,
            '#default_value' => $settings['max_chars'],
        ];
        
        $form += $this->_application->System_Util_iconSettingsForm($bundle, $settings, $parents, 10);
        
        if (!empty($bundle->info['is_taxonomy'])
            && ($taxonomy_content_bundle_types = $this->_application->Entity_TaxonomyContentBundleTypes($bundle->type))
        ) {
            $form['show_count'] = array(
                '#type' => 'checkbox',
                '#title' => __('Show post count', 'directories'),
                '#default_value' => !empty($settings['show_count']),
                '#weight' => 15,
            );
            if (count($taxonomy_content_bundle_types) > 1) {
                $options = [];
                foreach ($taxonomy_content_bundle_types as $content_bundle_type) {
                    $options[$content_bundle_type] = $this->_application->Entity_Bundle($content_bundle_type, $bundle->component, $bundle->group)->getLabel('singular');
                }
                $form['content_bundle_type'] = array(
                    '#type' => 'select',
                    '#options' => $options,
                    '#default_value' => $settings['content_bundle_type'],
                    '#states' => array(
                        'visible' => array(
                            sprintf('input[name="%s[show_count]"]', $this->_application->Form_FieldName($parents)) => array(
                                'type' => 'checked', 
                                'value' => true,
                            ),
                        ),
                    ),
                    '#weight' => 16,
                );
            } else {
                $form['content_bundle_type'] = array(
                    '#type' => 'hidden',
                    '#value' => current($taxonomy_content_bundle_types),
                );
            }
            $form['show_count_label'] = array(
                '#type' => 'checkbox',
                '#title' => __('Show post count with label', 'directories'),
                '#default_value' => !empty($settings['show_count_label']),
                '#weight' => 17,
                '#states' => array(
                    'visible' => array(
                        sprintf('input[name="%s[show_count]"]', $this->_application->Form_FieldName($parents)) => array(
                            'type' => 'checked', 
                            'value' => true,
                        ),
                    ),
                ),
            );
        }
        
        return $form;
    }

    protected function _fieldRendererRenderField(Field\IField $field, array &$settings, Entity\Type\IEntity $entity, array $values, $more = 0)
    {
        if (empty($options['no_link'])) {
            $title = isset($settings['link_custom_label']) && strlen($settings['link_custom_label']) ? $settings['link_custom_label'] : $this->_getTitle($field, $settings, $entity, $values);
        } else {
            $title = $this->_getTitle($field, $settings, $entity, $values);
        }
        // Limit number of chars?
        if (!empty($settings['max_chars'])) {
            $title = $this->_application->Summarize($title, $settings['max_chars']);
        }

        // Init permalink options
        $options = [
            'title' => $title,
            'atts' => ['target' => $settings['link_target']],
        ];
        if (!empty($settings['link'])
            && $this->_application->Entity_IsRoutable($entity->getBundleName(), 'link', $entity)
        ) {
            if ($settings['link'] === 'field'
                || $settings['link'] === 'field_post' // backward compat with 1.2.x
            ) {
                if (!empty($settings['link_field'])
                    && ($link_field = explode(',', $settings['link_field']))
                    && ($url = $entity->getSingleFieldValue($link_field[0], strlen($link_field[1]) ? $link_field[1] : null))
                ) {
                    $options['script_url'] = $url;
                    if (!empty($settings['link_rel'])) {
                        $options['atts']['rel'] = implode(' ', $settings['link_rel']);
                    }
                }
            }
        } else {
            $options['no_link'] = true;
        }
        $options += $this->_application->System_Util_iconSettingsToPermalinkOptions($entity, $settings);

        // Render permalink
        $ret = $this->_application->Entity_Permalink($entity, $options);

        // Add count if any
        if (!empty($settings['show_count'])
            && !empty($settings['content_bundle_type'])
            && ($count = (int)$entity->getSingleFieldValue('entity_term_content_count', '_' . $settings['content_bundle_type']))
        ) {
            if (!empty($settings['show_count_label'])
                && ($bundle = $field->Bundle)
                && ($content_bundle = $this->_application->Entity_Bundle($settings['content_bundle_type'], $bundle->component, $bundle->group))
            ) {
                $count = sprintf(_n($content_bundle->getLabel('count'), $content_bundle->getLabel('count2'), $count), $count);
            } else {
                $count = '(' . $count . ')';
            }
            $ret .= ' <span style="vertical-align:middle">' . $count . '</span>';
        }
        
        return $ret;
    }

    protected function _getTitle(Field\IField $field, array &$settings, Entity\Type\IEntity $entity, array $values)
    {
        return $this->_application->Entity_Title($entity);
    }
    
    protected function _getTitleLinkTypeOptions()
    {
        return [
            'post' => __('Permalink', 'directories'),
            '' => __('Do not link', 'directories'),
        ];
    }
    
    protected function _getTitleLinkTypeExtraOptions()
    {
        return [
            'field' => __('Link to URL of another field', 'directories'),
        ];
    }
    
    protected function _fieldRendererReadableSettings(Field\IField $field, array $settings)
    {
        $bundle = $this->_application->Entity_Bundle($field->bundle_name);
        if ($settings['link'] === 'field'
            || $settings['link'] === 'field_post' // backward compat with 1.2.x
        ) {
            $link_options = $this->_getTitleLinkTypeExtraOptions();
            $link_value = $link_options[$settings['link']] . ' - ' . explode(',', $settings['link_field'])[0];
        } else {
            $link_options = $this->_getTitleLinkTypeOptions();
            $link_value = $link_options[$settings['link']];
        }
        $targets = $this->_getLinkTargetOptions();
        $ret = [
            'link' => [
                'label' => __('Link type', 'directories'),
                'value' => $link_value,
            ],
        ];
        if ($settings['link'] !== '') {
            if ($settings['link'] !== 'post') {
                if (!empty($settings['link_rel'])) {
                    $rels = $this->_getLinkRelAttrOptions();
                    $value = [];
                    foreach ($settings['link_rel'] as $rel) {
                        $value[] = $rels[$rel];
                    }
                    $ret['link_rel'] = [
                        'label' => __('Link "rel" attribute', 'directories'),
                        'value' => implode(', ', $value),
                    ];
                }
            }
            $ret['link_target'] = [
                'label' => __('Open link in', 'directories'),
                'value' => $targets[$settings['link_target']],
            ];
            if (isset($settings['link_custom_label'])
                && strlen($settings['link_custom_label'])
            ) {
                $ret['link_custom_label'] = [
                    'label' => __('Custom link label', 'directories'),
                    'value' => $settings['link_custom_label'],
                ];
            }
        }
        if (isset($settings['icon'])) {
            $ret['icon'] = [
                'label' => __('Show icon', 'directories'),
                'value' => !empty($settings['icon']),
                'is_bool' => true, 
            ];
        }
        if (!empty($bundle->info['is_taxonomy'])) {
            $ret['show_count'] = [
                'label' => __('Show post count', 'directories'),
                'value' => !empty($settings['show_count']),
                'is_bool' => true,
            ];
        }
        return $ret;
    }
}
