<?php
namespace SabaiApps\Directories\Component\Entity\FieldType;

use SabaiApps\Directories\Component\Field;
use SabaiApps\Directories\Component\Field\IField;
use SabaiApps\Directories\Component\Field\Query;
use SabaiApps\Directories\Component\Entity\Type\IEntity;

class AuthorFieldType extends Field\Type\AbstractType implements
    Field\Type\ISchemable,
    Field\Type\IQueryable,
    Field\Type\IHumanReadable,
    IPersonalDataAuthor,
    Field\Type\IConditionable
{
    use Field\Type\QueryableUserTrait;

    protected function _fieldTypeInfo()
    {
        return [
            'label' => __('Author', 'directories'),
            'creatable' => false,
            'icon' => 'fas fa-user',
            'admin_only' => true,
            'entity_types' => ['post'],
        ];
    }

    public function fieldSchemaProperties()
    {
        return array('author');
    }

    public function fieldSchemaRenderProperty(IField $field, $property, IEntity $entity)
    {
        return array(array(
            '@type' => 'Person',
            'name' => $this->_application->Entity_Author($entity)->name,
        ));
    }

    public function fieldHumanReadableText(IField $field, IEntity $entity, $separator = null, $key = null)
    {
        return $this->_application->Entity_Author($entity)->name;
    }

    public function fieldPersonalDataQuery(Query $query, $fieldName, $email, $userId)
    {
        $query->fieldIs($fieldName, $userId);
    }

    public function fieldPersonalDataAnonymize(IField $field, IEntity $entity)
    {
        return 0;
    }

    public function fieldConditionableInfo(IField $field, $isServerSide = false)
    {
        return [
            '' => [
                'compare' => ['value', '!value', 'empty', 'filled'],
                'tip' => __('Enter a single numeric value', 'directories'),
                'example' => 7,
            ],
        ];
    }

    public function fieldConditionableRule(IField $field, $compare, $value = null, $name = '')
    {
        switch ($compare) {
            case 'value':
            case '!value':
                return is_numeric($value) ? ['type' => $compare, 'value' => $value] : null;
            case 'empty':
                return ['type' => 'filled', 'value' => false];
            case 'filled':
                return ['type' => 'empty', 'value' => false];
            default:
                return;
        }
    }

    public function fieldConditionableMatch(IField $field, array $rule, array $values, IEntity $entity)
    {
        $author_id = $entity->getAuthorId();
        switch ($rule['type']) {
            case 'value':
            case '!value':
                foreach ((array)$rule['value'] as $rule_value) {
                    if ($author_id == $rule_value) {
                        if ($rule['type'] === '!value') return false;
                        continue;
                    }
                    // One of rules did not match
                    if ($rule['type'] === 'value') return false;
                }
                // All rules matched or did not match.
                return true;
            case 'empty':
                return empty($author_id) === $rule['value'];
            case 'filled':
                return !empty($author_id) === $rule['value'];
            default:
                return false;
        }
    }
}
