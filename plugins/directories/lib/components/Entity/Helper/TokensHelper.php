<?php
namespace SabaiApps\Directories\Component\Entity\Helper;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\Entity;
use SabaiApps\Directories\Component\Field\Type\IHumanReadable;

class TokensHelper
{
    public function help(Application $application, Entity\Model\Bundle $bundle, $includeFields = false)
    {
        $tokens = [
            '%id%',
            '%title%',
            '%author_id%',
            '%author_name%',
            '%timestamp%',
            '%current_user_id%',
            '%current_user_name%',
            '%current_user_display_name%',
            '%permalink_url%'
        ];
        if ($includeFields) {
            $fields = $application->Entity_Field_options(
                $bundle->name,
                ['interface' => 'Field\Type\IHumanReadable', 'return_disabled' => false, 'exclude_property' => true, 'name_prefix' => 'field_']
            );
            foreach (array_keys($fields) as $field_name) {
                $tokens[] = '%' . $field_name . '%';
            }
        }
        return $application->Filter('entity_tokens', $tokens, [$bundle]);
    }

    public function replace(Application $application, $text, Entity\Type\IEntity $entity, $includeFields = false)
    {
        if (strpos($text, '%') === false) return $text;

        $replacements = [
            '%id%' => $entity->getId(),
            '%title%' => $application->Entity_Title($entity),
            '%author_id%' => $entity->getAuthorId(),
            '%author_name%' => $application->Entity_Author($entity)->username,
            '%timestamp%' => $entity->getTimestamp(),
            '%current_user_id%' => $application->getUser()->id,
            '%current_user_name%' => $application->getUser()->username,
            '%current_user_display_name%' => $application->getUser()->name,
            '%permalink_url%' => $application->Entity_PermalinkUrl($entity),
        ];
        if ($includeFields
            && preg_match_all('#%field_(.+?)%#', $text, $matches)
        ) {
            foreach ($matches[1] as $field_name) {
                $tag = '%field_' . $field_name . '%';
                if (($field = $application->Entity_Field($entity, $field_name))
                    && ($field_type = $application->Field_Type($field->getFieldType(), true))
                    && $field_type instanceof IHumanReadable
                ) {
                    $replacements[$tag] = $field_type->fieldHumanReadableText($field, $entity);
                } else {
                    $replacements[$tag] = '';
                }
            }
        }

        return strtr($text, $application->Filter('entity_tokens_replace', $replacements, [$entity]));
    }
}
