<?php
namespace SabaiApps\Directories\Component\Display\Helper;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\Display;
use SabaiApps\Directories\Exception;

class ElementFormHelper
{
    public function help(Application $application, Display\Model\Display $display, $element, $action, array $submittedValues = null)
    {
        if ($element instanceof Display\Model\Element) {
            $element_name = $element->name;
            $element_id = $element->id;
            $element_data = (array)$element->data;
            $is_edit = true;
            unset($element);
        } else {
            $element_name = $element;
            $element_data = [];
            $is_edit = false;
        }

        // Get bundle and element
        if (!$bundle = $application->Entity_Bundle($display->bundle_name)) {
            throw new Exception\RuntimeException('Invalid bundle: ' . $display->bundle_name);
        }
        if (!$element = $application->Display_Elements_impl($bundle, $element_name, true, false)) {
            throw new Exception\RuntimeException('Invalid display element: ' . $element_name);
        }

        if (!$element->displayElementSupports($bundle, $display)) {
            throw new Exception\RuntimeException('The element is not supported: ' . $element_name);
        }

        // Define form
        $form = array(
            '#header' => [],
            '#action' => $application->Url($action),
            '#token_reuseable' => true,
            '#enable_storage' => true,
            '#bundle' => $bundle,
            '#element_name' => $element_name,
            '#inherits' => array(
                'display_admin_add_element_' . strtolower($element_name),
            ),
            '#element_validate' => [
                [__CLASS__, '_filterSettings'],
            ],
            '#tab_style' => 'pill',
            '#tabs' => [
                'general' => array(
                    '#active' => true,
                    '#title' => _x('General', 'settings tab', 'directories'),
                    '#weight' => 1,
                ),
            ],
            'general' => array(
                '#tree' => true,
                '#tab' => 'general',
                '#weight' => 1,
            ),
            'display_id' => array(
                '#type' => 'hidden',
                '#value' => $display->id,
            ),
        );
        if (isset($element_id)) {
            $form['element_id'] = [
                '#type' => 'hidden',
                '#value' => $element_id,
            ];
        } else {
            $form['element'] = [
                '#type' => 'hidden',
                '#value' => $element_name,
            ];
            $form['parent_id'] = [
                '#type' => 'hidden',
                '#id' => 'drts-display-add-element-parent',
                '#value' => null,
            ];
        }

        $tab_weight = 5;
        $info = $element->displayElementInfo($bundle);
        $settings = isset($element_data['settings']) ? $element_data['settings'] : [];
        $settings += (array)$info['default_settings'];
        if ($settings_form = (array)@$element->displayElementSettingsForm($bundle, $settings, $display, ['general', 'settings'], null, $is_edit, isset($submittedValues['settings']) ? $submittedValues['settings'] : [])) {
            if (isset($settings_form['#tabs'])) {
                $form['settings'] = array(
                    '#tree' => true,
                    '#tree_allow_override' => false,
                    '#weight' => 2,
                );
                foreach ($settings_form['#tabs'] as $tab_name => $tab_info) {
                    if ($_settings_form = (array)@$element->displayElementSettingsForm($bundle, $settings, $display, ['settings', $tab_name], $tab_name, $is_edit, isset($submittedValues['settings'][$tab_name]) ? $submittedValues['settings'][$tab_name] : [])) {
                        $_tab_name = 'settings-' . $tab_name;
                        $form['settings'][$tab_name] = [
                            '#tab' => $_tab_name,
                        ] + $_settings_form;
                        if (is_string($tab_info)) {
                            $tab_info = [
                                '#title' => $tab_info,
                                '#weight' => $tab_weight += 5,
                            ];
                        }
                        if (isset($form['#tabs'][$_tab_name])) {
                            $form['#tabs'][$_tab_name] += $tab_info;
                            $form['#tabs'][$_tab_name]['#disabled'] = false;
                        } else {
                            $form['#tabs'][$_tab_name] = $tab_info;
                        }
                    }
                }
                unset($settings_form['#tabs']);
            }
            if (isset($settings_form['#header'])) {
                $form['#header'] += (array)$settings_form['#header'];
                unset($settings_form['#header']);
            }
            $form['general']['settings'] = [
                '#tree' => true,
                '#tree_allow_override' => false,
                '#type' => 'fieldset',
                '#weight' => 10,
            ];
            $form['general']['settings'] += $settings_form;
        } else {
            $form['#tabs']['general']['#disabled'] = true;
        }

        // Heading settings
        if (!isset($info['headingable'])
            || false !== $info['headingable']
        ) {
            $form['heading'] = $application->Display_ElementLabelSettingsForm(
                isset($element_data['heading']) ? $element_data['heading'] : (isset($info['headingable']) && is_array($info['headingable']) ? $info['headingable'] : []),
                ['heading'],
                false
            );
        }

        // Visibility settings
        if (!empty($bundle->info['parent'])) {
            $form['visibility']['hide_on_parent'] = [
                '#title' => __('Hide on parent content page', 'directories'),
                '#type' => 'checkbox',
                '#default_value' => !empty($element_data['visibility']['hide_on_parent']),
                '#weight' => 50,
            ];
        }
        if ($display->type === 'entity') {
            if ($display->name === 'detailed'
                && (!isset($info['designable'])
                    || false !== $info['designable']
                )
            ) {
                $form['visibility']['globalize'] = [
                    '#type' => 'checkbox',
                    '#title' => __('Add rendered content to global scope', 'directories'),
                    '#default_value' => !empty($element_data['visibility']['globalize']),
                    '#weight' => 99,
                ];
                $form['visibility']['globalize_remove'] = [
                    '#type' => 'checkbox',
                    '#title' => __('Remove rendered content from display', 'directories'),
                    '#default_value' => !empty($element_data['visibility']['globalize_remove']),
                    '#weight' => 99,
                    '#states' => [
                        'visible' => [
                            'input[name="visibility[globalize]"]' => ['type' => 'checked', 'value' => true],
                        ],
                    ],
                ];
            }
        } elseif ($display->type === 'form') {
            $form['visibility']['admin_only'] = [
                '#type' => 'checkbox',
                '#title' => __('Visible in backend only', 'directories'),
                '#default_value' => !empty($element_data['visibility']['admin_only']),
                '#weight' => 40,
            ];
        }

        // Advanced settings
        if (!isset($info['designable'])
            || false !== $info['designable']
        ) {
            $form['advanced']['css_class'] = [
                '#title' => __('CSS class', 'directories'),
                '#type' => 'textfield',
                '#default_value' => isset($element_data['advanced']['css_class']) ? $element_data['advanced']['css_class'] : null,
            ];
            $form['advanced']['css_id'] = [
                '#title' => __('CSS ID', 'directories'),
                '#type' => 'textfield',
                '#default_value' => isset($element_data['advanced']['css_id']) ? $element_data['advanced']['css_id'] : null,
                '#description' => $display->type === 'entity' ? sprintf(
                    __('Available tokens: %s', 'directories'),
                    '%id%'
                ) : null,
            ];
            if (isset($info['designable'])
                && is_array($info['designable'])
            ) {
                if (in_array('margin', $info['designable'])
                    || in_array('padding', $info['designable'])
                ) {
                    $sizes = [
                        0 => __('None', 'sabai_plugin_name'),
                        1 => __('X-Small', 'sabai_plugin_name') . ' (+)',
                        2 => __('Small', 'sabai_plugin_name') . ' (+)',
                        3 => __('Medium', 'sabai_plugin_name') . ' (+)',
                        4 => __('Large', 'sabai_plugin_name') . ' (+)',
                        5 => __('X-Large', 'sabai_plugin_name') . ' (+)',
                        -1 => __('X-Small', 'sabai_plugin_name') . ' (-)',
                        -2 => __('Small', 'sabai_plugin_name') . ' (-)',
                        -3 => __('Medium', 'sabai_plugin_name') . ' (-)',
                        -4 => __('Large', 'sabai_plugin_name') . ' (-)',
                        -5 => __('X-Large', 'sabai_plugin_name') . ' (-)',
                    ];
                    foreach ([
                        'margin' => [
                            0 => _x('Margin', 'directories'),
                            'add' => __('Add margin', 'directories'),
                        ],
                        'padding' => [
                            0 => _x('Padding', 'directories'),
                            'add' => __('Add padding', 'directories'),
                        ],
                    ] as $css_prop => $css_prop_labels) {
                        if (in_array($css_prop, $info['designable'])) {
                            $form['advanced'][$css_prop . '_enable'] = [
                                '#title' => $css_prop_labels['add'],
                                '#type' => 'checkbox',
                                '#default_value' => !empty($element_data['advanced'][$css_prop . '_enable'])
                            ];
                            foreach ([
                                'top' => __('Top', 'directories'),
                                'right' => __('Right', 'directories'),
                                'bottom' => __('Bottom', 'directories'),
                                'left' => __('Left', 'directories'),
                            ] as $css_pos => $css_pos_label) {
                                $key = $css_prop . '_' . $css_pos;
                                $form['advanced'][$key] = [
                                    '#title' => $css_prop_labels[0] . ' - ' . $css_pos_label,
                                    '#type' => 'select',
                                    '#options' => $sizes,
                                    '#default_value' => isset($element_data['advanced'][$key]) ? $element_data['advanced'][$key] : null,
                                    '#states' => [
                                        'visible' => [
                                            'input[name="advanced[' . $css_prop . '_enable]"]' => ['type' => 'checked', 'value' => true],
                                        ],
                                    ],
                                ];
                            }
                        }
                    }
                }
                if (in_array('font', $info['designable'])) {
                    $form['advanced'] += $this->fontSettingsForm($application, isset($element_data['advanced']) ? $element_data['advanced'] : [], ['advanced']);
                }
            }

        }
        if ($display->type === 'entity'
            && !empty($info['cacheable'])
        ) {
            $form['advanced']['cache'] = $application->System_Util_cacheSettingsForm(
                isset($element_data['advanced']['cache']) ? $element_data['advanced']['cache'] : null,
                is_array($info['cacheable']) ? $info['cacheable'] : null
            );
        }

        // Let other components modify settings
        $form = $application->Filter('display_element_settings_form', $form, [$bundle, $display, $element, $element_data]);

        // Add tabs if settings available
        $weight = 30;
        foreach ([
            'heading' => _x('Heading', 'settings tab', 'directories'),
            'visibility' => _x('Visibility', 'settings tab', 'directories'),
            'advanced' => _x('Advanced', 'settings tab', 'directories'),
        ] as $key => $label) {
            $weight += 10;
            if (!empty($form[$key])) {
                $form[$key] += [
                    '#tab' => $key,
                    '#tree' => true,
                    '#tree_allow_override' => false,
                    '#horizontal_children' => true,
                    '#weight' => ++$weight,
                ];
                $form['#tabs'][$key] = [
                    '#title' => $label,
                    '#weight' => ++$tab_weight,
                ];
            }
        }

        return $form;
    }

    public static function filterSettings(array &$settings)
    {
        if (empty($settings['visibility']['author_only'])) {
            unset($settings['visibility']['author_only']);
        }
        if (empty($settings['visibility']['hide_on_parent'])) {
            unset($settings['visibility']['hide_on_parent']);
        }
        if (empty($settings['visibility'])) $settings['visibility'] = null;

        if (empty($settings['advanced']['css_class'])) {
            unset($settings['advanced']['css_class']);
        }
        if (empty($settings['advanced']['css_id'])) {
            unset($settings['advanced']['css_id']);
        }
        if (empty($settings['advanced'])) $settings['advanced'] = null;
    }

    public static function _filterSettings(Form\Form $form, array &$settings)
    {
        self::filterSettings($settings);
    }

    protected function _getResponsiveWidthOptions()
    {
        return ['xs' => '<= 320px', 'sm' => '> 320px', 'md' => '> 480px', 'lg' => '> 720px', 'xl' => '> 960px'];
    }

    protected function _getCssSizeOptions()
    {
        return [
            1 => __('X-Small', 'sabai_plugin_name'),
            2 => __('Small', 'sabai_plugin_name'),
            3 => __('Medium', 'sabai_plugin_name'),
            4 => __('Large', 'sabai_plugin_name'),
            5 => __('X-Large', 'sabai_plugin_name'),
        ];
    }

    public function fontSettingsForm(Application $application, array $settings, array $parents = [], $horizontal = true, $prefix = '')
    {
        return [
            'font_size' => [
                '#title' => $prefix . __('Font size', 'directories'),
                '#type' => 'select',
                '#default_value' => isset($settings['font_size']) ? $settings['font_size'] : '',
                '#options' => [
                    '' => __('Default', 'directories'),
                    'rel' => __('Relative size', 'directories'),
                    'px' => __('Absolute size', 'directories'),
                ],
                '#horizontal' => $horizontal,
            ],
            'font_size_rel' => [
                '#type' => 'slider',
                '#default_value' => isset($settings['font_size_rel']) ? $settings['font_size_rel'] : 1,
                '#min_value' => 0.1,
                '#max_value' => 3,
                '#step' => 0.1,
                '#field_prefix' => 'x',
                '#states' => [
                    'visible' => [
                        $font_size_selector = sprintf('select[name="%s"]', $application->Form_FieldName(array_merge($parents, ['font_size']))) => ['value' => 'rel'],
                    ],
                ],
                '#horizontal' => $horizontal,
            ],
            'font_size_abs' => [
                '#type' => 'slider',
                '#default_value' => isset($settings['font_size_abs']) ? $settings['font_size_abs'] : 16,
                '#min_value' => 1,
                '#max_value' => 50,
                '#step' => 1,
                '#field_suffix' => 'px',
                '#states' => [
                    'visible' => [
                        $font_size_selector => ['value' => 'px'],
                    ],
                ],
                '#horizontal' => $horizontal,
            ],
            'font_weight' => [
                '#title' => $prefix . __('Font weight', 'directories'),
                '#type' => 'select',
                '#default_value' => isset($settings['font_weight']) ? $settings['font_weight'] : '',
                '#options' => [
                    'light' => __('Light', 'directories'),
                    '' => __('Default', 'directories'),
                    'bold' => __('Bold', 'directories'),
                ],
                '#horizontal' => $horizontal,
            ],
            'font_style' => [
                '#title' => $prefix . __('Font style', 'directories'),
                '#type' => 'select',
                '#default_value' => isset($settings['font_style']) ? $settings['font_style'] : '',
                '#options' => [
                    '' => __('Default', 'directories'),
                    'italic' => __('Italic', 'directories'),
                ],
                '#horizontal' => $horizontal,
            ],
        ];
    }
}
