<?php
namespace SabaiApps\Directories\Component\Display\Button;

use SabaiApps\Directories\Component\Entity;

class CustomButton extends AbstractButton
{    
    protected function _displayButtonInfo(Entity\Model\Bundle $bundle)
    {
        $info = array(
            'label' => __('Custom buttons', 'directories'),
            'default_settings' => array(
                '_label' => '',
                '_color' => 'secondary',
                '_icon' => '',
                'link_type' => 'current',
                'url' => null,
                'path' => null,
                'fragment' => null,
                'target' => '_self',
                'rel' => null,
            ),
            'multiple' => [],
            'weight' => 50,
        );
        foreach ($this->_application->Filter('entity_button_custom_button_num', range(1, 3), array($bundle)) as $num) {
            $info['multiple'][$num] = array(
                'default_checked' => $num === 1,
                'label' => sprintf(__('Custom button #%d', 'directories'), $num)
            );
        }
        
        return $info;
    }
    
    public function displayButtonSettingsForm(Entity\Model\Bundle $bundle, array $settings, array $parents = [])
    {
        $ret = array(
            'link_type' => array(
                '#type' => 'select',
                '#options' => array(
                    'current' => __('Link to current content URL', 'directories'),
                    'url' => __('Link to external URL', 'directories'),
                ),
                '#title' => __('Button link type', 'directories'),
                '#horizontal' => true,
                '#default_value' => $settings['link_type'],
            ),
            'path' => array(
                '#title' => __('Extra URL path', 'directories'),
                '#type' => 'textfield',
                '#field_prefix' => '/',
                '#default_value' => $settings['path'],
                '#horizontal' => true,
                '#states' => array(
                    'invisible' => array(
                        sprintf('select[name="%s[link_type]"]', $this->_application->Form_FieldName($parents)) => array('value' => 'url')
                    ),
                ),
            ),
            'fragment' => array(
                '#title' => __('URL fragment identifier', 'directories'),
                '#description' => __('Add a fragment identifier to the link URL in order to link to a specific section of the page.', 'directories'),
                '#type' => 'textfield',
                '#field_prefix' => '#',
                '#default_value' => $settings['fragment'],
                '#horizontal' => true,
                '#states' => array(
                    'invisible' => array(
                        sprintf('select[name="%s[link_type]"]', $this->_application->Form_FieldName($parents)) => array('value' => 'url')
                    ),
                ),
            ),
            'url' => array(
                '#type' => 'url',
                '#title' => __('External URL', 'directories'),
                '#placeholder' => 'http://',
                '#default_value' => $settings['url'],
                '#horizontal' => true,
                '#states' => [
                    'visible' => [
                        sprintf('select[name="%s[link_type]"]', $this->_application->Form_FieldName($parents)) => ['value' => 'url'],
                    ],
                ],
            ),
            'target' => [
                '#title' => __('Open link in', 'directories'),
                '#type' => 'select',
                '#options' => $this->_getLinkTargetOptions(),
                '#default_value' => $settings['target'],
                '#horizontal' => true,
                '#states' => [
                    'visible' => [
                        sprintf('select[name="%s[link_type]"]', $this->_application->Form_FieldName($parents)) => ['value' => 'url'],
                    ],
                ],
            ],
            'rel' => [
                '#title' => __('Link "rel" attribute', 'directories'),
                '#type' => 'checkboxes',
                '#options' => $this->_getLinkRelAttrOptions(),
                '#default_value' => $settings['rel'],
                '#horizontal' => true,
                '#states' => [
                    'visible' => [
                        sprintf('select[name="%s[link_type]"]', $this->_application->Form_FieldName($parents)) => ['value' => 'url'],
                    ],
                ],
            ],
        );
        if (!empty($bundle->info['parent'])) {
            $ret['link_type']['#options']['parent'] = __('Link to parent page', 'directories'); 
        }
        
        return $ret;
    }

    protected function _getLinkTargetOptions()
    {
        return [
            '_self' => __('Current window', 'directories'),
            '_blank' => __('New window', 'directories'),
        ];
    }

    protected function _getLinkRelAttrOptions()
    {
        return [
            'nofollow' => __('Add "nofollow"', 'directories'),
            'external' => __('Add "external"', 'directories'),
            'ugc' => __('Add "ugc"', 'directories'),
            'sponsored' => __('Add "sponsored"', 'directories'),
        ];
    }
    
    public function displayButtonLink(Entity\Model\Bundle $bundle, Entity\Type\IEntity $entity, array $settings, $displayName)
    {
        switch ($settings['link_type']) {
            case 'current':
                if (empty($settings['path'])) {
                    $url = $this->_application->Entity_PermalinkUrl($entity, $settings['fragment']);
                } else {
                    $url = $this->_application->Entity_Url($entity, $settings['path'], [], $settings['fragment']);
                }
                break;
            case 'parent':
                if (!$parent = $this->_application->Entity_ParentEntity($entity)) return;

                if (empty($settings['path'])) {
                    $url = $this->_application->Entity_PermalinkUrl($parent, $settings['fragment']);
                } else {
                    $url = $this->_application->Entity_Url($parent, $settings['path'], [], $settings['fragment']);
                }
                break;
            case 'url':
                $url = $settings['url'];
                break;
            default:
                return;
        }
        $attr = [
            'class' => $settings['_class'],
            'style' => $settings['_style'],
        ];
        if (!empty($settings['target'])) {
            $attr['target'] = $settings['target'];
        }
        if (!empty($settings['rel'])) {
            $attr['rel'] = implode(' ', $settings['rel']);
        }

        return $this->_application->LinkTo($settings['_label'], ['script_url' => (string)$url], ['icon' => $settings['_icon']], $attr);
    }
}
