<?php
namespace SabaiApps\Directories\Component\Field\Filter;

use SabaiApps\Directories\Component\Field\IField;
use SabaiApps\Directories\Component\Field\Query;
use SabaiApps\Directories\Component\Entity;

class RangeFilter extends AbstractFilter implements IConditionable
{
    protected $_maxSuffix = null;

    protected function _fieldFilterInfo()
    {
        return array(
            'label' => __('Slider input field', 'directories'),
            'field_types' => array('number', 'range'),
            'default_settings' => array(
                'ignore_min_max' => true,
            ),
        );
    }

    public function fieldFilterSettingsForm(IField $field, array $settings, array $parents = [])
    {
        return [
            'step' => [
                '#type' => 'number',
                '#title' => __('Slider step', 'directories'),
                '#default_value' => isset($settings['step']) ? $settings['step'] : $this->_getDefaultStep($field),
                '#size' => 5,
                '#numeric' => true,
                '#element_validate' => [
                    [[$this, 'validateStep'], [$field, $settings]],
                ],
                '#min_value' => 0,
            ],
            'ignore_min_max' => [
                '#type' => 'checkbox',
                '#title' => __('Do not filter if min/max values are selected', 'directories'),
                '#default_value' => !empty($settings['ignore_min_max']),
            ],
        ];
    }
    
    public function validateStep($form, &$value, $element, $field, $settings)
    {
        if (empty($value)) return;
        
        $min = $this->_getDefaultMin($field, $settings);
        $max = $this->_getDefaultMax($field, $settings);
        
        $range = $max - $min;
        $i = $range / $value;
        if ($i <= 0
            || $range - (floor($i) * $value) > 0
        ) {
            $form->setError(sprintf(__('The full specified value range of the slider (%s - %s) should be evenly divisible by the step', 'directories'), $min, $max), $element);
        }
    }

    protected function _getDefaultMin(IField $field, array $settings)
    {
        $field_settings = $field->getFieldSettings();
        return isset($field_settings['min']) ? $field_settings['min'] : 0;
    }

    protected function _getDefaultMax(IField $field, array $settings)
    {
        $field_settings = $field->getFieldSettings();
        return isset($field_settings['max']) ? $field_settings['max'] : 100;
    }
    
    protected function _getDefaultStep(IField $field)
    {
        $settings = $field->getFieldSettings();
        return empty($settings['decimals']) ? 1 : ($settings['decimals'] == 1 ? 0.1 : 0.01);
    }
    
    public function fieldFilterForm(IField $field, $filterName, array $settings, $request = null, Entity\Type\Query $query = null, array $current = null, $autoSubmit = true, array $parents = [])
    {
        $field_settings = $field->getFieldSettings();        
        return array(
            '#type' => 'range',
            '#min_value' =>  $this->_getDefaultMin($field, $settings),
            '#max_value' => $this->_getDefaultMax($field, $settings),
            '#numeric' => true,
            '#field_prefix' => isset($field_settings['prefix']) && strlen($field_settings['prefix'])
                ? $this->_application->getPlatform()->translateString($field_settings['prefix'], $field->getFieldName() . '_field_prefix', 'entity_field')
                : null,
            '#field_suffix' => isset($field_settings['suffix']) && strlen($field_settings['suffix'])
                ? $this->_application->getPlatform()->translateString($field_settings['suffix'], $field->getFieldName() . '_field_suffix', 'entity_field')
                : null,
            '#step' => !empty($settings['step']) ? $settings['step'] : $this->_getDefaultStep($field),
            '#slider_max_postfix' => $this->_maxSuffix,
            '#entity_filter_form_type' => 'slider',
        );
    }
    
    public function fieldFilterIsFilterable(IField $field, array $settings, &$value, array $requests = null)
    {
        if (!strlen($value)
            || (!$_value = explode(';', $value))
        ) return false;
        
        $_value[0] = (string)@$_value[0];
        $_value[1] = (string)@$_value[1];
        
        if (!empty($settings['ignore_min_max'])) {
            if (strlen($_value[0])
                && strlen($_value[1])
            ) {
                $min = $this->_getDefaultMin($field, $settings);
                $max = $this->_getDefaultMax($field, $settings);
                if ($_value[0] == $min
                    && $_value[1] == $max
                ) {
                    return false;
                }
                return true;
            }
        }
        
        return strlen($_value[0]) || strlen($_value[1]);
    }
    
    public function fieldFilterDoFilter(Query $query, IField $field, array $settings, $value, array &$sorts)
    {
        if (!isset($value['min'])) {
            $value['min'] = $this->_getDefaultMin($field, $settings);
        }
        if (!isset($value['max'])) {
            $value['max'] = $this->_getDefaultMax($field, $settings);
        }
        $this->_fieldFilterDoFilter($query, $field, $settings, $value['min'], $value['max'], $sorts);
    }

    protected function _fieldFilterDoFilter(Query $query, IField $field, array $settings, $min, $max, array &$sorts)
    {
        if ($field->getFieldType() === 'number') {
            $query->fieldIsOrGreaterThan($field, $min)
                ->fieldIsOrSmallerThan($field, $max);
        } else {
            $query->fieldIsOrGreaterThan($field, $min, 'min')
                ->fieldIsOrSmallerThan($field, $max, 'max');
        }
    }

    protected function _getDefaultLabel($defaultLabel, array $settings)
    {
        return $defaultLabel;
    }

    public function fieldFilterLabels(IField $field, array $settings, $value, $form, $defaultLabel)
    {
        $field_settings = $field->getFieldSettings();
        $prefix = isset($field_settings['prefix']) && strlen($field_settings['prefix'])
            ? $this->_application->getPlatform()->translateString($field_settings['prefix'], $field->getFieldName() . '_field_prefix', 'entity_field')
            : '';
        $suffix = isset($field_settings['suffix']) && strlen($field_settings['suffix'])
            ? $this->_application->getPlatform()->translateString($field_settings['suffix'], $field->getFieldName() . '_field_suffix', 'entity_field')
            : '';
        if (!isset($value['min'])) $value['min'] = $this->_getDefaultMin($field, $settings);
        if (!isset($value['max'])) $value['max'] = $this->_getDefaultMax($field, $settings);
        $label = $this->_getDefaultLabel($defaultLabel, $settings);

        return ['' => $this->_application->H($label . ': ' . $prefix . $value['min'] . $suffix . ' - ' . $prefix . $value['max'] . $suffix)];
    }

    public function fieldFilterConditionableInfo(IField $field)
    {
        return [
            '' => [
                'compare' => ['<value', '>value', '<>value'],
                'tip' => __('Enter a single numeric value or two numeric values separated with a comma', 'directories'),
                'example' => 3,
            ],
        ];
    }

    public function fieldFilterConditionableRule(IField $field, $filterName, array $settings, $compare, $value = null, $name = '')
    {
        switch ($compare) {
            case '<value':
            case '>value':
                return is_numeric($value) ? ['type' => $compare, 'value' => $value] : null;
            case '<>value':
                if (!strpos($value, ',')
                    || (!$value= explode(',', $value))
                    || !is_numeric($value[0])
                    || !is_numeric($value[1])
                ) return;

                return ['type' => $compare, 'value' => $value[0] . ',' . $value[1]];
            default:
        }
    }
}
