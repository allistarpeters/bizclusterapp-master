<?php
namespace SabaiApps\Directories\Component\Field\Widget;

use SabaiApps\Directories\Component\Entity;
use SabaiApps\Directories\Component\Field\IField;

class RadioButtonsWidget extends AbstractWidget
{
    protected function _fieldWidgetInfo()
    {
        return array(
            'label' => __('Radio buttons', 'directories'),
            'field_types' => array('choice'),
            'default_settings' => array(
                'columns' => 3,
                'sort' => false,
            ),
        );
    }
    
    public function fieldWidgetSettingsForm($fieldType, Entity\Model\Bundle $bundle, array $settings, array $parents = [], array $rootParents = [])
    {
        return array(
            'columns'  => array(
                '#type' => 'select',
                '#title' => __('Number of columns', 'directories'),
                '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 6 => 6, 12 => 12),
                '#default_value' => $settings['columns'],
            ),
            'sort' => [
                '#title' => __('Sort by label', 'directories'),
                '#type' => 'checkbox',
                '#default_value' => !empty($settings['sort']),
            ],
        );
    }

    public function fieldWidgetForm(IField $field, array $settings, $value = null, Entity\Type\IEntity $entity = null, array $parents = [], $language = null)
    {
        $options = $this->_application->Field_ChoiceOptions($field, !empty($settings['sort']), $language);
        $default_value = isset($value) ? $value : (empty($options['default']) ? null : array_shift($options['default'])); 
        return array(
            '#type' => 'radios',
            '#options' => $options['options'],
            '#default_value' => $default_value,
            '#columns' => $settings['columns'],
        );
    }
}