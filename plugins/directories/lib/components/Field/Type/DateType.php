<?php
namespace SabaiApps\Directories\Component\Field\Type;

use SabaiApps\Directories\Component\Entity\Type\IEntity;
use SabaiApps\Directories\Component\Entity\Model\Bundle;
use SabaiApps\Directories\Component\Field\IField;
use SabaiApps\Directories\Component\Field\Query;
use SabaiApps\Directories\Application;

class DateType extends AbstractType
    implements ISortable, IQueryable, IOpenGraph, IHumanReadable, ISchemable, ICopiable, IConditionable
{
    protected function _fieldTypeInfo()
    {
        return array(
            'label' => __('Date', 'directories'),
            'default_settings' => array(
                'date_range_enable' => false,
                'date_range' => null,
                'enable_time' => true,
                'month_only' => false,
            ),
            'icon' => 'far fa-calendar-alt',
        );
    }

    public function fieldTypeSettingsForm($fieldType, Bundle $bundle, array $settings, array $parents = [], array $rootParents = [])
    {
        return [
            'month_only' => [
                '#type' => 'checkbox',
                '#title' => __('Month and year only', 'directories'),
                '#default_value' => !empty($settings['month_only']),
            ],
            'enable_time' => [
                '#type' => 'checkbox',
                '#title' => __('Enable time (hour and minute)', 'directories'),
                '#default_value' => !empty($settings['enable_time']),
                '#states' => [
                    'visible' => [
                        sprintf('input[name="%s[month_only]"]', $this->_application->Form_FieldName($parents)) => ['type' => 'checked', 'value' => false],
                    ],
                ],
            ],
            'date_range_enable' => [
                '#type' => 'checkbox',
                '#title' => __('Restrict dates', 'directories'),
                '#default_value' => !empty($settings['date_range_enable']),
                '#states' => [
                    'visible' => [
                        sprintf('input[name="%s[month_only]"]', $this->_application->Form_FieldName($parents)) => ['type' => 'checked', 'value' => false],
                    ],
                ],
            ],
            'date_range' => [
                '#type' => 'datepicker',
                '#enable_range' => true,
                '#default_value' => is_array($settings['date_range']) ? $settings['date_range'] : null,
                '#states' => [
                    'visible' => [
                        sprintf('input[name="%s[month_only]"]', $this->_application->Form_FieldName($parents)) => ['type' => 'checked', 'value' => false],
                        sprintf('input[name="%s[date_range_enable]"]', $this->_application->Form_FieldName($parents)) => ['type' => 'checked', 'value' => true],
                    ],
                ],
            ],
        ];
    }

    public function fieldTypeSchema()
    {
        return array(
            'columns' => array(
                'value' => array(
                    'type' => Application::COLUMN_INTEGER,
                    'notnull' => true,
                    'unsigned' => false,
                    'default' => 0,
                    'length' => 10,
                ),
            ),
            'indexes' => array(
                'value' => array(
                    'fields' => array('value' => array('sorting' => 'ascending')),
                ),
            ),
        );
    }

    public function fieldTypeOnSave(IField $field, array $values, array $currentValues = null, array &$extraArgs = [])
    {
        $ret = [];
        foreach ($values as $weight => $value) {
            if (!is_numeric($value)
                && (!$value = strtotime($value))
            ) {
                continue;
            } else {
                $value = intval($value);
            }
            $ret[]['value'] = $value;
        }

        return $ret;
    }

    public function fieldTypeOnLoad(IField $field, array &$values, IEntity $entity)
    {
        foreach ($values as $key => $value) {
            $values[$key] = $value['value'];
        }
    }

    public function fieldTypeIsModified(IField $field, $valueToSave, $currentLoadedValue)
    {
        $new = [];
        foreach ($valueToSave as $value) {
            $new[] = $value['value'];
        }
        return $currentLoadedValue !== $new;
    }

    public function fieldSortableOptions(IField $field)
    {
        return array(
            [],
            array('args' => array('desc'), 'label' => __('%s (desc)', 'directories'))
        );
    }

    public function fieldSortableSort(Query $query, $fieldName, array $args = null)
    {
        $query->sortByField($fieldName, isset($args) && $args[0] === 'desc' ? 'DESC' : 'ASC');
    }

    public function fieldQueryableInfo(IField $field)
    {
        return array(
            'example' => '12/31/2017,28-3-99,now',
            'tip' => __('Enter a single date string for exact date match, two date strings separated with a comma for date range search.', 'directories'),
        );
    }

    public function fieldQueryableQuery(Query $query, $fieldName, $paramStr, Bundle $bundle)
    {
        $params = $this->_queryableParams($paramStr, false);
        switch (count($params)) {
            case 0:
                break;
            case 1:
                if (strlen($params[0])
                    && ($time = strtotime($params[0]))
                ) {
                    $query->fieldIs($fieldName, $this->_application->getPlatform()->getSiteToSystemTime($time));
                }
                break;
            default:
                if (strlen($params[0])
                    && ($time = strtotime($params[0]))
                ) {
                    $query->fieldIsOrGreaterThan($fieldName, $this->_application->getPlatform()->getSiteToSystemTime($time));
                }
                if (strlen($params[1])
                    && ($time = strtotime($params[1]))
                ) {
                    $query->fieldIsOrSmallerThan($fieldName, $this->_application->getPlatform()->getSiteToSystemTime($time));
                }
        }
    }

    public function fieldOpenGraphProperties()
    {
        return array('books:release_date', 'music:release_date', 'video:release_date');
    }

    public function fieldOpenGraphRenderProperty(IField $field, $property, IEntity $entity)
    {
        if (!$value = $entity->getSingleFieldValue($field->getFieldName())) return;

        return array(date('c', $value));
    }

    public function fieldHumanReadableText(IField $field, IEntity $entity, $separator = null, $key = null)
    {
        if (!$values = $entity->getFieldValue($field->getFieldName())) return '';

        $ret = [];
        foreach ($values as $value) {
            $ret[] = $this->_application->System_Date_datetime($value);
        }
        return implode(isset($separator) ? $separator : PHP_EOL, $ret);
    }

    public function fieldSchemaProperties()
    {
        return ['datePublished', 'dateModified', 'dateCreated', 'startDate', 'endDate'];
    }

    public function fieldSchemaRenderProperty(IField $field, $property, IEntity $entity)
    {
        if (!$value = $entity->getSingleFieldValue($field->getFieldName())) return;

        return array(date('Y-m-d', $value));
    }

    public function fieldCopyValues(IField $field, array $values, array &$allValue, $lang = null)
    {
        return $values;
    }

    public function fieldConditionableInfo(IField $field, $isServerSide = false)
    {
        if (!$isServerSide) return;

        return [
            '' => [
                'compare' => ['value', '!value', '<value', '>value', '<>value'],
                'tip' => __('Enter a single date string for exact date match, two date strings separated with a comma for date range search.', 'directories'),
                'example' => '2020/3/28,today',
            ],
        ];
    }

    public function fieldConditionableRule(IField $field, $compare, $value = null, $name = '')
    {
        switch ($compare) {
            case 'value':
            case '!value':
            case '<value':
            case '>value':
                if (!$num = strtotime($value)) return;

                return [
                    'type' => $compare,
                    'value' => $this->_application->getPlatform()->getSiteToSystemTime($num),
                ];
            case '<>value':
                if (!strpos($value, ',')
                    || (!$value= explode(',', $value))
                    || !isset($value[0])
                    || !isset($value[1])
                    || (!$min = strtotime($value[0]))
                    || (!$max = strtotime($value[1]))
                    || $min > $max
                ) return;

                return [
                    'type' => $compare,
                    'value' => $this->_application->getPlatform()->getSiteToSystemTime($min) . ',' . $this->_application->getPlatform()->getSiteToSystemTime($max),
                ];
            default:
                return;
        }
    }

    public function fieldConditionableMatch(IField $field, array $rule, array $values, IEntity $entity)
    {
        switch ($rule['type']) {
            case 'value':
            case '!value':
                if (empty($values)) return $rule['type'] === '!value';

                foreach ($values as $input) {
                    if (is_array($input)) {
                        $input = $input['value'];
                    }
                    if ($input == $rule['value']) {
                        if ($rule['type'] === '!value') return false;
                    } else {
                        if ($rule['type'] === 'value') return false;
                    }
                }
                return true;
            case '<value':
            case '>value':
                if (empty($values)) return false;

                foreach ($values as $input) {
                    if (is_array($input)) {
                        $input = $input['value'];
                    }
                    if ($input < $rule['value']) {
                        if ($rule['type'] === '<value') return true;
                    } elseif ($input > $rule['value']) {
                        if ($rule['type'] === '>value') return true;
                    }
                }
                return false;
            case '<>value':
                if (empty($values)) return false;

                if (!strpos($rule['value'], ',')
                    || (!$rule_value= explode(',', $rule['value']))
                    || !is_numeric($rule_value[0])
                    || !is_numeric($rule_value[1])
                ) return;

                foreach ($values as $input) {
                    if (is_array($input)) {
                        $input = $input['value'];
                    }

                    if ($input >= $rule_value[0] && $input <= $rule_value[1]) return true;
                }
                return false;
            default:
                return false;
        }
    }
}
