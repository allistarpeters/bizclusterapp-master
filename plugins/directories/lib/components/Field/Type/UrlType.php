<?php
namespace SabaiApps\Directories\Component\Field\Type;

use SabaiApps\Directories\Component\Entity;
use SabaiApps\Directories\Component\Field\IField;

class UrlType extends AbstractStringType
{
    protected function _fieldTypeInfo()
    {
        return array(
            'label' => __('URL', 'directories'),
            'default_settings' => array(
                'min_length' => null,
                'max_length' => null,
                'char_validation' => 'url',
            ),
            'icon' => 'fas fa-link',
        );
    }

    public function fieldTypeSettingsForm($fieldType, Entity\Model\Bundle $bundle, array $settings, array $parents = [], array $rootParents = [])
    {
        $form = parent::fieldTypeSettingsForm($fieldType, $bundle, $settings, $parents, $rootParents);
        $form['char_validation']['#type'] = 'hidden';
        $form['char_validation']['#value'] = 'url';
        return $form;
    }

    public function fieldTypeDefaultValueForm($fieldType, Entity\Model\Bundle $bundle, array $settings, array $parents = [])
    {
        return [
            '#type' => 'url',
        ];
    }

    public function fieldSchemaProperties()
    {
        return array('url');
    }

    public function fieldOpenGraphProperties()
    {
        return array('og:audio', 'og:video', 'books:sample', 'product:product_link');
    }

    public function fieldOpenGraphRenderProperty(IField $field, $property, Entity\Type\IEntity $entity)
    {
        if (!$url = $entity->getSingleFieldValue($field->getFieldName())) return;

        return array($url);
    }

    public function fieldPersonalDataErase(IField $field, Entity\Type\IEntity $entity)
    {
        if (!$field->isFieldRequired()
            || (!$value = $entity->getSingleFieldValue($field->getFieldName()))
        ) return true; // delete

        return $this->_application->getPlatform()->anonymizeUrl($value); // anonymize
    }
}
