<?php
namespace SabaiApps\Directories\Component\Field\Type;

use SabaiApps\Directories\Component\Field\IField;
use SabaiApps\Directories\Component\Entity;

interface ITitle
{
    public function fieldTitle(IField $field, Entity\Type\IEntity $entity);
}