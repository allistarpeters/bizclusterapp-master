<?php
namespace SabaiApps\Directories\Component\Field\Renderer;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\Field\IField;
use SabaiApps\Directories\Component\Entity\Type\IEntity;

class PriceRenderer extends AbstractRenderer
{
    protected function _fieldRendererInfo()
    {
        return [
            'field_types' => ['price'],
            'default_settings' => [
                'trim' => false,
                'trim_length' => 200,
                'trim_marker' => '...',
                'trim_link' => false,
                '_separator' => ' ',
            ],
        ];
    }

    protected function _fieldRendererRenderField(IField $field, array &$settings, IEntity $entity, array $values, $more = 0)
    {
        foreach (array_keys($values) as $i) {
            $values[$i] = self::formatCurrency($this->_application, $values[$i]['value'], $values[$i]['currency']);
        }
        return implode($settings['_separator'], $values);
    }
    
    protected static $_currencies = [
        'AUD' => [2, 0, 'AU$'], // Australian Dollar
        'BHD' => [3], // Bahraini Dinar
        'BRL' => [2, 0, 'R$'], // Brazilian Real
        'CAD' => [2, 0, 'CA$'], // Canadian Dollar
        'CLP' => [0], // Chilean Peso
        'CNY' => [2, 0, 'CN&yen;'], // China Yuan Renminbi
        'CZK' => [2, 1, 'Kc'], // Czech Koruna
        'XCD' => [2, 0, 'EC$'], // East Caribbean Dollar
        'EUR' => [2, 0, '&euro;'], // Euro
        'HKD' => [2, 0, 'HK$'], // Hong Kong Dollar
        'HUF' => [0], // Hungary Forint
        'ISK' => [0, 1, 'kr'], // Iceland Krona
        'INR' => [2, 0, '&#2352;'], // Indian Rupee
        'JPY' => [0, 0, '&yen;'], // Japan Yen
        'JOD' => [3], // Jordanian Dinar
        'KWD' => [3], // Kuwaiti Dinar
        'LBP' => [0], // Lebanese Pound
        'LTL' => [2, 1, 'Lt'], // Lithuanian Litas
        'MUR' => [0], // Mauritius Rupee
        'MXN' => [2, 0, 'MX$'], // Mexican Peso
        'ILS' => [2, 0, '&#8362;'], // New Israeli Shekel
        'NZD' => [2, 0, 'NZ$'], // New Zealand Dollar
        'NOK' => [2, 1, 'kr'], // Norwegian Krone
        'GBP' => [2, 0, '&pound;'], // Pound Sterling
        'OMR' => [3], // Rial Omani
        'ZAR' => [2, 0, 'R'], // South Africa Rand
        'KRW' => [0, 0, '&#8361;'], // Korea (South) Won
        'SEK' => [2, 1, 'kr'], // Swedish Krona
        'CHF' => [2, 0, 'SFr '], // Swiss Franc
        'TWD' => [2, 0, 'NT$'], // Taiwan Dollar
        'THB' => [2, 1, '&#3647;'], // Thailand Baht
        'USD' => [2, 0, '$'], // US Dollar
        'VUV' => [0], // Vanuatu Vatu
        'VND' => [0, 0, '&#x20ab;'], // Viet Nam Dong
    ]; 
    
    public static function formatCurrency(Application $application, $value, $currency)
    {
        if (isset(self::$_currencies[$currency])) {
            $format = self::$_currencies[$currency];
        } else {
            $format = [2];
        }
        $value = $application->getPlatform()->numberFormat($value, $format[0]);
        if (!isset($format[2])) {
            // No symbol
            $value .= ' ' . $currency;
        } else {
            // Has symbol
            if (empty($format[1])) {
                $value = $format[2] . $value; // prepend symbol
            } else {
                $value .= $format[2]; // append symbol
            }
        }

        return $value;
    }
}