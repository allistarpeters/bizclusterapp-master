<?php
namespace SabaiApps\Directories\Component\Form\Field;

use SabaiApps\Directories\Component\Form\Form;

class MonthPickerField extends DatePickerField
{
    public function formFieldInit($name, array &$data, Form $form)
    {
        parent::formFieldInit($name, $data, $form);
        self::$_loadMonthSelect = true;
    }

    protected function _getDateInput($data)
    {
        if (!isset($data['#attributes']['placeholder'])) {
            $data['#attributes']['placeholder'] = __('Select month', 'directories');
        }
        return parent::_getDateInput($data);
    }
}
