<?php return [
    'header' => [
        '#class' => 'drts-view-entities-header',
        '#markup' => empty($nav[0])? null : $this->View_Nav($CONTEXT, $nav[0]),
    ],
    'filter_form' => [
        '#id' => '',
        '#class' => 'drts-view-entities-filter-form ' . DRTS_BS_PREFIX . 'collapse' . (empty($settings['filter']['shown']) ? '' : ' ' .  DRTS_BS_PREFIX . 'show'),
        '#attr' => empty($settings['filter']['shown']) ? ['data-collapsible' => 1] : [],
        '#markup' => $this->View_FilterForm_render($filter['form'], null, !empty($filter['is_external'])),
    ],
    'entities' => [
        '#prefix' => $this->Action('view_before_entities', array($entities, $CONTEXT), true),
        '#class' => 'drts-view-entities drts-view-' . $bundle->entitytype_name . '-entities drts-view-entities-' . $view,
        '#markup' => $this->render($settings['template'], $CONTEXT->getAttributes()),
        '#suffix' => $this->Action('view_after_entities', array($entities, $CONTEXT), true),
    ],
    'footer' => [
        '#class' => 'drts-view-entities-footer',
        '#markup' => empty($nav[1])? null : $this->View_Nav($CONTEXT, $nav[1]),
    ],
];
