<?php if (empty($entities) && !empty($hide_empty)) return;

return [
    '#prefix' => $this->Action('view_before_entities_container', array($entities, $CONTEXT), true),
    '#class' => 'drts-view-entities-container drts-view-entities-container-' . $view,
    'view' => $this->render($container_template, $CONTEXT->getAttributes()),
    '#suffix' => $this->Action('view_after_entities_container', array($entities, $CONTEXT), true),
];