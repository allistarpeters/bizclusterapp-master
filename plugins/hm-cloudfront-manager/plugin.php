<?php
/*
Plugin Name: Haymarket CloudFront Manager
Version: 1.6
Description: Rewrites the origin URLs to production CDN URLs ONLY if the request comes in from the CloudFront. In addition is changes the protocol from HTTP to HTTPS for compatibility with an Origin-Pull CDN. Read more in Jira at  <a href='https://haymarket.atlassian.net/browse/WP-828' target='_blank'>WP-828</a>.
Author: Mikel King
Text Domain: hm-cfcdn-mgr
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2019, Mikel King, haymarketmedia.com, (mikel.king AT haymarketmedia DOT com)
	All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice, this
          list of conditions and the following disclaimer.

        * Redistributions in binary form must reproduce the above copyright notice,
          this list of conditions and the following disclaimer in the documentation
          and/or other materials provided with the distribution.

        * Neither the name of the {organization} nor the names of its
          contributors may be used to endorse or promote products derived from
          this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

include( 'inc/aws-cfcdn-manager-admin.php' );

/**
 * Class URL_Fixer
 *
 * Use php's output buffer to replace insecure URLs with secure versions.
 */
final class HM_CFCDN_Manager extends WP_Base {
	const PLUGIN_NAME = 'Haymarket CloudFront Manager';
	const VERSION     = '1.6';
	const FORCE_HTTPS = true; // Default true
	const OUTPUT_RAW  = true; // Default true
	const DEBUG       = false; // Default false
	const CDN_AGENT   = 'Amazon CloudFront';
	const DEF_ORIGIN  = 'origin';
	const PROD_URL    = 'www';
	const HTTP        = 'http';
	const HTTPS       = 'https';
	const BASE_DELIM  = '//';
	const ENCD_DELIM  = '%3A%2F%2F';
	const ESCD_DELIM  = ':\/\/';
	const CDN_HEADER  = 'HTTP_X_EDGE_HOST';

	public $cdn_settings;
	public $hostname;
	public $apex;
	public $tld;
	public $old_site_url;
	public $old_home_url;
	public $new_sitehome_url;

	private $mapped_params = array();
	private $needles       = array();
	private $replacements  = array();

	/**
	 * URL_Fixer constructor
	 *
	 * Adds actions only when request URL, including protocol, matches the activation string.
	 */
	public function __construct() {
		parent::__construct();
		$this->old_site_url = get_site_url();
		$this->old_home_url = get_home_url();
		$cfcdn = new AWS_CFCDN_Manager_Admin();
		$this->cdn_settings = $cfcdn->get_options();
		$u = new URL_Magick();
		$this->get_domain_sections( $u::$host );
		if ( $this->find_mapped_params( $u ) ){
			$this->cleanup_content();
		}
		$this->debug( $u );
		add_action( 'wp_head', array( $this, 'print_user_agent'), 1 );
		add_action( 'wp_head', array( $this, 'print_sitehome_urls'), 1 );
		add_action( 'wp_head', array( $this, 'print_cdn_header'), 1 );
	}

	/**
	 *
	 */
	public function print_sitehome_urls() {
		print( '<!-- The site_url is: ' . $this->old_site_url . ' -->' . PHP_EOL );
		print( '<!-- The home_url is: ' . $this->old_home_url . ' -->' . PHP_EOL );
	}

	/**
	 *
	 */
	public function print_cdn_header() {
		if ( array_key_exists( self::CDN_HEADER, $_SERVER ) ) {
			print( '<!-- The CDN Header ' . self::CDN_HEADER . ' was found -->' . PHP_EOL );
		}
	}

	/**
	 *
	 */
	public function print_user_agent() {
		$user_agent = filter_var( filter_var( $_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_STRING ) );
		print( '<!-- This page has been requested by: ' . $user_agent . ' -->' . PHP_EOL );
	}

	/**
	 *
	 */
	public function print_plugin_header_notice() {
		print( '<!-- This page has been cached by: ' . self::PLUGIN_NAME . ' version: ' . self::VERSION . ' -->' . PHP_EOL );
	}

	/**
	 *
	 */
	public function is_cdn_request() {
		$user_agent = filter_var( filter_var( $_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_STRING ) );
		if ( $user_agent === self::CDN_AGENT ) {
			add_action( 'wp_head', array( $this, 'print_plugin_header_notice' ), 1 );
			return true;
		}
		return false;
	}

	/**
	 * Checks if the User agent string is from the Amazon Cloudfront CDN
	 */
	public function cleanup_content() {
		$this->needles      = array_keys( $this->mapped_params );
		$this->replacements = array_values( $this->mapped_params );

		add_action( 'after_setup_theme', array( $this, 'buffer_start' ) );
		add_action( 'shutdown', array( $this, 'buffer_end' ) );
	}

	/**
	 * Find the mapped parameters
	 */
	public function find_mapped_params( URL_Magick $u ) {
		$key   = $this->cdn_settings['originurl'];
		$value = $this->cdn_settings['produrl'];

		/**
		 * If the admin has not been set then use the default
		 */
		if ( empty( $key ) ) {
			$key = self::DEF_ORIGIN . '.' . $this->apex . '.' . $this->tld ;
		}

		/**
		 * If the admin has not been set then use the default
		 */
		if ( empty( $value ) ) {
			$value = self::PROD_URL . '.' . $this->apex . '.' . $this->tld ;
		}

		$u2 = new URL_Magick( $key );


		/**
		 * Since this has been refactored it may no longer be necessary to
		 * confirm we are on the right domain or site. Simply confirming that
		 * it's a CDN request may suffice.
		 */
		if ( strpos( $u::$host, $u2::$host ) === 0 &&
			$this->is_cdn_request() )
		{
			// protocal agnotic
			$this->mapped_params[self::BASE_DELIM . $key] = self::BASE_DELIM . $value;
			$this->mapped_params[self::HTTP . self::ENCD_DELIM . $key] = self::HTTPS . self::ENCD_DELIM . $value;
			$this->mapped_params[self::HTTP . self::ESCD_DELIM . $key] = self::HTTPS . self::ESCD_DELIM . $value;
			$this->mapped_params[self::HTTP . ':' . self::BASE_DELIM . $key] = self::HTTPS . ':' . self::BASE_DELIM . $value;
			if ( self::FORCE_HTTPS ) {
				$this->mapped_params[self::HTTP . ':' . self::BASE_DELIM . $value] = self::HTTPS . ':' . self::BASE_DELIM . $value;
				$this->new_sitehome_url = self::HTTPS . ':' . self::BASE_DELIM . $value;
			} else {
				$this->new_sitehome_url = self::HTTP . ':' . self::BASE_DELIM . $value;
			}
//			$this->mapped_params[$key] = $value;
			return true;
		}
		return false;
	}

	/**
	 *
	 */
	public function rewrite_site_url( $url, $path, $orig_scheme, $blog_id ) {
		return str_replace( $this->old_site_url, $this->new_sitehome_url, $url );
	}

	/**
	 *
	 */
	public function rewrite_home_url( $url, $path, $orig_scheme, $blog_id ) {
		return str_replace( $this->old_home_url, $this->new_sitehome_url, $url );
	}

/*
	const HTTP        = 'http';
	const HTTPS       = 'https';
	const BASE_DELIM  = '//';
	const ENCD_DELIM  = '%3A%2F%2F';
	const ESCD_DELIM  = ':\/\/';
*/

	/**
	 * At some point this will live in bacon.
	 */
	public function get_domain_sections( $domain ) {
		$domain_parts = explode( '.', $domain );
		$part_count = count( $domain_parts );

		if ( $part_count === 3 ) {
			$this->hostname = $domain_parts[0];
			$this->apex = $domain_parts[1];
			$this->tld = $domain_parts[2];
		}
	}

	/**
	 * Buffer Start
	 *
	 * Prevents any output from being sent to the browser, instead capturing it in php's
	 * output buffer.  Sets up $this->url_fix() to receive the page data when the buffer
	 * is flushed.
	 */
	public function buffer_start() {
		ob_start( array( $this, 'url_fix' ) );
	}

	/**
	 * URL Fix
	 *
	 * Callback triggered by flushing php's output buffer.  Replaces all instances of the
	 * array $this->needles of $this->urls with the corresponding value and returns the updated string.
	 *
	 * @param $buffer string
	 *
	 * @return string
	 */
	private function url_fix( $haystack ) {
		//$stack = $this->fix_encoded_urls( $haystack );
		return ( str_replace( $this->needles, $this->replacements, $haystack ) );
	}

	/*
	private function fix_encoded_urls( $haystack ) {
		return( str_replace( $this->needles, $this->replacements, $haystack ) );
	}
	*/

	/**
	 * Buffer End
	 *
	 * Flushes php's output buffer, consequently triggering the callback $this->url_fix().
	 * This allows page content to finally be sent to the browser.
	 */
	public function buffer_end() {
		if ( ob_get_contents() ) {
			ob_end_flush();
		}
	}

	/**
	 * Is Debug active
	 *
	 * Always use array_key_exists() to check for the key in question prior to calling
	 * it against undefined arrays
	 */
	private function is_debug() {
		$debug = null;
		if ( array_key_exists( 'debug', $_REQUEST ) ){
			$debug = filter_var( filter_var( $_REQUEST['debug'], FILTER_SANITIZE_NUMBER_INT ) );
		}

		if ( self::DEBUG ||  $debug ) {
			return true;
		}
		return false;
	}


	/**
	 * Debug
	 */
	private function debug( URL_Magick $u ) {
		$cdn_request = 'false';

		if ( $this->is_debug() ) {
			$requested_url = strtolower( "$_SERVER[REQUEST_SCHEME]://$_SERVER[HTTP_HOST]" );
			$this->output( 'Protocol: ' . $u::$protocol );
			$this->output( 'Host: ' . $u::$host );
			$this->output( 'Hostname: ' . $this->hostname );
			$this->output( 'Apex: ' . $this->apex );
			$this->output( 'TLD: ' . $this->tld );
			$this->output( 'User: ' . $u::$user );
			$this->output( 'Password: ' . $u::$pass );
			$this->output( 'URI: ' . $u::$uri );
			$this->output( 'Query string: ' . $u::$query );
			$this->output( 'URL fragment: ' . $u::$fragment );
			$this->output( 'Endpoint: ' . $u::$endpoint );
			$this->output( 'Cleaned URL: ' . $u::$cleaned_url );
			$this->output( 'Requested URL: ' . $requested_url );
			if ( $this->is_cdn_request() ) {
				$cdn_request = 'true';
			}
			$this->output( 'CDN Request: ' . $cdn_request  );
			// phpinfo();
		}
	}

	/**
	 * Pretty print data
	 */
	public function output( $data ) {
		$fmt = '%s<br>' . PHP_EOL;
		if ( ! self::OUTPUT_RAW ) {
			$fmt = '<!-- %s -->' . PHP_EOL;
		}

		printf( $fmt, $data );
	}
}

HM_CFCDN_Manager::get_instance();
