<?php

class AWS_CFCDN_Manager_Admin extends Base_Manager_Admin {
	const PAGE_TITLE    = 'AWS CloudFront Manager';
	const MENU_SLUG     = 'aws-cfcdn-manager';
	const METHOD_PREFIX = 'aws_cfcdn_manager';
    const LINE_WIDTH    = '300px';
    const SECTION_INFO  = '<b>Note</b> Enter the as appropriate needle and substitution parameters relevant to this site. Remember to exclude the protocol. Exmple: origin.some-site-domain.com ';

	public $options;

	public $fields= array(
			'Origin URL'      => 'originurl',
			'Production URL'  => 'produrl'
	);

	public function __construct() {
		$this->get_admin_options();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}

}
