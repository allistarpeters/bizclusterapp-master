<?php

/**
 * Class LinkPress_Controller
 * @todo experiment w/ php namespaces
 */
class LinkPress_Controller {
	const VERSION  = '1.6';
	const URL_FMT  = '<a href="%s%s">%s</a>';
	const JIRA_URL = 'https://haymarket.atlassian.net/browse/';
	const GITLAB   = 'https://gitlab.com/';
	const GITHUB   = 'https://github.com/';
	const GL10UP   = 'https://gitlab.10up.com/';

	public $prefixes_stack = array(
		'ad'    => self::JIRA_URL,
		'data'  => self::JIRA_URL,
		'do'    => self::JIRA_URL,
		'wp'    => self::JIRA_URL,
		'prx'   => self::JIRA_URL,
		'wpe'   => self::JIRA_URL,
		'wpmck' => self::JIRA_URL,
		'wpmed' => self::JIRA_URL,
		'wpmmm' => self::JIRA_URL,
		'wpsc'  => self::JIRA_URL,
		'mr'    => self::GITLAB . 'mikelking/HmEvents/merge_requests/',
		'bcmr'  => self::GITLAB . 'hmdev/bizclusterapp/merge_requests',
		'ecmr'  => self::GITLAB . 'hmdev/eventsclusterapp/merge_requests',
		'mcmr'  => self::GITLAB . 'hmdev/medicalclusterapp/merge_requests',
		'scmr'  => self::GITLAB . 'hmdev/securityclusterapp/merge_requests',
		'10mr'  => self::GL10UP . 'haymarket/haymarket/merge_requests/',
		'bpr'   => self::GITHUB . 'mikelking/bacon/pull/'
	);

	public function __construct() {}

	/**
	 * Added by Mikel King
	 * @param $needle
	 * @return string || null
	 */
	public function issue_linker( $issue ) {
		$parts = explode( "-", $issue );
		$needle = $parts[0];

		$result = array_search( $needle, array_keys( $this->prefixes_stack ) );
		if ( $result !== false ) {
			if (stripos( $this->prefixes_stack[$needle], 'atlassian' ) ) {
				$output = sprintf( self::URL_FMT, $this->prefixes_stack[$needle], $issue, $issue );
			} elseif (stripos( $this->prefixes_stack[$needle], 'gitlab' ) ) {
				$output = sprintf( self::URL_FMT, $this->prefixes_stack[$needle], $parts[1], $issue );
			}
			 elseif (stripos( $this->prefixes_stack[$needle], 'github' ) ) {
				$output = sprintf( self::URL_FMT, $this->prefixes_stack[$needle], $parts[1], $issue );
			}
			return ( $output );
		}
		return( null );
	}

	/**
	 * Here the jira issues strings are replaced by links.
	 *
	 * @param array $matches
	 * @return string || null
	 */
	public function external_linker( $matches ) {
		$output = '';
		foreach ( $matches as $match ) {
			$issue = strtolower( trim( $match, '[]' ) );
			$output .= $this->issue_linker( $issue );
		}

		if( $output && $output != '' ) {
			return( $output );
		}

		return( null );
	}

	/**
	 *
	 * Scans the content for:
	 *     jira issues in format [xx-123]
	 *     github pull requests [xxx_pr-123]
	 *
	 * @param $content
	 * @return string
	 */
	public function content_filter( $content ) {
		$patterns = array(
			'/\[[A-Za-z0-9]+\-[0-9]+\]/',
			'/\[[A-Za-z0-9]+\_[A-Za-z0-9]+\-[0-9]+\]/'
		);
		$output = preg_replace_callback (
			$patterns,
			function ( $matches ) {
				$issue = strtolower( trim( $matches[0], '[]' ) );
				$output = $this->issue_linker( $issue );
				if( $output ) {
					return( $output );
				}
			},
			$content
		);
		return( $output );
	}
}
