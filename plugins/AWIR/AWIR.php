<?php

/*
  Plugin Name: AWIR
  Description:
  Author: Haymarket
  Version: 0.1
  Text Domain: awir
 */

require_once 'classes/base.php';
require_once 'classes/profile.php';
require_once 'classes/messages.php';
require_once 'classes/board.php';
require_once 'classes/acf.php';
require_once 'classes/mailchimp.php';
require_once 'classes/discounts.php';
require_once 'classes/membership.php';
require_once 'classes/log.php';


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');

class awir extends awir_singleton
{
	public function __construct()
	{
		parent::__construct();

		$this->add_filter('register_post_type_args', 'fix_posts_title', 10, 2);
		add_filter( 'auto_update_plugin', '__return_false' );
	}

	public function init()
	{
		$this->add_action('login_init', 'login_redirects');

		//TODO: Remove it
		// $this->add_filter('ms_helper_utility_get_ssl_url', 'force_non_ssl', 100 );

		$this->add_action( 'admin_init' );
		//$this->add_action( 'admin_menu' );

		$this->add_filter( 'show_admin_bar' );

		//if ( !is_admin() )
		//	$this->add_filter('pre_get_posts', 'filter_events_per_page');

		$this->add_filter('get_avatar_url', null, 10, 3);
		$this->add_filter('tribe_events_register_default_linked_post_types', 'events_remove_organizers' );

		/*
		if ( !current_user_can( 'edit_others_posts') )
		{
			define('TRIBE_DISABLE_TOOLBAR_ITEMS', true);
			$this->add_action('admin_bar_menu', null, 99 );
		}
		*/

		if ( current_user_can('update_plugins') )
			$this->add_filter('upload_mimes');
	}

	public function admin_init()
	{
		/*
		if ( !current_user_can( 'edit_others_posts') )
		{
			$this->add_filter( 'pre_get_posts', 'admin_get_posts' );
			$this->add_filter( 'wp_count_posts', 'admin_count_posts', 10, 3 );
			$this->add_action( 'transition_post_status', 'admin_count_posts_clear_cache', 10, 3 );
		}
		*/

		$this->ajax_nopriv_action('login');
		$this->ajax_nopriv_action('forgot_password');

		$this->add_action('admin_print_scripts');

		if ( defined('DOING_AJAX') && DOING_AJAX )
			return;

		if ( !current_user_can( 'edit_others_posts') )
			self::user_page_redirect();
			//$this->add_action( 'current_screen', 'filter_admin_screens' );

		/*
		if ( !self::is_member() )
			self::user_page_redirect('memberships');
		*/

		remove_action( 'admin_notices', 'cfdb7_admin_notice' );

		if ( current_user_can('update_plugins') && !empty($_GET['awir_import_regonline']) && $_GET['awir_import_regonline'] === 'GGhdas86' )
		{
			require_once 'classes/import_users.php';
			new awir_import_users();
		}
	}

	/*
	public function admin_menu()
	{
		if ( !current_user_can( 'edit_others_posts') )
		{
			remove_menu_page( 'index.php' );
			//remove_menu_page( 'edit.php' );
			//remove_menu_page( 'edit-comments.php' );
			remove_menu_page( 'profile.php' );
			remove_menu_page( 'upload.php' );
		}
	}

	public function filter_admin_screens( $screen )
	{

		if ( is_object($screen) && !empty( $screen->id ) )
		{
			if ( $screen->id == 'board' || $screen->id == 'edit-board' )
				return;

			if ( $screen->id == 'profile' )
				self::user_page_redirect( 'user' );
		}

		$boards_url = admin_url( 'edit.php?post_type=board' );
		wp_redirect( $boards_url );
		exit;
	}
	*/

	/*
	 * Redirect from wp-login.php to UM pages (if set)
	 */
	public function login_redirects()
	{
		global $action;

		if ( $action == 'login')
		{
			if ( !empty( $_REQUEST['reauth'] ) || !empty($_REQUEST['interim-login']) )
				return;

			if ( $_POST )
				return;
		}

		$redirects = [
			//action => page
			'register' => 'register',
			'login' => 'become_member',
			'lostpassword' => 'become_member',
			'retrievepassword' => 'become_member',
			'resetpass' => 'become_member',
			'rp' => 'become_member',
		];

		if ( isset( $redirects[ $action ] ) )
			self::user_page_redirect( $redirects[ $action ] );
	}

	/*
	private static function memberships_url( $page = 'memberships' )
	{
		$ms_pages = get_option('ms_model_pages');

		if ( !is_array($ms_pages) || empty( $ms_pages['settings'][ $page ] ) )
			return false;

		return get_permalink( $ms_pages['settings'][ $page ] );
	}
	*/

	public static function user_page_url( $page = 'user', $query = null )
	{
		/*if ( $page === 'memberships' )
			$url = self::memberships_url();

		else*/if ( $page === 'home' )
			$url = home_url();

		elseif ( $page === 'meeting_content' )
			$url = get_post_type_archive_link('post');

		else
		{
			if ( $page === 'become_member' )
				$page = 'membership';

			$post_id = get_option( "options_{$page}_page" );

			if ( !$post_id && $post = get_page_by_path( $page ) )
				$post_id = $post->ID;

			//var_dump( $post_id, $post );

			$url = $post_id ? get_permalink( $post_id ) : home_url();
		}

		if ( is_array( $query ) )
			$url = add_query_arg( $query, $url );

		return $url;
	}

	public static function user_page_redirect( $page = 'user', $query = null )
	{
		$url = self::user_page_url( $page, $query );
		if ( !$url )
			$url = home_url();

		wp_redirect( $url );
		exit;
	}

	//TODO: Remove it, and repair in membership/app/gateway/authorize/class-ms-gateway-authorize.php
	// public function force_non_ssl( $url )
	// {
	// 	return preg_replace( '|^https://|', 'http://', $url );
	// }

	public static function is_meeting_content()
	{
		if ( is_home() || is_singular('post') )
			return true;

		if ( is_archive() && in_array(get_query_var('post_type'), ['post','']) )
			return true;

		return false;
	}

	public static function is_member_area()
	{
		return awir_memberships::is_member_area();
	}

	public static function is_member( $user_id = null )
	{
		return awir_memberships::is_member( $user_id );
	}

	public function admin_get_posts( $query )
	{
		$query->set( 'author', get_current_user_id() );
		return $query;
	}

	public function admin_count_posts( $counts, $type, $perm )
	{
		if ( 'readable' != $perm )
			return $counts;

		$cache_key = _count_posts_cache_key( $type, $perm ).'_admin';
		$counts = wp_cache_get( $cache_key, 'counts' );

		if ( false !== $counts ) {
			return $counts;
		}

		global $wpdb;

		$results = (array)$wpdb->get_results(
			$wpdb->prepare( "
				select
					post_status, count(*) as num_posts
				from {$wpdb->posts}
				where
					post_type = %s and post_author = %d
				group by post_status
				", $type, get_current_user_id()
			) );

		$counts = array_fill_keys( get_post_stati(), 0 );

		foreach ( $results as $row ) {
			$counts[ $row->post_status ] = $row->num_posts;
		}

		$counts = (object)$counts;
		wp_cache_set( $cache_key, $counts, 'counts' );

		return $counts;
	}

	public function admin_count_posts_clear_cache( $new_status, $old_status = null, $post = null )
	{
		if ( $new_status != $old_status && $post )
			wp_cache_delete( _count_posts_cache_key( $post->post_type, 'readable' ).'_admin', 'counts' );
	}

	/*public function filter_events_per_page( $query )
	{
		if ( $query->is_post_type_archive('tribe_events') && $query->get('posts_per_page') > 1 )
		{
			$query->set('posts_per_page', 1000);
			$query->set('posts_per_archive_page', 1000);
		}

		return $query;
	}*/

	public function events_remove_organizers( $default_types ) {
		if (
			! is_array( $default_types )
			|| empty( $default_types )
			|| empty( Tribe__Events__Main::ORGANIZER_POST_TYPE )
		)
			return $default_types;

		if ( ( $key = array_search( Tribe__Events__Main::ORGANIZER_POST_TYPE, $default_types ) ) !== false )
			unset( $default_types[ $key ] );

		return $default_types;
	}

	public function show_admin_bar()
	{
		return current_user_can('edit_others_posts');
	}

	public function ajax_login()
	{
		$user = wp_signon();

		if ( is_wp_error( $user ) )
			self::ajax_error( 'Invalid username or password.' );

		else
			self::ajax_success();
	}

	public function ajax_forgot_password()
	{
		if ( empty( $_POST['user_login'] ) || ! is_string( $_POST['user_login'] ) )

			self::ajax_error( 'Enter a username or email address.' );

		elseif ( strpos( $_POST['user_login'], '@' ) )
		{
			$user_data = get_user_by( 'email', trim( wp_unslash( $_POST['user_login'] ) ) );
			if ( empty( $user_data ) )
				self::ajax_error( 'There is no user registered with that email address.' );
		}
		else
		{
			$login = trim( $_POST['user_login'] );
			$user_data = get_user_by('login', $login);
			if ( !$user_data )
				self::ajax_error( 'There is no user registered with that login.' );
		}

		// Redefining user_login ensures we return the right case in the email.
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;
		$key = get_password_reset_key( $user_data );

		if ( is_wp_error( $key ) )
			self::ajax_error( $key->get_error_message() );

		$site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

		// $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
		// $message .= sprintf( __( 'Site Name: %s'), $site_name ) . "\r\n\r\n";
		// $message .= sprintf( __( 'Username: %s'), $user_login ) . "\r\n\r\n";
		// $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
		// $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
		// $message .= '<' . self::user_page_url('reset-password', [ 'key' => $key, 'login' => $user_login ]) . ">\r\n";

		// $title = sprintf( __( '[%s] Password Reset' ), $site_name );

		$headers = array(
			'content-type: text/html',
		);

		$resetPassMail = get_field('reset_pass_email', 'option');
		$title = $resetPassMail['subject'];

		$user_info = get_userdata($user_data->ID);
		$message = str_replace(
			array(
				'%first_name%',
				'%last_name%',
				'%reset-pass-link%',
			), 
			array(
				$user_info->first_name,
				$user_info->last_name,
				'<a href="' . self::user_page_url('reset-password', [ 'key' => $key, 'login' => $user_login ]) . '">Password Reset</a>'
			), 
			$resetPassMail['body']
		);

		if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message, $headers ) )
		{
			self::$debug = true;
			self::log( 'The email could not be sent.', $user_email, $title, $message );
			self::ajax_error();
		}

		self::ajax_success();
	}

	public function get_avatar_url( $url, $object, $args)
	{
		if ( $object instanceof WP_User )
			$user_id = $object->ID;

		elseif ( $object instanceof WP_Post )
			$user_id = $object->post_author;

		elseif ( $object instanceof WP_Comment )
			$user_id = $object->comment_author;

		elseif ( !is_scalar( $object ) )
			return $url;

		elseif ( preg_match('/^\d+$/', $object) )
			$user_id = (int)$object;

		elseif ( strpos( $object, '@') !== false && ( $user = get_user_by('email', $object) ) )
			$user_id = $user->ID;

		else
			return $url;

		$attachment_id = get_user_meta( $user_id, 'avatar', true );

		if ( !$attachment_id )
			return $url;

		$src = wp_get_attachment_image_src( $attachment_id, [ $args['width'], $args['height'] ] );

		return $src ? $src[0] : $url;
	}

	/*
	public function admin_bar_menu( $wp_admin_bar )
	{
		$wp_admin_bar->remove_node('new-content');
		$wp_admin_bar->remove_node('archive');
	}
	*/

	public function upload_mimes( $types )
	{
		$types['svg'] = 'image/svg+xml';
		return $types;
	}

	public function admin_print_scripts()
	{
		?>
		<style>
		td.media-icon img[src$=".svg"] {
			/*
			width: 100% !important;
			height: auto !important;
			*/
		}
		.acf-field-5b15171101e4a .acf-image-uploader .image-wrap img, .acf-field-5b27ddd983cf0 .acf-image-uploader .image-wrap img{
			width: 24px;
			height: 24px;
			min-width: 24px;
			min-height: 24px;
			background-color: #1a0835;
		}
		.acf-field-5b27ddd983cf0 .acf-image-uploader .image-wrap img {
			background-color: #f9fafc;
		}
		</style>
		<?php
	}

	public static function have_messages( $user_id = null )
	{
		$board = new awir_user_messages( $user_id );

		return $board->have_new();
	}

	public static function member_since( $user_id = null, $format = 'F d, Y' )
	{
		return awir_memberships::member_since( $user_id, $format );
	}

	public function fix_posts_title( $args, $name )
	{
		if ( $name == 'post' )
		{
			$args['labels'] = [
				'name' => __( 'Meeting Content', 'awir' ),
				'all_items' => __( 'Posts' ),
				'singular_name' => __( 'Post' ),
				'name_admin_bar' => __( 'Meeting Content' ),
			];
		}
		return $args;
	}
}

awir::getInstance();
