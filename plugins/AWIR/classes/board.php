<?php

class awir_message_board extends awir_singleton
{
	public function init()
	{
		register_taxonomy( 'board_tag', 'board', array(
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => false,
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms' => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'edit_boards',
			)
		) );

		$labels = array(
			'name' => __( 'Message Board', 'awir' ),
			'all_items' => __( 'Posts' ),
			'singular_name' => __( 'Post' ),
			'archives' => __('Message Board', 'awir'),
		);

		register_post_type(
			'board',
			array(
				'has_archive' => 'board',// or archive slug
				'label' => __('Message Board','awir'),
				'labels' => $labels,
				'public' => true,
				'show_ui' => true,
				'show_in_nav_menus' => true,
				//'menu_icon' => '', //url or data-url or dashicon-xxx
				'supports' => array( 'title', 'slug', 'editor', 'thumbnail', 'author', 'comments', 'revisions' ),
				'menu_position' => 5,
				'capability_type' => 'board',
				'capabilities' => array(
					'edit_others_posts' => 'edit_others_posts',
					'delete_others_posts' => 'delete_others_posts',
				),
				'map_meta_cap' => true,
				'taxonomies' => array( 'board_tag' ),
				'rewrite' => true,
			)
		);

		$this->add_action('admin_init');
		$this->add_action('pre_get_posts');
		//wp_roles()->remove_cap('subscriber', 'manage_categories');
		//wp_roles()->add_cap('subscriber', 'delete_published_boards');
	}

	public function admin_init()
	{
		if ( !current_user_can('edit_others_posts') )
			$this->add_action('get_terms', 'filter_tags', 10, 3 );
	}

	public function filter_tags( $terms, $taxonomy, $vars )
	{
		if ( count($taxonomy) == 1 && $taxonomy[0] == 'board_tag' && !$vars['object_ids'] && $vars['fields'] == 'all' )
			return self::user_tags();

		return $terms;
	}

	public function pre_get_posts( $query )
	{
		$type = $query->get('post_type');

		if ( $type != 'board' && $type != ['board'] )
			return;

		if ( $query->is_archive && $query->is_main_query() && !is_admin() )
		{
			if ( $query->get('tax_query') )
				return;

			$tags = self::user_tags();
			if ( !$tags )
				return;

			$tags_ids = [];
			foreach ( $tags as $tag )
				$tags_ids[] = $tag->term_id;

			$tax_query = [
				[
					'taxonomy' => 'board_tag',
					'field' => 'term_id',
					'terms' => $tags_ids,
				]
			];

			$query->set( 'tax_query', $tax_query );
		}
		//else var_dump( $query );

	}

	public static function user_tags( $user_id = null, $meta = ['interests','activities'] )
	{
		$user_id = self::current_user_if_null( $user_id );

		$user_tags = [];
		foreach ( (array)$meta as $meta_key )
		{
			$tags = get_user_meta( $user_id, $meta_key, true );
			if ( !is_array( $tags ) )
				continue;

			$user_tags = array_merge( $user_tags, $tags );
		}

		$user_tags = array_unique( $user_tags );

		$real_tags = [];

		foreach ( $user_tags as $tag )
			if ( $real_tag = get_term_by( 'term_id', $tag, 'board_tag' ) )
				$real_tags[] = $real_tag;

		return $real_tags;
	}

	public static function url( $category = null )
	{
		$url = get_post_type_archive_link( 'board' );

		if ( is_object( $category ) )
			$category = $category->slug;

		if ( $category )
			$url = add_query_arg('board_tag', $category, $url);

		return $url;
	}
}

awir_message_board::getInstance();
