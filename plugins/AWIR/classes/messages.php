<?php

require_once 'base.php';

class awir_messages extends awir_singleton
{
	const NOTIFY_THRESHOLD = 15; //Minutes

	public function init()
	{
		$this->add_action( 'admin_init' );
		$this->cron_event( 'check_notify_messages', 'often' );
		//var_dump( get_field('pending_membership_body', 'options') ); exit;
	}

	public function admin_init()
	{
		$this->ajax_action('add_message');
		$this->ajax_action('get_dialog');
		$this->ajax_action('new_messages');
		$this->ajax_action('unread_messages');
	}

	public function ajax_add_message()
	{
		check_ajax_referer('awir_add_message');

		if ( empty( $_POST['to'] ) || !isset($_POST['message']) || !is_string($_POST['message']) )
			self::ajax_error();

		$message = trim( stripslashes( $_POST['message'] ) );
		if ( !strlen( $message ) )
			self::ajax_error();

		$user = get_userdata( (int)$_POST['to'] );
		if ( !$user )
			self::ajax_error();

		$board = new awir_user_messages();
		$id = $board->add_message( $user->ID, $message );

		if ( $id )
			self::ajax_success( $id );
		else
			self::ajax_error();
	}

	public function ajax_get_dialog()
	{
		check_ajax_referer( 'awir_get_dialog' );

		if ( empty( $_REQUEST['user_id'] ) )
			self::ajax_error();

		$user = get_userdata( (int)$_REQUEST['user_id'] );
		if ( !$user )
			self::ajax_error();

		$offset = empty( $_REQUEST['offset'] ) ? 0 : (int)$_REQUEST['offset'];
		$count = empty( $_REQUEST['count'] ) ? 25 : (int)$_REQUEST['count'];

		$board = new awir_user_messages();
		$messages = $board->get_dialog( $user->ID, $offset, $count );

		self::ajax_success( $messages );
	}

	public function ajax_new_messages()
	{
		$board = new awir_user_messages();

		$new_messages = $board->new_messages();

		self::ajax_success( $new_messages );
	}

	public function ajax_unread_messages()
	{
		check_ajax_referer( 'awir_get_dialog' );

		if ( empty( $_REQUEST['user_id'] ) )
			self::ajax_error();

		$board = new awir_user_messages();

		$messages = $board->unread_messages( (int)$_REQUEST['user_id'] );
		self::ajax_success( $messages );
	}

	public function check_notify_messages()
	{
		$users = get_users([
			'meta_query' => [
				[
					'key' => 'has_notify_message',
					'compare' => 'EXISTS',
				],
				[
					'key' => 'notify_message_sent',
					'compare' => 'NOT EXISTS',
				],
			],
		]);

		foreach ( $users as $user )
		{
			$time = get_user_meta( $user->ID, 'has_notify_message', true );
			$time_threshold = time() - (self::NOTIFY_THRESHOLD * MINUTE_IN_SECONDS);

			if ( !$time || $time > $time_threshold )
				continue;

			$user_messages = new awir_user_messages( $user->ID );

			if ( !$user_messages->have_new() )
				continue;

			$user_messages->notify();
		}
	}
}

awir_messages::getInstance();

class awir_user_messages extends awir_base {
	protected $tb;
	protected $user_id = null;

	public function __construct( $user_id = null )
	{
		global $wpdb;
		$this->tb = $wpdb->prefix.'awir_messages';

		$this->user_id = (int)self::current_user_if_null( $user_id );

		parent::__construct();
	}

	public function my_contacts()
	{
		global $wpdb;

		$list = $wpdb->get_results("
			select
				u.ID user_id, u.user_login, u.display_name, GROUP_CONCAT(meta_degree.meta_value) med_degree,
				MAX(UNIX_TIMESTAMP(m.date)) latest_time, MIN(m.is_readed) is_readed, MAX(m.from_user_id) latest_author_id
			from
				{$wpdb->prefix}users u
				left join {$this->tb} m on m.ID=(
					SELECT id from {$this->tb}
					where from_user_id={$this->user_id} and to_user_id=u.ID or from_user_id=u.ID and to_user_id={$this->user_id}
					order by date desc limit 1
				)
				left join {$wpdb->prefix}usermeta meta_degree on meta_degree.user_id=u.ID and meta_degree.meta_key='med_degree'
			where
				u.ID != {$this->user_id}
			group by u.ID
			order by latest_time desc
		" );

		$this->notify_break();

		return $list;
	}

	public function unread_messages( $recipient_id, $set_as_readed = true )
	{
		global $wpdb;
		$messages = $wpdb->get_results( $wpdb->prepare("
			select
				ID, from_user_id author_id, to_user_id, UNIX_TIMESTAMP(date) timestamp, message, is_readed
			from {$this->tb}
			where
				to_user_id=%d and from_user_id=%d and is_readed=0
			order by date desc",
			$this->user_id, $recipient_id
		) );

		if ( $set_as_readed )
			$this->set_as_readed( $recipient_id );

		return $messages;
	}

	public function get_dialog( $recipient_id, $offset = 0, $count = 25, $set_as_readed = null )
	{
		global $wpdb;

		$messages = [];

		$messages = array_merge( $messages, $wpdb->get_results( $wpdb->prepare("
			select
				ID, from_user_id author_id, to_user_id, UNIX_TIMESTAMP(date) timestamp, message, is_readed
			from {$this->tb}
			where
				( from_user_id=%d and to_user_id=%d ) or ( from_user_id=%d and to_user_id=%d )
			order by date desc
			limit %d, %d",
			$this->user_id, $recipient_id, $recipient_id, $this->user_id,
			$offset, $count
		) ) );

		if ( $set_as_readed === null )
			$set_as_readed = empty( $offset );

		if ( $set_as_readed )
			$this->set_as_readed( $recipient_id );

		return $messages;
	}

  public function add_message( $to_user_id, $message )
	{
		global $wpdb;

		$rez = $wpdb->insert( $this->tb, [
			'from_user_id' => $this->user_id,
			'to_user_id' => $to_user_id,
			'message' => $message
			], ['%d','%d','%s']
		);

		$message = $wpdb->get_row( $wpdb->prepare("
			select
				ID, from_user_id author_id, to_user_id, UNIX_TIMESTAMP(date) timestamp, message, is_readed
			from {$this->tb}
			where
				ID=%d",
			$wpdb->insert_id
		) );

		if ( !get_user_meta( $to_user_id, 'has_notify_message', true ) )
			update_user_meta( $to_user_id, 'has_notify_message', time() );

		return $message;
	}

	public function set_as_readed( $recipient_id )
	{
		global $wpdb;
		$wpdb->update( $this->tb, [
			'is_readed' => 1,
		], [
			'from_user_id' => $recipient_id,
			'to_user_id' => $this->user_id,
			'is_readed' => 0
		] );

		$this->notify_break();
	}

	public function have_new()
	{
		global $wpdb;
		$rez = $wpdb->get_var( $wpdb->prepare("
			select
				ID
			from {$this->tb}
			where
				to_user_id=%d and is_readed=0
			limit 1",
			$this->user_id
		) );

		return $rez;
	}

	public function new_messages()
	{
		global $wpdb;
		return $wpdb->get_col( "
			select
				from_user_id
			from {$this->tb}
			where
				is_readed=0 and to_user_id={$this->user_id}
			group by from_user_id"
		);
	}

	public function notify_break()
	{
		delete_user_meta( $this->user_id, 'has_notify_message' );
		delete_user_meta( $this->user_id, 'notify_message_sent' );
	}

	public function notify()
	{
		if ( get_user_meta( $this->user_id, 'notify_message_sent', true ) )
			return;

		update_user_meta( $this->user_id, 'notify_message_sent', 1 );

		$subject = get_field('notify_messages_subject', 'options');
		$body = get_field('notify_messages_body', 'options');

		if ( !$body )
		{
			self::log('body is empty');
			return; //TODO: Maybe this is not the best behavior
		}

		$profile = new awir_profile( $this->user_id );
		$data = [
			'first_name' => $profile->first_name,
			'last_name' => $profile->last_name,
			'name' => $profile->name,
			'url' => awir::user_page_url('messages', ['login'=>1]),
		];

		foreach ( $data as $key => $value )
			$body = str_replace( "%{$key}%", $value, $body );

		wp_mail( $profile->email, $subject, $body, ['Content-Type: text/html; charset=utf-8'] );
	}
}