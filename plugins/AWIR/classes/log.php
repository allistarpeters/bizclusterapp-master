<?php

class awir_admin_log extends awir_singleton
{
	protected function __construct()
	{
		parent::__construct();

		if ( !empty( $_POST ) && get_option('options_admin_log') )
			$this->add_action('init', 'pre_init', 1 );
	}

	public function init()
	{
		$this->add_action('admin_menu');
	}

	public function admin_menu()
	{
		if ( get_option('options_admin_log') )
			add_options_page( 'Admin Actions Log', 'Admin Log', 'manage_options', 'admin-actions-log', array( $this, 'admin_page' ) );
	}

	public function pre_init()
	{
		if ( !current_user_can('edit_others_posts') )
			return;

		$data = stripslashes_deep( $_POST );

		if ( defined('DOING_AJAX') && DOING_AJAX && !empty( $data['action'] ) )
		{
			$action = (string)$data['action'];
			unset( $data['action'] );

			if ( in_array( $action, ['heartbeat','awir_new_messages','awir_unread_messages','awir_get_dialog'] ) )
				return;
		}

		else
		{
			if ( $data === [ 'action' => 'awir_new_messages' ] )
				return;

			$action = $_SERVER['REQUEST_URI'];
		}

		global $wpdb;
		$wpdb->insert( $wpdb->prefix.'awir_admin_log', [
			'user_id' => get_current_user_id(),
			'action' => $action,
			'ip' => $_SERVER['REMOTE_ADDR'],
			'data' => http_build_query($data),
		] );
	}

	public function admin_page()
	{
		$table = new admin_log_list_table();
		$table->prepare_items();
		?>
		<div class="wrap">
			<form method="get">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<?php $table->display() ?>
			</form>
		</div>
		<script>
		jQuery( function($){
			$('.column-data .short').click( function(e) {
				e.preventDefault();
				$(this).siblings('.full').toggle();
			} );
		} );
		</script>
		<?php
	}
}

awir_admin_log::getInstance();


require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

class admin_log_list_table extends WP_List_Table
{
	public $rows_per_page = 50;

	public function __construct()
	{
		parent::__construct([
			'singular'  => 'admin_action',
			'plural'    => 'admin_actions',
			'ajax'      => false,
		]);
	}

	function column_default( $row, $column )
	{
		return $row->{$column};
	}

	function column_user( $row )
	{
		$user = get_userdata( $row->user_id );

		if ( $user )
			return esc_html( $user->display_name );

		return '';
	}

	function column_data( $row )
	{
		if ( mb_strlen( $row->data ) < 100 )
			return $row->data;

		$short = mb_substr( $row->data, 0, 90 );
		parse_str( $row->data, $data );
		$data = print_r( $data, true);
		ob_start();
		?>
		<a href="" class="short"><?= esc_html($short) ?>...</a>
		<pre class="full" style="display:none"><?= esc_html( $data ) ?></pre>
		<?php

		return ob_get_clean();
	}

	function get_columns()
	{
		return [
			'dt' => 'Date',
			'user' => 'User',
			'action' => 'Action',
			'ip' => 'IP',
			'data' => 'Request data',
		];
	}

	function prepare_items()
	{
		global $wpdb;
		$per_page = 50;

		$columns = $this->get_columns();

		$this->_column_headers = [ $columns, [], [] ];

		$current_page = $this->get_pagenum();

		$this->items = $wpdb->get_results( $wpdb->prepare("
			select *
			from {$wpdb->prefix}awir_admin_log
			order by dt desc
			limit %d, %d",
			$this->rows_per_page * ($current_page-1), $this->rows_per_page
		) );

		$count = $wpdb->get_var("
			select count(*) from {$wpdb->prefix}awir_admin_log"
		);

		$this->set_pagination_args( [
			'per_page' => $this->rows_per_page,
			'total_items' => $count,
			'total_pages' => ceil( $count / $this->rows_per_page ),
		] );
	}
}