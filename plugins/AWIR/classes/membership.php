<?php

class awir_memberships extends awir_singleton
{
	protected static $list;
	protected $pending_users = [];

	CONST TYPE_DEFAULT = 'default';
	CONST TYPE_SPECIAL = 'special';
	CONST TYPE_FREE = 'free';

	public function init()
	{
		$this->add_action('wp', 'check_member_area');
		$this->ajax_action('approve_member');
		$this->add_action('admin_menu', null, 99);
	}

	public function admin_menu()
	{
		$title = 'Pending';

		$users = get_users([
			'meta_key' => 'membership_pending',
			'meta_value' => '1',
		]);

		foreach ( $users as $user )
		{
			if ( self::is_member( $user->ID ) )
			{
				delete_user_meta( $user->ID, 'membership_pending' );
				continue;
			}
			$this->pending_users[] = $user;
		}

		if ( $this->pending_users )
		{

			$badge = sprintf(
				'<span class="update-plugins count-%01$d"><span class="plugin-count" id="pending_member_count">%01$d</span></span>',
				count( $this->pending_users )
			);
			$title .= $badge;

			global $menu;
			foreach ( $menu as $i => $item )
				if ( $item[2] == 'users.php' )
					$menu[ $i ][0] .= $badge;
		}

		add_users_page( 'Pending members', $title, 'edit_users', 'awir-pending', [$this,'pending_page'] );
	}

	public static function is_member_area()
	{
		if ( is_post_type_archive('tribe_events') )
			return false;

		if ( is_front_page() || is_404() )
			return false;

		if ( is_singular('ms_invoice') )
			return false;

		if ( AWIR::is_meeting_content() )
			return 'password';

		if ( !is_page() )
			return true;

		if ( is_page(['user','messages','account','account-2','profile','registration-complete']) )
			return true;

		return false;
	}

	public static function is_member( $user_id = null )
	{
		$user_id = self::current_user_if_null( $user_id );

		if ( !$user_id || !($user = get_userdata($user_id)) )
			return false;

		if ( $user->has_cap('manage_options') )
			return true;

		if ( !class_exists('MS_Factory') )
			return false;

		$member = MS_Factory::load( 'MS_Model_Member', $user_id );
		return $member->has_membership();
	}

	public static function user_type( $user_id = null )
	{
		do
		{
			if ( !class_exists('MS_Factory') )
				break;

			$user_id = self::current_user_if_null( $user_id );

			$member = MS_Factory::load( 'MS_Model_Member', $user_id );
			$func = function(){
				return $this->get_active_memberships();
			};
			$func = Closure::bind( $func, $member, $member );

			$membership = $func();
			if ( !$membership )
				break;

			$membership = reset( $membership );
			return self::get_type( $membership->id );
		}
		while(0);

		return self::TYPE_DEFAULT;
	}

	public function check_member_area()
	{
		if ( !is_admin() && is_singular('tribe_events') )
			awir::user_page_redirect( 'events' );

		if ( is_page('registration') )
			awir::user_page_redirect('register');

		$is_member = self::is_member();

		if ( !$is_member )
		{
			$is_member_area = self::is_member_area();
			if ( $is_member_area == 'password' && self::check_meeting_password() )
				return;

			if ( is_home() )
				awir::user_page_redirect( 'meeting-password' );

			if ( is_page('memberships') )
				awir::user_page_redirect( 'checkout' );

			if ( $is_member_area )
			{
				if ( is_user_logged_in() )
					awir::user_page_redirect( 'checkout' );

				elseif ( !empty( $_GET['login'] ) )
				{
					wp_redirect( sprintf(
						'%s?login_popup=%s',
						home_url(),
						urlencode( remove_query_arg( 'login' ) )
					) );
					exit;
				}
				else
					awir::user_page_redirect( 'become_member' );
			}
		}
		else
		{
			if ( is_page('memberships') )
				awir::user_page_redirect();

			if ( is_page('register') )
				awir::user_page_redirect('profile');
		}

		if ( is_page('registration-complete') )
			awir::user_page_redirect( 'user', ['registered'=>'1'] );
	}

	public function is_meeting_password( $password )
	{
		return !empty($password) && $password === get_field('meeting_content_password', 'options');
	}

	public static function check_meeting_password()
	{
		$token = $_COOKIE['meeting_token'] ?? null;
		$password = get_field('meeting_content_password', 'options');

		if ( empty($token) || empty($password) )
			return false;

		$salt = substr( $token, 0, 12 );
		$hash = substr( $token, 12 );
		return sha1( $salt.$password ) === $hash;
	}

	public function save_meeting_password( $password )
	{
		$salt = wp_generate_password( 12, false );
		$token = $salt.sha1( $salt.$password );
		setcookie('meeting_token', $token, 0, '/');
	}

	public static function member_since( $user_id = null, $format = 'F d, Y' )
	{
		$user_id = self::current_user_if_null( $user_id );

		if ( !($since = get_user_meta( $user_id, 'member_since', true )) || !is_numeric($since) )
		{
			if ( !self::is_member($user_id) )
				return 'Not a member';

			$since = time();
			update_user_meta( $user_id, 'member_since', $since );
		}

		return date( $format, $since );
	}

	public static function get_list( $cache = true )
	{
		if ( $cache && isset( self::$list ) )
			return self::$list;

		if ( !class_exists('MS_Model_Membership') )
			return [];

		self::$list = [];
		foreach ( MS_Model_Membership::get_public_memberships() as $membership )
			self::$list[ $membership->id ] = $membership;

		return self::$list;
	}

	/*
		Detect type of membership by id
		TYPE_DEFAULT - default "Member"
		TYPE_SPECIAL - Allied Specialty Member
		TYPE_FREE - Rheumatology Fellow-in-training ( Free )
	*/

	public static function get_type( $id )
	{
		$list = self::get_list();

		if ( !isset( $list[ $id ] ) )
			return self::TYPE_DEFAULT;

		$membership = $list[ $id ];
		if ( $membership->is_free() )
			return self::TYPE_FREE;

		static $spec;

		if ( !isset( $spec ) )
		{
			$spec = 0;
			$default = 0;
			foreach ( $list as $membership )
			{
				if ( $membership->is_free() )
					continue;

				if ( !$default )
					$default = $membership->id;

				else
				{
					$spec = $membership->id;
					break;
				}
			}
		}

		if ( $spec && $spec == $id )
			return self::TYPE_SPECIAL;

		return self::TYPE_DEFAULT;
	}

	public static function user_pending( $user_id = null )
	{
		$user_id = self::current_user_if_null( $user_id );

		update_user_meta( $user_id, 'membership_pending', 1 );

		// if ( !get_user_meta( $user_id, 'pending_ms_admin_alert_sent', true ) )
		// {
		// 	$email = get_field('pending_membership_email', 'options');
		// 	if ( !$email )
		// 		$email = get_bloginfo('admin_email');

		// 	$subject = get_field('pending_membership_subject', 'options');
		// 	$body = get_field('pending_membership_body', 'options');

		// 	$body = str_replace([
		// 		'%url%'
		// 	], [
		// 		admin_url('admin.php?page=awir-pending')
		// 	], $body );

		// 	$headers = [
		// 		'Content-Type: text/html; charset=utf-8'
		// 	];

		// 	if ( wp_mail( $email, $subject, $body, $headers ) )
		// 		update_user_meta( $user_id, 'membership_pending_admin_alert_sent', 1 );
		// }
	}

	public function ajax_approve_member()
	{
		$approved = false;
		while ( isset( $_GET['approve'], $_GET['_nonce'] ) )
		{
			if ( !wp_verify_nonce( $_GET['_nonce'], 'approve_member_'.$_GET['approve'] ) )
				break;

			$profile = new awir_profile( $_GET['approve'] );

			if ( !$profile->ID )
				break;

			//$membership_id = MS_Model_Membership::get_membership_id( $profile->membership );
			$membership = MS_Factory::load( 'MS_Model_Membership', $profile->membership );
			$member = MS_Factory::load( 'MS_Model_Member', $profile->ID );

			//var_dump( $profile, $membership, $member ); exit;

			if ( $member && $membership && !$member->has_membership() )
			{
				$member->add_membership( $membership->id );
				$member->is_member = true;
				$member->save();

				$approved = true;
			}

			break;
		}

		$url = admin_url('users.php') . '?page=awir-pending';
		if ( $approved )
			$url .= '&approved=1';

		wp_redirect( $url );
		exit;
	}

	public function pending_page()
	{
		$table = new pending_members_list_table( $this->pending_users );
		$table->prepare_items();
		?>

		<?php if ( !empty( $_GET['approved'] ) ): ?>

		<div class="notice notice-success"><p>
			Membership successfully approved.
		</p></div>

		<?php endif ?>

		<div class="wrap">
			<form method="get">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<?php $table->display() ?>
			</form>
		</div>
		<?php
	}
}

awir_memberships::getInstance();


require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
class pending_members_list_table extends WP_List_Table
{
	protected $users;

	public function __construct( $users )
	{
		$this->users = $users;
		parent::__construct([
			'singular'  => 'pending_member',
			'plural'    => 'pending_members',
			'ajax'      => false,
		]);
	}

	function column_default( $profile, $column )
	{
		return $profile->{$column};
	}

	function column_name( $profile )
	{
		ob_start();
		?>
		<a href="<?= admin_url('user-edit.php') ?>?user_id=<?=$profile->ID?>" target="_blank"><?=$profile->name?></a>
		<?php
	}

	function column_location( $profile )
	{
		$location = [ $profile->city, $profile->state, $profile->country ];
		$location = implode(', ', array_filter( $location ) );

		return $location;
	}

	function column_fellowship( $profile )
	{
		return sprintf(
			'Training Program: <b>%s</b><br>'.
			'Director: <b>%s</b>',
			esc_html( $profile->fellowship_name ),
			esc_html( $profile->fellowship_director )
		);
	}

	function column_confirmation( $profile )
	{
		$url = wp_get_attachment_url( $profile->fellowship_confirmation_letter );

		if($url !== false){
			?>
			<a href="<?= esc_attr( $url ) ?>" target="_blank"><?= esc_html( basename( $url ) ) ?></a>
			<?php
		}else{
			$confFiles = get_user_meta( $profile->ID, 'confirmation_letters', true ); 
			$list = explode(',', $confFiles);
			echo '<ol style="padding: 0; margin: 0; margin-left: 15px">';
			foreach($list as $attach_id){
				$url = wp_get_attachment_url( $attach_id );
				?>
				<li><a href="<?= esc_attr( $url ) ?>" target="_blank"><?= esc_html( basename( $url ) ) ?></a></li>
				<?php
			}	
			echo '</ol>';
		}
		?>
		<br>
		<a href="<?=admin_url('admin-ajax.php') ?>?action=awir_approve_member&amp;approve=<?= $profile->ID ?>&amp;_nonce=<?= wp_create_nonce('approve_member_'.$profile->ID)?>" onclick="return window.confirm('Are you sure?');">Approve</a>
		<?php
	}

	function column_registered( $profile )
	{
		if ( $profile->user )
			return $profile->user->user_registered;
	}

	function get_columns()
	{
		return [
			'name' => 'Full Name',
			'location' => 'Location',
			'fellowship' => 'Fellowship Details',
			'confirmation' => 'Confirmation Letter',
			'registered' => 'Reg Date',
		];
	}

	function prepare_items()
	{
		global $wpdb;

		$columns = $this->get_columns();
		$this->_column_headers = [ $columns, [], [] ];

		$this->items = [];

		foreach ( $this->users as $user )
			$this->items[] = new awir_profile( $user );

		$this->set_pagination_args( [
			'per_page' => 999,
			'total_items' => count( $this->items ),
			'total_pages' => 1,
		] );
	}

	function no_items()
	{
		echo 'There is no pendig members';
	}
}
