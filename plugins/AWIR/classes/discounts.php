<?php

class awir_discounts extends awir_singleton
{
	public function init()
	{
		$labels = array(
			'name' => __( 'Discounts', 'awir' ),
			'all_items' => __( 'Discounts', 'awir' ),
			'singular_name' => __( 'Discount', 'awir' ),
		);

		register_post_type(
			'discount',
			array(
				'has_archive' => false,
				'label' => __('Discounts','awir'),
				'labels' => $labels,
				'public' => false,
				'show_ui' => true,
				'show_in_nav_menus' => false,
				'show_in_admin_bar' => false,
				//'menu_icon' => '', //url or data-url or dashicon-xxx
				'supports' => array( 'title' ),
				'capability_type' => ['discount','discounts'],
				//'map_meta_cap' => true,
				'capabilities' => array(
					'edit_post'          => 'manage_options',
					'read_post'          => 'manage_options',
					'delete_post'        => 'manage_options',
					'edit_posts'         => 'manage_options',
					'edit_others_posts'  => 'manage_options',
					'delete_posts'       => 'manage_options',
					'delete_others_posts'=> 'manage_options',
					'publish_posts'      => 'manage_options',
					'read_private_posts' => 'manage_options',
				),
				'rewrite' => false,
			)
		);
	}

	public static function get_list( $user_id = null )
	{
		if ( !class_exists('MS_Factory') )
			return [];

		$user_id = self::current_user_if_null( $user_id );
		$member = MS_Factory::load( 'MS_Model_Member', $user_id );

		$all_discounts = get_posts([
			'post_type' => 'discount',
			'numberposts' => -1,
			'post_status' => 'publish',
		]);
		$user_discounts = [];

		foreach ( $all_discounts as $post )
		{
			$mss = get_post_meta( $post->ID, 'memberships', true );
			if ( !is_array( $mss ) )
				continue;

			foreach ( $mss as $ms_id )
			{
				if ( !$member->has_membership( $ms_id ) )
					continue;

				$user_discounts[ $post->ID ] = [
					'title' => $post->post_title,
					'code' => get_post_meta( $post->ID, 'code', true ),
					'description' => get_post_meta( $post->ID, 'description', true ),
				];
				break;
			}
		}

		return $user_discounts;
	}
}

awir_discounts::getInstance();
