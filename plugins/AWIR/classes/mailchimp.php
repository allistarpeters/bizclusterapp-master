<?php

class awir_mailchimp extends awir_singleton
{
	private static $saved_key;

	public function __construct()
	{
		parent::__construct();
		$this->add_filter('acf/load_field/name=members_list_id', 'mailchimp_list_choices');
		$this->add_filter('acf/load_field/name=mailchimp_camapaign', 'mailchimp_campaign_choices');
		$this->add_filter('pre_update_option_mc4wp_mailchimp_list_ids', 'reset_mailchimp_lists' );
	}

	public function init()
	{
		$this->cron_event('fetch_campaigns', 'daily');
		$this->cron_event('subscribe_members', 'hourly');

		if ( defined('DOING_AJAX') && DOING_AJAX )
			return;

		if ( awir::is_member() && !get_user_meta( get_current_user_id(), 'member_subscribe_attempt', true ) )
			self::subscribe_member();
	}

	public static function custom_api_key( $api, $key = null )
	{
		if ( $key == null )
			$key = get_field('archive_mailchimp_api_key', 'options');

		if ( !$key )
			return;

		$client = $api->get_client();
		$func = function( $api_key ){
			$saved_key = $this->api_key;
			$this->api_key = $api_key;

			$dash_position = strpos( $api_key, '-' );
			if( $dash_position !== false ) {
				$this->api_url = str_replace( '//api.', '//' . substr( $api_key, $dash_position + 1 ) . ".api.", 'https://api.mailchimp.com/3.0/' );
			}

			return $saved_key;
		};
		$func = Closure::bind( $func, $client, $client );
		self::$saved_key = $func( $key );
	}

	public static function restore_key( $api )
	{
		self::custom_api_key( $api, self::$saved_key );
	}

	public function reset_mailchimp_lists( $value )
	{
		self::set_option( 'mailchimp_lists', null );
		$this->fetch_campaigns();
		return $value;
	}

	public function mailchimp_list_choices( $field )
	{
		if ( !function_exists('mc4wp') || !($api = mc4wp('api')) )
			return;

		$choices = self::get_option('mailchimp_lists');
		if ( is_array( $choices) )
		{
			$field['choices'] = $choices;
			return $field;
		}

		try {
			$lists = $api->get_lists();
		} catch ( Exception $e ) {
			return $field;
		}

		foreach ( $lists as $list )
			$field['choices'][ $list->id ] = $list->name;

		self::set_option('mailchimp_lists', $field['choices'] );

		return $field;
	}

	public function subscribe_members()
	{
		//!get_user_meta( get_current_user_id(), 'member_subscribe_attempt', true )
		$users = get_users([
			'meta_query' => [
				'relation' => 'AND',
				[
					'key' => 'awir_imported',
					'compare' => 'EXISTS',
				],
				[
					'key' => 'member_subscribe_attempt',
					'compare' => 'NOT EXISTS',
				],
			],
		]);

		foreach ( $users as $user )
			if ( awir::is_member( $user->ID ) )
				self::subscribe_member( $user->ID );

	}

	public static function subscribe_member( $user_id = null )
	{
		$profile = new awir_profile( $user_id );
		if ( !$profile->ID || !is_email( $profile->email ) )
			return false;

		if ( !$list_id = get_option( 'options_members_list_id' ) )
		{
			self::log( 'list not set' );
			return false;
		}

		if ( !function_exists('mc4wp') || !($api = mc4wp('api')) )
		{
			self::log( 'can\'t init mc4wp api' );
			return false;
		}

		$data = [
			'email_address' => $profile->email,
			'email_type' => 'html',
			'status' => 'subscribed',
			'merge_fields' => [
				'FNAME' => $profile->first_name,
				'LNAME' => $profile->last_name,
			],
		];

		update_user_meta( $profile->ID, 'member_subscribe_attempt', 1 );

		try {
			$existing_member_data = $api->get_list_member( $list_id, $profile->email );

			if ( (string)$existing_member_data->status === 'unsubscribed' )
				return 0;

			$exists = true;
		}
		catch( \MC4WP_API_Resource_Not_Found_Exception $e )
		{
			$exists = false;
		}

		if( !$exists )
		{
			try
			{
				$api->add_list_member( $list_id, $data );
				update_user_meta( $profile->ID, 'member_subscribed', 1 );
			}
			catch ( Exception $e ) {
				self::log( $e );
				return false;
			}
		}
		/*
		try
		{
			$data

			$api->update_list_member( $list_id, $email, $data );
		}
		catch ( Exception $e ) {
			self::log( $e );
			return 0;
		}
		*/

		return true;
	}

	public function mailchimp_campaign_choices( $field )
	{
		$field['choices'] = [];
		foreach ( self::get_campaigns() as $id => $campaign )
			$field['choices'][ $id ] = $campaign['title'];

		return $field;
	}

	public static function get_campaigns()
	{
		$campaigns = self::get_option('mailchimp_campaigns');

		if ( !is_array( $campaigns ) )
			$campaigns = [];

		return $campaigns;
	}

	public function fetch_campaigns()
	{
		if ( !function_exists('mc4wp') || !($api = mc4wp('api')) )
			return false;

		self::custom_api_key( $api );

		try {
			$raw_campaigns = $api->get_campaigns([
				//'fields' => 'id,long_archive_url,send_time,settings.title',
				'count' => 200,
				'status' => 'sent',
				'sort_field' => 'send_time',
				'sort_dir' => 'DESC',
			]);
			self::restore_key( $api );
		}
		catch ( Exception $e )
		{
			self::log( $e->getMessage() );
			self::restore_key( $api );
			return false;
		}

		$campaigns = array();
		foreach ( $raw_campaigns->campaigns as $campaign )
		{
			if ( preg_match('/\(\s*copy[\s\d]*\)/i', $campaign->settings->title ) )
				continue;

			if ( preg_match('/^(.*\s)?test(\s.*)?$/i', $campaign->settings->title ) )
				continue;

			$campaigns[ $campaign->id ] = [
				'title' => $campaign->settings->title ?: $campaign->settings->subject_line,
				'url' => $campaign->long_archive_url,
				'time' => strtotime($campaign->send_time),
			];
		}

		self::set_option('mailchimp_campaigns', $campaigns );
		return true;
	}
}

awir_mailchimp::getInstance();