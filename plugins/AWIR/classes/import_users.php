<?php

class awir_import_users extends awir_base
{
	public function __construct()
	{
		if ( !empty( $_GET['reset_passwords'] ) )
			$this->reset_passwords();

		else
			$this->import( __DIR__.'/import.csv' );

		exit;
	}

	private function reset_passwords()
	{
		$users = get_users([
			'meta_query' => [
				[
					'key' => 'awir_imported',
					'compare' => 'EXISTS',
				],
			],
		]);

		header('Content-Disposition: attachment; filename="list.csv"');
		header('Content-Type: text/csv');
		$f = fopen('php://output', 'w');

		fputcsv( $f, ['Email', 'First Name', 'Last Name', 'link'] );

		foreach ( $users as $user )
		{
			$key = get_password_reset_key( $user );
			$link = awir::user_page_url('reset-password', [ 'key' => $key, 'login' => $user->user_login ]);

			fputcsv( $f, [ $user->user_email, $user->first_name, $user->last_name, $link ] );
		}

		exit;
	}

	private function import( $file )
	{
		echo '<pre>';

		if ( !$f = fopen( $file, 'r' ) )
		{
			echo 'File not readable';
			return;
		}

		$titles = fgetcsv( $f, 0, "\t" );

		foreach ( $titles as $i => $title )
		{
			if ( $group = $this->is_group_tag( $title ) )

				$title = $group;

			else
			{
				$title = strtolower( $title );
				$title = preg_replace( '/[^a-z\d]+/', '_', $title  );
				$title = trim( $title, '_' );
			}

			$titles[ $i ] = $title;
		}

		$data = [];

		while ( ($row = fgetcsv( $f, 0, "\t" )) !== false )
		{
			$user = [];
			foreach ( $row as $i => $val )
				if ( is_array( $titles[$i] ) )
				{
					if ( !empty( $val ) )
						$user[ $titles[$i][0] ][] = $titles[$i][1];
				}
				else
					$user[ $titles[$i] ] = $val;

			$data[] = $user;
		}

		//var_dump( $data ); exit;

		foreach ( $data as $i => $user )
		{
			echo "\nRow $i: ";
			$user_id = $this->create_user( $user['email'], $login );

			if ( !$user_id )
				continue;

			$this->update_fields( $user_id, $user );
		}

		echo "\nImport completed. Do not forget to remove import file\n";
	}

	private function create_user( $email, &$login )
	{
		if ( !is_email( $email ) )
		{
			echo "invalid email\n";
			return false;
		}

		if ( $user_id = email_exists( $email ) )
		{
			echo "user exists #$user_id\n";
			return $user_id;
		}

		$login = awir_profile::make_login( $email );

		$userdata = [
			'user_login' => $login,
			'user_email' => $email,
			'user_pass' => null,
		];

		$user_id = wp_insert_user( $userdata );

		if ( is_wp_error( $user_id ) )
		{
			printf( "can't create user - %s\n", $user_id->get_error_message() );
			return false;
		}
		else
			echo "user #$user_id created\n";

		return $user_id;
	}

	private function update_fields( $user_id, $data )
	{
		/*
			'membership_status' => 'Lapsed',Confirmed,Canceled
			membership_type' => 'Member',Rheumatology Fellow-in-training,Allied Specialty Member
		*/
		update_user_meta( $user_id, 'awir_imported', 1 );

		static $fields = [
			'member_number',
			'member_since',
			'first_name',
			'last_name',
			'address_line_1' => 'address',
			'address_line_2',
			'city',
			'us_state_canadian_province' => 'state',
			//'postal_code' => 'zip_code',
			'country',
			'cellphone' => 'phone',
			'date_of_birth' => 'birth_date',
			'gender',
			'race_ethnicity',
			'medical_degree' => 'med_degree',
			'acr_member',
			'interests',
			'activities',
			'video_and_media_consent_a' => 'media_consent',
			'membership_type', 'membership_status',
		];

		static $direct_meta_keys = [
			'member_number', 'member_since', 'membership_status',
		];

		foreach ( $fields as $import_key => $key )
		{
			if ( is_numeric($import_key) )
				$import_key = $key;

			if ( empty( $data[ $import_key ] ) )
				continue;

			$value = $data[ $import_key ];

			if ( $key == 'birth_date' )
			{
				if ( preg_match( '~^(\d+)/(\d+)/(\d+)$~', $value, $m ) )
					$value = sprintf( '%04d%02d%02d', $m[3], $m[1], $m[2] );
				else
					continue;
			}

			if ( isset( self::CONVERT_FIELDS[ $key ] ) )
			{
				if ( isset( self::CONVERT_FIELDS[ $key ][ $value ] ) )
					$value = self::CONVERT_FIELDS[ $key ][ $value ];

				elseif ( isset( self::CONVERT_FIELDS[ $key ]['_default'] ) )
					$value = self::CONVERT_FIELDS[ $key ]['_default'];
				else
					continue;
			}

			if ( $key == 'membership_type' )
			{
				$membership_id = MS_Model_Membership::get_membership_id( $value );
				echo "membership: $membership_id ( $value )\n";

				$member = MS_Factory::load( 'MS_Model_Member', $user_id );

				if ( $membership_id && !$member->has_membership() )
				{
					$member->add_membership( $membership_id );
					$member->is_member = true;
					$member->save();
				}
			}

			if ( $key == 'member_since' )
				$value = strtotime( $value );

			if ( in_array( $key, $direct_meta_keys ) )
			{
				if ( $value )
					update_user_meta( $user_id, $key, $value );

				continue;
			}

			update_field( $key, $value, 'user_'.$user_id );
		}

		$display_name = [ $data['first_name'], $data['last_name'] ];
		$display_name = implode(' ', array_filter( $display_name ) );

		wp_update_user([
			'ID' => $user_id,
			'display_name' => $display_name,
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
		]);
	}

	private function is_group_tag( $field )
	{
		$fields = [
			'interests' => [
				'Advocacy ?', 'Business Development ?', 'Clinical Trials ?',
				'Legal/ Consultation Services' => 'Legal/ Consultation Servi',
				'Mentoring ?', 'Musculoskeletal Imaging ?', 'Networking ?', 'Practice Admnistration ?', 'Public/ Health Policy ?', 'Research- Basic Science ?', 'Teaching ?'
			],
			'activities' => [
				'Administration', 'Advocacy',
				'Legal/ Consultation Services' => 'Legal/ Insurance Consults',
				'Mentoring', 'Musculoskeletal Imaging', 'Networking', 'Patient Care', 'Pharmaceutical Industry', 'Practice Administration', 'Program Director', 'Public/ Health Policy', 'Research- Basic Science', 'Research- Clinical', 'Retired', 'Teaching', 'Trainee Supervision'
			],
		];

		foreach ( $fields as $group => $list )
			if ( ( $i = array_search( (string)$field, $list, true  ) ) !== false )
			{
				$name = is_int($i) ? $list[ $i ] : $i;
				if ( substr( $name, -2 ) === ' ?' )
					$name = substr( $name, 0, -2 );

				$term = get_term_by('name', $name, 'board_tag');
				if ( $term && !is_wp_error($term) )

					$term_id = $term->term_id;

				elseif ( !is_wp_error( $info = wp_insert_term( $name, 'board_tag' ) ) )

					$term_id = $info['term_id'];

				else
				{
					printf("\nError add term $name: %s", $info->get_error_message() );
					$term_id = 0;
				}

				return [ $group, $term_id ];
			}

		return false;
	}

	const CONVERT_FIELDS = [
		'state' => [
			'AL' => 'Alabama',
			'AK' => 'Alaska',
			'AS' => 'American Samoa',
			'AZ' => 'Arizona',
			'AR' => 'Arkansas',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DE' => 'Delaware',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'HI' => 'Hawaii',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'IA' => 'Iowa',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'ME' => 'Maine',
			'MD' => 'Maryland',
			'MA' => 'Massachusetts',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MS' => 'Mississippi',
			'MO' => 'Missouri',
			'MT' => 'Montana',
			'NE' => 'Nebraska',
			'NV' => 'Nevada',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NY' => 'New York',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VT' => 'Vermont',
			'VA' => 'Virginia',
			'WA' => 'Washington',
			'WV' => 'West Virginia',
			'WI' => 'Wisconsin',
			'WY' => 'Wyoming',
			'_default' => 'Other',
		],
		'gender' => [
			'F' => 'Female',
			'M' => 'Male',
		],
		'country' => [
			'United States' => 'United States of America',
			'Canada' => 'Canada',
			'Mexico' => 'Mexico',
			'Saint Lucia' => 'St. Lucia',
			'_default' => 'United States of America',
		],
	];
}

