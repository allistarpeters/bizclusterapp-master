<?php

require_once 'db_migration.php';

abstract class awir_base
{
	public static $debug = null;

	const NAME = 'awir';

	protected function __construct()
	{
		if ( defined('AWIR_DEBUG') )
			static::$debug = AWIR_DEBUG;

		elseif ( static::$debug === null && defined('WP_DEBUG') )
			static::$debug = WP_DEBUG;

		if ( method_exists( $this, 'init' ) )
			$this->add_action( 'init' );
	}

	public static function log()
	{
		if ( !static::$debug )
			return;

		$log = func_get_args();
		error_log( print_r( $log, true ) );
	}

	protected function add_action( $hook, $method = null, $priority = 10, $accepted_args = 1 )
	{
		if ( $method === null )
			$method = $hook;

		add_action( $hook, array( $this, $method ), $priority, $accepted_args );
	}

	protected function add_filter( $hook, $method = null, $priority = 10, $accepted_args = 1 )
	{
		if ( $method === null )
			$method = $hook;

		add_filter( $hook, array( $this, $method ), $priority, $accepted_args );
	}

	protected function ajax_action( $action, $method = null )
	{
		if ( $method === null )
			$method = 'ajax_'.$action;

		add_action('wp_ajax_awir_'.$action, array( $this, $method ) );
	}

	protected function ajax_nopriv_action( $action, $method = null )
	{
		if ( $method === null )
			$method = 'ajax_'.$action;

		add_action('wp_ajax_nopriv_awir_'.$action, array( $this, $method ) );
	}

	protected static function set_option( $option, $value, $autoload = false )
	{
		$option = self::NAME.'_'.$option;

		if ( $value === null )
			delete_option( $option, $value );

		else
		{
			add_option( $option, $value, '', $autoload ? 'yes' : 'no' );
			update_option( $option, $value );
		}
	}

	protected static function get_option( $option, $default = null )
	{
		$option = self::NAME.'_'.$option;

		return get_option( $option, $default );
	}

	protected static function json_response( $data )
	{
		header( "Content-Type: application/json; charset=utf-8" );
		echo json_encode( $data );
		exit;
	}

	protected static function ajax_error( $message = '', $info = null )
	{
		$response = array(
			'success' => false,
			'message' => $message,
		);
		if ( $info !== null )
			$response['info'] = $info;

		self::json_response( $response );
	}

	protected static function ajax_success( $data = null )
	{
		$response = array(
			'success' => true,
			'data' => $data,
		);

		self::json_response( $response );
	}

	protected static function current_user_if_null( $user_id = null )
	{
		if ( $user_id === null )
			$user_id = get_current_user_id();

		return $user_id;
	}

	protected function cron_event( $action, $frequency, $method=null )
	{
		if ( $method === null )
			$method = $action;

		$action = self::NAME.'_'.$action;

		add_action( $action, [ $this, $method ] );

		if ( !is_admin() || (defined('DOING_AJAX') && DOING_AJAX) || (defined('DOING_CRON') && DOING_CRON) )
			return;

		if ( !wp_next_scheduled( $action ) )
			wp_schedule_event( time(), $frequency, $action );
	}
}

abstract class awir_singleton extends awir_base
{
	private static $instances = array();

	protected function __construct() {
		parent::__construct();
	}

	protected function __clone() {}

	public function __wakeup()
	{
		$class = get_called_class();
		throw new Exception("Cannot unserialize singleton $class");
	}

	public static function getInstance()
	{
		$class = get_called_class();

		if ( !isset(self::$instances[$class]) )
			self::$instances[$class] = new static;

		return self::$instances[$class];
	}
}

class awir_init extends awir_singleton
{
	public function __construct()
	{
		parent::__construct();
		$this->add_filter('cron_schedules');
	}

	public function cron_schedules( $schedules )
	{
		$schedules['often'] = [
			'interval' => 10 * MINUTE_IN_SECONDS,
			'display' => "Every 10 minutes",
		];

		return $schedules;
	}
}

awir_init::getInstance();
