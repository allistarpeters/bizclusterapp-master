<?php

require_once 'membership.php';

class awir_profile extends awir_base
{
	protected $data = [
		'email' => '',
		'password' => '',

		'new_password' => '',
		'old_password' => '',
		'email_verify' => '',
		'password_verify' => '',
		'nonce' => '',

		'membership' => '',

		'first_name' => '',
		'last_name' => '',
		'country' => '',
		'state' => '',

		'city' => '',
		'address' => '',
		'address_line_2' => '',
		'phone' => '',
		'birth_date' => '',
		'gender' => '',
		'race_ethnicity' => '',
		'med_degree' => '',

		'allied_speciality' => '',

		'acr_member' => '',

		'fellowship_name' => '',
		'fellowship_director' => '',
		'fellowship_confirmation_letter' => '',
		'fellowship_confirmation_files' => '',

		'interests' => [],
		'activities' => [],
		'media_consent' => '',

		'avatar' => '',
	];

	const SPEC_FIELDS = [
		'email_verify' => 'email',
		'new_password' => 'password',
		'password_verify' => 'password',
		'nonce' => '',
	];

	const CORE_FIELDS = [
		'ID' => 'ID',
		'email' => 'user_email',
		'password' => 'user_pass',
	];

	const ACF_KEYS = [
		'email' => 'field_5afbf65de55a5',
		'password' => 'field_5afbf6c7e55a6',
		'first_name' => 'field_5afad8c05cd51',
		'last_name' => 'field_5afad9425cd53',
	];

	const MEMBER_TYPE_FIELDS = [
		awir_memberships::TYPE_SPECIAL => ['allied_speciality'],
		awir_memberships::TYPE_FREE => ['fellowship_name', 'fellowship_director', 'fellowship_confirmation_letter'],
	];

	const MAX_FILE_SIZE = 1000 * 1024 * 5; // 5mb
	const MAX_CONF_FILES = 5; // max count of uploaded confirmation letters
	const AVAILABLE_CONF_MIME_TYPES = array( 
		// text
        'txt' => 'text/plain',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
		
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',

        // adobe
        'pdf' => 'application/pdf',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',

        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
	);

	public $form_prefix = 'profile';

	protected $errors = [];
	protected $user = null;
	protected $display_name;

	public function __construct( $user = null )
	{
		parent::__construct();

		$user = self::current_user_if_null( $user );

		if ( $user )
			$this->load( $user );
	}

	protected function load( $user )
	{
		if ( $user instanceof WP_User )
			$this->user = $user;

		else
			$this->user = get_userdata( $user );

		if ( !$this->user )
			return;

		foreach ( $this->data as $key => $dumm )
		{
			if ( $key == 'password' || isset( self::SPEC_FIELDS[ $key ] ) )
				continue;

			if ( isset( self::CORE_FIELDS[ $key ] ) )
				$this->data[ $key ] = $this->user->{ self::CORE_FIELDS[ $key ] };

			else
			{
				$value = get_field( $key, 'user_'.$this->user->ID, false );
				if ( gettype( $value ) == gettype( $dumm ) )
					$this->data[ $key ] = $value;
			}
		}
	}

	private function insert_tmp_conf_letter($attachmentID, $hash, $expireDatetime){
		if(!$attachmentID || !$expireDatetime)
			return false;

		global $wpdb;
			
		$table_name = $wpdb->prefix . $this->__get('CONF_LETTERS_TABLE');
		return $wpdb->insert(
			$table_name,
			array( 
				'attachment_id' => $attachmentID, 
				'user_token' => $hash,
				'expire_date' => $expireDatetime 
			),
			array( '%d', '%s', '%s' )
		);
	}

	/** Check and create if not exists Conf letters table */
	private function check_conf_attach_table(){
		global $wpdb;
			
		$table_name = $wpdb->prefix . $this->__get('CONF_LETTERS_TABLE');

		// if table not in database. Create new table
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE {$table_name} (
				id bigint(20) unsigned NOT NULL auto_increment,
				attachment_id int(10) NOT NULL default '0',
				user_token varchar(32) NOT NULL default '',
				expire_date datetime NOT NULL default '0000-00-00 00:00:00',
				PRIMARY KEY (id),
				UNIQUE KEY attachment_id (attachment_id)
			) {$charset_collate};";

			dbDelta( $sql );
		}
	}

	private function delete_expired_tmp_conf_letters(){
		global $wpdb;
			
		$table_name = $wpdb->prefix . $this->__get('CONF_LETTERS_TABLE'); // get custom table name
		$curTimestamp = time();
		$expiredList = $wpdb->get_results("SELECT attachment_id FROM $table_name WHERE UNIX_TIMESTAMP(expire_date) < $curTimestamp LIMIT 20");
	
		foreach ($expiredList as $expiredItem) {
			$this->delete_conf_letter($expiredItem->attachment_id);
		}
	}

	private function delete_conf_letter($attachment_id, $delete_file = true){
		global $wpdb;
			
		$table_name = $wpdb->prefix . $this->__get('CONF_LETTERS_TABLE'); // get custom table name

		if($delete_file === true)
			wp_delete_attachment( $attachment_id, true ); // delete attachment

		$wpdb->delete( $table_name, array( 'attachment_id' => $attachment_id ), array( '%d' ) ); // delete info in DB
	}

	/** Return array of ID uploaded files on registration for current user by he token (ip + user agent) */
	public function get_conf_letters_id_list(){
		global $wpdb;

		$table_name = $wpdb->prefix . $this->__get('CONF_LETTERS_TABLE'); // get custom table name
		$data = [];

		// create table if its not exists
		$this->check_conf_attach_table();

		// clean expired
		$this->delete_expired_tmp_conf_letters();

		// get user hash
		$userHash = $this->get_user_token();

		// get list of attachments
		$list = $wpdb->get_results("SELECT attachment_id FROM $table_name WHERE user_token = '$userHash' LIMIT " . self::MAX_CONF_FILES);
		
		if($list !== NULL && !empty($list)){
			foreach($list as $el){
				$data[] = $el->attachment_id;
			}
		}

		return $data;
	}

	/** Return md5 hash based on user IP address and user agent */
	private function get_user_token(){
		$ipaddress = '';
		$browser = $_SERVER['HTTP_USER_AGENT'];

		// retrive ip address
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';

		return md5($ipaddress . $browser);
	}

	public function process_form()
	{
		if ( empty($_POST[ $this->form_prefix ]) || !is_array($_POST[ $this->form_prefix ]) )
			return false;

		$data = stripslashes_deep( $_POST[ $this->form_prefix ] );

		if ( !$this->nonce_check( $data ) )
			return false;

		$this->fill( $data );

		if(!isset($data['interests']))
			$this->data['interests'] = [];
		if(!isset($data['activities']))
			$this->data['activities'] = [];

		$this->upload_field('avatar');

		// create table for tmp conf letters if its not exists
		$this->check_conf_attach_table();

		$isValid = $this->validate();
		$confFiles = $_FILES[ 'confirmation_letters' ];
		

		// empty($confFiles) && !$_COOKIE['fellowship_confirmation_files']
		// if( count($attachList) > 0 && empty($confFiles['name'][0]) ){
		// 	// no validation
		// }elseif(empty($confFiles['name'][0])){

		// upload files if its Free member type and file validation is OK
		if ( $this->member_type == awir_memberships::TYPE_FREE && !empty($confFiles['name'][0]) && !isset($this->errors[ 'confirmation_letters' ]) ){
			// delete old files
			$attachList = $this->get_conf_letters_id_list();

			foreach($attachList as $attachment_id){
				$this->delete_conf_letter((int)$attachment_id);
			}

			// upload files
			$attachment_ids = self::upload_multiple('confirmation_letters');

			// save info about attachments
			$expireUnixTimestamp = time() + (86400 * 5); // expire after 5 days
			$expireDatetime = date('Y-m-d H:i:s', $expireUnixTimestamp); 

			$userHash = $this->get_user_token();

			// add data to database
			if(is_array($attachment_ids)){
				foreach($attachment_ids as $attachment_id){
					$this->insert_tmp_conf_letter($attachment_id, $userHash, $expireDatetime);
				}
			}else{
				$this->insert_tmp_conf_letter($attachment_id, $userHash, $expireDatetime);
			}

			// delete old attachments
			$this->delete_expired_tmp_conf_letters();
		}

		if( !$isValid )
			return false;

		return $this->save();
	}

	public function fill( $data )
	{
		foreach ( $this->data as $key => $dumm )
		{
			if ( isset( $data[ $key ] ) && gettype( $data[ $key ] ) == gettype( $dumm ) )
				$this->data[ $key ] = $data[ $key ];

			elseif ( $key == 'birth_date' && isset($data['year'], $data['month'], $data['day']) )
				$this->data['birth_date'] = sprintf( '%04d%02d%02d', $data['year'], $data['month'], $data['day'] );
		}
	}

	public function upload_field( $field )
	{
		$input = $this->form_prefix.'_'.$field;

		if ( $field == 'avatar' )
			$attachment_id = self::upload_avatar( $input, $error);

		else
			$attachment_id = self::upload( $input, $error);

		if ( $attachment_id )
			$this->data[ $field ] = $attachment_id;

		elseif ( $error )
			 $this->error[ $input ] = $error;
	}

	public static function upload_avatar( $input, &$error = null )
	{
		$func = function( $file ) {
			if ( empty( $file['error'] ) && !preg_match( '~^image/~i', $file['type'] ) )
			{
				$file['error'] = 'File is not an image';
				@unlink( $file['file'] );
			}

			return $file;
		};
		add_filter( 'wp_handle_upload', $func );

		$rez = self::upload( $input, $error );
		remove_filter( 'wp_handle_upload', $func );

		return $rez;
	}

	public static function upload_multiple($input){
		self::log( $input, $_FILES );

		require_once ABSPATH.'wp-admin/includes/file.php';
		require_once ABSPATH.'wp-admin/includes/media.php';
		require_once ABSPATH.'wp-admin/includes/image.php';

		$attachment_ids = [];
		$files = $_FILES[ $input ];

		// Get the path to the upload directory.
		$wp_upload_dir = wp_upload_dir();

		foreach($files['name'] as $key => $value){
			if ($files['name'][$key]) { 
				$file = array( 
					'name' => $files['name'][$key],
					'type' => $files['type'][$key], 
					'tmp_name' => $files['tmp_name'][$key], 
					'error' => $files['error'][$key],
					'size' => $files['size'][$key]
				); 
				// $_FILES = array($input => $file); 
				// foreach ($_FILES as $file => $array) {   
				// 	$attachment_ids[] = self::upload($file); 
				// }

				$upload_overrides = array( 'test_form' => false );
                $upload = wp_handle_upload($file, $upload_overrides);


                // $filename should be the path to a file in the upload directory.
                $filename = $upload['file'];

                // The ID of the post this attachment is for.
                $parent_post_id = 0;

                // Check the type of tile. We'll use this as the 'post_mime_type'.
				$filetype = wp_check_filetype( basename( $filename ), null );

                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );

                // Insert the attachment.
                $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

                // Generate the metadata for the attachment, and update the database record.
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );
				
				$attachment_ids[] = $attach_id;
			} 
		}
		return $attachment_ids;
	}

	public static function upload( $input, &$error = null )
	{
		self::log( $input, $_FILES );

		if ( empty($_FILES[ $input ]) || empty($_FILES[ $input ]['tmp_name']) )
			return false;

		require_once ABSPATH.'wp-admin/includes/file.php';
		require_once ABSPATH.'wp-admin/includes/media.php';
		require_once ABSPATH.'wp-admin/includes/image.php';

		$attachment_id = media_handle_upload( $input, 0 );

		self::log( $attachment_id );

		if ( is_wp_error( $attachment_id ) )
		{
			$error = $attachment_id->get_error_message();
			return false;
		}

		return $attachment_id;
	}

	public function validate()
	{
		$required = [
			'email', 'first_name', 'last_name', 'country', 'city', 'address', 'phone', 'birth_date', 'gender', 'race_ethnicity', 'med_degree', 'acr_member', 'media_consent'
		];

		if ( !awir::is_member( $this->ID ) && !$this->choice_exists('membership') )
			$this->errors['membership'] = 'Please select a Membership';

		if ( $this->member_type == awir_memberships::TYPE_SPECIAL )
		{
			if ( !$this->choice_exists('allied_speciality') )
				$this->errors['allied_speciality'] = 'Please select a speciality';
		}

		elseif ( $this->member_type == awir_memberships::TYPE_FREE )
		{
			$required[] = 'fellowship_name';
			$required[] = 'fellowship_director';
			// $required[] = 'fellowship_confirmation_letter';


			$confFiles = $_FILES[ 'confirmation_letters' ];
			$attachList = $this->get_conf_letters_id_list(); // get list of exists files

			if( count($attachList) > 0 && empty($confFiles['name'][0]) ){
				// no validation
			}elseif(empty($confFiles['name'][0])){
				$this->errors[ 'confirmation_letters' ] = 'This field is required';
			}elseif(count($confFiles['tmp_name']) > self::MAX_CONF_FILES){
				$this->errors[ 'confirmation_letters' ] = 'Max uploaded files - ' . self::MAX_CONF_FILES;
			}else{
				foreach($confFiles['size'] as $key => $size){
					if($size > self::MAX_FILE_SIZE){
						$this->errors[ 'confirmation_letters' ] = 'Max file size is ' . (self::MAX_FILE_SIZE / 1024 / 1000 ) . 'mb';
						break;
					}else{
						// check file type
						$idx = explode( '.', $confFiles['name'][$key] );
						$count_explode = count($idx);
						$idx = strtolower($idx[$count_explode-1]);

						// if not available type
						if( !isset(self::AVAILABLE_CONF_MIME_TYPES[$idx]) ){
							$this->errors[ 'confirmation_letters' ] = '".' . $idx . '" format is not supported';
							break;
						}
					}
				}
			}
		}

		foreach ( $required as $field )
			if ( !$this->{$field} )
				$this->errors[ $field ] = 'This field is required';

		if ( !$this->email )
			$this->errors['email'] = 'Please type your email address.';

		elseif ( !is_email( $this->email ) )
			$this->errors['email'] = 'The email address isn\'t correct.';

		elseif ( ($user_id = email_exists($this->email)) && $user_id != $this->ID )
			$this->errors['email'] = 'This email is already registered, please choose another one.';

		if ( (!$this->ID || $this->user->user_email != $this->email ) && $this->email != $this->email_verify )
			$this->errors['email_verify'] = 'The verify email is not match.';

		if ( !$this->ID)
		{
			if ( !$this->password )
				$this->errors['password'] = 'Please enter your password.';

			elseif ( strlen(trim($this->password)) < 8 )
				$this->errors['password'] = 'Please use at least 8 symbols.';

			elseif ( $this->password != $this->password_verify )
				$this->errors['password_verify'] = 'Passwords not match.';
		}
		elseif ( $this->new_password || $this->user->user_email != $this->email )
		{
			if ( !$this->is_my_password( $this->password ) )
				$this->errors['password'] = 'The old Password incorrect';

			if ( $this->new_password != $this->password_verify )
				$this->error['password_verify'] = 'Passwords not match.';
		}

		if ( !$this->first_name )
			$this->errors['first_name'] = 'Please enter your First Name.';

		if ( !$this->last_name )
			$this->errors['last_name'] = 'Please enter your Last Name.';

		if ( !$this->choice_exists('country') )
			$this->errors['country'] = 'Please select a Country.';

		if ( $this->choice_exists('country') && $this->country === 'United States of America' && !$this->choice_exists('state') )
			$this->errors['state'] = 'Please select a State.';

		// if ( !$this->choice_exists('state') )
		// 	$this->errors['state'] = 'Please select a State.';

		if ( $this->state && !$this->choice_exists('state') )
			$this->data['state'] = '';

		if ( !$this->city )
			$this->errors['city'] = 'Please enter a City';

		if ( !$this->address )
			$this->errors['address'] = 'Please enter an Address';

		if ( !$this->phone )
			$this->errors['phone'] = 'Please enter your Phone Number';
		elseif( strlen($this->phone) < 6 )
			$this->errors['phone'] = 'Should be at least 6 chars';
		elseif( !preg_match("/^\d{3,}$/", $this->phone) )
			$this->errors['phone'] = 'Only numbers available';

		return empty( $this->errors );
	}

	public function save()
	{
		if ( !$this->ID )
		{
			$user_id = wp_insert_user([
				'user_email' => $this->email,
				'user_login' => self::make_login( $this->email ),
				'user_pass' => $this->password,
				'display_name' => $this->name,
			]);
		}
		else
		{
			$data = [
				'ID' => $this->ID,
				'display_name' => $this->name,
			];

			if ( $this->email != $this->user->user_email )
				$data['user_email'] = $this->email;

			if ( $this->new_password )
				$data['user_pass'] = $this->new_password;
			
			$user_id = wp_update_user( $data );
		}

		if ( is_wp_error( $user_id ) )
		{
			$this->errors['form'] = [ $user_id->get_error_messages() ];
			return false;
		}

		// if its Free member type save info about uploaded files
		if ( $this->member_type == awir_memberships::TYPE_FREE ){
			$attachList = $this->get_conf_letters_id_list(); // get list of exists files

			foreach($attachList as $attachment_id){
				$this->delete_conf_letter($attachment_id, false); // delete just info in tmp table
			}

			// save id list in user meta
			update_user_meta( $user_id, 'confirmation_letters', implode(',', $attachList) );
		}

		$this->user = get_userdata( $user_id );
		foreach ( $this->data as $key => $value )
		{
			if ( isset( $main_fields[ $key ] ) || isset( self::SPEC_FIELDS[ $key ] ) )
				continue;

			update_field( $key, $value, 'user_'.$user_id );
		}
		return true;
	}

	public static function make_login( $email )
	{
		$login = explode( '@', $email )[0];
		if ( !$login )
			$login = $email;

		$login = sanitize_user( $login );

		if ( !$login )
			$login = 'member';

		$full_login = $login;
		$i = 1;
		while ( username_exists( $full_login ) )
		{
			$i++;
			$full_login = $login.$i;
		}

		return $full_login;
	}

	public function choice_exists( $field, $value = null )
	{
		if ( $value === null )
			$value = $this->{$field};

		$info = self::get_field_info( $field );
		return isset( $info['choices'][ $value ] );
	}

	public static function get_field_info( $key )
	{
		if ( !empty( self::ACF_KEYS[ $key ] ) )
			$key = self::ACF_KEYS[ $key ];

		$field = (array)get_field_object( $key, 'user_1', false, false );

		if ( !isset( $field['required'] ) )
			$field['required'] = false;

		if ( !isset( $field['choices'] ) || !is_array( $field['choices'] ) )
			$field['choices'] = [];

		if ( empty( $field['type'] ) )
			$field['type'] = 'text';

		if ( empty( $field['label'] ) || !is_string( $field['label'] ) )
			$field['label'] = '';

		if ( $field['type'] == 'taxonomy' )
		{
			$field['type'] = 'checkbox';
			$terms = get_terms(['taxonomy' => $field['taxonomy'],'hide_empty' => false]);

			$field['choices'] = [];
			foreach ( $terms as $term ){
				// exclude QuickstartGuide by slug from list
				if($term->slug !== 'quickstart-guide')
					$field['choices'][ $term->term_id ] = $term->name;
			}
		}

		if ( !isset( $field['placeholder'] ) )
			$field['placeholder'] = '';

		//if ( $key == 'field_5afd4577b3179' || $key == 'activities' )
		//	self::log( $key, $field );

		return $field;
	}

	public function the_field( $key, $args = [] )
	{
		if ( isset( self::SPEC_FIELDS[ $key ] ) )
			$field = self::get_field_info( self::SPEC_FIELDS[ $key ] );

		else
			$field = self::get_field_info( $key );

		$field = array_merge( $field, $args );

		$field_value = $this->{$key};

		$required = ''; //$field['required'] ? ' required' : '';
		?>
			<?php if ( $field['type'] == 'select' ) : ?>

			<select name="<?=$this->form_prefix?>[<?=$key?>]" id="<?=$this->form_prefix?>_<?=$key?>" <?= $required ?>>
				<?php if ( $field['placeholder'] ): ?>
				<option value="" disabled <?= empty( $field['choices'][ $field_value ] ) ? 'selected ' : ''?>><?= esc_html($field['placeholder']) ?></option>
				<?php endif ?>
				<?php foreach ( $field['choices'] as $value => $label ): ?>
				<option value="<?=esc_attr( $value )?>" <?php if ( $value == $field_value ) echo 'selected ' ?>><?=esc_html($label)?></option>
				<?php endforeach ?>
			</select>

			<?php elseif ( in_array( $field['type'], ['radio','checkbox'] ) ): ?>

			<?php
				$i = 0;
				foreach ( $field['choices'] as $value => $label )
				{
					$i++;

					if ( !empty( $field['wrap'] ) )
							echo $field['wrap'][0];

					$name = $this->form_prefix.'['.$key.']';

					if ( $field['type'] == 'radio' )

						$checked = ($value == $field_value);

					else
					{
						$name .= '[]';
						$checked = in_array( $value, $field_value );
					}
			?>

				<input type="<?=$field['type']?>" id="<?="$key-$i"?>" name="<?=$name?>" value="<?=esc_attr($value) ?>" <?= $checked ? 'checked ' : '' ?>>
				<label for="<?="$key-$i"?>"><?= esc_html( $label ) ?></label>

			<?php
					if ( !empty( $field['wrap'] ) )
						echo $field['wrap'][1];
				}
			?>

			<?php elseif ( in_array( $field['type'], ['file','image'] ) ) : ?>

			<input type="hidden" name="<?=$this->form_prefix?>[<?=$key?>]" value="<?=esc_attr($field_value)?>" />
			<input type="file" name="<?=$this->form_prefix?>_<?=$key?>" id="<?=$this->form_prefix?>_<?=$key?>"<?= $required ?> placeholder="<?= esc_attr($field['placeholder'])?>"/>

			<?php else : ?>

			<input type="<?=$field['type']?>" name="<?=$this->form_prefix?>[<?=$key?>]" id="<?=$this->form_prefix?>_<?=$key?>" value="<?=esc_attr($this->{$key})?>"<?= $required ?> placeholder="<?= esc_attr($field['placeholder'])?>"/>

			<?php endif ?>

			<?php $this->the_error($key) ?>

		<?php
	}

	public function the_error( $field )
	{
		if ( empty( $this->errors[ $field ] ) )
			return;

		?>
			<label class="register-error active" for="<?=$this->form_prefix?>_<?=$field?>"><?=esc_html($this->errors[ $field ])?></label>
		<?php
	}

	public function nonce_field()
	{
		if ( !$this->ID )
			return;

		?><input type="hidden" name="<?=$this->form_prefix?>[nonce]" value="<?= wp_create_nonce('edit_profile_'.$this->ID ) ?>" /><?php
	}

	public function nonce_check( $data )
	{
		if ( !$this->ID )
			return true;

		if ( empty( $data['nonce'] ) )
			return false;

		return wp_verify_nonce( $data['nonce'], 'edit_profile_'.$this->ID );
	}

	public function is_my_password()
	{
		if ( !$this->ID )
			return false;

		return wp_check_password( $this->password, $this->user->user_pass, $this->ID );
	}

	public function birth_date( $format = 'Y-m-d' )
	{
		if ( preg_match( '/^(\d\d\d\d)(\d\d)(\d\d)$/', $this->birth_date, $m ) )
			$time = mktime( 12, 0, 0, $m[2], $m[3], $m[1] );

		else
			$time = 0;

		return date( $format, $time );
	}

	public function __get( $key )
	{
		if($key === 'CONF_LETTERS_TABLE')
			return 'awir_conf_letters';

		if ( isset( $this->data[ $key ] ) )
			return $this->data[ $key ];

		if ( in_array( $key, ['errors','user'] ) )
			return $this->{ $key };

		if ( $key == 'ID' )
			return $this->user ? $this->user->ID : 0;

		if ( $key == 'name' )
			return trim( $this->data['first_name'].' '.$this->data['last_name'] );

		if ( $key == 'member_type' )
		{
			if ( awir::is_member( $this->ID ) )
				return awir_memberships::user_type( $this->ID );
			else
				return awir_memberships::get_type( $this->membership );
		}

		if ( $key == 'user' )
			return $this->user;
	}

}


