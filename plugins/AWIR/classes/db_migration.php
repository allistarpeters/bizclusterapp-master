<?php

require_once 'base.php';

class awir_migration extends awir_singleton
{
	const DB_VERSION = 36;

	public function __construct()
	{
		parent::__construct();

		if ( ( defined('DOING_AJAX') && DOING_AJAX) )
			return;

		$current = (int)self::get_option('db_version');
		if ( self::DB_VERSION > $current )
			$this->migrate( $current );

		$this->add_action( 'init', 'pre_init', 1 );
	}

	public function pre_init()
	{
		if ( current_user_can('update_plugins') )
		{
			if ( !empty($_GET['force-awir-migration']) )
				self::set_option( 'db_version', self::DB_VERSION - 1 );

			if ( !empty($_GET['force-acf-sync']) )
				$this->add_action('acf/init', 'acf_sync');
		}
	}

	private function migrate( $current )
	{
		global $wpdb;

		if ( $current < 1 )
		{
			$wpdb->query("
				CREATE TABLE `{$wpdb->prefix}awir_messages` (
					`ID` bigint(20) UNSIGNED NOT NULL,
					`from_user_id` bigint(20) UNSIGNED NOT NULL,
					`to_user_id` bigint(20) UNSIGNED NOT NULL,
					`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					`message` text COLLATE utf8mb4_unicode_520_ci NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci
			");
			$wpdb->query("
				ALTER TABLE `{$wpdb->prefix}awir_messages`
					ADD PRIMARY KEY (`ID`)
			");
			$wpdb->query("
				ALTER TABLE `{$wpdb->prefix}awir_messages`
					MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT
			");
		}

		if ( $current < 2 )
		{
			$roles = wp_roles();
			foreach( $roles->get_names() as $role => $name )
			{
				$role = $roles->get_role( $role );

				$default_caps = array( 'publish_boards', 'edit_boards', 'delete_boards', 'edit_published_boards', 'delete_published_boards', 'upload_files' );
				foreach ( $default_caps as $cap )
					$role->add_cap( $cap );
			}
		}

		if ( $current < 3 )
		{
			$page = get_page_by_path('messages');
			if ( !$page )
			add_action( 'init', function(){
				wp_insert_post([
					'post_type' => 'page',
					'post_status' => 'publish',
					'post_title' => 'Direct Messages',
					'post_name' => 'messages',
					'post_author' => 1,
					'post_content' => '',
					'comment_status' => 'closed',
					'ping_status' => 'closed',
				]);
			}, 1 );
		}

		if ( $current < 4 )
		{
			$wpdb->query("
				ALTER TABLE `{$wpdb->prefix}awir_messages`
					ADD is_readed boolean NOT NULL DEFAULT 0
			");
		}

		if ( $current < 5 )
		{
			add_action( 'init', function(){
				$this->register_pages([
					'user_page' => [
						'post_title' => 'Profile',
						'post_name' => 'user',
					],
					'pofile_page' => [
						'post_title' => 'Edit Profile',
						'post_name' => 'profile',
					],
					'register_page' => [
						'post_title' => 'Register',
						'post_name' => 'register',
					]
				]);
			}, 1 );
		}

		if ( $current < 7 )
		{
			add_action( 'acf/init', function(){
				$about_id = (int)$this->add_page([
					'post_title' => 'About AWIR',
					'post_name' => 'about',
				]);

				$this->add_page([
					'post_title' => 'Our partners',
					'post_name' => 'partners',
					'post_parent' => $about_id,
				]);
			}, 1 );
		}

		if ( $current < 8 )
		{
			add_action( 'init', function(){
				$this->add_page([
					'post_title' => 'Events Archive',
					'post_name' => 'events-archive'
				]);
			}, 1 );
		}

		if ( $current < 9 )
		{
			add_action( 'acf/init', function(){
				$local_id = $this->find_page( 'local' );
				if ( $local_id )
				{
					wp_update_post([
						'ID' => $local_id,
						'post_name' => 'local-chapters',
					]);
				}
				else
					$this->add_page([
						'post_title' => 'Local Chapters',
						'post_name' => 'local-chapters',
						'post_parent' => $this->find_page('about'),
					]);
			}, 1);
		}

		if ( $current < 10 )
		{
			add_action( 'acf/init', function(){
				$this->add_page([
					'post_title' => 'Exhibitors',
					'post_name' => 'exhibitors',
				]);

				$advocacy_id = $this->add_page([
					'post_title' => 'Advocacy',
					'post_name' => 'advocacy',
				]);

				$this->add_page([
					'post_title' => 'Clinical trials',
					'post_name' => 'clinical-trials',
				]);

				$this->add_page([
					'post_title' => 'Education',
					'post_name' => 'education',
				]);

				$this->add_page([
					'post_title' => 'Membership',
					'post_name' => 'membership',
				]);
			}, 1 );
		}

		if ( $current < 11 )
		{
			add_action( 'acf/init', function(){
				$newsletter_id = $this->add_page([
					'post_title' => 'Newsletter',
					'post_name' => 'newsletter',
				]);

				$this->add_page([
					'post_title' => 'Archive',
					'post_name' => 'archive',
					'post_parent' => $newsletter_id,
				]);
			}, 1 );
		}

		if ( $current < 18 )
		{
			add_action( 'acf/init', function(){
				$advocacy_id = $this->add_page([
					'post_title' => 'Advocacy',
					'post_name' => 'advocacy',
				]);

				$this->add_page([
					'post_title' => 'Advocacy 101 ToolKit',
					'post_name' => 'toolkit',
					'post_parent' => $advocacy_id,
				]);

				$this->add_page([
					'post_title' => 'Become an Advocate',
					'post_name' => 'become',
					'post_parent' => $advocacy_id,
				]);

				$this->add_page([
					'post_title' => 'Policy Priorities',
					'post_name' => 'policy',
					'post_parent' => $advocacy_id,
				]);

			}, 1 );
		}

		if ( $current < 19 )
		{
			add_action( 'acf/init', function(){
				$id = $this->add_page([
					'post_title' => 'Contact',
					'post_name' => 'contact',
				]);
			}, 1 );
		}

		if ( $current < 20 )
		{
			require_once ABSPATH.'/wp-admin/includes/admin.php';
			if ( !is_plugin_active( 'post-views-counter/post-views-counter.php' ) )
				activate_plugins( 'post-views-counter/post-views-counter.php' );
		}

		if ( $current < 23 )
		{
			add_action( 'acf/init', function(){
				$this->add_page([
					'post_title' => 'Reset password',
					'post_name' => 'reset-password',
				]);

				$advocacy_id = $this->add_page([
					'post_title' => 'Advocacy',
					'post_name' => 'advocacy',
				]);

				$this->add_page([
					'post_title' => 'Take Action',
					'post_name' => 'take-action',
					'post_parent' => $advocacy_id,
				]);

				$this->add_page([
					'post_title' => 'Donate to RheumPAC',
					'post_name' => 'donate',
					'post_parent' => $advocacy_id,
				]);

				$this->add_page([
					'post_title' => 'Advocacy Partner Resources',
					'post_name' => 'resources',
					'post_parent' => $advocacy_id,
				]);

				$this->add_page([
					'post_title' => 'AWIR - Co Chairs of Advocacy',
					'post_name' => 'co-chairs',
					'post_parent' => $advocacy_id,
				]);
			}, 1 );
		}

		if ( $current < 32 )
		{
			add_action( 'init', function(){
				$this->add_page([
					'post_title' => 'Enter meeting password',
					'post_name' => 'meeting-password',
				]);
			} );
		}

		if ( $current < 34 )
		{
			$wpdb->query("
				CREATE TABLE `{$wpdb->prefix}awir_admin_log` (
					`ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					`dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
					`user_id` BIGINT UNSIGNED NOT NULL,
					`action` VARCHAR(64) NOT NULL,
					`ip` VARCHAR(39) NOT NULL,
					`data` TEXT NOT NULL,
					PRIMARY KEY (`ID`),
					INDEX (`dt`)
				) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;
			");
		}

		if ( $current < 35 )
		{
			add_action( 'init', function(){
				$this->add_page([
					'post_title' => 'Checkout',
					'post_name' => 'checkout',
					'post_content' =>
						'<h2 style="text-align: center;">Thank you for registration!</h2>'."\n\n".
						'<h3 style="text-align: center;">Please wait for confirmation...</h3>',
				]);
			} );
		}

		self::set_option( 'db_version', self::DB_VERSION, true );

		$this->add_action('acf/init', 'acf_sync');

		add_action( 'init', function() {
			flush_rewrite_rules();
		} );
	}

	private function register_pages( $pages, $template = [] )
	{
		foreach ( $pages as $option_slug => $page )
		{
			$option = 'options_'.$option_slug;
			if ( get_option( $option ) )
				continue;

			add_option( $option, 0, '', 'no' );

			$page_id = $this->add_page( $page );

			if ( $page_id )
				update_option( $option, $page_id );
		}
	}

	private function add_page( $page )
	{
		if ( $page_id = $this->find_page( $page['post_name'] ) )
			return $page_id;

		$page += [
			'post_type' => 'page',
			'post_status' => 'publish',
			'post_author' => 1,
			'post_content' => '',
			'comment_status' => 'closed',
			'ping_status' => 'closed',
		];

		$page_id = wp_insert_post( $page );

		return is_wp_error( $page_id ) ? false : $page_id;
	}

	private function find_page( $slug )
	{
		$query = new WP_Query( [ 'name' => $slug, 'post_type' => 'page' ] );
		if ( $query->have_posts() )
		{
			$posts = $query->get_posts();
			return $posts[0]->ID;
		}

		return false;
	}

	public function acf_sync()
	{
		static $runonce;

		if ( !empty( $runonce ) )
			die('!!!');

		$runonce = 1;

		$fields = file_get_contents( __DIR__.'/custom_fields.json' );
		$fields = json_decode( $fields, true );

		if ( !$fields )
			return;

		$relations = array(
			'About' => 'about',
			'Board Members' => 'about',
			'Gallery' => 'about',
			'Local Leaders' => 'local-chapters',
			'Contact Form' => ['local-chapters','advocacy','clinical-trials','membership','become','contact','co-chairs'],
			'Partners' => ['partners'],
			'Exhibitors' => 'exhibitors',
			'Pages links' => 'advocacy',
			'Clinical Trials' => 'clinical-trials',
			'Education' => 'education',
			'Membership' => 'membership',
			'Mailchimp Subscribe Block' => 'newsletter',
			'Policy Priorities' => 'policy',
			'Contact page' => 'contact',
			'Take Action' => 'take-action',
			'Donate' => 'donate',
			'Partner Resources' => 'resources',
			'Co Chairs' => 'co-chairs',
			'Downloads' => ['toolkit', 'exhibitors'],
		);

		$keys = array();

		$template = [
			'param' => 'page',
			'operator' => '==',
			'value' => '',
		];

		foreach ( $fields as $i => $group )
		{
			if ( empty( $relations[ $group['title'] ] ) )
				continue;

			$location = [];
			if ( in_array( $group['title'], ['Partners','Mailchimp Subscribe Block'], true ) )
				$location[] = array_shift( $group['location'] );

			foreach ( (array)$relations[ $group['title'] ] as $page )
			{
				if ( ($template['value'] = $this->find_page( $page )) )
					$location[] = [ $template ];
			}

			$fields[$i]['location'] = $location;
			$keys[ $group['title'] ] = $group['key'];
		}

		$this->acf_import_fields( $fields );
	}

	private function acf_import_fields( $json )
	{
    	// vars
    	$ids = array();
    	$keys = array();
    	$imported = array();


    	// populate keys
    	foreach( $json as $field_group ) {

	    	// append key
	    	$keys[] = $field_group['key'];

	    }


    	// look for existing ids
    	foreach( $keys as $key ) {

	    	// attempt find ID
	    	$field_group = _acf_get_field_group_by_key( $key );


	    	// bail early if no field group
	    	if( !$field_group ) continue;


	    	// append
	    	$ids[ $key ] = $field_group['ID'];

	    }


    	// enable local
		acf_enable_local();


		// reset local (JSON class has already included .json field groups which may conflict)
		acf_reset_local();


    	// add local field groups
    	foreach( $json as $field_group ) {

	    	// add field group
	    	acf_add_local_field_group( $field_group );

	    }


    	// loop over keys
    	foreach( $keys as $key ) {

	    	// vars
	    	$field_group = acf_get_local_field_group( $key );


	    	// attempt get id
	    	$id = acf_maybe_get( $ids, $key );

	    	if( $id ) {

		    	$field_group['ID'] = $id;

	    	}


	    	// append fields
			if( acf_have_local_fields($key) ) {

				$field_group['fields'] = acf_get_local_fields( $key );

			}


			// import
			$field_group = acf_import_field_group( $field_group );
    	}
	}
}

awir_migration::getInstance();