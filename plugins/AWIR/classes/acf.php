<?php

class awir_acf extends awir_singleton
{
	private $acf_fields = [];

	public function __construct()
	{
		parent::__construct();
		$this->add_action('acf/init', 'acf_init');
		$this->add_filter('acf/load_field/name=membership', 'membership_choices');
		$this->add_filter('acf/load_field/name=memberships', 'membership_choices');
	}

	public function acf_init()
	{
		$this->add_filter('acf/get_post_types', 'filter_post_types');
	}

	public function filter_post_types( $post_types )
	{
		$post_types[] = 'acf-field-group';
		return $post_types;
	}

	public function membership_choices( $field )
	{
		$field['choices'] = [];

		foreach ( awir_memberships::get_list() as $id => $membership )
			$field['choices'][ $id ] = $membership->name;

		return $field;
	}
}

awir_acf::getInstance();

if ( !is_admin() )
add_action('plugins_loaded', function(){
	if ( function_exists('get_field') )
		return;

	function get_field()
	{
		return '';
	}

	function the_field() {
		return;
	};

	function have_rows(){
		return false;
	}
});
