<?php

class CFCDN_Manager_Admin extends Base_Manager_Admin {
	const PAGE_TITLE    = 'CloudFront CDN Manager';
	const MENU_SLUG     = 'cfcdn-manager';
	const METHOD_PREFIX = 'cfcdn_manager';
    const LINE_WIDTH    = '300px';
    const SECTION_INFO  = '<b>Note</b> Enter the as appropriate needle and substitution parameters relevant to this site.';

	public $options;

	public $fields= array(
			'Origin URL'      => 'originurl',
			'Production URL'  => 'produrl'
	);

	public function __construct() {
		$this->get_admin_options();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}

}
