<?php

/**
 * Class NoIndexer_Base - may be extended for specific use cases
 */
class NoIndexer_Base {
	const PRIORITY       = 9;
	const FOLLOW_META    = '<meta name="robots" content="NOINDEX,FOLLOW" />';
	const NOFOLLOW_META  = '<meta name="robots" content="NOINDEX,NOFOLLOW" />';
	const LOCAL_TLD      = 'lcl';
	const PROD_HOSTNAME  = 'www';
	const ORIGIN_HOST    = 'origin';
	const CDN_AGENT      = 'Amazon CloudFront';

	public $um;
	public $plugin_identifier;

	public function __construct( $plugin_identifier ) {
		$this->plugin_identifier = $plugin_identifier . PHP_EOL;
		$this->um = new URL_Magick();
		add_action( 'wp_head', array( $this, 'print_noindex_markup' ), static::PRIORITY );
	}

	/**
	 * Redirection & abstraction
	 * @return bool
	 */
	public function is_noindexed() {
		return( $this->is_not_prod() || $this->is_local() );
	}

	/**
	 * Performs the heavy lifting by printing the appropriate noindex as per the SEO requirements
	 */
	public function print_noindex_markup() {
		if ( $this->is_noindexed() ) {
			$this->print_nofollow_meta();
		}
	}

	/**
	 * Prints the appropriate markup
	 */
	public function print_follow_meta() {
		print( $this->plugin_identifier . self::FOLLOW_META . PHP_EOL );
	}

	/**
	 * Prints the appropriate markup
	 */
	public function print_nofollow_meta() {
		print( $this->plugin_identifier . self::NOFOLLOW_META . PHP_EOL );
	}

	/**
	 * This only looks at the TLD portion of the URL to make the decision
	 * @return bool
	 */
	public function is_local() {
		if ( $this->um::tld === static::LOCAL_TLD ) {
			return true;
		}
		return false ;
	}

	/**
	 * This basically looks at the hostname portion of the URL to make the decision
	 * @return bool
	 */
	public function is_prod() {
		if ( $this->um::$hostname === static::PROD_HOSTNAME ||
			 $this->is_cdn_request()
		) {
			return true;
		}
		return false;
	}

	/**
	 * This basically is to prove a point that I've long forgotten
	 * @return bool
	 */
	public function is_not_prod() {
		return ( ! $this->is_prod() );
	}

	/**
	 * Checks if the User agent string is from the Amazon Cloudfront CDN
	 */
	public function is_cdn_request() {
		$user_agent = filter_var( filter_var( $_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_STRING ) );
		if ( $user_agent === self::CDN_AGENT ) {
			return true;
		}
		return false;
	}

}
