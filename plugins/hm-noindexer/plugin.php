<?php
/*
Plugin Name: Haymarket NoIndexer
Version: 1.0
Description: Provides a simple method of adding the noindex meta to all non www URLs. This is ideal for situation where WWW is served via a CDN and origin, staging and dev are hosted in a manner still accessible to the public. Read more in Jira at  <a href='https://haymarket.atlassian.net/browse/WP-304' target='_blank'>WP-304</a>.
Author: Mikel King
Text Domain: hm-noindexer
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2019, Mikel King, haymarketmedia.com, (mikel.king AT haymarketmedia DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
require( 'inc/noindexer-base.php' );
//Debug::enable_error_reporting();

class NoIndexer_Controller extends WP_Base {
	const VERSION       = '1.0';
	const PLUGIN_ID_FMT = '<!-- HM NoIndexer Version %s -->';

	public function __construct() {
		parent::__construct();
		$nib = new NoIndexer_Base( $this->get_plugin_id() );
	}

	/**
	 * This makes it easier for the plugin to self identify in the page source.
	 * @return string
	 */
	public function get_plugin_id() {
		return sprintf( static::PLUGIN_ID_FMT, static::VERSION );
	}

}

NoIndexer_Controller::get_instance();
