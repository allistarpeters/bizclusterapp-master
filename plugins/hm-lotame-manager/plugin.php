<?php
/*
Plugin Name: Haymarket Lotame Data Exchange
Version: 1.4
Description:  Plugin installs Haymarket Lotame data beacon.  Read more in Jira at  <a href='https://haymarket.atlassian.net/browse/WP-561' target='_blank'>WP-561</a>.
Author: Mikel King
Text Domain: hm-lotame
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

    Copyright (C) 2019, Mikel King, (mikel.king AT olivent DOT com)
    All rights reserved.
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice, this
          list of conditions and the following disclaimer.

        * Redistributions in binary form must reproduce the above copyright notice,
          this list of conditions and the following disclaimer in the documentation
          and/or other materials provided with the distribution.

        * Neither the name of the {organization} nor the names of its
          contributors may be used to endorse or promote products derived from
          this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
//Debug::enable_error_reporting();

include( 'inc/lotame-manager-admin.php' );

class HM_Lotame extends WP_Base {
	const VERSION        = '1.4';
	const PRIORITY       = 10;
	const SCRIPT_NAME    = 'lotame';
	const SCRIPT_URL_FMT = "https://tags.crwdcntrl.net/c/%s/cc.js";
	const SCRIPT_SFX_FMT = "ver=%s'>";
	const SCRIPT_FILTER  = "ver=%s&ns=_cc%s' id='LOTCC_%s'>";
	const FILE_SPEC      = __FILE__;
	const LOTAME_SLUG    = 'lotame-tag';
	const LOTAME_BCN_PLC = 'after';
	const LOTAME_BCN_FMT = '_cc%s.bcp();';
	
	public static $lotame_scripts = array();
	
	public $lotame_settings;
	public $lotame_api_id;
	
	public function __construct() {
		parent::__construct();
		$lma = new Lotame_Manager_Admin();
		$this->lotame_settings = $lma->get_options();
		$this->lotame_api_id = $this->get_lotame_api_id();
		$vb = new Variant_Base();
		if ( ! $vb->is_ad_blocked() ) {
			if ( ! is_admin() ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ), self::PRIORITY );
				add_filter( 'script_loader_tag', array( $this, 'lotame_filter_tag' ), self::PRIORITY, 3 );
				add_filter( 'script_loader_tag', array( __CLASS__, 'async_filter_tag' ), self::PRIORITY, 3 );
				
			}
		}
	}
	
	public function output( $data, $label = null ) {
		print( '<h1>' . $label . $data . "</h1>" . PHP_EOL );
	}
	
	/**
	 * This wll theoretically only modify the matching handle
	 * but of course it needs testing. If the handle is found in
	 * the async_scripts array then we will modify the script call
	 *
	 * The $tag is the line of script code
	 * The handle is the script slug registered by set_lotame_assets()
	 *
	 * @param $tag
	 * @param $handle
	 * @return string
	 */
	public function lotame_filter_tag( $tag, $handle, $src ) {
		$key = self::key_finder( $handle, self::$lotame_scripts );
		if ( $handle && in_array( $handle, self::$lotame_scripts ) ) {
			self::$lotame_scripts[$key] = $handle . '-DONE' ;
			return ( str_replace( $this->get_script_suffix(), $this->get_script_filter(), $tag ) );
		}
		return( $tag );
	}
	
	public function get_script_filter() {
			return( sprintf( self::SCRIPT_FILTER, self::VERSION, $this->lotame_api_id, $this->lotame_api_id ) );
	}
	
	public function get_script_suffix() {
		return( sprintf( self::SCRIPT_SFX_FMT, self::VERSION ) );
	}
	
	/**
	 * @param $asset_slug
	 */
	public function set_lotame_assets( $asset_slug ) {
		$buffer = self::$lotame_scripts;
		if ( isset( $asset_slug ) ) {
			$buffer[] = $asset_slug;
			self::$lotame_scripts = array_unique( $buffer, SORT_STRING );
		}
	}

	public function get_lotame_beacon() {
		return( sprintf( self::LOTAME_BCN_FMT, $this->get_lotame_api_id() ) );
	}
	
	public function get_lotame_api_id() {
		return( $this->lotame_settings[Lotame_Manager_Admin::FIELD_ID] );
	}

	public function get_lotame_script_url() {
		$lotame_script_url = sprintf(self::SCRIPT_URL_FMT, $this->lotame_api_id );
		return( $lotame_script_url );
	}
	
	public function transform_dashes( $data ) {
		return( str_replace( '-', '_', $data ) );
	}

	public function register_scripts() {
		wp_localize_script(
			self::SCRIPT_NAME, // identifies the script to associate the data element with
			$this->transform_dashes( self::LOTAME_SLUG ), // sets the JS var to associate the data element with
			$this->lotame_settings[Lotame_Manager_Admin::FIELD_ID] // data
		);
		
		wp_register_script(
			self::SCRIPT_NAME,
			$this->get_lotame_script_url(),
			array(),
			self::VERSION,
			static::IN_HEADER
		);
		
		wp_add_inline_script(
			self::SCRIPT_NAME,
			$this->get_lotame_beacon()
		);
		
		//Base_Plugin::set_async_assets( self::SCRIPT_NAME );
		$this->set_lotame_assets( self::SCRIPT_NAME );
		wp_enqueue_script( self::SCRIPT_NAME );
	}

	public static function get_lotame_id() {
		return ( self::LOTAME_ID );
	}
}
HM_Lotame::get_instance();
