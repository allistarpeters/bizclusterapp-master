<?php

class Lotame_Manager_Admin extends Base_Manager_Admin {
	const PAGE_TITLE    = 'Lotame Manager';
	const MENU_SLUG     = 'lotame-manager';
	const METHOD_PREFIX = 'lotame_manager';
    const LINE_WIDTH    = '200px';
	const SECTION_INFO  = '<b>Note</b> Enter the value of the Lotame API ID relevant to this site.';
	const FIELD_ID      = 'lotame_api_id';

	public $options;

	public $fields= array(
			'Lotame API ID' => self::FIELD_ID,
	);

	public function __construct() {
		$this->get_admin_options();
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}
}
