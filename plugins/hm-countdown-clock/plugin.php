<?php
/*
Plugin Name: Haymarket Events Clock
Version: 1.0
Description: A front end countdown clock
Author: Theresa Newman
Text Domain: hm-events-clock
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2014, Theresa Newman
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

include( 'inc/hm-countdown-clock-admin.php' );

class HM_Events_Clock extends WP_Base {
	const VERSION = '1.0';

	protected function __construct() {
		add_action( 'wp_enqueue_scripts', array($this,'events_clock_enqueue_script') );
		add_shortcode( 'events_clock', array($this,'events_clock_shortcode') );
		$hm_events_clock_admin_settings = new HM_Events_Clock_Admin();
	}

	protected function activation_actions() {}

	protected function deactivation_actions() {}

	public function events_clock_enqueue_script() {
			wp_register_script( 'countdown-js', plugin_dir_url( __FILE__ ) . 'js/jquery.countdown.js', array( 'jquery' ) );
			wp_register_script( 'render-event-clock-js', plugin_dir_url( __FILE__ ) . 'js/renderEventsClock.js', array( 'jquery' ), '', true );
			wp_enqueue_style( 'countdown-css', plugin_dir_url(__FILE__) . 'css/events-clock.css');
	}

	public function events_clock_shortcode( $atts ) {
			$atts = shortcode_atts(
				array(
					'enddate' => '',
					'header'    => '',
					'header_color' => '',
					'expired_msg' => '',
				),
				$atts,
				'events_clock'
			);

			wp_enqueue_script('countdown-js');
			wp_enqueue_script('render-event-clock-js');
			wp_localize_script('render-event-clock-js','shortcodeAtts', $atts);

			return '<div data-countdown="' . $atts['enddate'] . '"></div>';
	}

	public function init() {
		register_activation_hook( __FILE__, array( 'HM_Events_Clock', 'activator' ) );
		register_activation_hook( __FILE__, array( 'HM_Events_Clock', 'deactivator' ) );
	}

}

$hmec = HM_Events_Clock::get_instance();

