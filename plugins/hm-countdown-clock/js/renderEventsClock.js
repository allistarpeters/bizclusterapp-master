/*---------------------------------
Final Countdown http://hilios.github.io/jQuery.countdown/
-----------------------------------*/
(function($){
  $(document).ready(function($) {
    var clockHeader = shortcodeAtts.header;
    var expiredMsg = shortcodeAtts.expired_msg;
    var headerColor = shortcodeAtts.header_color;

    $('[data-countdown]').each(function() {
      var $this = $(this), finalDate = $(this).data('countdown');
      $this.countdown(finalDate)
      .on('update.countdown', function(event) {
        $(this).html('<div class="top" style="color:' + headerColor + '">' + clockHeader + '</div><div class="bottom">' + event.strftime('%D days %H:%M:%S') + '</div>');
      })
      .on('finish.countdown', function(event) {
        $(this).html('<div class="top" style="color:' + headerColor + '">' + expiredMsg + '</div>');
      });
    });

  });
})(jQuery)
