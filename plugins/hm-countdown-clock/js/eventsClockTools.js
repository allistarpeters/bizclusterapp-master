jQuery(document).ready(function($){
    $('#clockColor').wpColorPicker();
    $('#shortcodeCreator').submit(function(e){
        e.preventDefault();
     });
	$('#eventsClockCreateShortcodeButton').on('click', function(e) {
        var clockHeader = $('#clockHeader').val();
        var endDate = $('#endDate').val();
        var expiredMsg = $('#expiredMsg').val();
        var clockColor = $('#clockColor').val();
        if (endDate) {
            var buildShortcode = '[events_clock enddate=';
            buildShortcode += endDate;
            if (clockHeader) {
                buildShortcode += ' header="' + clockHeader + '"';
            }
            if (expiredMsg) {
                buildShortcode += ' expired_msg="' + expiredMsg + '"';
            }            
            if (clockColor) {
                buildShortcode += ' header_color=' + clockColor;
            } 
            buildShortcode += ' ]'
            $("#eventsClockShortcodeOutput").val(buildShortcode);
        } else {
            $("#eventsClockShortcodeOutput").val('An end date is required');
        }

	});
}(jQuery));	
