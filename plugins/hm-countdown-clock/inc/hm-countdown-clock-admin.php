<?php 
class HM_Events_Clock_Admin {
  public function __construct() {
		add_action( 'admin_menu', array($this, 'hay_events_clock_menu') );
	}
	public function hay_events_clock_menu() {
		$toolsPage = add_management_page( 'Haymarket Events Clock Options', 'Events Clock', 'manage_options', 'hay-events-clock-options', array($this, 'hay_events_clock_options') );
		add_action('admin_print_scripts-' . $toolsPage, array($this, 'hay_events_clock_admin_scripts') );
	}
	function hay_events_clock_admin_scripts() {
		wp_register_script( 'events-clock-tools-js', plugins_url( '../js/eventsClockTools.js', __FILE__ ), array( 'jquery','wp-color-picker' ), '', true );
		wp_enqueue_script( 'events-clock-tools-js' );
	}
	public function hay_events_clock_options() {
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		?>
		<div class="wrap">
			<h3>Haymarket Events Clock Shortcode</h3>
			<p>The Haymarket Events clock shortcode will display a countdown clock. It is required to specify an end date.</p>
			<p>Header text and expired message are optional. Header color is also optional and applies to both header text and expired message.</p>
			<p>Example:</p>
			<p>[events_clock enddate=1/24/2019 14:32:00 header="Entry deadline" expired_msg="Deadline has passed" header_color="#ff00ff"]</p>
			<form id="shortcodeCreator">
				<h3>Use this shortcode creator:</h3>
				<table class="form-table">
					<tbody>
						<tr>
							<th><label for="endDate"><strong>End Date:</strong></label></th>
							<td><input type="date" id="endDate" autofocus="true" autocomplete="false" required="true" min="2019-05-01" max="2021-12-31"></td>
						</tr>
						<tr>
							<th><label for="clockHeader"><strong>Header Text:</strong></label></th>
							<td><input type="text" id="clockHeader"></td>
						</tr>				
						<tr>
							<th><label for="expiredMsg"><strong>Expired Message:</strong></label></th>
							<td><input type="text" id="expiredMsg"></td>
						</tr>	
						<tr>
							<th><label for="clockColor"><strong>Pick a text color:</strong></label></th>
							<td><input id="clockColor" type="text" value="#bada55"></td>
						</tr>				
						<tr>
							<td><input class="button button-primary" type="submit" id="eventsClockCreateShortcodeButton" value="Generate Shortcode"></td>
							<td></td>
						</tr>												
	</tbody>
	</table>
			</form>
			<p>
				<label for="eventsClockShortcodeOutput"><em>shortcode will output here - copy and paste into a post or text widget</em></label>
				<div><input id="eventsClockShortcodeOutput" type="text" size="120" value="" /></div>
			</p>
		</div>

		<?php
	}

}