<?php
namespace SabaiApps\Directories\Component\Location\Helper;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\Entity;

class FormatAddressHelper
{
    protected static $_tags = [
        'street' => 'street',
        'street2' => 'street2',
        'city' => 'city',
        'province' => 'province',
        'zip' => 'zip',
        'country' => 'country',
        'country_code' => 'country_code',
        'address' => 'full_address',
        'lat' => 'latitude',
        'lng' => 'longitude',
        'timezone' => 'timezone',
    ];

    public function help(Application $application, array $value, $format, Entity\Type\IEntity $entity = null, array $locationHierarchy = null)
    {
        $replace = [];
        foreach (self::$_tags as $column => $tag) {
            $replace['{' . $tag . '}'] = isset($value[$column]) && strlen($value[$column]) ? $application->H($value[$column]) : '';
        }
        if (strlen($replace['{country}']) === 2) {
            $replace['{country_code}'] = $replace['{country}'];
            if (strpos($format, '{country}')) {
                // Get full country name
                $replace['{country}'] = $application->System_Countries($replace['{country_code}']);
            }
        }
        if (isset($entity)
            && !empty($locationHierarchy)
        ) {
            if (!empty($value['term_id'])
                && ($terms = $entity->getFieldValue('location_location'))
            ) {
                foreach ($terms as $term) {
                    if ($term->getId() === $value['term_id']) {
                        $location_titles = (array)$term->getCustomProperty('parent_titles');
                        $location_titles[$term->getId()] = $term->getTitle();
                        foreach (array_keys($locationHierarchy) as $key) {
                            $replace['{' . $key . '}'] = $application->H((string)array_shift($location_titles));
                        }
                    }
                }
            }
        }
        // Replace tags
        $formatted = strtr($format, $replace);
        // Replace multiple columns with single column
        $formatted = preg_replace('/,+/', ',', $formatted);
        // Replace columns with spaces in between
        $formatted = preg_replace('/,\s*,/', ',', $formatted);
        // Replace multiple spaces with single space
        $formatted = preg_replace('/\s+/', ' ', $formatted);
        // Remove starting/trailing spaces/commas
        $formatted = trim($formatted, ' ,');

        return $formatted;
    }

    public function tags(Application $application, Entity\Model\Bundle $bundle, array $locationHierarchy = null)
    {
        $tags = [];
        foreach (array_values(self::$_tags) as $tag) {
            $tags[$tag] = '{' . $tag . '}';
        }
        if (!$application->Map_Api()) {
            unset($tags['full_address']);
        }
        if (isset($locationHierarchy)) {
            foreach (array_keys($locationHierarchy) as $key) {
                $tags[$key] = '{' . $key . '}';
            }
        }
        return array_values($tags);
    }
}