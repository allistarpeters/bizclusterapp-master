<div class="drts-location-entities-map-container drts-location-entities-with-map drts-location-entities-with-map-<?php echo $settings['map']['position'];?> <?php echo DRTS_BS_PREFIX;?>row <?php echo DRTS_BS_PREFIX;?>no-gutters <?php if ($settings['map']['position'] === 'top'):?><?php echo DRTS_BS_PREFIX;?>flex-column-reverse<?php elseif ($settings['map']['position'] === 'left'):?><?php echo DRTS_BS_PREFIX;?>flex-sm-row-reverse<?php elseif ($settings['map']['position'] === 'right'):?><?php echo DRTS_BS_PREFIX;?>flex-sm-row<?php elseif ($settings['map']['position'] === 'bottom'):?><?php echo DRTS_BS_PREFIX;?>flex-column<?php endif;?>"<?php if (!empty($settings['map']['fullscreen_offset'])):?> data-fullscreen-offset="<?php echo $this->H($settings['map']['fullscreen_offset']);?>"<?php endif;?>>
    <div class="drts-location-entities-container <?php echo DRTS_BS_PREFIX;?>col <?php if (in_array($settings['map']['position'], ['left', 'right'])):?><?php echo DRTS_BS_PREFIX;?>col-sm-<?php echo 12 - $settings['map']['span'];?> <?php echo DRTS_BS_PREFIX;?>mb-3<?php if ($settings['map']['position'] === 'left'):?> <?php echo DRTS_BS_PREFIX;?>pl-sm-2<?php elseif ($settings['map']['position'] === 'right'):?> <?php endif;?><?php echo DRTS_BS_PREFIX;?>pr-sm-2<?php endif;?>">
        <div class="drts-view-entities drts-location-entities">
            <?php $this->display($settings['map']['template'], $CONTEXT->getAttributes());?>
        </div>
    </div>
    <div class="<?php echo DRTS_BS_PREFIX;?>col <?php if (in_array($settings['map']['position'], ['left', 'right'])):?><?php echo DRTS_BS_PREFIX;?>d-none <?php echo DRTS_BS_PREFIX;?>d-sm-block <?php echo DRTS_BS_PREFIX;?>col-sm-<?php echo intval($settings['map']['span']);?> <?php endif;?><?php echo DRTS_BS_PREFIX;?>mb-3 drts-location-map-container-container" data-span="<?php echo intval($settings['map']['span']);?>" data-fullscreen-span="<?php echo intval($settings['map']['fullscreen_span']);?>" data-position="<?php echo $settings['map']['position'];?>"<?php if (!empty($settings['map']['sticky_offset'])):?> data-sticky-scroll-top="<?php echo intval($settings['map']['sticky_offset']);?>"<?php endif;?><?php if (!empty($settings['map']['sticky_offset_selector'])):?> data-sticky-scroll-top-selector="<?php echo $this->H($settings['map']['sticky_offset_selector']);?>"<?php endif;?> style="height:<?php echo intval($settings['map']['height']);?>px;">
        <?php $this->Action('location_before_map', [$CONTEXT]);?>
        <?php $this->display(
            $this->Platform()->getAssetsDir('directories-pro') . '/templates/map_map',
            [
                'settings' => $settings['map'],
                'field' => isset($settings['map']['coordinates_field']) ? $settings['map']['coordinates_field'] : 'location_address',
                'draw_options' => $this->Location_DrawMapOptions([], $CONTEXT),
                'display' => null,
            ] + $CONTEXT->getAttributes()
        );?>
        <?php $this->Action('location_after_map', [$CONTEXT]);?>
    </div>
</div>
<div class="drts-location-sticky-scroll-stopper"></div>
