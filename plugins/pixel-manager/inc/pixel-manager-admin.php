<?php

class Pixel_Manager_Admin {
	const PIXEL_MANAGER_SLUG   = 'pixel-manager';
	const SETTING_NAME            = 'Pixel Manager';
	public $script_url;

	public function __construct() {
		$this->script_url = get_option( self::PIXEL_MANAGER_SLUG );
		add_action( 'admin_menu', array( $this, 'admin_settings' ) );
		add_action( 'admin_init', array( $this, 'admin_page_init' ) );
	}

	public function admin_settings() {
		add_options_page(
			'Pixel Manager',
			self::SETTING_NAME,
			'manage_options',
			self::PIXEL_MANAGER_SLUG,
			array( $this, 'show_pixel_manager_admin_page' )
		);
	}

	public function show_pixel_manager_admin_page() {
	?>
		<div class="wrap">
			<h1>Pixel Manager Admin</h1>
			<form method="post" action="options.php">
				<?php
				settings_fields( 'pixel_manager_options' );
				do_settings_sections( 'pixel_manager_settings' );
				submit_button();
				?>
			</form>
		</div>
	<?php }

	public function admin_page_init() {
		register_setting(
			'pixel_manager_options', // Option group
			self::PIXEL_MANAGER_SLUG // Option name
		);

		add_settings_section(
			'pixel_manager_main', // ID
			'Pixel Manager Settings', // Title
			array( $this, 'print_section_info' ), // Callback
			'pixel_manager_settings' // Page
		);

		add_settings_field(
			self::PIXEL_MANAGER_SLUG, // ID
			'Pixel URL', // Title
			array( $this, 'url_validator' ), // Callback
			'pixel_manager_settings', // Page
			'pixel_manager_main' // Section
		);
	}

	public function print_section_info() {
		print( '<p>Use this JavaScript pixel URL</p>' . PHP_EOL );
		print( '<p>Note all JavaScript pixel URLs will be asynchronously set in the footer</p>' . PHP_EOL );
	}

	public function url_validator() {
		$option = isset( $this->script_url ) ? esc_attr( $this->script_url ) : '';

		echo '<input type="text" id="' . self::PIXEL_MANAGER_SLUG . '" name="' . self::PIXEL_MANAGER_SLUG . '" value="' . $option . '" style="max-width:100%; width: 980px;" />';
	}

}
