# Copyright (C) 2020 unnamed project
# This file is distributed under the same license as the unnamed project package.
msgid ""
msgstr ""
"Project-Id-Version: unnamed project\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: directories-frontend.php:24
msgid "Make sure to bulk update all Directories plugins at once. See %s for more details."
msgstr ""

#: assets/templates/frontendsubmit_login_register_form.html.php:12, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:102
msgid "Login"
msgstr ""

#: assets/templates/frontendsubmit_login_register_form.html.php:20
msgid "or"
msgstr ""

#: assets/templates/frontendsubmit_login_register_form.html.php:26, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:171
msgid "Register"
msgstr ""

#: assets/templates/frontendsubmit_login_register_form.html.php:32, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:240
msgid "Continue as guest"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:44
msgid "Frontend Dashboard"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:45, lib/components/Dashboard/DashboardComponent.php:370
msgid "Dashboard"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:301
msgid "Logout"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:387
msgid "Dashboard Panel Settings (%s)"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:417
msgid "Dashboard Settings"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:419
msgid "Dashboard panels"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:437
msgid "Enable accordion effect"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:444
msgid "Show logout button"
msgstr ""

#: lib/components/Dashboard/DashboardComponent.php:456
msgid "Enable public dashboard"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:176, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:114
msgid "Lost your password?"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:178, lib/components/FrontendSubmit/Controller/ResetPassword.php:12
msgid "Reset Password"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:217
msgid "Login/Registration"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:218
msgid "Login or Register"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:352
msgid "Frontend Submit"
msgstr ""

#: lib/components/FrontendSubmit/FrontendSubmitComponent.php:372
msgid "Enable frontend submit"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:65
msgid "reCAPTCHA"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:82
msgid "reCAPTCHA API Settings"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:85
msgid "reCAPTCHA API version"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:87
msgid "reCAPTCHA v2 (Checkbox)"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:88
msgid "reCAPTCHA v3"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:94, lib/components/reCAPTCHA/reCAPTCHAComponent.php:119
msgid "reCAPTCHA API site key"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:101, lib/components/reCAPTCHA/reCAPTCHAComponent.php:126
msgid "reCAPTCHA API secret key"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:108
msgid "reCAPTCHA score threshold"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:134
msgid "reCAPTCHA size"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:136
msgid "Normal"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:137
msgid "Compact"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:145
msgid "reCAPTCHA type"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:147
msgid "Image"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:148
msgid "Audio"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:156
msgid "reCAPTCHA theme"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:158
msgid "Light"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:159
msgid "Dark"
msgstr ""

#: lib/components/reCAPTCHA/reCAPTCHAComponent.php:173, lib/components/reCAPTCHA/reCAPTCHAComponent.php:185, lib/components/reCAPTCHA/reCAPTCHAComponent.php:197
msgid "Add reCAPTCHA field"
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:12
msgid "Change password"
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:19, lib/components/Dashboard/Controller/DeleteAccount.php:36
msgid "Current password"
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:25
msgid "New password"
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:31
msgid "Confirm new password"
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:41
msgid "New passwords do not match."
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:46, lib/components/Dashboard/Controller/DeleteAccount.php:47
msgid "Your current password is incorrect."
msgstr ""

#: lib/components/Dashboard/Controller/ChangePassword.php:52
msgid "Your password changed successfully."
msgstr ""

#: lib/components/Dashboard/Controller/DeleteAccount.php:17
msgid "Your account may not be deleted."
msgstr ""

#: lib/components/Dashboard/Controller/DeleteAccount.php:24
msgid "Delete account"
msgstr ""

#: lib/components/Dashboard/Controller/DeleteAccount.php:32
msgid "Are you sure you want to permanently delete your account?"
msgstr ""

#: lib/components/Dashboard/Controller/DeleteAccount.php:37
msgid "Please enter your current password to confirm."
msgstr ""

#: lib/components/Dashboard/Controller/DeleteAccount.php:52
msgid "Your account could not be deleted."
msgstr ""

#: lib/components/Dashboard/Controller/DeletePost.php:15, lib/components/Dashboard/DisplayButton/PostDisplayButton.php:70
msgid "Delete"
msgstr ""

#: lib/components/Dashboard/Controller/DeletePost.php:31
msgid "Are you sure you want to delete this post?"
msgstr ""

#: lib/components/Dashboard/Controller/DeletePost.php:44
msgid "Your item has been deleted successfully."
msgstr ""

#: lib/components/Dashboard/Controller/EditPost.php:17
msgid "Save Changes"
msgstr ""

#: lib/components/Dashboard/Controller/EditPost.php:69
msgid "Your item has been updated successfully."
msgstr ""

#: lib/components/Dashboard/Controller/Panel.php:26
msgid "No dashboard panels found."
msgstr ""

#: lib/components/Dashboard/Controller/SubmitPost.php:22
msgid "Press the button below to submit for review."
msgstr ""

#: lib/components/Dashboard/Controller/SubmitPost.php:75
msgid "Your item has been submitted successfully. We will review your submission and publish it when it is approved."
msgstr ""

#: lib/components/Dashboard/Controller/SubmitPost.php:77
msgid "Your item has been submitted and published successfully."
msgstr ""

#: lib/components/Dashboard/DisplayButton/PostDisplayButton.php:36
msgid "Edit post button"
msgstr ""

#: lib/components/Dashboard/DisplayButton/PostDisplayButton.php:45
msgid "Delete post button"
msgstr ""

#: lib/components/Dashboard/DisplayButton/PostDisplayButton.php:54
msgid "Submit post button"
msgstr ""

#: lib/components/Dashboard/DisplayButton/PostDisplayButton.php:68
msgid "Edit"
msgstr ""

#: lib/components/Dashboard/DisplayButton/PostDisplayButton.php:72
msgid "Submit for review"
msgstr ""

#: lib/components/Dashboard/Panel/AccountPanel.php:18
msgid "Account"
msgstr ""

#: lib/components/Dashboard/Panel/AccountPanel.php:38
msgctxt "directories-frontend"
msgid "Change password"
msgstr ""

#: lib/components/Dashboard/Panel/AccountPanel.php:43
msgctxt "directories-frontend"
msgid "Delete account"
msgstr ""

#: lib/components/Dashboard/Panel/AccountPanel.php:60
msgid "Select pages"
msgstr ""

#: lib/components/Dashboard/Panel/PostsPanel.php:234
msgid "Show add item button"
msgstr ""

#: lib/components/Dashboard/Panel/PostsPanel.php:241
msgid "Include other user's posts"
msgstr ""

#: lib/components/Dashboard/Panel/PostsPanel.php:260
msgid "Show filter form"
msgstr ""

#: lib/components/Dashboard/Panel/PostsPanel.php:268
msgid "Show filter form in modal window"
msgstr ""

#: lib/components/Dashboard/Panel/PostsPanel.php:282
msgid "Select filter group"
msgstr ""

#: lib/components/Dashboard/ViewMode/DashboardViewMode.php:26
msgctxt "display name"
msgid "Dashboard Row"
msgstr ""

#: lib/components/Dashboard/WordPress/AbstractProfile.php:93
msgid "%s Profile Page Integration"
msgstr ""

#: lib/components/Dashboard/WordPress/AbstractProfile.php:96
msgid "Show dashboard panels"
msgstr ""

#: lib/components/Dashboard/WordPress/AbstractProfile.php:101
msgid "Check this option to show dashboard panels on the %s profile page."
msgstr ""

#: lib/components/Dashboard/WordPress/AbstractProfile.php:106
msgid "Redirect dashboard access"
msgstr ""

#: lib/components/Dashboard/WordPress/AbstractProfile.php:111
msgid "Check this option to redirect dashboard access to the %s profile page."
msgstr ""

#: lib/components/FrontendSubmit/Controller/AbstractAddEntity.php:36
msgid "You have already reached the maximum number of %1$s allowed."
msgstr ""

#: lib/components/FrontendSubmit/Controller/AbstractAddEntity.php:149
msgid "Your item has been submitted successfully."
msgstr ""

#: lib/components/FrontendSubmit/Controller/AbstractAddEntity.php:158
msgid "We will review your submission and post it on this site when it is approved."
msgstr ""

#: lib/components/FrontendSubmit/Controller/AddEntity.php:26
msgid "There is no submittable content."
msgstr ""

#: lib/components/FrontendSubmit/Controller/AddEntity.php:67
msgid "Select content type"
msgstr ""

#: lib/components/FrontendSubmit/Controller/AddEntity.php:92
msgid "%s: %s"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:80, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:139
msgid "Username"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:86, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:153
msgid "Password"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:92
msgid "Remember Me"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:126
msgid "Login to continue"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:146, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:266, lib/components/FrontendSubmit/CSVExporter/GuestCSVExporter.php:14, lib/components/FrontendSubmit/CSVImporter/GuestCSVImporter.php:14, lib/components/FrontendSubmit/FieldType/GuestFieldType.php:102, lib/components/FrontendSubmit/FieldWidget/GuestFieldWidget.php:35
msgid "E-mail Address"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:160
msgid "Confirm Password"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:192, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:295
msgid "You must agree to continue."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:202
msgid "Register an account"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:211
msgid "User registration is currently not allowed."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:256
msgid "Your Name"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:278, lib/components/FrontendSubmit/CSVExporter/GuestCSVExporter.php:15, lib/components/FrontendSubmit/CSVImporter/GuestCSVImporter.php:15, lib/components/FrontendSubmit/FieldType/GuestFieldType.php:103, lib/components/FrontendSubmit/FieldWidget/GuestFieldWidget.php:42
msgid "Website URL"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:311
msgid "I have read and agree to the %s"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:319, lib/components/FrontendSubmit/Controller/ResetPassword.php:47
msgid "Passwords do not match."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LoginOrRegister.php:331, lib/components/FrontendSubmit/Controller/LoginOrRegister.php:361
msgid "User with user ID %d does not exist."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:13
msgid "Get New Password"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:21
msgid "Lost your password? Please enter your username or email address. You will receive a link to create a new password via email."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:26
msgid "Username or E-mail"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:35, lib/components/FrontendSubmit/Controller/LostPassword.php:47
msgid "Invalid username or e-mail."
msgstr ""

#. translators: Password reset email subject. %s: Site name
#: lib/components/FrontendSubmit/Controller/LostPassword.php:64
msgid "[%s] Password reset"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:65
msgid "Someone has requested a password reset for the following account:"
msgstr ""

#. translators: %s: site name
#: lib/components/FrontendSubmit/Controller/LostPassword.php:67
msgid "Site Name: %s"
msgstr ""

#. translators: %s: user login
#: lib/components/FrontendSubmit/Controller/LostPassword.php:69
msgid "Username: %s"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:70
msgid "If this was a mistake, just ignore this email and nothing will happen."
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:71
msgid "To reset your password, visit the following address:"
msgstr ""

#: lib/components/FrontendSubmit/Controller/LostPassword.php:78
msgid "Password reset email has been sent."
msgstr ""

#: lib/components/FrontendSubmit/Controller/ResetPassword.php:20
msgid "Enter your new password below."
msgstr ""

#: lib/components/FrontendSubmit/Controller/ResetPassword.php:25
msgid "New Password"
msgstr ""

#: lib/components/FrontendSubmit/Controller/ResetPassword.php:30
msgid "Confirm New Password"
msgstr ""

#: lib/components/FrontendSubmit/Controller/ResetPassword.php:57
msgid "Your password has been reset successfully."
msgstr ""

#: lib/components/FrontendSubmit/CSVExporter/GuestCSVExporter.php:13, lib/components/FrontendSubmit/CSVImporter/GuestCSVImporter.php:13
msgid "Name"
msgstr ""

#: lib/components/FrontendSubmit/CSVExporter/GuestCSVExporter.php:16, lib/components/FrontendSubmit/CSVImporter/GuestCSVImporter.php:16
msgid "GUID"
msgstr ""

#: lib/components/FrontendSubmit/DisplayButton/AddEntityDisplayButton.php:27
msgid "Add %s button"
msgstr ""

#: lib/components/FrontendSubmit/FieldType/GuestFieldType.php:17, lib/components/FrontendSubmit/FieldWidget/GuestFieldWidget.php:12
msgid "Guest Author"
msgstr ""

#: lib/components/FrontendSubmit/FieldType/GuestFieldType.php:101, lib/components/FrontendSubmit/FieldWidget/GuestFieldWidget.php:29
msgid "Guest Name"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:15
msgid "Guest Post Settings"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:19
msgid "Collect guest name"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:26
msgid "Require guest name"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:38
msgid "Collect e-mail address"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:45
msgid "Require e-mail address"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:57
msgid "Do not allow e-mail address used by registered users"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:69
msgid "Collect website URL"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:76
msgid "Require website URL"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:90
msgid "Check MX record of e-mail address"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:103, lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:140
msgid "Add a privacy policy consent checkbox"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:111
msgid "User Login Settings"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:115
msgid "Show user login form"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:129
msgid "User Registration Settings"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:133
msgid "Show user registration form"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:171
msgid "%s (per %s)"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:180
msgid "Submission Restriction Settings"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:184
msgid "Restriction type"
msgstr ""

#: lib/components/FrontendSubmit/Helper/SettingsFormHelper.php:185
msgid "— Select —"
msgstr ""

#: lib/components/FrontendSubmit/Restrictor/AbstractRestrictor.php:49
msgid "Max number of submissions allowed"
msgstr ""

#: lib/components/FrontendSubmit/Restrictor/AbstractRestrictor.php:54
msgid "Other Settings"
msgstr ""

#: lib/components/FrontendSubmit/Restrictor/AbstractRestrictor.php:59
msgid "Exclude items in trash"
msgstr ""

#: lib/components/FrontendSubmit/Restrictor/DefaultRestrictor.php:11
msgid "Default"
msgstr ""

#: lib/components/FrontendSubmit/Restrictor/DefaultRestrictor.php:28
msgid "Unlimited"
msgstr ""

#: lib/components/reCAPTCHA/DisplayElement/CaptchaDisplayElement.php:14
msgid "Show a CAPTCHA field with reCAPTCHA API"
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:21
msgid "reCAPTCHA site/secret keys must be obtained from Google and configured under Directories -> Settings -> reCAPTCHA."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:138
msgid "Please fill out this field."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:140
msgid "Failed validating reCAPTCHA value."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:167
msgid "Invalid reCAPTCHA secret key."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:171
msgid "Invalid reCAPTCHA value."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:182
msgid "Failed obtaining valid reCAPTCHA verify response."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:187
msgid "Unknown error."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:192
msgid "Invalid or missing secret parameter."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:196
msgid "Invalid or missing response parameter"
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:199
msgid "Invalid or malformed request."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:213
msgid "Invalid action."
msgstr ""

#: lib/components/reCAPTCHA/Helper/CaptchaHelper.php:221
msgid "Request denied."
msgstr ""
