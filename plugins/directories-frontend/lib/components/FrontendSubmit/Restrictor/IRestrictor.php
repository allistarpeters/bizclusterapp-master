<?php
namespace SabaiApps\Directories\Component\FrontendSubmit\Restrictor;

use SabaiApps\Directories\Component\Entity\Model\Bundle;
use SabaiApps\Directories\Component\Entity\Type\IEntity;
use SabaiApps\Framework\User\AbstractIdentity;

interface IRestrictor
{
    public function frontendsubmitRestrictorInfo($key = null);
    public function frontendsubmitRestrictorEnabled();
    public function frontendsubmitRestrictorSettingsForm(array $bundles, array $settings, array $parents = []);
    /**
     * @param Bundle $bundle
     * @param AbstractIdentity|string $identity
     * @param array $settings
     * @param IEntity|null $parentEntity
     * @return bool
     */
    public function frontendsubmitRestrictorIsRestricted(Bundle $bundle, array $settings, $identity, IEntity $parentEntity = null);
}