<?php
namespace SabaiApps\Directories\Component\FrontendSubmit\Helper;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\Form;

class SettingsFormHelper
{
    public function help(Application $application, array $config, array $parents)
    {
        $form = [];
        $guest_field_name_prefix = $application->Form_FieldName(array_merge($parents, array('guest')));
        $form['guest'] = array(
            '#weight' => 10,
            '#title' => __('Guest Post Settings', 'directories-frontend'),
            '#tree' => true,
            'collect_name' => array(
                '#type' => 'checkbox',
                '#title' => __('Collect guest name', 'directories-frontend'),
                '#default_value' => !isset($config['guest']['collect_name']) || !empty($config['guest']['collect_name']),
                '#weight' => 1,
                '#horizontal' => true,
            ),
            'require_name' => array(
                '#type' => 'checkbox',
                '#title' => __('Require guest name', 'directories-frontend'),
                '#default_value' => !isset($config['guest']['require_name']) || !empty($config['guest']['require_name']),
                '#weight' => 3,
                '#states' => array(
                    'visible' => array(
                        'input[name="' . $guest_field_name_prefix . '[collect_name]"]' => array('type' => 'checked', 'value' => true),
                    ),
                ),
                '#horizontal' => true,
            ),
            'collect_email' => array(
                '#type' => 'checkbox',
                '#title' => __('Collect e-mail address', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['collect_email']),
                '#weight' => 5,
                '#horizontal' => true,
            ),
            'require_email' => array(
                '#type' => 'checkbox',
                '#title' => __('Require e-mail address', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['require_email']),
                '#weight' => 6,
                '#states' => array(
                    'visible' => array(
                        'input[name="' . $guest_field_name_prefix . '[collect_email]"]' => array('type' => 'checked', 'value' => true),
                    ),
                ),
                '#horizontal' => true,
            ),
            'check_exists' => array(
                '#type' => 'checkbox',
                '#title' => __('Do not allow e-mail address used by registered users', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['check_exists']),
                '#weight' => 7,
                '#states' => array(
                    'visible' => array(
                        'input[name="' . $guest_field_name_prefix . '[collect_email]"]' => array('type' => 'checked', 'value' => true),
                    ),
                ),
                '#horizontal' => true,
            ),
            'collect_url' => array(
                '#type' => 'checkbox',
                '#title' => __('Collect website URL', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['collect_url']),
                '#weight' => 10,
                '#horizontal' => true,
            ),
            'require_url' => array(
                '#type' => 'checkbox',
                '#title' => __('Require website URL', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['require_url']),
                '#weight' => 11,
                '#states' => array(
                    'visible' => array(
                        'input[name="' . $guest_field_name_prefix . '[collect_url]"]' => array('type' => 'checked', 'value' => true),
                    ),
                ),
                '#horizontal' => true,
            ),
        );
        if (Form\Field\TextField::canCheckMx()) {
            $form['guest']['check_mx'] = array(
                '#type' => 'checkbox',
                '#title' => __('Check MX record of e-mail address', 'directories-frontend'),
                '#default_value' => !empty($config['guest']['check_mx']),
                '#weight' => 8,
                '#states' => array(
                    'visible' => array(
                        'input[name="' . $guest_field_name_prefix . '[collect_email]"]' => array('type' => 'checked', 'value' => true),
                    ),
                ),
                '#horizontal' => true,
            );
        }
        $form['guest']['collect_privacy'] = [
            '#type' => 'checkbox',
            '#title' => __('Add a privacy policy consent checkbox', 'directories-frontend'),
            '#default_value' => !empty($config['guest']['collect_privacy']),
            '#weight' => 15,
            '#horizontal' => true,
        ];

        $form['login'] = array(
            '#weight' => 20,
            '#title' => __('User Login Settings', 'directories-frontend'),
            '#tree' => true,
            'form' => array(
                '#type' => 'checkbox',
                '#title' => __('Show user login form', 'directories-frontend'),
                '#default_value' => (!isset($config['login']['form']) && !empty($config['login']['login_form'])) || !empty($config['login']['form']),
                '#horizontal' => true,
                '#weight' => 1,
            ),
        );


        if ($application->getPlatform()->isLoginFormRequired()) {
            $form['login']['form']['#type'] = 'hidden';
        }

        $form['register'] = [
            '#weight' => 20,
            '#title' => __('User Registration Settings', 'directories-frontend'),
            '#tree' => true,
            'form' => array(
                '#type' => 'checkbox',
                '#title' => __('Show user registration form', 'directories-frontend'),
                '#default_value' => (!isset($config['register']['form']) && !empty($config['login']['register_form'])) || !empty($config['register']['form']),
                '#horizontal' => true,
                '#weight' => 3,
            ),
            'privacy' => [
                '#type' => 'checkbox',
                '#title' => __('Add a privacy policy consent checkbox', 'directories-frontend'),
                '#default_value' => !empty($config['register']['privacy']),
                '#weight' => 5,
                '#horizontal' => true,
                '#states' => [
                    'visible' => [
                        'input[name="' . $application->Form_FieldName(array_merge($parents, ['register'])) . '[form]"]' => ['type' => 'checked', 'value' => true],
                    ],
                ],
            ],
        ];

        if ($restrictors = $application->FrontendSubmit_Restrictors()) {
            $bundles = [];
            foreach ($application->getModel('Directory', 'Directory')->fetch(0, 0, array('directory_type', 'directory_name'), array('ASC', 'ASC')) as $directory) {
                foreach ($application->Entity_Bundles_sort(null, 'Directory', $directory->name) as $bundle) {
                    $info = $application->Entity_BundleTypeInfo($bundle);
                    if (empty($info['frontendsubmit_enable'])) continue;

                    $bundles[$bundle->name] = $bundle;
                }
            }
            if (!empty($bundles)) {
                $bundle_labels = [];
                foreach(array_keys($bundles) as $bundle_name) {
                    $bundle = $bundles[$bundle_name];
                    $bundle_labels[$bundle_name] = $bundle->getGroupLabel() . ' - ' . $bundle->getLabel();
                    if (!empty($bundle->info['parent'])
                        && isset($bundles[$bundle->info['parent']])
                    ) {
                        $bundle_labels[$bundle_name] = sprintf(
                            __('%s (per %s)', 'directories-frontend'),
                            $bundle_labels[$bundle_name],
                            $bundles[$bundle->info['parent']]->getLabel('singular')
                        );
                    }
                }
                unset($bundles);
                $form['restrict'] = [
                    '#weight' => 25,
                    '#title' => __('Submission Restriction Settings', 'directories-frontend'),
                    '#tree' => true,
                    'type' => [
                        '#type' => 'select',
                        '#title' => __('Restriction type', 'directories-frontend'),
                        '#options' => ['' => __('— Select —', 'directories-frontend')],
                        '#horizontal' => true,
                        '#default_value' => isset($config['restrict']['type']) ? $config['restrict']['type'] : '',
                    ],
                ];
                $type_field_name = $application->Form_FieldName(array_merge($parents, ['restrict', 'type']));
                foreach (array_keys($restrictors) as $restrictor_name) {
                    if (!$restrictor = $application->FrontendSubmit_Restrictors_impl($restrictor_name, true))continue;

                    $form['restrict']['type']['#options'][$restrictor_name] = $restrictor->frontendsubmitRestrictorInfo('label');
                    if (!$restrictor->frontendsubmitRestrictorEnabled()) {
                        $form['restrict']['type']['#options_disabled'][] = $restrictor_name;
                        continue;
                    }
                    $form['restrict']['settings'][$restrictor_name] = $restrictor->frontendsubmitRestrictorSettingsForm(
                        $bundle_labels,
                        isset($config['restrict']['settings'][$restrictor_name]) ? $config['restrict']['settings'][$restrictor_name] : [],
                        array_merge($parents, ['restrict', 'settings', $restrictor_name])
                    );
                    $form['restrict']['settings'][$restrictor_name]['#states'] = [
                        'visible' => [
                            'select[name="' . $type_field_name . '"]' => ['value' => $restrictor_name],
                        ],
                    ];
                }
            }
        }

        return $form;
    }
}
