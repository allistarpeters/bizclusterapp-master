<?php
namespace SabaiApps\Directories\Component\FrontendSubmit\Helper;

use SabaiApps\Directories\Application;
use SabaiApps\Directories\Component\FrontendSubmit\Restrictor\IRestrictor;
use SabaiApps\Directories\Component\Entity;
use SabaiApps\Directories\Exception;
use SabaiApps\Framework\User\RegisteredIdentity;

class RestrictorsHelper
{
    public function help(Application $application, $useCache = true)
    {
        if (!$useCache
            || (!$restrictors = $application->getPlatform()->getCache('frontendsubmit_restrictors'))
        ) {
            $restrictors = [];
            foreach ($application->InstalledComponentsByInterface('FrontendSubmit\IRestrictors') as $component_name) {
                if (!$application->isComponentLoaded($component_name)) continue;

                foreach ($application->getComponent($component_name)->frontendsubmitGetRestrictorNames() as $restrictor_name) {
                    if (!$restrictor = $application->getComponent($component_name)->frontendsubmitGetRestrictor($restrictor_name)) continue;

                    $restrictors[$restrictor_name] = $component_name;
                }
            }
            $application->getPlatform()->setCache($restrictors, 'frontendsubmit_restrictors');
        }

        return $restrictors;
    }

    private $_impls = [];

    /**
     * Gets an implementation of IRestrictor interface for a given restrictor name
     * @param Application $application
     * @param string $restrictor
     * @param bool $returnFalse
     * @return IRestrictor
     */
    public function impl(Application $application, $restrictor, $returnFalse = false)
    {
        if (!isset($this->_impls[$restrictor])) {
            if ((!$restrictors = $application->FrontendSubmit_Restrictors())
                || !isset($restrictors[$restrictor])
                || !$application->isComponentLoaded($restrictors[$restrictor])
            ) {
                if ($returnFalse) return false;
                throw new Exception\UnexpectedValueException(sprintf('Invalid restrictor: %s', $restrictor));
            }
            $this->_impls[$restrictor] = $application->getComponent($restrictors[$restrictor])->frontendsubmitGetRestrictor($restrictor);
        }

        return $this->_impls[$restrictor];
    }

    /**
     * @param Entity\Model\Bundle $bundle
     * @param RegisteredIdentity|string $identity
     * @param Entity\Type\IEntity|null $parentEntity
     * @return bool
     */
    public function isRestricted(Application $application, Entity\Model\Bundle $bundle, $identity, Entity\Type\IEntity $parentEntity = null)
    {
        $result = false;
        if ($identity instanceof RegisteredIdentity
            || strlen($identity) // anonymous identity requires an e-mail address
        ) {
            $restrict_config = $application->getComponent('FrontendSubmit')->getConfig('restrict');
            if (!empty($restrict_config['type'])) {
                $settings = isset($restrict_config['settings'][$restrict_config['type']]) ? (array)$restrict_config['settings'][$restrict_config['type']] : [];
                try {
                    $restrictor = $this->impl($application, $restrict_config['type']);
                    $settings += (array)$restrictor->frontendsubmitRestrictorInfo('default_settings');
                    if ($restrictor->frontendsubmitRestrictorEnabled()
                        && $restrictor->frontendsubmitRestrictorIsRestricted($bundle, $settings, $identity, $parentEntity)
                    ) {
                        $application->Action('frontendsubmit_submit_restricted', [$bundle, $identity, $restrict_config['type'], $settings, $parentEntity]);
                        $result = true;
                    }
                } catch (Exception\IException $e) {
                    $application->logError($e);
                    return $result;
                }
            }
        } else {
            $application->logError('Submission restriction requires a valid registered user identity or guest e-mail address.');
            return $result;
        }

        return $application->Filter(
            'frontendsubmit_submit_restriction_result',
            $result,
            [$bundle, $identity, $restrict_config['type'], $parentEntity]
        );
    }
}