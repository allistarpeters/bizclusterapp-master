<?php
namespace SabaiApps\Directories\Component\FrontendSubmit\Controller;

use SabaiApps\Directories\Context;
use SabaiApps\Directories\Component\Form;

class LostPassword extends Form\Controller
{
    protected function _doGetFormSettings(Context $context, array &$storage)
    {
        $this->_cancelUrl = '/' . $this->getComponent('FrontendSubmit')->getSlug('login');
        $this->_submitButtons[] = [
            '#btn_label' => __('Get New Password', 'directories-frontend'),
            '#btn_color' => 'primary',
        ];

        return [
            '#header' => [
                [
                    'level' => 'info',
                    'message' => __('Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'directories-frontend'),
                ],
            ],
            'user' => [
                '#type' => 'textfield',
                '#title' => __('Username or E-mail', 'directories-frontend'),
                '#required' => true,
            ],
        ];
    }

    public function submitForm(\SabaiApps\Directories\Component\Form\Form $form, Context $context)
    {
        if (empty($form->values['user'])) {
            $form->setError(__('Invalid username or e-mail.', 'directories-frontend'), 'user');
            return;
        }

        if (strpos($form->values['user'], '@')) {
            $identity = $this->getPlatform()->getUserIdentityFetcher()->fetchByEmail($form->values['user']);
        } else {
            $identity = $this->getPlatform()->getUserIdentityFetcher()->fetchByUsername($form->values['user']);
        }
        if (!$identity
            || !$identity->email
        ) {
            $form->setError(__('Invalid username or e-mail.', 'directories-frontend'), 'user');
            return;
        }

        $login_path = '/' . $this->getComponent('FrontendSubmit')->getSlug('login');
        $url = $this->Url(
            $login_path . '/reset_password',
            [
                'key' => $this->getPlatform()->getResetPasswordKey($identity),
                'id' => $identity->id,
            ],
            '',
            '&'
        );
        $site_name = $this->getPlatform()->getSiteName();
        $site_url = $this->getPlatform()->getSiteUrl();
        /* translators: Password reset email subject. %s: Site name */
        $title = sprintf(__('[%s] Password reset', 'directories-frontend'), $site_name);
        $message = __('Someone has requested a password reset for the following account:', 'directories-frontend') . "\r\n\r\n";
        /* translators: %s: site name */
        $message .= sprintf(__('Site Name: %s', 'directories-frontend'), $site_name) . "\r\n\r\n";
        /* translators: %s: user login */
        $message .= sprintf(__('Username: %s', 'directories-frontend'), $identity->username) . "\r\n\r\n";
        $message .= __('If this was a mistake, just ignore this email and nothing will happen.', 'directories-frontend') . "\r\n\r\n";
        $message .= __('To reset your password, visit the following address:', 'directories-frontend') . "\r\n\r\n";
        $message .= $url . "\r\n\r\n";
        $message .= $site_name . "\r\n" . $site_url;

        $this->getPlatform()->mail($identity->email, $title, $message);

        $context->setSuccess($login_path)
            ->addFlash(__('Password reset email has been sent.', 'directories-frontend'), 'success', 60000);
    }
}