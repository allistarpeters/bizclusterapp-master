<?php
namespace SabaiApps\Directories\Component\Dashboard\Panel;

use SabaiApps\Framework\User\AbstractIdentity;

class AccountPanel extends AbstractPanel
{
    protected function _dashboardPanelInfo()
    {
        return array(
            'weight' => 10,
            'wp' => false,
        );
    }

    public function dashboardPanelLabel()
    {
        return __('Account', 'directories-frontend');
    }

    protected function _dashboardPanelLinks(array $settings, AbstractIdentity $identity = null)
    {
        $links = [];
        $options = $this->_accountPanelPages();
        $pages = isset($settings['pages']['default']) ? $settings['pages']['default'] : array_keys($options);
        foreach ($pages as $page_name) {
            if (!isset($options[$page_name])) continue;

            $links[$page_name] = $options[$page_name];
        }
        return $links;
    }

    protected function _accountPanelPages()
    {
        $pages = [
            'change_password' => [
                'title' => _x('Change password', 'directories-frontend'),
                'icon' => 'fas fa-key',
                'weight' => 10,
            ],
            'delete_account' => [
                'title' => _x('Delete account', 'directories-frontend'),
                'icon' => 'fas fa-user-times',
                'weight' => 20,
            ],
        ];

        return $this->_application->Filter('dashboard_account_panel_pages', $pages);
    }

    public function dashboardPanelSettingsForm(array $settings, array $parents)
    {
        $options = [];
        foreach ($this->_accountPanelPages() as $link_name => $link) {
            $options[$link_name] = $link['title'];
        }
        return [
            'pages' => [
                '#title' => __('Select pages', 'directories-frontend'),
                '#type' => 'options',
                '#horizontal' => true,
                '#disable_add' => true,
                '#disable_icon' => true,
                '#disable_add_csv' => true,
                '#multiple' => true,
                '#options_value_disabled' => true,
                '#default_value' => array(
                    'options' => isset($settings['pages']['options']) ? $settings['pages']['options'] : $options,
                    'default' => isset($settings['pages']['default']) ? $settings['pages']['default'] : array_keys($options),
                ),
                '#options_placeholder' => $options,
            ],
        ];
    }

    public function dashboardPanelContent($link, array $settings, array $params, AbstractIdentity $identity = null)
    {
        if (isset($identity)) return;

        return $this->_application->getPlatform()->render(
            $this->_application->getComponent('Dashboard')->getPanelUrl('account', $link, '/' . $link, [], true, $identity),
            ['is_dashboard' => false] // prevent rendering duplicate panel sections on reload panel
        );
    }
}