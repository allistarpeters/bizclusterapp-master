#!/bin/bash

# Print commands to the screen
set -x

# Catch Errors
set -euo pipefail

composer install --no-dev -o

pushd plugins/haymarket-migration || exit 1
composer install --no-dev -o
popd || exit 1

# Stop printing commands to screen
set +x

