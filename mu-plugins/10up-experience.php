<?php

/**
 *  Remove the rewrite_rules options from the protected list
 *
 * @param $options
 *
 * @return mixed
 */
add_filter( 'tenup_experience_required_options', function ( $options ) {
	if ( false !== ( $key = array_search( 'rewrite_rules', $options ) ) ) {
		unset( $options[ $key ] );
	}

	return $options;
} );
