<?php
if ( ! defined( 'TENUPSSO_DEFAULT_ROLE' ) ) {
	define( 'TENUPSSO_DEFAULT_ROLE', 'administrator' );
}

if ( ! defined( 'TENUPSSO_GRANT_SUPER_ADMIN' ) ) {
	define( 'TENUPSSO_GRANT_SUPER_ADMIN', true );
}
/**
 * Hide SSO login once site is launched
 */
add_action( 'init', function () {
	$url = home_url();
	if( false === strpos( $url, 'wp.') && false === strpos( $url, 'wpstage.') && false === strpos( $url, '10uplabs') ){
		remove_action( 'login_form', 'tenup_sso_update_login_form' );
	}
} );