#!/usr/bin/env bash

####
# WPE mangles production and staging
# @SEE https://wpengine.com/git/
# @SEE https://wpengine.com/support/set-git-push-user-portal
#
SILENT=/dev/null 2>&1
CWD=$(pwd)

PROD_BRANCH=master
STG_BRANCH=release
DEV_BRANCH=develop
DEV_USER=devhme-gitlab

# Simple alias for git@git.wpengine.com:production
BASEURL="wpe-git:production/"

PROD=production
STG=staging
#PROJECT="events-cluster"

# Need to experiment with the delete options
# old OPTIONS="--partial --append --stats --delete -avzrp"
OPTIONS="--partial --stats -avzrp"
EXCLUSIONS="--exclude-from=etc/rsync/exclusions"

DEST_BASE=.tmp/${PROJECT}

LOCAL=false
GITLAB=false
GITPUSH=false

function setLocalSettings() {
	# source in local settings from test file
    # this file IS NOT included in the repo.
    if [ ${LOCAL} = true ]; then
		source etc/local-settings
	fi
}

function setSSHPermissions() {
	chmod 700 ~/.ssh
	chmod 600 ~/.ssh/*
}

function setupSSHKeys() {
	echo ${SSH_PRIVATE_KEY} > ~/.ssh/id_rsa
	echo ${SSH_PUBLIC_KEY} > ~/.ssh/id_rsa.pub
	echo ${SSH_KNOWN_HOSTS} > ~/.ssh/known_hosts
}

function setupSSHConfig() {
	echo -e "Host *\n\tStrictHostKeyChecking no\n\tPreferredauthentications publickey\n\n" > ~/.ssh/config
}

function setupGitPushHost(){
	# Inserts the git push alias into the ssh config; things just
	# work better this way
	echo -e "Host wpe-git\n\tHostname git.wpengine.com\n\tUser git\n\tStrictHostKeyChecking no\n\tPreferredauthentications publickey\n\n" >> ~/.ssh/config

}

function setupRsyncHost() {
	# Inserts the rsync host alias into the ssh config; things just
	# work better this way
	echo -e "Host wpe-dest\n\tHostname ${DEST_HOST}.ssh.wpengine.net\n\tUser ${DEV_USER}\n\tStrictHostKeyChecking no\n\tPreferredauthentications publickey\n\n" >> ~/.ssh/config

}

####
# Localizes the setup of the SSH security info to this script.
#
# Only used during CICD operations on GitLab
#
function setupAuth() {
	# Check CLI -g option which indicates this is a GitLab runner
	# This ensures that you do not clobber your local ssh environment
	if [ ${GITLAB} = true ]; then
		# run ssh-agent
		eval $(ssh-agent -s)

		# add ssh key stored in SSH_PRIVATE_KEY variable to the agent store
		ssh-add <(echo "$SSH_PRIVATE_KEY")

		# disable host key checking (NOTE: makes you susceptible
		# to man-in-the-middle attacks)
		# WARNING: use only in docker container, if you use it
		# with shell you will overwrite your user's ssh config
		mkdir -p ~/.ssh

		setupSSHConfig

		setupGitPushHost

		setupRsyncHost

		setupSSHKeys

		setSSHPermissions

		echo "Complete auth setup."
	fi
}

####
# Only used during CICD operations on GitLab
#
function setGitConfig() {
	# Check CLI -g option which indicates this is a GitLab runner
	# This ensures that you do not clobber your local git environment
	if [ ${GITLAB} = true ]; then
		git config --global user.name "${1}-gitlab@olivent.com"
		git config --global user.email "gitlab@olivent.com"
		git config --global push.default simple
    fi
}

####
# Need to add more HELP details and use a HEREDOC; but for now...
function helpMsg() {
    CMD=${0##*/}
	echo "Usage: ${CMD} -c -t (production|staging|dev)"
	echo "EXAMPLES:"
	echo "CleanUp Dev Build: ${CMD} -c -t dev"
	echo "Standard Dev Build: ${CMD} -t dev"
	echo "Build using GitLab runner: ${CMD} -c -t -g dev"
	echo "Force deploy using WPE's git push: ${CMD} -c -t -g -p dev"
	exit 1
}

function getDest() {
    DEST=${DEST_BASE}/${1}
    echo $DEST
}

function getRepoURL() {
	#git config --global user.name "${2}"
    REPOURL=${BASEURL}${1}.git
    echo $REPOURL
}

####
# This may as it turns our have been overkill
#
function getSrcDir() {
    REPO_DIR=${SRC_REPO##*/}
    SRC_DIR=${REPO_DIR%.*}
    #echo $SRC_DIR
    echo src-dir
}

####
# Simply verify that the destination exists.
# If not create it.
function verifyTempSpace() {
    if [ ! -d "${DEST_BASE}" ]
    then
        # Create a local hidden temp directory using an indempotent operation
        mkdir -p  ${DEST_BASE}
    fi
}

####
# verify and update the build copy of the src repo exists as
# it relates to the indented target destination in .tmp/
#
function verifyAndUpdateSrcRepo() {
	echo -e "Verifying the source repo.\n\n"

    BRANCH=${1}
    SRC_DIR=$(getSrcDir)

    # Get into the temp dest base and check is the
    # repo directory already exists
    pushd "${DEST_BASE}" || exit 1
    if [ ! -d "${SRC_DIR}" ]
    then
	    setSrcURL
        # Create the repo directory by explicitly
        # cloning the SRC Git Repo
        echo "The src url is: ${SRC_URL}"
        git clone ${SRC_URL} ${SRC_DIR}
    fi

    # Ensure that we have the right branch and it is up to date
    cd "${SRC_DIR}" || exit 1
    git checkout ${BRANCH}
    git pull
    # It's always advisable to return to the starting point.
    cleanupBuild
    popd || exit 1
}

####
# verify and update the local copy of the WPE repo exists as it relates
# to the target destination
#
function verifyAndUpdateTempRepo() {
	echo -e "Verifying the temp WPE repo.\n\n"
    ENV_REPO=${1}
    # Get into the temp dest base and check is the repo directory
    # already exists
    pushd "${DEST_BASE}" || exit 1
    if [ ! -d "${ENV_REPO}" ]
    then
        # Create the repo directory by explicitly
        # cloning the WPE Git Repo
        git clone ${REPOURL} ${ENV_REPO}

        # Ensure wp-content already exists in the
        # repo, since WP Engine repos start at the siteroot
        # This command is indempotent
        mkdir -p "${ENV_REPO}/wp-content"

    else
        # Since it already exists just ensure that it is up to date
        cd "${ENV_REPO}" || exit 1
        git pull
    fi
    # It's always advisable to return to the starting point.
    popd || exit 1
}

####
# Dev deployments should only follow develop
# Staging deploys should only track release
# Prod should track master
#
# Attempt to correct any mismatch or fail
function verifySrcBranch() {
    BRANCH=$(git branch |awk '/\*/{print $2}')
    if [ ! "$BRANCH" = "$1" ]; then
        git checkout ${1} || exit 1
    fi
}

####
# Sets up the local temp copy of the WPE repo if needed
function prepDeploy() {
	if [ ${GITPUSH} = true ]; then
	    DEST=$(getDest ${1})
	    REPOURL=$(getRepoURL ${1})
	    verifyAndUpdateTempRepo ${1}
    fi
}

function setRevision() {
    # Save the HM Repo's commit hash in the WordPress site for later use.
    echo ${COMMIT_HASH} > .revision
}

####
# This function handles all of the steps necessary to deploy to a WP Engine
# site via GitPush
# Currently it is the default method for deployment but and alternate
# function could handle
# all of the steps needed for a rsync over ssh deployment thus making
# this script more universal
#
function deployToWPE() {
    # Change to the temp space for wpe repo
    pushd ${CWD}/${DEST}/wp-content/ || exit 1

    # Brute force cleanup of the destination directories
    # i.e., the corresponding directories in WPE's git repo
    rm -rf plugins/*
    rm -rf mu-plugins/*
    rm -rf themes/*

    popd || exit1

    # Change to the temp src dir
    SRC_DIR=$(getSrcDir)
    pushd ${CWD}/${DEST_BASE}/${SRC_DIR}/ || exit 1

    pwd

    ####
    # This is extremely explicit and less problematic than the
    # other methods previously attempted. Copy from the SRC into WPE git
    rsync ${OPTIONS} plugins ${CWD}/${DEST}/wp-content/

    echo "Debugging the mu-plugin routines in the build process."
    ls -al mu-plugins

    rsync ${OPTIONS} mu-plugins ${CWD}/${DEST}/wp-content/


    rsync ${OPTIONS} ${EXCLUSIONS} themes ${CWD}/${DEST}/wp-content/

	# Troubleshooting code REDACT
	echo -e "Checking the source tree.\n"
	pwd

    # Change to the WPE repo space for the project
    popd || exit 1
    pushd ${CWD}/${DEST}/wp-content/ || exit 1

	setRevision

    # Add all files staging the changes
    git add .

    # Output log of what changed, for debugging deploys helpful for debugging
    git status

    # Commit Files from project repo to WPE repo
    # technically this is a bit of a lie because the hash refers to
    # the last commit of the branch that is being merged into WPE
    git commit --quiet -m "Commiting ${COMMIT_HASH} WPE repo for ${PROJECT}."

    # Get the WPE commit hash also helpful for debugging
    WPE_COMMIT_HASH=$(git rev-parse --short HEAD)

    # For logging purposes
    echo "Deploying ${WPE_COMMIT_HASH} $PROJECT."

    # Deploy origin master tp WPE on develop try adding -vvv for extremely verbose output
    git push

    popd || exit 1
}

####
# This function handles all of the steps necessary to deploy to a WP Engine
# site via rsync
#
# all of the steps needs for a rsync over ssh deployment thus making
# this script more universal
#
function rsyncDeploy() {
	DEST_PATH=${DEST_HOST}:sites/${DEST_HOST}/wp-content/

	echo "The expected destination is: ${DEST_PATH}"

    # Change to the temp src dir
    SRC_DIR=$(getSrcDir)
    pushd ${CWD}/${DEST_BASE}/${SRC_DIR}/ || exit 1

	echo "The current path is: "
	pwd

	setRevision

	echo "The current deployment option are: ${OPTIONS}"

    ####
    # This is extremely explicit and less problematic than the
    # other methods previously attempted. Copy from the SRC into WPE git
    echo "Sending the current revision."
    rsync ${OPTIONS} .revision ${DEST_PATH}

    #echo "Sending the mu-plugins."
    #rsync ${OPTIONS} mu-plugins ${DEST_PATH}

    #echo "Sending the plugins."
    #rsync ${OPTIONS} plugins ${DEST_PATH}

    #echo "Sending the themes."
    #rsync ${OPTIONS} themes ${DEST_PATH}

	echo "Fingers crossed the deployment is complete."
}

function setSrcURL() {
	if [ ${LOCAL} = true ]; then
		SRC_URL="${REPO_URL}"
	else
		SRC_URL="https://${RABBIT_USER}:${RABBIT_TOKEN}@${REPO_URL}"
    fi
}

function deploy() {
	if [ ${GITPUSH} = true ]; then
		deployToWPE
	else
		rsyncDeploy
    fi
}

function purgeTempSrcRepo() {
    if [ ! -z "${CLEANUP}" ];then
        SRC_DIR=$(getSrcDir)
        SRC_DEST=${DEST_BASE}/${SRC_DIR}
        echo "Cleaning up ${SRC_DEST}"
        rm -rf ${SRC_DEST}
    fi
}

function cleanupBuild() {
	echo "Clearing the composer cache."
	composer clear-cache
	# GitLab/Composer suggests--no-plugins --no-scripts
    COMPOSER_OPTIONS="--no-suggest --prefer-dist"
    echo "Setting composer options."
    if [ "${TARGET}" == "production"  ];then
        COMPOSER_OPTIONS="--no-dev --no-suggest --prefer-dist"
    fi

    echo -e "Installing composer managed assets for ${TARGET} branch\n"
    composer install ${COMPOSER_OPTIONS}
}

# MAIN

# Process CLI options
while getopts ":hcglpt:" opt; do
    case ${opt} in
        h )
         helpMsg
        ;;
        c )
            CLEANUP=true
        ;;
        g )
            GITLAB=true
        ;;
        l )
            LOCAL=true
        ;;
        p )
            GITPUSH=true
        ;;
        t )
            TARGET=$OPTARG
        ;;
        : )
            echo "Option -$OPTARG requires and argument (dev || staging || production)."
            exit 1
        ;;
        \? )
            echo "Invalid Option: -$OPTARG" 1>&2
            exit 1
        ;;
        : )
         helpMsg
        ;;
        * )
         helpMsg
        ;;
    esac
done
shift $((OPTIND -1))

# Setup the ssh auth in the deploy module or failure is eminent
setupAuth

####
# Setup deployment destination

purgeTempSrcRepo

if [ ! -z "${TARGET}" ];then
    echo "Deploying to ${TARGET} "
fi

# Print commands to the screen <= Could move up to end of getopts...
set -x

# Catch Errors
set -euo pipefail

# Get the last commit hash to the Haymarket Repo
COMMIT_HASH=$(git rev-parse --short HEAD)

verifyTempSpace

####
# Update the mu-plugins, plugins and themes as necessary
#
case "$TARGET" in
	production)
		DEST_HOST=${PROD_REPO}
	    verifyAndUpdateSrcRepo ${PROD_BRANCH} ${PROD_REPO}
	    prepDeploy ${PROD_REPO}
	    setGitConfig ${DEV_REPO}
		deployToWPE
		;;
	staging)
		DEST_HOST=${STG_REPO}
	    verifyAndUpdateSrcRepo ${STG_BRANCH}
	    prepDeploy ${STG_REPO}
	    setGitConfig ${DEV_REPO}
	    deployToWPE
		;;
	dev)
		setLocalSettings
		DEST_HOST=${DEV_REPO}
	    verifyAndUpdateSrcRepo ${DEV_BRANCH}
	    prepDeploy ${DEV_REPO}
	    setGitConfig ${DEV_REPO}
	    deploy
		;;
	*)
		echo "Invalid environment: '$env'. Must be 'production', 'preprod', 'staging' or 'local'"
		exit 1
		;;
esac

# Stop printing commands to the screen
set +x


# Old code to be redacted.
function verifyEntity() {
    ENTITY=${1}
    if [ ! -e work/${ENTITY} ]
    then
        echo
        echo "The '${ENTITY}' target was not found."
        echo "Suggest trying to execute 'build ${ENTITY}' first."
        echo
        exit 1
    fi
}
